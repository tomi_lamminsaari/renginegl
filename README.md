# rengineGL #

RengineGL is a hobby and study project for making an OpenGL 4 based rendering engine. I will advance it when I find time for it and therefor
I don't have any long term roadmaps for how to develop it further. Current functionalities include things like:

- Meshes with variabling vertex attributes.
- Deferred lighting with multiple light sources.
- Shadowmapping with one directional light source.
- Materials with multiple textures and normalmaps.
- Multiple lightsources for diffuse and specular lights.
- Import Wavefront objects and materials to the scene.
- Very simple particle system support.

RengineGL has been written in C++ and it uses GLEW-library to access the OpenGL functions. SDL2 library is used for creating the window, OpenGL context and to load images. ImGui is used for rendering the user interface.

![Demo scenary 1](/screenshots/screen-01.jpg)
![Demo scenary 2](/screenshots/screen-02.jpg)

### How to setup development environment ###

This project has been developed with Microsoft Visual Studio 2022 and C++17 features have been enabled for the compiler. In addition to that, you will need to have some extra libraries available on you machine.

The list of libraries that are needed for building the rengineGL:

- SDL2, installed in `C:\dev\SDL2`
- SDL2_image, installed in `C:\dev\SDL2_image`
- glew, installed in `C:\dev\glew`

Navigate to https://www.libsdl.org/download-2.0.php and download the SDL2 VisualStudio C++ libraries there. Extract the SDL2 content to `C:\dev\SDL2` directory.

Navigate to https://www.libsdl.org/projects/SDL_image/ and download the SDL2_image library binaries from there. Extract and copy contents to `C:\dev\SDL2_image` directory.

Navigate to http://glew.sourceforge.net/ and download the OpenGL Extension Wrangler Library from there. Extract its contents to `C:\dev\glew` directory.

### Contribution guidelines ###

This is my personal project so I am not going to accept any pull requests to it at the moment. But if you find it useful, please make a fork and go ahead developing it to your on needs.

### Who do I talk to? ###

My name is Tomi Lamminsaari. If you want to contact me, you can do it by emailing me to my firstname.lastname GMail address.