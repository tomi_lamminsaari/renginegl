
#include <sstream>
#include "../core/rendercore.h"
#include "../core/material.h"
#include "../core/materialmanager.h"
#include "../core/mesh.h"
#include "../core/meshmanager.h"
#include "../core/model.h"
#include "../core/modelmanager.h"
#include "../core/rengineexception.h"
#include "../core/texture.h"
#include "../core/texturemanager.h"
#include "../core/vertexlayout.h"
#include "../utils/filenameutils.h"
#include "wavefrontimporter.h"

#pragma region PrivateStructures

WaveFrontImporter::FaceStruct::FaceStruct()
{
	componentFlags = 0;
	for( int i = 0; i < 4; ++i )
	{
		int v = i < 3 ? 0 : -1;
		vertexIndex[ i ] = v;
		textureIndex[ i ] = v;
		normalIndex[ i ] = v;
	}
	vertexCount = 3;
}

WaveFrontImporter::FaceGroup::FaceGroup()
{

}

unsigned int WaveFrontImporter::FaceGroup::getVertexComponentFlags()
{
	if( faces.size() > 0 )
	{
		return faces.at( 0 ).componentFlags;
	}
	return 0;
}

WaveFrontImporter::GroupData::GroupData() :
	smoothShading( "off" ),
	isObject( false )
{

}

WaveFrontImporter::MaterialData::MaterialData( const std::string &name ) :
	materialName( name ),
	ambient( 1.0f, 1.0f, 1.0f, 1.0f ),
	diffuse( 1.0f, 1.0f, 1.0f, 1.0f ),
	specular( 1.0f, 1.0f, 1.0f, 1.0f ),
	specularExponent( 0.0f ),
	dissolveFactor( 1.0f ),
	reflectionIndex( 1.0f ),
	illumModel( 2 )
{

}

#pragma endregion

WaveFrontImporter::WaveFrontImporter( RenderCore &core, const std::string& texturesPath ) :
	mCore( core ),
	mScaleFactor( 1.0f ),
	mTexturesPath( texturesPath )
{

}

WaveFrontImporter::~WaveFrontImporter()
{

}

std::shared_ptr<Model> WaveFrontImporter::importModel(
	const std::string& path,
	const std::string& overrideModelId,
	bool importMat )
{
	// Reset the importer so that we can import new object.
	reset();

	// Load the 3D object file to memory.
	std::string objectData;
	bool success = mCore.loadTextFile( path, objectData );
	if( !success )
	{
		// Failed to load object.
		throw RengineError( "Failed to load Wavefront object: " + path );
	}

	std::vector< std::shared_ptr<GroupData> > objects;
	std::string materialLibFile;

	// Model file name from the object file name.
	std::string modelFileName = FileNameUtils::getFileNamePart( path );

	// Start parsing the object file.
	std::istringstream iss( objectData );
	GroupData *activeGroup = nullptr;
	FaceGroup *activeFaceGroup = nullptr;
	while( iss.peek() != EOF )
	{
		std::string s;
		iss >> s;

		if( s == "v" )
		{
			// Parse vertex data.
			float vx, vy, vz;
			iss >> vx >> vy >> vz;
			mVertices.push_back( Vec3( vx * mScaleFactor, vy * mScaleFactor, vz * mScaleFactor ) );
		}
		else if( s == "vn" )
		{
			// Parse normals data.
			Vec3 nv;
			iss >> nv.mX >> nv.mY >> nv.mZ;
			nv.normalize();
			mNormals.push_back( nv );
		}
		else if( s == "vt" )
		{
			// Parse texture coordinates.
			Vec2 tc;
			iss >> tc.mX >> tc.mY;
			mTextureCoords.push_back( tc );
		}
		else if( s == "f" )
		{
			// Parse face data.
			FaceStruct fs = parseFaceInfo( iss );
			if( activeFaceGroup != nullptr )
			{
				activeFaceGroup->faces.push_back( fs );
			}
		}
		else if( s == "o" )
		{
			// Object is a special group type. Create new group and make it the active group.
			std::string objectName;
			iss >> objectName;
			std::shared_ptr<GroupData> newGroup( new GroupData );
			activeGroup = newGroup.get();
			activeGroup->objectName = objectName;
			activeGroup->isObject = true;
			objects.push_back( newGroup );

			activeFaceGroup = nullptr;
		}
		else if( s == "g" )
		{
			std::string groupname;
			iss.ignore( 4096, '\n' );
			std::shared_ptr<GroupData> newGroup( new GroupData );
			activeGroup = newGroup.get();
			activeGroup->isObject = false;
			objects.push_back( newGroup );

			activeFaceGroup = nullptr;
		}
		else if( s == "usemtl" )
		{
			std::string materialName;
			iss >> materialName;
			std::unique_ptr<FaceGroup> groupPtr( new FaceGroup() );
			groupPtr->materialName = modelFileName + materialName;
			activeFaceGroup = groupPtr.get();
			activeGroup->faceGroups.push_back( std::move( groupPtr ) );
		}
		else if( s == "s" )
		{
			std::string smoothShading;
			iss >> smoothShading;
			if( activeGroup )
				activeGroup->smoothShading = smoothShading;
		}
		else if( s == "mtllib" )
		{
			// Read the material file name. It is the rest of the line contents.
			char matFileName[ 512 ];
			iss.getline( matFileName, sizeof( matFileName ) );
			materialLibFile = FileNameUtils::trimFilename( matFileName );
			if( importMat )
			{
				std::string fullMTLPath = FileNameUtils::replaceFileName( path, materialLibFile );
				bool success = importMaterial( fullMTLPath, modelFileName );
			}
		}
		else
		{
			iss.ignore( 4096, '\n' );
		}
	}

	// Data has been read from file. Create the resources to the rendering engine.
	MeshManager &meshMan = mCore.getMeshManager();
	MaterialManager &matMan = mCore.getMaterialManager();
	ModelManager &modelMan = mCore.getModelManager();
	std::shared_ptr<Model> model;

	// Construct mesh objects.
	int objectNum = 0;
	for( auto &subObject : objects )
	{
		objectNum++;
		GroupData *gd = subObject.get();
		for( auto &fg : gd->faceGroups )
		{
			// Find out the vertex elements that each vertex should have.
			VertexLayout vertexLayout = VertexLayout::getDefaultLayout_DeferredShading();

			// Create the mesh.
			std::string meshId = mCore.generateId( RenderCore::IDGenerator::Mesh );
			fg->meshName = meshId;
			std::shared_ptr<Mesh> mesh = meshMan.createMesh( meshId );
			mesh->initialize( vertexLayout );
			mesh->setVertexOrder( Mesh::FaceWinding::ECounterClockwise );
			processFaceGroup( *subObject, *( fg.get() ), *mesh );
			mesh->setTextureCoordinatesToDisabled( VertexLayout::KNormalmapTextureIndex );
		}
	}

	// Create the model that uses the created meshes.
	std::string modelId = overrideModelId == "" ? modelFileName : overrideModelId;
	model = modelMan.createModel( modelId );
	for( auto &subobject : objects )
	{
		GroupData *gd = subobject.get();
		for( auto &fg : gd->faceGroups )
		{
			std::shared_ptr<Mesh> mesh = meshMan.findMesh( fg->meshName );
			std::shared_ptr<Material> material = matMan.findMaterial( fg->materialName );

			// Create the section.
			model->addSection( mesh, material );
		}
	}

	assert( model != nullptr );
	return model;
}

bool WaveFrontImporter::importMaterial( const std::string &path, const std::string &materialNamePrefix )
{
	// Load the material file.
	std::string materialData;
	bool success = mCore.loadTextFile( path, materialData );
	if( !success )
	{
		// Loading of material file failed.
		return false;
	}

	std::vector< std::shared_ptr<MaterialData> > materials;
	std::istringstream iss( materialData );
	std::shared_ptr<MaterialData> activeMaterial;
	while( iss.peek() != EOF )
	{
		// Load next token from material file.
		std::string s;
		iss >> s;

		if( s == "newmtl" )
		{
			// Start reading new material.
			std::string materialName;
			iss >> materialName;
			materialName = materialNamePrefix + materialName;
			activeMaterial.reset( new MaterialData( materialName ) );
			materials.push_back( activeMaterial );
		}
		else if( s == "Ns" )
		{
			// Specular coefficient.
			float val;
			iss >> val;
			activeMaterial->specularExponent = val;
		}
		else if( s == "Ka" )
		{
			// Ambient color.
			float r, g, b;
			iss >> r >> g >> b;
			activeMaterial->ambient = Vec4( r, g, b, 1.0f );
		}
		else if( s == "Kd" )
		{
			// Diffuse color.
			float r, g, b;
			iss >> r >> g >> b;
			activeMaterial->diffuse = Vec4( r, g, b, 1.0f );
		}
		else if( s == "Ks" )
		{
			// Specular color.
			float r, g, b;
			iss >> r >> g >> b;
			activeMaterial->specular = Vec4( r, g, b, 1.0f );
		}
		else if( s == "Ni" )
		{
			float val;
			iss >> val;
			activeMaterial->reflectionIndex = val;
		}
		else if( s == "d" )
		{
			float val;
			iss >> val;
			activeMaterial->dissolveFactor = val;
		}
		else if( s == "illum" )
		{
			int val;
			iss >> val;
			activeMaterial->illumModel = val;
		}
		else if( s == "map_Kd" )
		{
			char txt[ 512 ];
			memset( txt, 0, sizeof( txt ) );
			iss.getline( txt, sizeof( txt ) );
			activeMaterial->map_Kd = FileNameUtils::trimFilename( txt );
		}
		else if( s == "map_Ka" )
		{
			char txt[ 512 ];
			memset( txt, 0, sizeof( txt ) );
			iss.getline( txt, sizeof( txt ) );
			activeMaterial->map_Ka = FileNameUtils::trimFilename( txt );
		}
		else if( s == "map_Ks" )
		{
			char txt[ 512 ];
			memset( txt, 0, sizeof( txt ) );
			iss.getline( txt, sizeof( txt ) );
			activeMaterial->map_Ks = FileNameUtils::trimFilename( txt );
		}
		else
		{
			// Ignore all contents from this line.
			iss.ignore( 4096, '\n' );
		}
	}

	// Create the resources to the rendering engine.
	TextureManager &texman = mCore.getTextureManager();
	MaterialManager &matman = mCore.getMaterialManager();
	for( auto &md : materials )
	{
		// Create new material. 
		std::shared_ptr<Material> mat = matman.findMaterial( md->materialName );
		assert( mat == nullptr );
		if( mat )
		{
			// Material exists already. This is an error.
			return false;
		}
		else
		{
			// Create new material object.
			mat = matman.createMaterial( md->materialName );
			assert( mat != nullptr );

			// Set common color values.
			mat->setSolidColor( md->diffuse );
			mat->setSpecularColor( SpecularColor( md->specular, md->specularExponent ) );

			// Set the material data to the new Material object.
			mat->setShaderProgramById( BuiltIn::SHADER_WAVEFRONT_TEX_SOLID );
			if( md->map_Kd.length() > 0 )
			{
				// This material has texture. Load the texture and assign it to the material.
				std::string diffuseTexName = FileNameUtils::getFileNamePart( md->map_Kd );
				std::string textureFileName = mTexturesPath + "/" + diffuseTexName;
				std::shared_ptr<Texture> tex = texman.findTexture( textureFileName );
				if( tex == nullptr )
				{
					// Texture hasn't been loaded yet. Create the texture now.
					tex = texman.createTexture( textureFileName, textureFileName );
					tex->setMipMaps( true );
				}
				
				// Set texture to the material.
				if( tex )
				{
					mat->setTexture( 0, textureFileName );
				}
				else
				{
					// Failed to create texture.
					assert( tex != nullptr );
					return false;
				}
			}
		}
	}
	return true;
}

void WaveFrontImporter::setScalingFactor( float scaleFactor )
{
	mScaleFactor = scaleFactor;
}

void WaveFrontImporter::setTexturesPath( const std::string& texturesPath )
{
	mTexturesPath = texturesPath;
}

void WaveFrontImporter::reset()
{
	mVertices.clear();
	mNormals.clear();
	mTextureCoords.clear();
}

WaveFrontImporter::FaceStruct WaveFrontImporter::parseFaceInfo( std::istringstream &stream )
{
	// Read the line that contains the face vertex indices.
	std::string faceLine;
	std::getline( stream, faceLine );
	faceLine = FileNameUtils::trimFilename( faceLine );

	// Prepare the stringstream for parsing the vertex indices from face line.
	std::istringstream iss( faceLine );
	std::string indexStr1;
	std::string indexStr2;
	std::string indexStr3;
	std::string indexStr4;

	// Read the first 3 vertices of the face.
	iss >> indexStr1 >> indexStr2 >> indexStr3;

	// Read 4th vertex if this face has 4 vertices.
	int faceVertexCount = 3;
	if( iss.peek() != EOF )
	{
		iss >> indexStr4;
		faceVertexCount = 4;
	}
	else
	{
		faceVertexCount = 3;
	}

	// Construct the FaceStruct object from the parsed data.
	FaceStruct fs;
	parseFaceInfoToken( indexStr1, 0, &fs );
	parseFaceInfoToken( indexStr2, 1, &fs );
	parseFaceInfoToken( indexStr3, 2, &fs );
	if( faceVertexCount == 4 )
	{
		parseFaceInfoToken( indexStr4, 3, &fs );
		assert( fs.vertexIndex[ 3 ] >= 0 );
	}
	fs.vertexCount = faceVertexCount;

	return fs;
}

bool WaveFrontImporter::parseFaceInfoToken( const std::string &faceComp, int cornerIndex, FaceStruct *outStruct )
{
	size_t p1 = std::string::npos;
	size_t p2 = std::string::npos;
	p1 = faceComp.find( "/" );
	if( p1 != std::string::npos )
	{
		p2 = faceComp.find( "/", p1 + 1 );
	}

	bool ret = true;
	if( p1 == std::string::npos )
	{
		// No slash character. String contains only face index.
		int vi;
		std::istringstream iss( faceComp );
		iss >> vi;
		outStruct->vertexIndex[ cornerIndex ] = vi;
		outStruct->componentFlags |= FaceStruct::FLAGS_VTX;
		ret = true;

	}
	else if( p2 == std::string::npos )
	{
		// There is only one slash e.g. "12/12". This means there is no
		// normal vectors but only position and texture coordinate.
		std::string faceInd = faceComp.substr( 0, p1 );
		std::string texInd = faceComp.substr( p1 + 1 );
		if( faceInd.length() > 0 )
		{
			std::istringstream iss( faceInd );
			iss >> outStruct->vertexIndex[ cornerIndex ];
			outStruct->componentFlags |= FaceStruct::FLAGS_VTX;
		}
		if( texInd.length() > 0 )
		{
			std::istringstream iss( texInd );
			iss >> outStruct->textureIndex[ cornerIndex ];
			outStruct->componentFlags |= FaceStruct::FLAGS_TEX;
		}

	}
	else
	{
		// There are 2 slashes.
		std::string faceInd = faceComp.substr( 0, p1 );
		std::string texInd = faceComp.substr( p1 + 1, p2 - p1 - 1 );
		std::string normInd = faceComp.substr( p2 + 1 );
		if( faceInd.length() > 0 )
		{
			std::istringstream iss( faceInd );
			iss >> outStruct->vertexIndex[ cornerIndex ];
			outStruct->componentFlags |= FaceStruct::FLAGS_VTX;
		}
		if( texInd.length() > 0 )
		{
			std::istringstream iss( texInd );
			iss >> outStruct->textureIndex[ cornerIndex ];
			outStruct->componentFlags |= FaceStruct::FLAGS_TEX;
		}
		if( normInd.length() > 0 )
		{
			std::istringstream iss( normInd );
			iss >> outStruct->normalIndex[ cornerIndex ];
			outStruct->componentFlags |= FaceStruct::FLAGS_NOR;
		}
	}
	return ret;
}

bool WaveFrontImporter::processFaceGroup( GroupData &subObj, const FaceGroup &fg, Mesh &mesh )
{
	int faceNum = 0;
	for( const FaceStruct &fs : fg.faces )
	{
		int i1 = fs.vertexIndex[ 0 ] - 1;
		int i2 = fs.vertexIndex[ 1 ] - 1;
		int i3 = fs.vertexIndex[ 2 ] - 1;
		int i4 = fs.vertexIndex[ 3 ] - 1;
		if( i1 >= 0 && i2 >= 0 && i3 >= 0 )
		{
			// Create the face vertices.
			size_t vi1 = mesh.addVertex();
			size_t vi2 = mesh.addVertex();
			size_t vi3 = mesh.addVertex();

			mesh.setPosition( vi1, getVtx( i1 ) );
			mesh.setPosition( vi2, getVtx( i2 ) );
			mesh.setPosition( vi3, getVtx( i3 ) );

			if( fs.componentFlags & FaceStruct::FLAGS_TEX )
			{
				int ti1 = fs.textureIndex[ 0 ] - 1;
				int ti2 = fs.textureIndex[ 1 ] - 1;
				int ti3 = fs.textureIndex[ 2 ] - 1;
				mesh.setTexture2D( vi1, 0, getTex( ti1 ) );
				mesh.setTexture2D( vi2, 0, getTex( ti2 ) );
				mesh.setTexture2D( vi3, 0, getTex( ti3 ) );
			}

			// If model has normal vectors for vertices, we use those. If
			// there are no normal vectors, we calculate them here.
			if( fs.componentFlags & FaceStruct::FLAGS_NOR )
			{
				int ni1 = fs.normalIndex[ 0 ] - 1;
				int ni2 = fs.normalIndex[ 1 ] - 1;
				int ni3 = fs.normalIndex[ 2 ] - 1;
				mesh.setNormal( vi1, getNormal( ni1 ).normalized() );
				mesh.setNormal( vi2, getNormal( ni2 ).normalized() );
				mesh.setNormal( vi3, getNormal( ni3 ).normalized() );
			}
			else
			{
				// No normals provided with the object data. We calculate the normal vectors
				// here because our shaders require them.
				Vec3 dv1 = getVtx( i2 ) - getVtx( i1 );
				Vec3 dv2 = getVtx( i3 ) - getVtx( i2 );
				Vec3 normalVec = Vec3::crossProduct( dv1, dv2 );
				normalVec.normalize();
				normalVec.mX += 0.00001f;
				mesh.setNormal( vi1, normalVec );
				mesh.setNormal( vi2, normalVec );
				mesh.setNormal( vi3, normalVec );
			}

			// Create the face.
			mesh.addFace( vi1, vi2, vi3 );

			// If this face consists of 4 vertices, we must create the 4th vertex as well and
			// create extra face. So the quadrilateral gets constructed from 2 triangles.
			if( fs.vertexCount == 4 )
			{
				// This face has 4 vertices. Add the 4th vertex and set its position.
				size_t vi4 = mesh.addVertex();
				mesh.setPosition( vi4, getVtx( i4 ) );
				if( fs.componentFlags & FaceStruct::FLAGS_TEX )
				{
					int ti4 = fs.textureIndex[ 3 ] - 1;
					mesh.setTexture2D( vi4, 0, getTex( ti4 ) );
				}
				if( fs.componentFlags & FaceStruct::FLAGS_NOR )
				{
					int ni4 = fs.normalIndex[ 3 ] - 1;
					mesh.setNormal( vi4, getNormal( ni4 ) );
				}
				else
				{
					// Normals not provided. Calculate them from other vertices.
					Vec3 dv1 = getVtx( i3 ) - getVtx( i1 );
					Vec3 dv2 = getVtx( i4 ) - getVtx( i3 );
					Vec3 normalVec = Vec3::crossProduct( dv1, dv2 ).normalized();
					normalVec.mX += 0.00001f;
					mesh.setNormal( vi4, normalVec );
				}

				// Create the second face of this quadrilateral.
				mesh.addFace( vi1, vi3, vi4 );
			}
		}
	}

	//exit(-1);

	return true;
}

Vec3 WaveFrontImporter::getVtx( size_t index ) const
{
	return mVertices.at( index );
}

Vec2 WaveFrontImporter::getTex( size_t index ) const
{
	return mTextureCoords.at( index );
}

Vec3 WaveFrontImporter::getNormal( size_t index ) const
{
	return mNormals.at( index );
}
