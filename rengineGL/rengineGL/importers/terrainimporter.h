
#pragma once

#include <memory>
#include <string>

// Forward declarations.
class Mesh;
class RenderCore;

/// <summary>
/// TerrainImporterParameters defines the parameters about the mesh that is
/// going to be created from the height map.
/// </summary>
class TerrainImporterParameters
{
public:
    TerrainImporterParameters();
    TerrainImporterParameters( const TerrainImporterParameters &obj );
    ~TerrainImporterParameters();
    TerrainImporterParameters& operator = ( const TerrainImporterParameters &obj );

    /// <summary>
    /// Set the range of Y-coordinates that get produced from the height map.
    /// </summary>
    /// <param name="minHeight">Y-coordinate set to black heightmap pixels.</param>
    /// <param name="maxHeight">Y-coordinate set to white heightmap pixels.</param>
    void setAltitudeRange( float minHeight, float maxHeight );
    void setSize( float width, float height );
    void setTextureWrapCount( float wrapCountU, float wrapCountV );

public:
    float mMinHeight;
    float mMaxHeight;
    float mWidth;
    float mHeight;
    float mWrapCountU;
    float mWrapCountV;
};

/// <summary>
/// An utility class for creating terrain meshes from greyscale heightmap images.
/// <para>
/// The terrain mesh is basically a grid on XZ-plane where Y-coordinate depends on
/// the pixel values from heightmap image. Black pixels indicate the lowest
/// Y-coordinate values and white pixels the heighest Y-coordinate values. Each
/// pixel from heightmap image gets its own vertex from the terrain mesh.
/// </para>
/// </summary>
class TerrainImporter
{
public:
    /// <summary>
    /// Constructor.
    /// </summary>
    TerrainImporter( RenderCore &core );

    /// <summary>
    /// Destructor.
    /// </summary>
    ~TerrainImporter();

    TerrainImporter( const TerrainImporter &obj ) = delete;
    TerrainImporter& operator = ( const TerrainImporter &obj ) = delete;

public:

    /// <summary>
    /// Creates the grid mesh from given height map image. The heightMapImageFile is expected
    /// to contain a greyscale image. The terrain parameters define the size and height
    /// range of the range of the grid mesh. The grid mesh is located on x- and z- plane and the
    /// Y-coordinate is read from the height map.
    /// </summary>
    /// <param name="heightMapImageFile">
    /// Name of the image file that contains the height map. This image is expected to contain a
    /// greyscale image.
    /// </param>
    /// <param name="params">The terrain parameters such as coordinate range where the grid mesh is created.</param>
    /// <param name="terrainMeshId">ID of the grid mesh that gets created.</param>
    /// <returns>Returns the created mesh. The mesh has been added to the render engine's mesh manager as well.</returns>
    std::shared_ptr<Mesh> createFromHeightMap( const std::string &heightMapImageFile, 
            const TerrainImporterParameters &params,
            const std::string &terrainMeshId );

private:
    void calculateNormals( Mesh *terrainGrid );

private:
    RenderCore &mCore;
};