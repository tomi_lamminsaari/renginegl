
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "../math/vec2.h"
#include "../math/vec4.h"

// Forward declarations.
class Model;
class RenderCore;

/// <summary>
/// This class implements importer that can import Wavefront OBJ-objects to the render engine.
/// <para>
/// The usage goes so that you must first create an instance of this importer class. Then call 'importModel()' function.
/// This will import the 3D model to the engine. You can later get access to the imported model by requesting a model
/// from ModelManager with the imported model's file name.
/// </para>
/// <para>
/// The texture are expected to be located to a one certain file. When we are importing the textures, the texture
/// path from material file will be replaced with 
/// </summary>
class WaveFrontImporter
	{
	private:
#pragma region PrivateStructures

		/// <summary>
		/// A struct for containing a single face information.
		/// </summary>
		class FaceStruct
			{
			public:
				static const int FLAGS_VTX = 1;
				static const int FLAGS_TEX = 2;
				static const int FLAGS_NOR = 4;

				int vertexIndex[ 4 ];
				int textureIndex[ 4 ];
				int normalIndex[ 4 ];
				int vertexCount;
				unsigned int componentFlags;

				FaceStruct();
			};

		/// <summary>
		/// Collection of faces. All these faces using the same material.
		/// </summary>
		class FaceGroup
			{
			public:
				std::vector<FaceStruct> faces;
				std::string materialName;
				std::string meshName;

				FaceGroup();
				unsigned int getVertexComponentFlags();
			};

		/// <summary>
		/// GroupData is a logical collection of face groups that construct a single object. One OBJ-file
		/// may contain multiple objects and each of them is a separate group.
		/// </summary>
		class GroupData
			{
			public:
				std::string objectName;
				bool isObject;
				std::vector<std::string> groupNames;
				std::string smoothShading;
				std::vector<std::shared_ptr<FaceGroup>> faceGroups;

				GroupData();
			};

		/// <summary>
		/// Container for a material specific information.
		/// </summary>
		class MaterialData
			{
			public:
				std::string materialName;
				Vec4 ambient;
				Vec4 diffuse;
				Vec4 specular;
				float specularExponent;
				float dissolveFactor;
				float reflectionIndex;
				int illumModel;
				std::string map_Kd;
				std::string map_Ka;
				std::string map_Ks;

				MaterialData( const std::string &name );
				bool operator==( const MaterialData &x ) const
					{
					return false;
					}
			};

#pragma endregion

	public:

		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="core">Reference to render engine's core object.</param>
		/// <param name="texturesPath">The path where all textures are located to.</param>
		WaveFrontImporter( RenderCore &core, const std::string& texturesPath );

		/// <summary>
		/// Destructor.
		/// </summary>
		~WaveFrontImporter();

		WaveFrontImporter( const WaveFrontImporter &obj ) = delete;  // Disabled
		WaveFrontImporter& operator = ( const WaveFrontImporter &obj ) = delete;  // Disabled

	public:
		/// <summary>
		/// Imports the 3D model to the ModelManager. By default the ID of the imported model
		/// will be the name of the imported WaveFront model file.
		/// </summary>
		/// <param name="path">Path to the 3D model file.</param>
		/// <param name="importFormat">Defines if imported format can be included to the render batches.</param>
		/// <param name="overrideModelId">
		/// Overrides the default filename based model ID with specific given model ID.
		/// </param>
		/// <param name="importMat">Should we import the materials to the MaterialManager during the loading as well.</param>
		/// <returns>Pointer to created model. Object is owned by the ModelManager.</returns>
		std::shared_ptr<Model> importModel(
			const std::string& path,
			const std::string& overrideModelId = "",
			bool importMat = true );

		/// <summary>
		/// Sets the scaling factor. Default is 1.0f
		/// </summary>
		/// <param name="scaleFactor">New scaling factor.</param>
		void setScalingFactor( float scaleFactor );

		/// <summary>
		/// Sets the path where this importer expects the texture to be located at.
		/// </summary>
		/// <param name="texturePath">The path where texture are located to.</param>
		void setTexturesPath( const std::string& texturePath );

	private:

		/// <summary>
		/// Imports the Wavefront material data to the MaterialManager and loads the textures.
		/// </summary>
		/// <param name="path">Path to the material file.</param>
		/// <param name="materialNamePrefix">
		/// Name prefix that will be added to the each created material instance. This
		/// will reduce the risk of getting name conflicts.
		/// </param>
		/// <returns>Returns true if materials were successfully imported.</returns>
		bool importMaterial( const std::string& path, const std::string& materialNamePrefix );

		void reset();
		FaceStruct parseFaceInfo( std::istringstream &stream );
		bool parseFaceInfoToken( const std::string &faceComp, int cornerIndex, FaceStruct *outStruct );
		bool processFaceGroup( GroupData &subObj, const FaceGroup &fg, Mesh &mesh );
		Vec3 getVtx( size_t index ) const;
		Vec2 getTex( size_t index ) const;
		Vec3 getNormal( size_t index ) const;
	private:
		/// <summary>Reference to main rendering engine.</summary>
		RenderCore &mCore;

		/// <summary>List of object vertices after it has been imported from Wavefront file.</summary>
		std::vector<Vec3> mVertices;

		/// <summary>List of vertex normal vectors after the object has been imported from Wavefront file.</summary>
		std::vector<Vec3> mNormals;

		/// <summary>List of vertex texture coordinates after the object has been imported from Wavefront file.</summary>
		std::vector<Vec2> mTextureCoords;

		/// <summary>Scaling factor that defines how much bigger or smaller the object becomes after importing.</summary>
		float mScaleFactor;

		/// <summary>Path where texture are expected to be located to.</summary>
		std::string mTexturesPath;
	};
