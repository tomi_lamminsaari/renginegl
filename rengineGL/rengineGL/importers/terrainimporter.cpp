
#include <exception>
#include <SDL_image.h>
#include "../core/mesh.h"
#include "../core/meshmanager.h"
#include "../core/vertexlayout.h"
#include "../core/rendercore.h"
#include "../math/matrix3x3.h"
#include "../math/quaternion.h"
#include "terrainimporter.h"

TerrainImporterParameters::TerrainImporterParameters() :
    mMinHeight( 0.0f ),
    mMaxHeight( 5.0f ),
    mWidth( 100.0f ),
    mHeight( 100.0f ),
    mWrapCountU( 1.0f ),
    mWrapCountV( 1.0f )
{

}

TerrainImporterParameters::TerrainImporterParameters( const TerrainImporterParameters &obj ) :
    mMinHeight( obj.mMinHeight ),
    mMaxHeight( obj.mMaxHeight ),
    mWidth( obj.mWidth ),
    mHeight( obj.mHeight ),
    mWrapCountU( obj.mWrapCountU ),
    mWrapCountV( obj.mWrapCountV )
{

}

TerrainImporterParameters::~TerrainImporterParameters()
{

}

TerrainImporterParameters &TerrainImporterParameters::operator=( const TerrainImporterParameters &obj )
{
    if( this != &obj )
    {
        mMinHeight = obj.mMinHeight;
        mMaxHeight = obj.mMaxHeight;
        mWidth = obj.mWidth;
        mHeight = obj.mHeight;
        mWrapCountU = obj.mWrapCountU;
        mWrapCountV = obj.mWrapCountV;
    }
    return *this;
}

void TerrainImporterParameters::setAltitudeRange( float minHeight, float maxHeight )
{
    if( mMinHeight > mMaxHeight )
    {
        throw std::invalid_argument( "TerrainImporterParameters::setAltitudeRange(): Minimum height is bigger than maximum height." );
    }
    mMinHeight = minHeight;
    mMaxHeight = maxHeight;
}

void TerrainImporterParameters::setSize( float width, float height )
{
    mWidth = width;
    mHeight = height;
}

void TerrainImporterParameters::setTextureWrapCount( float wrapCountU, float wrapCountV )
{
    mWrapCountU = wrapCountU;
    mWrapCountV = wrapCountV;
}

//
// class TerrainImporter
//

TerrainImporter::TerrainImporter( RenderCore &core ) :
    mCore( core )
{

}

TerrainImporter::~TerrainImporter()
{

}

std::shared_ptr<Mesh> TerrainImporter::createFromHeightMap(
    const std::string &heightMapImageFile,
    const TerrainImporterParameters& params,
    const std::string &terrainMeshId )
{
    // Load image file that works as a height map.
    std::string imagePath = mCore.getAbsolutePath( heightMapImageFile );
    SDL_Surface* heightMapImage = IMG_Load( imagePath.c_str() );
    if( heightMapImage == nullptr )
    {
        throw RengineError( "Failed to load heightmap: " + heightMapImageFile );
    }
    if( heightMapImage->w < 2 || heightMapImage->h < 2 )
    {
        throw RengineError( "Invalid heightmap. Heightmap must be at least 2x2 by size." );
    }
    SDL_PixelFormat* pxformat = heightMapImage->format;
    if( pxformat->BitsPerPixel != 32 )
    {
        throw RengineError( "Only 32 bit color depths are supported." );
    }
    size_t vertexCountX = heightMapImage->w;
    size_t vertexCountY = heightMapImage->h;
    size_t vertexCount = vertexCountX * vertexCountY;
    std::shared_ptr<Mesh> terrainMesh;
    terrainMesh = mCore.getMeshManager().createMesh_Grid( terrainMeshId,
                                                          VertexLayout::getDefaultLayout_DeferredShading(),
                                                          vertexCountX,
                                                          vertexCountY,
                                                          params.mWidth,
                                                          params.mHeight,
                                                          12.0f,
                                                          12.0f );
    float heightDifference = params.mMaxHeight - params.mMinHeight;
    SDL_LockSurface( heightMapImage );
    for( size_t y = 0; y < vertexCountY; ++y )
    {
        for( size_t x = 0; x < vertexCountX; ++x )
        {
            size_t index = y * vertexCountX + x;
            uint8_t pixel = 0;
            if( pxformat->BitsPerPixel == 32 )
            {
                uint32_t* pixeldata = reinterpret_cast< uint32_t* >( heightMapImage->pixels );
                uint32_t nativePixel = pixeldata[ index ];
                nativePixel = ( nativePixel & pxformat->Rmask ) >> pxformat->Rshift;
                pixel = static_cast< uint8_t >( nativePixel );
            }
            float h = ( static_cast< float >( pixel ) / 255.0f ) * heightDifference;

            Vec3 vertex = terrainMesh->getPosition( index );
            vertex.mY = params.mMinHeight + h;
            terrainMesh->setPosition( index, vertex );
        }
    }
    SDL_UnlockSurface( heightMapImage );
    SDL_FreeSurface( heightMapImage );
    heightMapImage = nullptr;

    // Calculate vertex normals.
    for( size_t faceIndex = 0; faceIndex < terrainMesh->getFaceCount(); ++faceIndex )
    {
        size_t vi1 = 0, vi2 = 0, vi3 = 0;
        terrainMesh->getFace( faceIndex, vi1, vi2, vi3 );
        Vec3 v1 = terrainMesh->getPosition( vi1 );
        Vec3 v2 = terrainMesh->getPosition( vi2 );
        Vec3 v3 = terrainMesh->getPosition( vi3 );
        Vec3 dir1 = v3 - v1;
        Vec3 dir2 = v2 - v1;
        Vec3 n = Vec3::crossProduct( dir1, dir2 ).normalized() * ( 1.0f );
        terrainMesh->setNormal( vi1, n );
        terrainMesh->setNormal( vi2, n );
        terrainMesh->setNormal( vi3, n );

        // Calculate tangent.
        auto rotQuat = Quaternion::newAxisRotation( 90.0f, dir1.normalized() );
        auto rotMat = rotQuat.toRotationMatrix3();
        Vec3 vertexTangent = ( rotMat * n ).normalized();
        terrainMesh->setVec3Attribute( vi1, VertexElement::Tangent3f, 0, vertexTangent );
        terrainMesh->setVec3Attribute( vi2, VertexElement::Tangent3f, 0, vertexTangent );
        terrainMesh->setVec3Attribute( vi3, VertexElement::Tangent3f, 0, vertexTangent );
    }
    return terrainMesh;
}
