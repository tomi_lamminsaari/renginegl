
#include <GL/glew.h>
#include <SDL_image.h>
#include "defines.h"
#include "rendercore.h"
#include "rengineexception.h"
#include "texture.h"
#include "texturemanager.h"
#include "../utils/gamelog.h"

Texture::Texture( TextureManager &texman ) :
	mManager( texman ),
	mSDLSurface( nullptr ),
	mGL_TextureObj( 0 ),
	mHorizontalWrap( Wrap::ClampToEdge ),
	mVerticalWrap( Wrap::ClampToEdge ),
	mBorderColor( 0.0f, 0.0f, 0.0f, 1.0f ),
	mUseMipMaps( false )
{

}

Texture::~Texture()
{
	removeFromGPU();
}

const std::string &Texture::getId() const
{
	return mId;
}

void Texture::setTextureFilePath( const std::string &texturePath )
{
	// First unload the current texture.
	removeFromGPU();

	// Set the new path.
	mTexturePath = texturePath;

	// Inform the manager that this texture needs to be reloaded.
	std::shared_ptr< Texture > self = shared_from_this();
	mManager.notifyTextureChanged( self );
}

GLuint Texture::getOpenGLTextureObject() const
{
	return mGL_TextureObj;
}

void Texture::activateTexture( size_t textureUnitNumber )
{
	assert( mGL_TextureObj != 0 );

	// Bind the texture to texture unit.
	GLuint texUnitId = GL_TEXTURE0 + static_cast< GLuint >( textureUnitNumber );
	GLASSERT( glActiveTexture( texUnitId ) );
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGL_TextureObj ) );

	// Set wrapping mode.
	GLint horizontalWrapMode = getWrapModeOpenGL( mHorizontalWrap );
	GLint verticalWrapMode = getWrapModeOpenGL( mVerticalWrap );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, horizontalWrapMode ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, verticalWrapMode ) );
	if( mHorizontalWrap == Wrap::ClampToBorder || mVerticalWrap == Wrap::ClampToBorder )
	{
		// Clamp-to-border is used. Set the border color.
		glTexParameterfv( GL_TEXTURE_2D, GL_TEXTURE_BORDER_COLOR, mBorderColor.dataPtr() );
	}
}

void Texture::setWrapMode( Wrap horizontalWrap, Wrap verticalWrap )
{
	mHorizontalWrap = horizontalWrap;
	mVerticalWrap = verticalWrap;
}

void Texture::setMipMaps( bool mipmaps )
{
	mUseMipMaps = mipmaps;
}

void Texture::setBorderColor( const Rgba& borderColor )
{
	mBorderColor = borderColor;
}

bool Texture::isUsingMipMaps() const
{
	return mUseMipMaps;
}

bool Texture::initializeWithParameters( const TextureParameters &textureParams )
{
	mTextureParameters = textureParams;

	// Relase the current texture.
	removeFromGPU();

	// Create the OpenGL texture and bind it.
	GLASSERT( glGenTextures( 1, &mGL_TextureObj ) );
	if( mGL_TextureObj == 0 )
	{
		// Failed to create new texture.
		return false;
	}
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGL_TextureObj ) );

	// Apply the parameters.
	mTextureParameters.apply();

	// Allocate the texture data memory.
	GLenum sourceFormat = GL_DEPTH_COMPONENT;
	GLenum sourceType = GL_FLOAT;
	GLint internalFormat = mTextureParameters.getInternalFormat();
	mTextureParameters.getSourceFormat( &sourceFormat, &sourceType );
	GLsizei width = mTextureParameters.getWidth();
	GLsizei height = mTextureParameters.getHeight();
	GLASSERT( glTexImage2D( GL_TEXTURE_2D, 0, internalFormat, width, height, 0, sourceFormat, sourceType, nullptr ) );
	return true;
}

void Texture::uploadToGPU()
{
	if( mGL_TextureObj == 0 )
	{
		// If there is a file path given, we use Allegro to create the OpenGL texture when it loads images.
		if( mSDLSurface == nullptr && mTexturePath.length() > 0 )
		{
			// Load texture image. Allegro library will create the OpenGL texture from it.
			std::string textureFullPath = mManager.getCore().getAbsolutePath( mTexturePath );
			mSDLSurface = IMG_Load( textureFullPath.c_str() );
			if( mSDLSurface == nullptr )
			{
				// Loading failed.
				throw RengineError( "Failed to load bitmap file '" + mTexturePath + "'" );
			}

			// Create OpenGL texture and upload the image data there.
			createOpenGLTexture();
			assert( mGL_TextureObj != 0 );

			// Set mipmaps.
			if( mUseMipMaps )
			{
				// Bind the texture and generate the mipmaps.
				GLASSERT( glBindTexture( GL_TEXTURE_2D, mGL_TextureObj ) );
				GLASSERT( glGenerateMipmap( GL_TEXTURE_2D ) );
				GLASSERT( glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR ) );
				GLASSERT( glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );

				// Unbind the texture.
				GLASSERT( glBindTexture( GL_TEXTURE_2D, 0 ) );
			}
			else
			{
				// Bind the texture and set the interpolation modes without mipmaps.
				GLASSERT( glBindTexture( GL_TEXTURE_2D, mGL_TextureObj ) );
				GLASSERT( glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR ) );
				GLASSERT( glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR ) );

				// Unbind the texture.
				GLASSERT( glBindTexture( GL_TEXTURE_2D, 0 ) );
			}
		}
	}
}

void Texture::removeFromGPU()
{
	// TODO: Remove resources.
	mGL_TextureObj = 0;
	
}

void Texture::setId( const std::string &id )
{
	mId = id;
}

void Texture::createOpenGLTexture()
{
	GLenum internalFormat = GL_RGB;
	GLenum sourceFormat = GL_RGB;

	uint32_t format = mSDLSurface->format->format;
	GameLog::get() << "SDL format of texture  " << mId << ": " << format << GameLog::EndLine;
	if( mSDLSurface->format->BitsPerPixel == 8 ) {
		internalFormat = GL_R8;
		sourceFormat = GL_RED;
	} else {
		SDL_Surface* convertedSurface = SDL_ConvertSurfaceFormat( mSDLSurface, SDL_PIXELFORMAT_RGB24, 0 );
		SDL_FreeSurface( mSDLSurface );
		mSDLSurface = convertedSurface;
	}
	RengineError::check( "Error state active before creating texture" );
	GLASSERT( glGenTextures( 1, &mGL_TextureObj ) );
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGL_TextureObj ) );
	GLASSERT( glTexImage2D( GL_TEXTURE_2D, 0, internalFormat,
			mSDLSurface->w, mSDLSurface->h,
			0, sourceFormat, GL_UNSIGNED_BYTE, mSDLSurface->pixels ) );
}

GLint Texture::getWrapModeOpenGL( Wrap wrapMode )
{
	GLint wrapModeGL = GL_REPEAT;
	switch( wrapMode )
	{
	case Wrap::Repeat:
		wrapModeGL = GL_REPEAT;
		break;
	case Wrap::MirroredRepeat:
		wrapModeGL = GL_MIRRORED_REPEAT;
		break;
	case Wrap::ClampToEdge:
		wrapModeGL = GL_CLAMP_TO_EDGE;
		break;
	case Wrap::ClampToBorder:
		wrapModeGL = GL_CLAMP_TO_BORDER;
		break;
	default:
		break;
	}
	return wrapModeGL;
}
