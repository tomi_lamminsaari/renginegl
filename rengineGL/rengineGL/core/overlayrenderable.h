
#pragma once

#include <memory>
#include "../math/vec3.h"

class Material;
class Mesh;
class RenderCore;
class SceneGlobalRenderPassInfo;

class OverlayRenderable
{
public:
	OverlayRenderable( RenderCore& scene );
	virtual ~OverlayRenderable();

	OverlayRenderable( const OverlayRenderable& obj ) = delete;
	OverlayRenderable& operator=( const OverlayRenderable& obj ) = delete;

public:
	void setGeometryObjects( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material );
	void setHidden( bool hidden );
	bool isHidden() const;
	void setOffsetPosition( const Vec3& offsetPosition );
	const Vec3& getOffsetPosition() const;
	void render( SceneGlobalRenderPassInfo* globalRenderPassInfo );

private:
	RenderCore& mCore;
	bool mHidden;
	Vec3 mOffsetPosition;
	std::shared_ptr<Mesh> mMesh;
	std::shared_ptr<Material> mMaterial;
};