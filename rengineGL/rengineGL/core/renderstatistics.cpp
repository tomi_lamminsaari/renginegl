
#include <limits>
#include "renderstatistics.h"

RenderStatistics::RenderStatistics() :
	mRenderPassCount( 0 ),
	mAabbSmallestZ( std::numeric_limits<float>::max() ),
	mAabbLargestZ( std::numeric_limits<float>::min() ),
	mProgramChangeCount( 0 )
{

}

RenderStatistics::~RenderStatistics()
{

}

RenderStatistics::RenderStatistics( const RenderStatistics &obj ) :
	mRenderPassCount( obj.mRenderPassCount ),
	mAabbSmallestZ( obj.mAabbSmallestZ ),
	mAabbLargestZ( obj.mAabbLargestZ ),
	mProgramChangeCount( obj.mProgramChangeCount )
{

}

RenderStatistics &RenderStatistics::operator = ( const RenderStatistics &obj )
{
	if( this != &obj )
	{
		mRenderPassCount = obj.mRenderPassCount;
		mAabbSmallestZ = obj.mAabbSmallestZ;
		mAabbLargestZ = obj.mAabbLargestZ;
		mProgramChangeCount = obj.mProgramChangeCount;
	}
	return *this;
}

void RenderStatistics::increaseRenderPassCount()
{
	mRenderPassCount++;
}

void RenderStatistics::increaseProgramChangeCount()
{
	mProgramChangeCount++;
}

void RenderStatistics::includeAabbZ( float z )
{
	if( z < mAabbSmallestZ )
		mAabbSmallestZ = z;
	if( z > mAabbLargestZ )
		mAabbLargestZ = z;
}

size_t RenderStatistics::getRenderPassCount() const
{
	return mRenderPassCount;
}

size_t RenderStatistics::getProgramChangeCount() const
{
	return mProgramChangeCount;
}

void RenderStatistics::reset()
{
	mRenderPassCount = 0;
	mAabbSmallestZ = std::numeric_limits<float>::max();
	mAabbLargestZ = std::numeric_limits<float>::min();
	mProgramChangeCount = 0;
}
