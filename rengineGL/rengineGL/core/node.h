
#pragma once

#include <string>
#include <vector>
#include <memory>
#include "../math/matrix4x4.h"
#include "../math/quaternion.h"
#include "../math/vec3.h"

// Forward declarations.
class Camera;
class LightSource;
class Renderable;
class SceneGlobalRenderPassInfo;

/// <summary>
/// Node-class is a collection of renderable objects and other nodes. The common nominator is that
/// the node applies same transformation operation to all these child members.
/// </summary>
class Node : public std::enable_shared_from_this< Node >
	{
public:
	/// <summary>
	/// Constructor.
	/// </summary>
	Node();

	/// <summary>
	/// Destructor.
	/// </summary>
	~Node();

	Node(const Node &obj) = delete;
	Node& operator = (const Node &obj) = delete;

public:
	/// <summary>
	/// Sets name for this node. Names don't have to be unique but they make debugging and
	/// finding the important nodes easier.
	/// </summary>
	/// <param name="name">Name of the node.</param>
	void setName( const std::string &name );

	/// <summary>
	/// Returns the name of this node.
	/// </summary>
	/// <returns>Name of this node.</returns>
	const std::string& getName() const;

	/// <summary>
	/// Creates a child node to this node.
	/// </summary>
	/// <returns>Pointer to created child node. Ownership is not transferred.</returns>
	std::shared_ptr<Node> createSubNode();

	/// <summary>
	/// Detaches this node from its parent node. Does not delete anything.
	/// </summary>
	void detachFromParent();

	/// <summary>
	/// Detaches the given node from this node.
	/// </summary>
	/// <param name="node">Pointer to node that will be removed from this node.</param>
	/// <returns>Returns true if 'node' was our child node and was detached from us. False if 'node' was not our child node.</returns>
	bool detachSubNode( std::shared_ptr<Node> node );

	/// <summary>
	/// Attaches a renderable to this node.
	/// </summary>
	/// <param name="obj">Pointer to renderable that will be attached.</param>
	void attachRenderable( std::shared_ptr<Renderable> obj );

	/// <summary>
	/// Detaches the given renderable from this node.
	/// </summary>
	/// <param name="obj">Pointer to renderable that will be detached from us.</param>
	void detachRenderable( std::shared_ptr<Renderable> obj );
	void attachLightSource( std::shared_ptr<LightSource>& lightsource );
	void detachLightSource( std::shared_ptr<LightSource>& lightsource );
	void setPosition( const Vec3 &pos );
	void movePosition( const Vec3 &dirVec );
	const Vec3& getPosition() const;
	void setRotation( const Quaternion &quat );
	const Quaternion& getRotation() const;
	Matrix4x4 getTransformationMatrix() const;
	size_t getRenderableCount() const;
	size_t getNodeCount() const;
	void setScalingFactor( float scaling );

	void prepareRender( const Camera *camera );
	void render( const Camera *camera, SceneGlobalRenderPassInfo *globalRenderPassInfo );

private:
	typedef std::vector< std::shared_ptr<Node> > NodeArray;
	typedef std::vector< std::shared_ptr<Renderable> > ObjectArray;
	typedef std::vector< std::shared_ptr<LightSource> > LightsArray;

	/// <summary>Parent node. Not owned.</summary>
	std::shared_ptr<Node> mParentNode;

	/// <summary>Name of this node.</summary>
	std::string mName;

	/// <summary>Current position of this node in relation to parent node.</summary>
	Vec3 mPos;

	/// <summary>Rotation amount of this node.</summary>
	Quaternion mRot;

	/// <summary>Renderables that have been attached to this node.</summary>
	ObjectArray mRenderables;

	/// <summary>Light sources that have been attached to this node.</summary>
	LightsArray mLights;

	/// <summary>List of child nodes that have been attached to this node.</summary>
	NodeArray mChildNodes;

	/// <summary>Tells if we have cached the parent nodes' transformation matrix.</summary>
	bool mHasCachedParentTransform;

	/// <summary>Cached parent node transformation matrix.</summary>
	Matrix4x4 mCachedParentTransform;

	/// <summary>Transformation matrix of this node.</summary>
	Matrix4x4 mNodeMat;

	/// <summary>Scaling factor of the objects. 1.0 means that no scaling is being done.</summary>
	float mScalingFactor;
	};
