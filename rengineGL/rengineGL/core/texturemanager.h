
#pragma once

#include <map>
#include <memory>
#include <vector>
#include "../math/vec2.h"
#include "managerbase.h"

// Forward declarations.
class Texture;
class TextureParameters;

/// <summary>
/// TextureManager manages the textures that are currently in use.
/// </summary>
class TextureManager : public ManagerBase
{
public:

	/// <summary>
	/// An utility class for carrying texture grid information for texture atlasing purposes.
	/// </summary>
	class TextureGridParams
	{
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="w">Width of the texture.</param>
		/// <param name="h">Height of the texture.</param>
		/// <param name="guardZoneWidth">
		/// The empty zone width between grids. Located at bottom and right edge of each texture in the grid.
		/// </param>
		TextureGridParams( unsigned int w, unsigned int h, unsigned int guardZoneWidth );

		/// <summary>
		/// Sets the offset of the left and bottom edge of the texture image.
		/// </summary>
		/// <param name="offsetX">The X coordinate offset.</param>
		/// <param name="offsetY">The Y coordinate offset.</param>
		void setLeftAndBottomOffsets( int offsetX, int offsetY );

		/// <summary>
		/// Sets the row and column count.
		/// </summary>
		/// <param name="columnCount">Number of columns the big texture atlas has.</param>
		/// <param name="rowCount">Number of columns the big texture atlas has.</param>
		void setRowsAndColumns( unsigned int columnCount, unsigned int rowCount );

		/// <summary>
		/// Calculates the texture corder coordinates of a texture that is located at given
		/// row and column in texture atlas.
		/// </summary>
		/// <param name="gridColumnIndex">The column of the texture.</param>
		/// <param name="gridRowIndex">The row of the texture.</param>
		/// <returns>
		/// The 4 corder coordinates of the texture. The order how the texture coordianates
		/// are inside the returned array is: bottom-left, bottom right, top-right, top-left.
		/// </param>
		std::vector< Vec2 > calculateTextureCornerCoordinates(
			unsigned int gridColumnIndex,
			unsigned int gridRowIndex ) const;

	public:
		/// <summary>The width of the texture.</summary>
		unsigned mWidth;

		/// <summary>The height of the texture.</summary>
		unsigned mHeight;

		/// <summary>Number of texture rows there are in the atlas.</summary>
		unsigned int mGridRows;

		/// <summary>Number of texture columns there are in the atlas.</summary>
		unsigned int mGridColumns;

		/// <summary>Empty guard zone width between the textures in the grid.</summary>
		unsigned int mTextureGuardZoneWidth;

		/// <summary>The left offset of the texture.</summary>
		int mOffsetX;

		/// <summary>The bottom edge offset of the texture.</summary>
		int mOffsetY;
	};

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="renderCore">The render engine root object.</param>
	TextureManager( RenderCore& renderCore );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~TextureManager();
	TextureManager( const TextureManager& obj ) = delete;
	TextureManager& operator = ( const TextureManager& obj ) = delete;

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup
	/// </summary>
	virtual void cleanup() override;

public:
	/// <summary>
	/// Creates new texture with given ID. Could be also already existing texture.
	/// </summary>
	/// <param name="textureId">ID of the new texture.</param>
	/// <returns>Pointer to new texture. Nullptr if creation fails.</returns>
	std::shared_ptr<Texture> createTexture( const std::string& textureId );

	/// <summary>
	/// Creates new texture with given ID and makes it load texture image from a file.
	/// </summary>
	/// <param name="textureId">ID of the new texture.</param>
	/// <param name="textureFile">File path to the texture image.</param>
	/// <returns>Pointer to new texture. Nullptr if creation fails.</returns>
	std::shared_ptr<Texture> createTexture(
		const std::string& textureId,
		const std::string& textureFile );

	/// <summary>
	/// Creates new texture from the given parameters.
	/// </summary>
	/// <param name="textureId">ID of the texture.</param>
	/// <param name="texParams">Texture parameters.</param>
	/// <returns>Smart pointer to the texture</returns>
	std::shared_ptr<Texture> createTextureWithParams(
		const std::string& textureId,
		const TextureParameters& texParams );

	/// <summary>
	/// Destroys the texture with given ID.
	/// </summary>
	/// <param name="textureId">ID of the texture that will be destroyed.</param>
	void destroyTexture( const std::string& textureId );

	/// <summary>
	/// Searches for a texture that has the given ID.
	/// <param name="textureId">ID of the texture to be searched for."</param>
	/// <returns>Smart pointer to found texture. Can contain null object.
	std::shared_ptr<Texture> findTexture( const std::string& textureId ) const;

	/// <summary>
	/// This should be called when changing the texture object. This will upload the changed texture
	/// back to GPU.
	/// </summary>
	/// <param name="texture">Pointer to texture that has changed.</param>
	void notifyTextureChanged( std::shared_ptr<Texture>& texture );

	/// <summary>
	/// Loads all the textures from texture list XML-file.
	/// </summary>
	/// <param name="textureListFile">Path to the texture list XML-file.</param>
	/// <returns>Returns the number of textures were loaded from the file.</returns>
	size_t loadTexturesFromListFile( const std::string& textureListFile );

	/// <summary>
	/// Uploads all textures to GPU memory.
	/// </summary>
	void uploadToGPU();

	/// <summary>
	/// Releases all GPU resources that were allocated for the textures.
	/// </summary>
	void removeFromGPU();

private:
	static const std::string KTextureAttribMipmapYes;
	static const std::string KTextureAttribMipmapNo;
	static const std::string KTextureWrapRepeat;
	static const std::string KTextureWrapMirroredRepeat;
	static const std::string KTextureWrapClampToEdge;
	static const std::string KTextureWrapClampToBorder;

private:
	/// <summary>Container type of the textures.</summary>
	typedef std::map< std::string, std::shared_ptr<Texture> > TextureContainer;

	/// <summary>Collection of textures.</summary>
	TextureContainer mTextures;

	/// <summary>Indicates if all textures have been uploaded to GPU memory.</summary>
	bool mAllTexturesUploaded;
};
