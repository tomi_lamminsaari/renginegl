
#include <assert.h>
#include "lightsources.h"
#include "node.h"
#include "renderable.h"

Node::Node() :
	mParentNode( nullptr ),
	mName( "" ),
	mPos( 0.0f, 0.0f, 0.0f ),
	mRot( 0.0f, Vec3( 0.0f, 0.0f, 1.0f ) ),
	mHasCachedParentTransform( false ),
	mScalingFactor( 1.0f )
{

}

Node::~Node()
{

}

void Node::setName( const std::string &nodeName )
{
	mName = nodeName;
}

const std::string &Node::getName() const
{
	return mName;
}

std::shared_ptr<Node> Node::createSubNode()
{
	std::shared_ptr<Node> node( new Node );
	node->mParentNode = shared_from_this();
	mChildNodes.push_back( node );
	return node;
}

void Node::detachFromParent()
{
	if( mParentNode != nullptr )
	{
		mParentNode->detachSubNode( shared_from_this() );
		mParentNode = nullptr;
	}
}

bool Node::detachSubNode( std::shared_ptr<Node> nodeToDetach )
{
	for( size_t i = 0; i < mChildNodes.size(); ++i )
	{
		if( mChildNodes[ i ] == nodeToDetach )
		{
			mChildNodes.erase( mChildNodes.begin() + i );
			return true;
		}
	}
	return false;
}

void Node::attachRenderable( std::shared_ptr<Renderable> renderable )
{
	assert( renderable->getParentNode() == nullptr );

	mRenderables.push_back( renderable );
	renderable->setParentNode( shared_from_this() );
}

void Node::detachRenderable( std::shared_ptr<Renderable> renderable )
{
	assert( renderable->getParentNode().get() == this );

	auto iter = mRenderables.begin();
	while( iter != mRenderables.end() )
	{
		if( *iter == renderable )
		{
			// We found the renderable from our child renderables list.
			renderable->setParentNode( nullptr );
			mRenderables.erase( iter );
			return;
		}

		// Move to next renderable.
		iter++;
	}
}

void Node::attachLightSource( std::shared_ptr<LightSource> &lightsource )
{
	assert( lightsource->getParentNode() == nullptr );

	mLights.push_back( lightsource );
	std::shared_ptr<Node> n = shared_from_this();

	lightsource->setParentNode( shared_from_this() );
}

void Node::detachLightSource( std::shared_ptr<LightSource> &lightsource )
{
	assert( lightsource->getParentNode().get() == this );

	auto iter = mLights.begin();
	while( iter != mLights.end() )
	{
		if( *iter == lightsource )
		{
			// We found the lightsource from our child lights list.
			lightsource->setParentNode( nullptr );
			mLights.erase( iter );
			return;
		}

		// Move to next lightsource.
		iter++;
	}
}

void Node::setPosition( const Vec3 &newPos )
{
	mPos = newPos;
}

void Node::movePosition( const Vec3 &dirVec )
{
	mPos += dirVec;
}

const Vec3 &Node::getPosition() const
{
	return mPos;
}

void Node::setRotation( const Quaternion &quat )
{
	mRot = quat;
}

const Quaternion &Node::getRotation() const
{
	return mRot;
}

Matrix4x4 Node::getTransformationMatrix() const
{
	// Check if we are already in the scene's root node.
	if( mParentNode == nullptr )
	{
		// Yes, this is a root node. Just create the transformation matrix
		// and return it.
		Matrix4x4 rootMatrix = mRot.toRotationMatrix4();
		rootMatrix.setColumn( 3, Vec4( mPos, 1.0f ) );
		return rootMatrix;
	}

	// This is not scene's root node. Get the combined transformation matrix from our
	// parent node. These transformation matrix contains the combined transformations
	// of all our parent nodes.
	Matrix4x4 parentMat = mParentNode->getTransformationMatrix();

	// Combine the parent nodes' transformation with this node's transformation by
	// multiplying them together.
	Matrix4x4 nodeMat = mRot.toRotationMatrix4();
	nodeMat.setColumn( 3, Vec4( mPos, 1.0f ) );
	return parentMat * nodeMat;
}

size_t Node::getRenderableCount() const
{
	return mRenderables.size();
}

size_t Node::getNodeCount() const
{
	return mChildNodes.size();
}

void Node::setScalingFactor( float scaling )
{
	mScalingFactor = scaling;
}

#if 0
void Node::prepareRender(const Camera *camera)
{
	// Prepare the 'renderables' of this node.
	for (Renderable *rend : mRenderables)
	{
		rend->prepareRender(camera);
	}

	// Prepare child nodes.
	for (Node *node : mChildNodes)
	{
		node->prepareRender(camera);
	}
}
#endif

#if 0
void Node::render(const Camera *camera, SceneGlobalRenderPassInfo *globalRenderPassInfo )
{
	// Prepare the 'renderables' of this node.
	for( Renderable *rend : mRenderables )
	{
		if( rend->isHidden() == false )
		{
			rend->render( camera, globalRenderPassInfo );
		}
	}

	// Prepare child nodes.
	for( Node *node : mChildNodes )
	{
		node->render( camera, globalRenderPassInfo );
	}
}
#endif
