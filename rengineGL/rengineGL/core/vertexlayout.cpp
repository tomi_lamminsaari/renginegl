
#include <GL/glew.h>
#include <algorithm>
#include "vertexlayout.h"

size_t VertexLayout::getElementSizeBytes( VertexElement element )
{
	size_t elementSize = 0;
	switch( element )
	{
	case VertexElement::Position3f:
		elementSize = 3 * sizeof( GLfloat );
		break;

	case VertexElement::Normal3f:
		elementSize = 3 * sizeof( float );
		break;

	case VertexElement::Texture2f:
		elementSize = 2 * sizeof( float );
		break;

	case VertexElement::Color4f:
		elementSize = 4 * sizeof( float );
		break;

	case VertexElement::Tangent3f:
		elementSize = 3 * sizeof( float );
		break;

	case VertexElement::Bitangent3f:
		elementSize = 3 * sizeof( float );
		break;

	case VertexElement::Color3f:
		elementSize = 3 * sizeof( float );
		break;

	case VertexElement::TextureIndex1f:
		elementSize = sizeof( float );
		break;

	case VertexElement::Specular1f:
		elementSize = sizeof( float );
		break;

	default:
		break;
	}
	return elementSize;
}

size_t VertexLayout::getFloatCountOfElement( VertexElement element )
{
	size_t floatCount = 0;
	switch( element )
	{
	case VertexElement::Position3f:
		floatCount = 3;
		break;

	case VertexElement::Normal3f:
		floatCount = 3;
		break;

	case VertexElement::Texture2f:
		floatCount = 2;
		break;

	case VertexElement::Color4f:
		floatCount = 4;
		break;

	case VertexElement::Tangent3f:
		floatCount = 3;
		break;

	case VertexElement::Bitangent3f:
		floatCount = 3;
		break;

	case VertexElement::Color3f:
		floatCount = 3;
		break;

	case VertexElement::TextureIndex1f:
		floatCount = 1;
		break;

	case VertexElement::Specular1f:
		floatCount = 1;
		break;

	default:
		break;
	}
	return floatCount;
}

VertexLayout VertexLayout::getDefaultLayout_DeferredShading()
{
	return VertexLayout( {
			VertexElement::Position3f,  // vertex coordinate
			VertexElement::Normal3f,  // vertex normal
			VertexElement::Texture2f,  // diffuse texture coordinates
			VertexElement::Color3f,  // solid vertex color if diffuse texture is not in use
			VertexElement::TextureIndex1f,  // -1 if solid color in use, 0 if diffuse texture in use
			VertexElement::Specular1f,  // specular exponent
			VertexElement::Texture2f,  // normal map texture coordinate. If u-coord is -1, it means no normal map
			VertexElement::Tangent3f  // vertex tangent vector
		} );
}

VertexLayout::VertexLayout() :
	mVertexSizeBytes( 0 ),
	mFloatCount( 0 )
{

}

VertexLayout::VertexLayout( const std::vector<VertexElement> &vertexElementOrder ) :
	mVertexElementOrder( vertexElementOrder ),
	mVertexSizeBytes( 0 ),
	mFloatCount( 0 )
{
	// Calculate the size of vertex data based on given vertex element array.
	for( VertexElement element : vertexElementOrder )
	{
		mVertexSizeBytes += getElementSizeBytes( element );
		mFloatCount += getFloatCountOfElement( element );
	}
}

VertexLayout::~VertexLayout()
{

}

VertexLayout::VertexLayout( const VertexLayout& obj ) :
	mVertexElementOrder( obj.mVertexElementOrder ),
	mVertexSizeBytes( obj.mVertexSizeBytes ),
	mFloatCount( obj.mFloatCount )
{

}

VertexLayout& VertexLayout::operator = ( const VertexLayout& obj )
{
	if( this != &obj )
	{
		mVertexElementOrder = obj.mVertexElementOrder;
		mVertexSizeBytes = obj.mVertexSizeBytes;
		mFloatCount = obj.mFloatCount;
	}
	return *this;
}

bool VertexLayout::operator==( const VertexLayout& obj ) const
{
	if( obj.mFloatCount != mFloatCount )
		return false;
	if( obj.mVertexSizeBytes != mVertexSizeBytes )
		return false;
	for( size_t elemIndex = 0; elemIndex < mVertexElementOrder.size(); elemIndex++ )
	{
		if( mVertexElementOrder[ elemIndex ] != obj.mVertexElementOrder[ elemIndex ] )
			return false;
	}
	return true;
}

VertexElement VertexLayout::operator[] ( size_t index ) const
{
	return mVertexElementOrder[ index ];
}

size_t VertexLayout::getSizeBytes() const
{
	return mVertexSizeBytes;
}

size_t VertexLayout::getFloatCount() const
{
	return mFloatCount;
}

size_t VertexLayout::getElementCount() const
{
	return mVertexElementOrder.size();
}

int VertexLayout::getFloatIndex( VertexElement element, int instanceOfType ) const
{
	int index = 0;
	int currentInstanceNum = 0;
	for( size_t i = 0; i < mVertexElementOrder.size(); ++i )
	{
		if( mVertexElementOrder[ i ] == element )
		{
			if( currentInstanceNum == instanceOfType )
			{
				// This is the element we're looking for.
				return index;
			}
			else
			{
				// Element type is correct but this is not the n'th instance of the element.
				currentInstanceNum++;
			}
		}

		// Increase the index count by the element size.
		index += static_cast< int >( getFloatCountOfElement( mVertexElementOrder[ i ] ) );
	}
	return -1;
}

bool VertexLayout::hasElement( VertexElement element ) const
{
	auto iter = std::find( mVertexElementOrder.begin(), mVertexElementOrder.end(), element );
	return iter != mVertexElementOrder.end();
}
