
#include <cassert>
#include "meshinfo.h"
#include "rengineexception.h"

const std::string MeshInfo::KBoolIsGrid = "_isGrid";
const std::string MeshInfo::KIntGridHeight = "_gridWidth";
const std::string MeshInfo::KIntGridWidth = "_gridHeight";
const std::string MeshInfo::KIntConeVerticesCount = "_coneVertices";
const std::string MeshInfo::KStringMeshShape = "_meshtype";

MeshInfo::MeshInfo()
{

}

MeshInfo::MeshInfo( const MeshInfo &obj ) :
	mIntData( obj.mIntData ),
	mBoolData( obj.mBoolData )
{

}

MeshInfo::~MeshInfo()
{

}

MeshInfo &MeshInfo::operator=( const MeshInfo &obj )
{
	if( this != &obj )
	{
		mIntData = obj.mIntData;
		mBoolData = obj.mBoolData;
	}
	return *this;
}

void MeshInfo::setGridInfo( int widthVertices, int heightVertices )
{
	assert( widthVertices > 0 );
	assert( heightVertices > 0 );
	mBoolData[ KBoolIsGrid ] = true;
	mIntData[ KIntGridWidth ] = widthVertices;
	mIntData[ KIntGridHeight ] = heightVertices;
}

void MeshInfo::clearGridInfo()
{
	auto iterBool = mBoolData.find( KBoolIsGrid );
	if( iterBool != mBoolData.end() )
		mBoolData.erase( iterBool );
	auto iterInt = mIntData.find( KIntGridWidth );
	if( iterInt != mIntData.end() )
		mIntData.erase( iterInt );
	iterInt = mIntData.find( KIntGridHeight );
	if( iterInt != mIntData.end() )
		mIntData.erase( iterInt );
}

void MeshInfo::addIntInfo( const std::string& infoId, int val )
{
	mIntData[ infoId ] = val;
}

void MeshInfo::addBoolInfo( const std::string& infoId, bool val )
{
	mBoolData[ infoId ] = val;
}

void MeshInfo::addStringInfo( const std::string& infoId, const std::string& stringData )
{
	mStringData[ infoId ] = stringData;
}

bool MeshInfo::isGrid() const
{
	auto iterIsGrid = mBoolData.find( KBoolIsGrid );
	return iterIsGrid != mBoolData.end();
}

void MeshInfo::getGridInfo( int& outWidthVertices, int& outHeightVertices ) const
{
	if( isGrid() )
	{
		// Get the grid information.
		bool infoExists = false;
		getIntInfo( KIntGridWidth, outWidthVertices );  // Ignore return value.
		getIntInfo( KIntGridHeight, outHeightVertices );  // Ignore return value.
	}
	else
	{
		// This is not a grid. Throw an error.
		throw RengineError( "Can't get grid info because mesh is not a grid." );
	}
}

bool MeshInfo::getIntInfo( const std::string& infoId, int& outValue ) const
{
	auto iter = mIntData.find( infoId );
	if( iter != mIntData.end() )
	{
		outValue = iter->second;
		return true;
	}
	else
	{
		outValue = 0;
		return false;
	}
}

bool MeshInfo::getBoolInfo( const std::string& infoId, bool& outValue ) const
{
	auto iter = mBoolData.find( infoId );
	if( iter != mBoolData.end() )
	{
		outValue = iter->second;
		return true;
	}
	else
	{
		return false;
	}
}

bool MeshInfo::getStringInfo( const std::string& infoId, std::string& outValue ) const
{
	outValue = "";
	auto iter = mStringData.find( infoId );
	if( iter == mStringData.end() )
		return false;

	// Info was found. Return it.
	outValue = iter->second;
	return true;
}
