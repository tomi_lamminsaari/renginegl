
#include "managerbase.h"

ManagerBase::ManagerBase(RenderCore &core) :
	mCore(core),
	mIDCounter(1)
{

}

ManagerBase::~ManagerBase()
{

}

RenderCore& ManagerBase::getCore()
{
	return mCore;
}

const RenderCore& ManagerBase::getCore() const
{
	return mCore;
}

int ManagerBase::generateUniqueID()
{
	// Reserve lock that releases the mutex automatically when exiting this function.
	std::lock_guard<std::mutex> lock(mIDCounterMutex);

	// Take the next ID.
	int id = mIDCounter++;
	return id;
}
