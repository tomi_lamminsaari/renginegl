
#include "../../math/quaternion.h"
#include "../meshmanager.h"
#include "../rendercore.h"
#include "cylindermesh.h"

std::shared_ptr<Mesh> MeshCreation::createCylinder(
	RenderCore& core,
	const std::string& meshId,
	const VertexLayout& vertexLayout,
	float radius1,
	float radius2,
	float cylinderLength,
	int numOfVerticesInCircumference )
{
	// Verify the input parameters.
	if( radius1 <= 0.0f )
		throw std::runtime_error( "Parameter 'radius1' must be positive when creating a cylinder." );
	if( radius2 <= 0.0f )
		throw std::runtime_error( "Parameter 'radius2' must be positive when creating a cylinder." );
	if( cylinderLength <= 0.0f )
		throw std::runtime_error( "Parameter 'cylinderLength' must be positive when creating a cylinder." );
	if( numOfVerticesInCircumference < 5 )
		throw std::runtime_error( "Parameter 'numOfVerticesInCircumference' cannot be smaller than 5." );

	// Create the mesh.
	std::shared_ptr<Mesh> mesh = core.getMeshManager().createMesh( meshId );
	mesh->initialize( vertexLayout );

	// Calculate the number of texture coordinate attributes there are.
	int textureAttributeCount = 0;
	for( size_t vai = 0; vai < vertexLayout.getElementCount(); vai++ )
	{
		const VertexElement& ve = vertexLayout[ vai ];
		if( ve == VertexElement::Texture2f )
			textureAttributeCount++;
	}

	// Calculate the number of degrees we must rotate for each vertex in the circumference.
	float textureUPerVertex = 1.0f / static_cast< float >( numOfVerticesInCircumference );
	float y1 = 0.0f - cylinderLength / 2.0f;
	float y2 = cylinderLength / 2.0f;
	float degsPerVertex = 360.0f / static_cast< float >( numOfVerticesInCircumference );
	float baseRotationDegs = 0.0f;
	float baseTextureU = 0.0f;
	Vec3 centerPointY1( 0.0f, y1, 0.0f );
	Vec3 centerPointY2( 0.0f, y2, 0.0f );
	for( int i = 0; i < numOfVerticesInCircumference; ++i )
	{
		// Prepare the rotation matrices.
		Matrix3x3 baseRotationMat = Quaternion::newAxisRotation( baseRotationDegs, Vec3( 0.0f, 1.0f, 0.0f ) ).toRotationMatrix3();
		Matrix3x3 nextRotationMat = Quaternion::newAxisRotation( baseRotationDegs + degsPerVertex, Vec3( 0.0f, 1.0f, 0.0f ) ).toRotationMatrix3();

		// First triangle.
		Vec3 v1( radius1, y1, 0.0f );
		Vec3 v2( radius1, y1, 0.0f );
		Vec3 v3( radius2, y2, 0.0f );
		v1 = baseRotationMat * v1;
		v2 = nextRotationMat * v2;
		v3 = baseRotationMat * v3;
		Vec2 v1Tex( baseTextureU, 0.0f );
		Vec2 v2Tex( baseTextureU + textureUPerVertex, 0.0f );
		Vec2 v3Tex( baseTextureU, 1.0f );
		Vec3 faceNormalV1 = v1 - centerPointY1;
		Vec3 faceNormalV2 = v2 - centerPointY1;
		Vec3 faceNormalV3 = v3 - centerPointY2;

		// Second triangle.
		Vec3 v4( radius1, y1, 0.0f );
		Vec3 v5( radius2, y2, 0.0f );
		Vec3 v6( radius2, y2, 0.0f );
		v4 = nextRotationMat * v4;
		v5 = nextRotationMat * v5;
		v6 = baseRotationMat * v6;
		Vec2 v4Tex( baseTextureU + textureUPerVertex, 0.0f );
		Vec2 v5Tex( baseTextureU + textureUPerVertex, 1.0f );
		Vec2 v6Tex( baseTextureU, 1.0f );
		Vec3 faceNormalV4 = v4 - centerPointY1;
		Vec3 faceNormalV5 = v5 - centerPointY2;
		Vec3 faceNormalV6 = v6 - centerPointY2;

		// Add the vertices and indices of first triangle to the mesh.
		size_t vertexIndex1 = mesh->addVertex();
		size_t vertexIndex2 = mesh->addVertex();
		size_t vertexIndex3 = mesh->addVertex();
		mesh->addFace( vertexIndex1, vertexIndex3, vertexIndex2 );
		size_t vertexIndex4 = mesh->addVertex();
		size_t vertexIndex5 = mesh->addVertex();
		size_t vertexIndex6 = mesh->addVertex();
		mesh->addFace( vertexIndex4, vertexIndex6, vertexIndex5 );

		// Set the vertex attribute values to the mesh.
		mesh->setPosition( vertexIndex1, v1 );
		mesh->setNormal( vertexIndex1, faceNormalV1 );
		mesh->setPosition( vertexIndex2, v2 );
		mesh->setNormal( vertexIndex2, faceNormalV2 );
		mesh->setPosition( vertexIndex3, v3 );
		mesh->setNormal( vertexIndex3, faceNormalV3 );
		mesh->setPosition( vertexIndex4, v4 );
		mesh->setNormal( vertexIndex4, faceNormalV4 );
		mesh->setPosition( vertexIndex5, v5 );
		mesh->setNormal( vertexIndex5, faceNormalV5 );
		mesh->setPosition( vertexIndex6, v6 );
		mesh->setNormal( vertexIndex6, faceNormalV6 );
		for( int texElemIndex = 0; texElemIndex < textureAttributeCount; ++texElemIndex )
		{
			// Set the texture coordinates.
			mesh->setTexture2D( vertexIndex1, texElemIndex, v1Tex );
			mesh->setTexture2D( vertexIndex2, texElemIndex, v2Tex );
			mesh->setTexture2D( vertexIndex3, texElemIndex, v3Tex );
			mesh->setTexture2D( vertexIndex4, texElemIndex, v4Tex );
			mesh->setTexture2D( vertexIndex5, texElemIndex, v5Tex );
			mesh->setTexture2D( vertexIndex6, texElemIndex, v6Tex );
		}

		// Update the rotation angles and texture coordintanes for
		// next face segment of the cylinder.
		baseRotationDegs += degsPerVertex;
		baseTextureU += textureUPerVertex;
	}

	// Set normal mapping coordinates to (-1.0, -1.0) to disable it.
	if( textureAttributeCount > 1 )
		mesh->setTextureCoordinatesToDisabled( VertexLayout::KNormalmapTextureIndex );

	// Cylinder mesh is ready. Return it.
	return mesh;
}
