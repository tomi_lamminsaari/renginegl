
#include "../../math/vec2.h"
#include "../../math/vec3.h"
#include "../mesh.h"
#include "../meshmanager.h"
#include "../rendercore.h"
#include "quadmesh.h"

std::shared_ptr<Mesh> MeshCreation::createQuad(
	RenderCore &core,
	const std::string &meshId,
	const VertexLayout &vertexLayout,
	const FaceSides faceSides,
	const float quadWidth,
	const float quadHeight,
	const UVTextureRange &textureRange
)
{
	std::vector<UVTextureRange> textureRanges;
	textureRanges.push_back( textureRange );
	return createQuad( core, meshId, vertexLayout, faceSides, quadWidth, quadHeight, textureRanges );
}

std::shared_ptr<Mesh>MeshCreation::createQuad(
	RenderCore &core,
	const std::string &meshId,
	const VertexLayout &vertexLayout,
	const FaceSides faceSides,
	const float quadWidth,
	const float quadHeight,
	const std::vector<UVTextureRange> &textureRanges
)
{
	// Create the mesh object.
	std::shared_ptr<Mesh> mesh = core.getMeshManager().createMesh( meshId );
	if( !mesh )
	{
		return nullptr;
	}

	// Calculate the mesh dimensions.	
	const float x = quadWidth / 2.0f;
	const float y = quadHeight / 2.0f;

	// Prepare the coordinate and index vectors.
	std::vector<Vec3> positionVector
	{
		// First side of quad.
		Vec3( -x, -y, 0.0f ),
		Vec3( x, -y, 0.0f ),
		Vec3( x, y, 0.0f ),
		Vec3( -x, y, 0.0f ),

		// Second side of quad.
		Vec3( x, -y, 0.0f ),
		Vec3( -x, -y, 0.0f ),
		Vec3( -x, y, 0.0f ),
		Vec3( x, y, 0.0f )
	};
	std::vector<Vec3> normalVector
	{
		// First side of the quad.
		Vec3( 0.0f, 0.0f, -1.0f ),
		Vec3( 0.0f, 0.0f, -1.0f ),
		Vec3( 0.0f, 0.0f, -1.0f ),
		Vec3( 0.0f, 0.0f, -1.0f ),

		// Second side of the quad.
		Vec3( 0.0f, 0.0f, 1.0f ),
		Vec3( 0.0f, 0.0f, 1.0f ),
		Vec3( 0.0f, 0.0f, 1.0f ),
		Vec3( 0.0f, 0.0f, 1.0f )
	};
	std::vector<Vec3> tangentVector
	{
		// First side of the quad.
		Vec3( 1.0f, 0.0f, 0.0f ),
		Vec3( 1.0f, 0.0f, 0.0f ),
		Vec3( 1.0f, 0.0f, 0.0f ),
		Vec3( 1.0f, 0.0f, 0.0f ),

		// Second side of the quad.
		Vec3( -1.0f, 0.0f, 0.0f ),
		Vec3( -1.0f, 0.0f, 0.0f ),
		Vec3( -1.0f, 0.0f, 0.0f ),
		Vec3( -1.0f, 0.0f, 0.0f )
	};
	std::vector<Vec3> bitangentVector
	{
		// First side of the quad.
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ),

		// Second side of the quad.
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f )
	};

	// Initialize the vertices.
	size_t vertexCount = ( faceSides == FaceSides::SingleSide ) ? 4 : 8;
	size_t faceCount = faceSides == FaceSides::SingleSide ? 2 : 4;
	mesh->initialize( vertexLayout, vertexCount, faceCount );

	// Set the vertex position, normal and tanget vectors.
	for( size_t i = 0; i < vertexCount; i++ )
	{
		mesh->setPosition( i, positionVector[ i ] );
		mesh->setNormal( i, normalVector[ i ] );
		if( vertexLayout.hasElement( VertexElement::Tangent3f ) )
			mesh->setVec3Attribute( i, VertexElement::Tangent3f, 0, tangentVector[ i ] );
		if( vertexLayout.hasElement( VertexElement::Bitangent3f ) )
			mesh->setVec3Attribute( i, VertexElement::Bitangent3f, 0, bitangentVector[ i ] );
	}

	// Set vertex texture coordinates.
	for( int textureIndex = 0; textureIndex < ( int )textureRanges.size(); textureIndex++ )
	{
		// Construct a vector for texture coordinates.
		const UVTextureRange &texRange = textureRanges[ textureIndex ];
		std::vector<Vec2> textureCoords
		{
			// First side of the quad.
			Vec2( texRange.getMinU(), texRange.getMinV() ),
			Vec2( texRange.getMaxU(), texRange.getMinV() ),
			Vec2( texRange.getMaxU(), texRange.getMaxV() ),
			Vec2( texRange.getMinU(), texRange.getMaxV() ),

			// Second side of the quad.
			Vec2( texRange.getMinU(), texRange.getMinV() ),
			Vec2( texRange.getMaxU(), texRange.getMinV() ),
			Vec2( texRange.getMaxU(), texRange.getMaxV() ),
			Vec2( texRange.getMinU(), texRange.getMaxV() )
		};
		for( size_t i = 0; i < vertexCount; ++i )
		{
			mesh->setTexture2D( i, textureIndex, textureCoords[ i ] );
		}
	}

	// Initialize the faces.
	mesh->setFace( 0, 0, 1, 2 );
	mesh->setFace( 1, 0, 2, 3 );
	if( faceCount == 4 )
	{
		mesh->setFace( 2, 4, 5, 6 );
		mesh->setFace( 3, 4, 6, 7 );
	}

	// Set shininess.
	mesh->setFloatAttributes( 0, mesh->getVertexCount(), VertexElement::Specular1f, 0, 255.0f );

	mesh->setVertexOrder( Mesh::FaceWinding::ECounterClockwise );
	return mesh;
}
