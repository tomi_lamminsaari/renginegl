
#include "../rendercore.h"
#include "../mesh.h"
#include "../meshmanager.h"
#include "../../math/quaternion.h"
#include "conemesh.h"

namespace MeshCreation
{

std::shared_ptr<Mesh> createSolidColorCone(
	RenderCore& core,
	const std::string& meshId,
	const VertexLayout& vertexLayout,
	float coneBottomRadius,
	float coneHeight,
	int numOfVerticesInCircumference,
	bool convex )
{
	// Validate input parameters.
	if( coneBottomRadius <= 0.0f )
		throw std::invalid_argument( "Invalid bottom radius when creating cone mesh." );
	if( coneHeight <= 0.0f )
		throw std::invalid_argument( "Invalid height when creating cone mesh." );
	if( numOfVerticesInCircumference < 5 )
		throw std::invalid_argument( "Cone circumference must have at least 5 vertices." );

	// Create the mesh.
	std::shared_ptr<Mesh> mesh = core.getMeshManager().createMesh( meshId );
	mesh->initialize( vertexLayout );

	// Calculate the number of texture coordinate attributes there are.
	int textureAttributeCount = 0;
	for( size_t vai = 0; vai < vertexLayout.getElementCount(); vai++ )
	{
		const VertexElement& ve = vertexLayout[ vai ];
		if( ve == VertexElement::Texture2f )
			textureAttributeCount++;
	}

	// Calculate the cone head's center vertex location.
	Vec3 coneCenterPos( 0.0f, coneHeight / 2.0f, 0.0f );
	Vec3 coneCenterLowPoint( 0.0f, -1.5f * coneHeight, 0.0f );

	// Start creating vertices the cone's bottom part's circumference.
	std::vector< std::pair< Vec3, Vec3 > > positionsOnCircumference;
	float coneBottomY = 0.0f - coneHeight / 2.0f;
	float degsPerVertex = 360.0f / static_cast< float >( numOfVerticesInCircumference );
	float baseDegs = 0.0f;
	for( int vertexIndex = 0; vertexIndex < numOfVerticesInCircumference; ++vertexIndex )
	{
		// Prepare the rotation matrices.
		Matrix3x3 baseRotationMat = Quaternion::newAxisRotation( baseDegs, Vec3( 0.0f, 1.0f, 0.0f ) ).toRotationMatrix3();
		Matrix3x3 nextRotationMat = Quaternion::newAxisRotation( baseDegs + degsPerVertex, Vec3( 0.0f, 1.0f, 0.0f ) ).toRotationMatrix3();
		Matrix3x3 nextNextRotationMat = Quaternion::newAxisRotation( baseDegs + degsPerVertex + degsPerVertex, Vec3( 0.0f, 1.0f, 0.0f ) ).toRotationMatrix3();

		// Calculate the vertices on cone bottom's circumference.
		Vec3 vertex = baseRotationMat * Vec3( coneBottomRadius, coneBottomY, 0.0f );
		Vec3 vertexNext = nextRotationMat * Vec3( coneBottomRadius, coneBottomY, 0.0f );
		Vec3 vertexNextNext = nextNextRotationMat * Vec3( coneBottomRadius, coneBottomY, 0.0f );
		Vec3 faceNormal = Vec3::crossProduct( vertexNext - vertex, coneCenterPos - vertex ).normalized();
		Vec3 faceNormalNext = Vec3::crossProduct( vertexNextNext - vertexNext, coneCenterPos - vertexNext ).normalized();

		// Add vertices and face indices to the mesh.
		size_t vindex = mesh->addVertex();
		size_t vindexNext = mesh->addVertex();
		size_t vindexCenter = mesh->addVertex();
		mesh->addFace( vindex, vindexCenter, vindexNext );
		mesh->setPosition( vindex, vertex );
		mesh->setPosition( vindexNext, vertexNext );
		mesh->setPosition( vindexCenter, coneCenterPos );
		mesh->setNormal( vindex, faceNormal );
		mesh->setNormal( vindexNext, faceNormalNext );
		mesh->setNormal( vindexCenter, faceNormal );

		mesh->setVec3Attribute( vindex, VertexElement::Color3f, 0, Vec3( 0.0, 1.0, 0.0 ) );
		mesh->setFloatAttribute( vindex, VertexElement::TextureIndex1f, 0, -100.0f );
		mesh->setVec3Attribute( vindexNext, VertexElement::Color3f, 0, Vec3( 0.0, 1.0, 0.0 ) );
		mesh->setFloatAttribute( vindexNext, VertexElement::TextureIndex1f, 0, -100.0f );
		mesh->setVec3Attribute( vindexCenter, VertexElement::Color3f, 0, Vec3( 0.0, 1.0, 0.0 ) );
		mesh->setFloatAttribute( vindexCenter, VertexElement::TextureIndex1f, 0, -100.0f );

		// Store the locations on circumference to a vector so that we can use the same coordinates
		// if we should fill the cone's bottom part with faces as well.
		positionsOnCircumference.push_back( std::make_pair( vertex, vertexNext ) );

		// Update the rotation angles and texture coordintanes for
		// next face segment of the cylinder.
		baseDegs += degsPerVertex;
	}

	// Fill the bottom part if the mesh should be convex.
	if( convex )
	{
		// Add mesh to the center and start doing triangles around the circumference of the cone's
		// bottom.
		size_t centerVertexIndex = mesh->addVertex();
		mesh->setPosition( centerVertexIndex, Vec3( 0.0f, coneBottomY, 0.0f ) );
		mesh->setNormal( centerVertexIndex, Vec3( 0.0f, -1.0f, 0.0f ) );
		for( const auto& positionPair : positionsOnCircumference )
		{
			size_t vindex = mesh->addVertex();
			size_t vindexNext = mesh->addVertex();
			mesh->addFace( vindex, vindexNext, centerVertexIndex );
			mesh->setPosition( vindex, positionPair.first );
			mesh->setPosition( vindexNext, positionPair.second );
			mesh->setNormal( vindex, Vec3( 0.0f, -1.0f, 0.0f ) );
			mesh->setNormal( vindexNext, Vec3( 0.0f, -1.0f, 0.0f ) );
		}

		mesh->setFloatAttributes( 0, mesh->getVertexCount(), VertexElement::Specular1f, 0, 16.0f );
	}
	return mesh;
}

}