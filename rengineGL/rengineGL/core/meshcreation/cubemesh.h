#pragma once

#include <memory>
#include <string>
#include "../mesh.h"
#include "../meshmanager.h"
#include "../rendercore.h"

// Forward declarations.
class Size3;
namespace MeshCreation { class UVTextureRange; }

namespace MeshCreation
{

	/// <summary>
	/// Creates a cube mesh. The cuboid will consist of either 8 vertices and 8 triangle faces or of
	/// 24 vertices and 12 triangle faces. This depends on the 'facesShareVertices' parameter.
	/// The cuboid will be placed along the X, Y and Z axels.
	/// <para>
	/// Please not that this function will not set texture coordinate even in that case when
	/// 'vertexLayout' has vertex attributes available. 
	/// </para>
	/// <para>
	/// The created mesh will be added to the mesh manager of given render engine's core object.
	/// </para>
	/// </summary>
	/// <param name="core">Reference to main render engine object.</param>
	/// <param name="meshId">ID of the cuboid mesh.</param>
	/// <param name="vertexLayout>The vertex attributes to create for the mesh vertices.</param>
	/// <param name="sizeInfo">The extents of the cuboid along different axels.</param>
	/// <param name="facesShareVertices">
	/// Make the adjacent faces to share vertices. If this is set to true, the cuboid creation will fail
	/// if vertex layout has normal attribute set. This is because the vertex normals will be ambiquous
	/// in that case.
	/// </param>
	/// <returns>Returns the cuboid mesh.</returns>
	std::shared_ptr<Mesh> createCuboidNotTextured(
		RenderCore &core,
		const std::string &meshId,
		const VertexLayout &vertexLayout,
		const Size3 &sizeInfo,
		const bool facesShareVertices
	);

	/// <summary>
	/// Calculates the texture coordinates for each of the sides of the cuboid. This function expects the
	/// the mesh was created with MeshCreation::createCuboidNotTextured() function with 'facesShareVertices' parameter
	/// set to false.
	/// </summary>
	/// <param name="cuboidMesh">The cuboid mesh.</param>
	/// <param name="textureCoordIndex">The texture index, in case the mesh has multiple texture coordinates.</param>
	/// <param name="uvCoordsFront">The front face texture coordinate range.</param>
	/// <param name="uvCoordsBack">The back face texture coordinate range.</param>
	/// <param name="uvCoordsTop">The top face texture coordinate range.</param>
	/// <param name="uvCoordsBottom">The back face texture coordinate range.</param>
	/// <param name="uvCoordsLeft">The back face texture coordinate range.</param>
	/// <param name="uvCoordsRight">The back face texture coordinate range.</param>
	void wrapCuboidTexture(
		std::shared_ptr<Mesh> cuboidMesh,
		size_t textureCoordIndex,
		const UVTextureRange& uvCoordsFront,
		const UVTextureRange& uvCoordsBack,
		const UVTextureRange& uvCoordsTop,
		const UVTextureRange& uvCoordsBottom,
		const UVTextureRange& uvCoordsLeft,
		const UVTextureRange& uvCoordsRight
	);
};
