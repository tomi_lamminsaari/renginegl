
#include "cubemesh.h"
#include "uvtexturerange.h"
#include "../../math/aabb.h"
#include "../../math/quaternion.h"
#include "../../math/size3.h"

std::shared_ptr<Mesh> MeshCreation::createCuboidNotTextured(
	RenderCore& core,
	const std::string& meshId,
	const VertexLayout& vertexLayout,
	const Size3& sizeInfo,
	const bool facesShareVertices
)
{
	// Check the input parameters.
	if( facesShareVertices && vertexLayout.hasElement( VertexElement::Normal3f ) )
	{
		// Cannot have shared vertices and vertex normals.
		throw std::logic_error( "Cannot create cuboid with shared vertices and normal vectors." );
	}

	// Create the mesh object.
	std::shared_ptr<Mesh> mesh = core.getMeshManager().createMesh( meshId );
	
	// Calculate the coordinate values.
	const float KHalfX = sizeInfo.mSizeX / 2.0f;
	const float KHalfY = sizeInfo.mSizeY / 2.0f;
	const float KHalfZ = sizeInfo.mSizeZ / 2.0f;

	// Initialize either the shared vertices or individual vertices.
	if( facesShareVertices )
	{
		// Make cuboid with shared vertices.

		// Initialize the mesh vertices.
		mesh->initialize( vertexLayout, 8, 12 );
		mesh->setPosition( 0, Vec3( -KHalfX, -KHalfY, -KHalfZ ) );
		mesh->setPosition( 1, Vec3( KHalfX, -KHalfY, -KHalfZ ) );
		mesh->setPosition( 2, Vec3( KHalfX, KHalfY, -KHalfZ ) );
		mesh->setPosition( 3, Vec3( -KHalfX, KHalfY, -KHalfZ ) );

		mesh->setPosition( 4, Vec3( KHalfX, -KHalfY, KHalfZ ) );
		mesh->setPosition( 5, Vec3( -KHalfX, -KHalfY, KHalfZ ) );
		mesh->setPosition( 6, Vec3( -KHalfX, KHalfY, KHalfZ ) );
		mesh->setPosition( 7, Vec3( KHalfX, KHalfY, KHalfZ ) );

		// Initialize faces.
		mesh->setFace( 0, 0, 1, 3 );  // front 1
		mesh->setFace( 1, 3, 1, 2 );  // front 2
		mesh->setFace( 2, 5, 0, 6 );  // left 1
		mesh->setFace( 3, 6, 0, 3 );  // left 2
		mesh->setFace( 4, 1, 4, 2 );  // right 1
		mesh->setFace( 5, 2, 4, 7 );  // right 2
		mesh->setFace( 6, 3, 2, 6 );  // top 1
		mesh->setFace( 7, 6, 2, 7 );  // top 2
		mesh->setFace( 8, 5, 4, 0 );  // bottom 1
		mesh->setFace( 9, 0, 4, 1 );  // bottom 2
		mesh->setFace( 10, 4, 5, 7 );  // back 1
		mesh->setFace( 11, 7, 5, 6 );  // back 2
	}
	else
	{
		// Add metadata that that tells that the mesh has been created with MeshCreation utility
		// functions. This helps helps us later because we can do special tricks with meshes that
		// we know have certain structure.
		mesh->accessMeshInfo().addStringInfo( MeshInfo::KStringMeshShape, "meshcreation::cuboid" );

		// Make cube with 24 vertices. Each face has its own vertices.
		Aabb cuboid( sizeInfo );
		std::vector< Vec3 > corners = cuboid.getCorners();

		// Initialize the mesh vertices.
		mesh->initialize( vertexLayout, 24, 12 );

		// Front vertices.
		mesh->setPosition( 0, corners[ Aabb::KCornerIndex_NearLeftBottom ] );
		mesh->setPosition( 1, corners[ Aabb::KCornerIndex_NearRightBottom ] );
		mesh->setPosition( 2, corners[ Aabb::KCornerIndex_NearRightTop ] );
		mesh->setPosition( 3, corners[ Aabb::KCornerIndex_NearLeftTop ] );
		mesh->setFace( 0, 0, 1, 2 );
		mesh->setFace( 1, 0, 2, 3 );

		// Right vertices.
		mesh->setPosition( 4, corners[ Aabb::KCornerIndex_NearRightBottom ] );
		mesh->setPosition( 5, corners[ Aabb::KCornerIndex_FarRightBottom ] );
		mesh->setPosition( 6, corners[ Aabb::KCornerIndex_FarRightTop ] );
		mesh->setPosition( 7, corners[ Aabb::KCornerIndex_NearRightTop ] );
		mesh->setFace( 2, 4, 5, 6 );
		mesh->setFace( 3, 4, 6, 7 );

		// Back vertices.
		mesh->setPosition( 8, corners[ Aabb::KCornerIndex_FarRightBottom ] );
		mesh->setPosition( 9, corners[ Aabb::KCornerIndex_FarLeftBottom ] );
		mesh->setPosition( 10, corners[ Aabb::KCornerIndex_FarLeftTop ] );
		mesh->setPosition( 11, corners[ Aabb::KCornerIndex_FarRightTop ] );
		mesh->setFace( 4, 8, 9, 10 );
		mesh->setFace( 5, 8, 10, 11 );

		// Left vertices.
		mesh->setPosition( 12, corners[ Aabb::KCornerIndex_FarLeftBottom ] );
		mesh->setPosition( 13, corners[ Aabb::KCornerIndex_NearLeftBottom ] );
		mesh->setPosition( 14, corners[ Aabb::KCornerIndex_NearLeftTop ] );
		mesh->setPosition( 15, corners[ Aabb::KCornerIndex_FarLeftTop ] );
		mesh->setFace( 6, 12, 13, 14 );
		mesh->setFace( 7, 12, 14, 15 );

		// Bottom vertices.
		mesh->setPosition( 16, corners[ Aabb::KCornerIndex_NearLeftTop ] );
		mesh->setPosition( 17, corners[ Aabb::KCornerIndex_NearRightTop ] );
		mesh->setPosition( 18, corners[ Aabb::KCornerIndex_FarRightTop ] );
		mesh->setPosition( 19, corners[ Aabb::KCornerIndex_FarLeftTop ] );
		mesh->setFace( 8, 16, 17, 18 );
		mesh->setFace( 9, 16, 18, 19 );

		// Top vertices.
		mesh->setPosition( 20, corners[ Aabb::KCornerIndex_NearRightBottom ] );
		mesh->setPosition( 21, corners[ Aabb::KCornerIndex_NearLeftBottom ] );
		mesh->setPosition( 22, corners[ Aabb::KCornerIndex_FarLeftBottom ] );
		mesh->setPosition( 23, corners[ Aabb::KCornerIndex_FarRightBottom ] );
		mesh->setFace( 10, 20, 21, 22 );
		mesh->setFace( 11, 20, 22, 23 );

		// Calculate face normals and tangents.
		for( size_t index = 0; index < mesh->getFaceCount(); ++index )
		{
			// Get the vertex coordinates of index'th face.
			size_t vi1 = 0;
			size_t vi2 = 0;
			size_t vi3 = 0;
			mesh->getFace( index, vi1, vi2, vi3 );
			Vec3 v1 = mesh->getPosition( vi1 );
			Vec3 v2 = mesh->getPosition( vi2 );
			Vec3 v3 = mesh->getPosition( vi3 );

			// Calculate normal vector if vertex has attribute for that.
			if( vertexLayout.hasElement( VertexElement::Normal3f ) )
			{
				Vec3 normalVec = Vec3::crossProduct( ( v1 - v2 ), ( v3 - v2 ) ).normalized();
				mesh->setNormal( vi1, normalVec );
				mesh->setNormal( vi2, normalVec );
				mesh->setNormal( vi3, normalVec );
			}

			// Calculate tangent vector if vertex has attribute for that.
			if( vertexLayout.hasElement( VertexElement::Tangent3f ) )
			{
				Vec3 tangVec = ( v1 - v2 ).normalized();
				mesh->setVec3Attribute( vi1, VertexElement::Tangent3f, 0, tangVec );
				mesh->setVec3Attribute( vi2, VertexElement::Tangent3f, 0, tangVec );
				mesh->setVec3Attribute( vi3, VertexElement::Tangent3f, 0, tangVec );
			}

			// Calculate bitangent vector if vertex has attribute for that.
			if( vertexLayout.hasElement( VertexElement::Bitangent3f ) )
			{
				Vec3 bitangVec = ( v3 - v2 ).normalized();
				mesh->setVec3Attribute( vi1, VertexElement::Bitangent3f, 0, bitangVec );
				mesh->setVec3Attribute( vi2, VertexElement::Bitangent3f, 0, bitangVec );
				mesh->setVec3Attribute( vi3, VertexElement::Bitangent3f, 0, bitangVec );
			}
		}
	}

	// Return the created mesh.
	return mesh;
}

void MeshCreation::wrapCuboidTexture(
	std::shared_ptr<Mesh> cuboidMesh,
	size_t textureCoordIndex,
	const UVTextureRange& uvCoordsFront,
	const UVTextureRange& uvCoordsBack,
	const UVTextureRange& uvCoordsTop,
	const UVTextureRange& uvCoordsBottom,
	const UVTextureRange& uvCoordsLeft,
	const UVTextureRange& uvCoordsRight
)
{
	// Verify the mesh.
	std::string meshShape;
	if( cuboidMesh->accessMeshInfo().getStringInfo( MeshInfo::KStringMeshShape, meshShape ) == false ||
		meshShape != "meshcreation::cuboid" )
	{
		// Mesh is not created with createCuboidNotTextured() function.
		throw RengineError( "Cannot wrap textures to cuboid because it has not been created with createCuboidNotTextured() functio." );
	}
	if( cuboidMesh->getVertexLayout().hasElement( VertexElement::Texture2f ) == false )
		throw RengineError( "Cannot wrap textures to cuboid because it does not have texture coordiantes." );

	// Texture the front face.
	int ti = static_cast< int >( textureCoordIndex );
	cuboidMesh->setTexture2D( 0, ti, Vec2( uvCoordsFront.getMinU(), uvCoordsFront.getMinV() ) );
	cuboidMesh->setTexture2D( 1, ti, Vec2( uvCoordsFront.getMaxU(), uvCoordsFront.getMinV() ) );
	cuboidMesh->setTexture2D( 2, ti, Vec2( uvCoordsFront.getMaxU(), uvCoordsFront.getMaxV() ) );
	cuboidMesh->setTexture2D( 3, ti, Vec2( uvCoordsFront.getMinU(), uvCoordsFront.getMaxV() ) );

	// Texture the back face.
	cuboidMesh->setTexture2D( 8, ti, Vec2( uvCoordsBack.getMinU(), uvCoordsBack.getMinV() ) );
	cuboidMesh->setTexture2D( 9, ti, Vec2( uvCoordsBack.getMaxU(), uvCoordsBack.getMinV() ) );
	cuboidMesh->setTexture2D( 10, ti, Vec2( uvCoordsBack.getMaxU(), uvCoordsBack.getMaxV() ) );
	cuboidMesh->setTexture2D( 11, ti, Vec2( uvCoordsBack.getMinU(), uvCoordsBack.getMaxV() ) );
	
	// Texture the right face.
	cuboidMesh->setTexture2D( 4, ti, Vec2( uvCoordsRight.getMinU(), uvCoordsRight.getMinV() ) );
	cuboidMesh->setTexture2D( 5, ti, Vec2( uvCoordsRight.getMaxU(), uvCoordsRight.getMinV() ) );
	cuboidMesh->setTexture2D( 6, ti, Vec2( uvCoordsRight.getMaxU(), uvCoordsRight.getMaxV() ) );
	cuboidMesh->setTexture2D( 7, ti, Vec2( uvCoordsRight.getMinU(), uvCoordsRight.getMaxV() ) );

	// Texture the left face.
	cuboidMesh->setTexture2D( 12, ti, Vec2( uvCoordsLeft.getMinU(), uvCoordsLeft.getMinV() ) );
	cuboidMesh->setTexture2D( 13, ti, Vec2( uvCoordsLeft.getMaxU(), uvCoordsLeft.getMinV() ) );
	cuboidMesh->setTexture2D( 14, ti, Vec2( uvCoordsLeft.getMaxU(), uvCoordsLeft.getMaxV() ) );
	cuboidMesh->setTexture2D( 15, ti, Vec2( uvCoordsLeft.getMinU(), uvCoordsLeft.getMaxV() ) );

	// Texture the top face.
	cuboidMesh->setTexture2D( 20, ti, Vec2( uvCoordsTop.getMinU(), uvCoordsTop.getMinV() ) );
	cuboidMesh->setTexture2D( 21, ti, Vec2( uvCoordsTop.getMaxU(), uvCoordsTop.getMinV() ) );
	cuboidMesh->setTexture2D( 22, ti, Vec2( uvCoordsTop.getMaxU(), uvCoordsTop.getMaxV() ) );
	cuboidMesh->setTexture2D( 23, ti, Vec2( uvCoordsTop.getMinU(), uvCoordsTop.getMaxV() ) );

	// Texture the bottom face.
	cuboidMesh->setTexture2D( 16, ti, Vec2( uvCoordsBottom.getMinU(), uvCoordsBottom.getMinV() ) );
	cuboidMesh->setTexture2D( 17, ti, Vec2( uvCoordsBottom.getMaxU(), uvCoordsBottom.getMinV() ) );
	cuboidMesh->setTexture2D( 18, ti, Vec2( uvCoordsBottom.getMaxU(), uvCoordsBottom.getMaxV() ) );
	cuboidMesh->setTexture2D( 19, ti, Vec2( uvCoordsBottom.getMinU(), uvCoordsBottom.getMaxV() ) );
}
