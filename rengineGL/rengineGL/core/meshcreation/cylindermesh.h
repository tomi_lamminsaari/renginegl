#pragma once

#include <memory>
#include <string>
#include "../mesh.h"

// Forward declaration.
class RenderCore;
class VertexLayout;

namespace MeshCreation
{
	/// <summary>
	/// Creates new cylinder mesh. The cylinder will consist of given number of vertices in its circumfenrece.
	/// Radius of either end of the cylinder can be provided. The cylinder coordinates will be positioned
	/// around the origin along the Y-axis.
	/// <para>
	/// The vertex attributes that will be set by this function are:
	/// - vertex position
	/// - vertex normal
	/// - texture coordinate from 0.0 to 1.0 if vertexLayout contains texture parameter.
	/// - normal map coordinates get disabled.
	/// - tangent and bitangent vectors are cleared.
	/// </para>
	/// </summary>
	/// <param name="core">The render engine's core object.</param>
	/// <param name="meshId">ID of the new mesh.</param>
	/// <param name="vertexLayout">The vertex attributes the mesh vertices have.</param>
	/// <param name="radius1">Radius of the first end of the cylinder.</param>
	/// <param name="radius2">Radius of the second end of the cylinder.</param>
	/// <param name="cylinderLength">Length of the cylinder. Distance between the ends of the cylider.</param>
	/// <param name="numOfVerticesInCircumference">Number of vertices there are on the cylinder circumference. Must be greater than 5.</param>
	/// <returns>Pointer to created new mesh.</returns>
	std::shared_ptr<Mesh> createCylinder(
		RenderCore& core,
		const std::string& meshId,
		const VertexLayout& vertexLayout,
		float radius1,
		float radius2,
		float cylinderLength,
		int numOfVerticesInCircumference );
}
