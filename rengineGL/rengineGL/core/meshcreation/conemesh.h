
#pragma once

#include <memory>
#include <string>
#include "../mesh.h"

// Forward declarations.
class RenderCore;
class VertexLayout;

namespace MeshCreation
{

	/// <summary>
	/// Creates new cone mesh. The cone will const of given number of vertices in its
	/// bottom circumference. The cylider coodinates will be positioned around of origin and is along
	/// the Y-axis.
	/// </summary>
	/// <param name="core">Reference to render engine's main object.</param>
	/// <param name="meshId">ID of the mesh.</param>
	/// <param name="vertexLayout">The vertex attributes and the mesh's vertices.</param>
	/// <param name="coneBottomRadius">Radius of the cone in its lowest parts.</param>
	/// <param name="coneHeight">Height of the new cone.</param>
	/// <param name="numOfVerticesInCircumference">Number of vertices in cone circumference.</param>
	/// <param name="convex">Pass true to make the cone's bottom be solid. This makes the object entirely convex.</param>
	/// <returns>New instance of the cone mesh.</returns>
	/// </summary>
	std::shared_ptr<Mesh> createSolidColorCone(
		RenderCore& core,
		const std::string& meshId,
		const VertexLayout& vertexLayout,
		float coneBottomRadius,
		float coneHeight,
		int numOfVerticesInCircumference,
		bool convex );

}
