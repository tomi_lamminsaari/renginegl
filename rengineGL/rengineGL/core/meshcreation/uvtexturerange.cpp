
#include "uvtexturerange.h"

namespace MeshCreation
{

	UVTextureRange UVTextureRange::repeatTexture( float repeatU, float repeatV )
	{
		return UVTextureRange( 0.0f, repeatU, 0.0f, repeatV );
	}

	UVTextureRange UVTextureRange::repeatTexturePerUnit(
		float repeatU,
		float repeatV,
		float unitDistanceU,
		float unitDistanceV,
		float distanceU,
		float distanceV
	)
	{
		float bigU = ( repeatU / unitDistanceU ) * distanceU;
		float bigV = ( repeatV / unitDistanceV ) * distanceV;
		return UVTextureRange( 0.0f, bigU, 0.0f, bigV );
	}

	UVTextureRange UVTextureRange::mapPixelsToUV(
		float pixelPositionX1,
		float pixelPositionX2,
		float pixelPositionY1,
		float pixelPositionY2,
		float textureWidthPixels,
		float textureHeightPixels
	)
	{
		float minU = pixelPositionX1 / textureWidthPixels;
		float maxU = pixelPositionX2 / textureWidthPixels;
		float minV = pixelPositionY1 / textureHeightPixels;
		float maxV = pixelPositionY2 / textureHeightPixels;
		return UVTextureRange( minU, maxU, minV, maxV );
	}

	UVTextureRange UVTextureRange::empty()
	{
		return UVTextureRange();
	}

	float UVTextureRange::getMinU() const
	{
		return mMinU;
	}

	float UVTextureRange::getMaxU() const
	{
		return mMaxU;
	}

	float UVTextureRange::getMinV() const
	{
		return mMinV;
	}

	float UVTextureRange::getMaxV() const
	{
		return mMaxV;
	}



	UVTextureRange::UVTextureRange() :
		mMinU( 0.0f ),
		mMaxU( 1.0f ),
		mMinV( 0.0f ),
		mMaxV( 1.0f )
	{

	}

	UVTextureRange::UVTextureRange( float minU, float maxU, float minV, float maxV ) :
		mMinU( minU ),
		mMaxU( maxU ),
		mMinV( minV ),
		mMaxV( maxV )
	{

	}

}
