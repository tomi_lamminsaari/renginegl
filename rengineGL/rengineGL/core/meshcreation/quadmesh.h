
#pragma once

#include <memory>
#include <string>
#include <vector>
#include "../defines.h"
#include "uvtexturerange.h"

// Forward declarations.
class Mesh;
class RenderCore;
class VertexLayout;

namespace MeshCreation
{
	/// <summary>
	/// Create a quad mesh. The quad coordinates will be placed around the origo.
	/// <summary>
	/// <param name="core">The render engine's core object.</param>
	/// <param name="meshId">ID of the mesh to create.</param>
	/// <param name="vertexLayout">The vertex attribute layout.</param>
	/// <param name="faceSides">Defines if quad is single or double sided.</param>
	/// <param name="quadWidth">Width of the quad.</param>
	/// <param name="quadHeight">Height of the quad.</param>
	/// <param name="textureRange">
	/// The texture range in case the vertexLayout has texture attribute. This texture coordinate will be set to the
	/// first texture attribute there is.
	/// </param>
	/// <returns>Created mesh. Mesh will be added to the mesh manager as well.</returns>
	std::shared_ptr<Mesh> createQuad(
		RenderCore &core,
		const std::string &meshId,
		const VertexLayout &vertexLayout,
		const FaceSides faceSides,
		const float quadWidth,
		const float quadHeight,
		const UVTextureRange &textureRange
	);

	/// <summary>
	/// Create a quad mesh. The quad coordinates will be placed around the origo. This method can set multiple
	/// texture attribute coordinates.
	/// </summary>
	/// <param name="core">The render engine's core object.</param>
	/// <param name="meshId">ID of the mesh to create.</param>
	/// <param name="vertexLayout">The vertex attribute layout.</param>
	/// <param name="faceSides">Defines if quad is single or double sided.</param>
	/// <param name="quadWidth">Width of the quad.</param>
	/// <param name="quadHeight">Height of the quad.</param>
	/// <param name="textureRanges">
	/// A vector of texture coordinates. This way you can set multiple texture attributes to the mesh.
	/// </param>
	/// <returns>Created mesh. Mesh will be added to the mesh manager as well.</returns>
	std::shared_ptr<Mesh> createQuad(
		RenderCore &core,
		const std::string &meshId,
		const VertexLayout &vertexLayout,
		const FaceSides faceSides,
		const float quadWidth,
		const float quadHeight,
		const std::vector<UVTextureRange> &textureRanges
	);
};