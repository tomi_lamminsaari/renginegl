
#pragma once

namespace MeshCreation
{

	/// <summary>
	/// A class that represents a texture coordinate range.
	/// </summary>
	class UVTextureRange
	{
	public:

		/// <summary>
		/// Create texture range that when applied to a mesh will repeat the
		/// texture given number of times in U direction and V direction. When applied
		/// to a quad, it means that texture coordinate will stretch out from 0.0 to repeatU or
		/// repeatV value.
		/// </summary>
		/// <param name="repeatU">Number of times the texture should be repeated on U direction.</param>
		/// <param name="repeatV">Number of times the texture should be repeated on V direction.</param>
		/// <returns>Returns the texture range object.</returns>
		static UVTextureRange repeatTexture( float repeatU, float repeatV );

		/// <summary>
		/// Create texture range that repeats the texture number of time per unit distance. Then
		/// you must give the full distances in U direction and V direction. This function construct
		/// a texture range that repeats the texture number of time per distance unit and as it
		/// will be repeated to the width and height of the mesh.
		/// <para>
		/// The texture range will start from UV coordinates (0,0) and run all the way to a
		/// value that is (repeatU / unitDistanceU) * distanceU and (repeatV / unitDistanceV) * distanceV.
		/// </para>
		/// </summary>
		/// <param name="repeatU">Number of times the texture should be repeated in U direction.</param>
		/// <param name="repeatV">Number of times the texture should be repeated in V direction.</param>
		/// <param name="unitDistanceU">The number of times per this distance the texture will be repeated.</param>
		/// <param name="unitDistanceV">The number of times per this distance the texture will be repeated.</param>
		/// <param name="distanceU">The entire extent in U direction. Basically the width of the mesh.</param>
		/// <param name="distanceV">The entire extent in V direction. Basically the height of the mesh.</param>
		/// <returns>The texture range object.</returns>
		static UVTextureRange repeatTexturePerUnit(
			float repeatU,
			float repeatV,
			float unitDistanceU,
			float unitDistanceV,
			float distanceU,
			float distanceV
		);

		/// <summary>
		/// Creates a texture coordinate range for a rectangular texture area. The given
		/// area coordinates are the pixel coordinates and this maps those values
		/// to texture coordinate range 0.0-1.0.
		/// </summary>
		/// <param name="pixelPositionX1">First X-coordinate in pixel values. This is mapped to getMinU().</param>
		/// <param name="pixelPositionX2">Second X-coordinate in pixel values. This is mapped to getMaxU().</param>
		/// <param name="pixelPositionY1">First Y-coordinate in pixel values. This is mapped to getMinV().</param>
		/// <param name="pixelPositionY2">Second Y-coordinate in pixel values. This is mapped to getMaxV().</param>
		/// <param name="textureWidthPixels">The width of the texture in pixels.</param>
		/// <param name="textureHeightPixels">The height of the texture in pixels.</param>
		/// <returns>The texture range object.</returns>
		static UVTextureRange mapPixelsToUV(
			float pixelPositionX1,
			float pixelPositionX2,
			float pixelPositionY1,
			float pixelPositionY2,
			float textureWidthPixels,
			float textureHeightPixels
		);

		/// <summary>
		/// Creates an empty texture range. Basically from 0.0 to 0.0.
		/// </summary>
		/// <returns>An empty texture range.</returns>
		static UVTextureRange empty();

		/// <summary>
		/// Returns the minimum U coordinate.
		/// </summary>
		/// <returns>Minimum U coordinate.</returns>
		float getMinU() const;

		/// <summary>
		/// Returns the maximum U coordinate.
		/// </summary>
		/// <returns>Maximum U coordinate.</returns>
		float getMaxU() const;

		/// <summary>
		/// Returns the minimum V coordinate.
		/// </summary>
		/// <returns>Minimum V coordinate.</returns>
		float getMinV() const;

		/// <summary>
		/// Returns the maximum V coordinate.
		/// </summary>
		/// <returns>Maximum V coordinate.</returns>
		float getMaxV() const;

	private:

		/// <summary>
		/// Private constructor. We want to construct objects through the static creation
		/// functions.
		/// </summary>
		UVTextureRange();

		/// <summary>
		/// Private constructor that initializes the texture coordinate values. When stretched over a quad,
		/// the texture coordinates will start from (minU, minV) to (maxU and maxV).
		/// </summary>
		/// <param name="minU">The minimum U coordinate.</param>
		/// <param name="maxU">The maximum U coordinate.</param>
		/// <param name="minV">The minimum V coordinate.</param>
		/// <param name="maxV">The maximum V coordinate.</param>
		UVTextureRange( float minU, float maxU, float minV, float maxV );

	private:
		/// <summary>The minimum U coordinate.</summary>
		float mMinU;

		/// <summary>The minimum V coordinate.</summary>
		float mMinV;

		/// <summary>The maximum U coordinate.</summary>
		float mMaxU;

		/// <summary>The maximum V coordinate.</summary>
		float mMaxV;
	};

};