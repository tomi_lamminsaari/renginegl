
#include <GL/glew.h>
#include "material.h"
#include "mesh.h"
#include "overlayrenderable.h"
#include "renderpassinfo.h"
#include "shaderprogram.h"

OverlayRenderable::OverlayRenderable( RenderCore& core ) :
	mCore( core ),
	mHidden( false ),
	mOffsetPosition( 0.0f, 0.0f, 0.0f )
{

}

OverlayRenderable::~OverlayRenderable()
{

}

void OverlayRenderable::setGeometryObjects( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material )
{
	mMesh = mesh;
	mMaterial = material;
}

void OverlayRenderable::setHidden( bool hidden )
{
	mHidden = hidden;
}

bool OverlayRenderable::isHidden() const
{
	return mHidden;
}

void OverlayRenderable::setOffsetPosition( const Vec3& offsetPosition )
{
	mOffsetPosition = offsetPosition;
}

const Vec3& OverlayRenderable::getOffsetPosition() const
{
	return mOffsetPosition;
}

void OverlayRenderable::render( SceneGlobalRenderPassInfo* globalRenderPassInfo )
{
	// Activate the material.
	if( ! mMaterial || ! mMesh )
		return;
	mMaterial->apply();

	// Set the offset of the overlay mesh.
	mMaterial->getShaderProgram()->setUniformVec3( ShaderProgram::KUniformName_OffsetPosition, mOffsetPosition );

	// Render the mesh.
	RenderPassInfo renderPassInfo( globalRenderPassInfo );
	mMesh->getRenderPassInfo( renderPassInfo );
	GLASSERT( glBindVertexArray( renderPassInfo.mVAO ) );
	GLASSERT( glDrawElements( renderPassInfo.mDrawMode, renderPassInfo.mDrawCount, renderPassInfo.mElementType, 0 ) );

	// Unbind buffer.s
	glBindVertexArray( 0 );
	glBindBuffer( GL_ARRAY_BUFFER, 0 );
	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
}
