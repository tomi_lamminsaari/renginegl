
#include <limits>
#include <set>
#include "../math/matrix3x3.h"
#include "../math/quaternion.h"
#include "defines.h"
#include "material.h"
#include "mesh.h"
#include "renderpassinfo.h"
#include "rengineexception.h"

constexpr float KDisabledTextureIndexValue = -100.0f;

/// <summary>
/// Returns the OpenGL enumeration for vertex order.
/// </summary>
inline GLenum FaceWinding2VertexOrder( Mesh::FaceWinding winding )
{
	return winding == Mesh::FaceWinding::EClockwise ? GL_CW : GL_CCW;
}

std::unique_ptr<Mesh> Mesh::createMesh( const VertexLayout &vertexLayout, size_t vertexCount, size_t faceCount )
{
	// Create the mesh 
	std::unique_ptr<Mesh> mesh( new Mesh );
	mesh->initialize( vertexLayout, vertexCount, faceCount );
	return mesh;
}

Mesh::Mesh() :
	mVertexLayout(),
	mVertexCount( 0 ),
	mFaceCount( 0 ),
	mVertexDataChanged( true ),
	mIndexDataChanged( true ),
	mVAO( 0 ),
	mFaceWinding( FaceWinding::EClockwise ),
	mMaxY( std::numeric_limits<float>::min() ),
	mMinY( std::numeric_limits<float>::max() ),
	mBufferUsage( BufferUsage::StaticMesh )
{

}

Mesh::~Mesh()
{
	removeFromGPU();
}

void Mesh::setId( const std::string& meshId )
{
	mId = meshId;
}

const std::string& Mesh::getId() const
{
	return mId;
}

bool Mesh::vertexLayoutEquals( const VertexLayout& vertexLayout ) const
{
	return ( vertexLayout == mVertexLayout );
}

void Mesh::initialize( const VertexLayout& vertexLayout, size_t vertexCount, size_t faceCount )
{
	// Copy the vertex layout data.
	mVertexLayout = vertexLayout;

	// Allocate RAM buffer for vertex data.
	mVertexCount = vertexCount;
	if( vertexCount > 0 )
	{
		size_t vertexFloatCount = mVertexLayout.getFloatCount() * vertexCount;
		mVertexData.resize( vertexFloatCount, 0.0f );
	}

	// Allocate RAM buffer for index data.
	mFaceCount = faceCount;
	if( faceCount > 0 )
	{
		size_t indexNumberCount = faceCount * 3;
		mIndexData.resize( indexNumberCount, 0 );
	}

}

void Mesh::initialize( const VertexLayout &vertexLayout )
{
	mVertexLayout = vertexLayout;
	mVertexData.clear();
	mIndexData.clear();
	mVertexCount = 0;
	mFaceCount = 0;
}

void Mesh::setBufferUsage( BufferUsage usage )
{
	mBufferUsage = usage;
}

void Mesh::setVertexOrder( FaceWinding order )
{
	mFaceWinding = order;
}

Mesh::FaceWinding Mesh::getVertexOrder() const
{
	return mFaceWinding;
}

void Mesh::setPosition( size_t vertexIndex, const Vec3 &pos )
{
	size_t elementIndex = mVertexLayout.getFloatIndex( VertexElement::Position3f );

	// Mesh should always have at least position data.
	assert( elementIndex >= 0 );

	// Change the position data.
	size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
	baseIndex += elementIndex;
	mVertexData[ baseIndex + 0 ] = pos.mX;
	mVertexData[ baseIndex + 1 ] = pos.mY;
	mVertexData[ baseIndex + 2 ] = pos.mZ;

	mVertexDataChanged = true;
}

Vec3 Mesh::getPosition( size_t vertexIndex ) const
{
	size_t elementIndex = mVertexLayout.getFloatIndex( VertexElement::Position3f );
	assert( elementIndex >= 0 );

	size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
	baseIndex += elementIndex;
	Vec3 ret;
	ret.mX = mVertexData[ baseIndex + 0 ];
	ret.mY = mVertexData[ baseIndex + 1 ];
	ret.mZ = mVertexData[ baseIndex + 2 ];
	return ret;
}

void Mesh::setNormal( size_t vertexIndex, const Vec3 &vec )
{
	int elementIndex = mVertexLayout.getFloatIndex( VertexElement::Normal3f );
	if( elementIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( elementIndex );
		mVertexData[ baseIndex + 0 ] = vec.mX;
		mVertexData[ baseIndex + 1 ] = vec.mY;
		mVertexData[ baseIndex + 2 ] = vec.mZ;
		mVertexDataChanged = true;
	}
}

bool Mesh::getNormal( size_t vertexIndex, Vec3 &normalOut ) const
{
	bool hasNormals = false;
	size_t elementIndex = mVertexLayout.getFloatIndex( VertexElement::Normal3f );
	if( elementIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += elementIndex;
		normalOut.mX = mVertexData[ baseIndex + 0 ];
		normalOut.mY = mVertexData[ baseIndex + 1 ];
		normalOut.mZ = mVertexData[ baseIndex + 2 ];
		hasNormals = true;
	}
	return hasNormals;
}

void Mesh::setVec3Attribute( size_t vertexIndex, VertexElement element, int elementIndex, const Vec3 &vec )
{
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( vertexBuffIndex );
		mVertexData[ baseIndex + 0 ] = vec.mX;
		mVertexData[ baseIndex + 1 ] = vec.mY;
		mVertexData[ baseIndex + 2 ] = vec.mZ;
		mVertexDataChanged = true;
	}
}

bool Mesh::getVec3Attribute( size_t vertexIndex, VertexElement element, int elementIndex, Vec3 &outVec ) const
{
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( vertexBuffIndex );
		outVec.mX = mVertexData[ baseIndex + 0 ];
		outVec.mY = mVertexData[ baseIndex + 1 ];
		outVec.mZ = mVertexData[ baseIndex + 2 ];
		return true;
	}
	return false;
}

void Mesh::setVec3Attributes( size_t startIndex, size_t vertexCount, VertexElement element, int elementIndex, const Vec3& val )
{
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		// Start from vertex 'startIndex' and set vec3 to 'vertexCount' number of consequtive vertices.
		for( size_t i = 0; i < vertexCount; ++i )
		{
			size_t baseIndex = ( startIndex + i ) * mVertexLayout.getFloatCount();
			baseIndex += static_cast< size_t >( vertexBuffIndex );
			mVertexData[ baseIndex + 0 ] = val.mX;
			mVertexData[ baseIndex + 1 ] = val.mY;
			mVertexData[ baseIndex + 2 ] = val.mZ;
		}
		mVertexDataChanged = true;
	}
}

void Mesh::setFloatAttribute( size_t vertexIndex, VertexElement element, int elementIndex, float val )
{
	// Sanity check for the values.
	if( element == VertexElement::Specular1f )
	{
		if( val < 0.0f || val > 1.0f )
			throw std::invalid_argument( "Invalid specular value for vertex attribute. Value was: " + std::to_string( val ) );
	}

	// Set the value.
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( vertexBuffIndex );
		mVertexData[ baseIndex ] = val;
		mVertexDataChanged = true;
	}
}

bool Mesh::getFloatAttribute( size_t vertexIndex, VertexElement element, int elementIndex, float& outVal ) const
{
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( vertexBuffIndex );
		outVal = mVertexData[ baseIndex ];
		return true;
	}
	return false;
}

void Mesh::setFloatAttributes( size_t startIndex, size_t vertexCount, VertexElement element, int elementIndex, float val )
{
	// Sanity check for the values.
	if( element == VertexElement::Specular1f )
	{
		if( val < 0.0f || val > 1000.0f )
			throw std::invalid_argument( "Invalid specular value for vertex attribute. Must in range 0 - 1000. Value was: " + std::to_string( val ) );
	}

	// Set the float value.
	int vertexBuffIndex = mVertexLayout.getFloatIndex( element, elementIndex );
	if( vertexBuffIndex >= 0 )
	{
		// Start from index 'startIndex' and set the float value to 'vertexCount' consequtive vertices.
		for( size_t i = 0; i < vertexCount; ++i )
		{
			size_t baseIndex = ( startIndex + i ) * mVertexLayout.getFloatCount();
			baseIndex += static_cast< size_t >( vertexBuffIndex );
			mVertexData[ baseIndex ] = val;
		}
		mVertexDataChanged = true;
	}
}

void Mesh::setTexture2D( size_t vertexIndex, int textureIndex, const Vec2 &texCoord )
{
	int elementIndex = mVertexLayout.getFloatIndex( VertexElement::Texture2f, textureIndex );
	if( elementIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += static_cast< size_t >( elementIndex );
		mVertexData[ baseIndex + 0 ] = texCoord.mX;
		mVertexData[ baseIndex + 1 ] = texCoord.mY;
		mVertexDataChanged = true;
	}
}

void Mesh::setTextureCoordinatesToDisabled( int textureIndex )
{
	int elementIndex = mVertexLayout.getFloatIndex( VertexElement::Texture2f, textureIndex );
	if( elementIndex >= 0 ) {
		for( size_t i = 0; i < getVertexCount(); ++i )
		{
			size_t baseIndex = i * mVertexLayout.getFloatCount();
			baseIndex += static_cast< size_t >( elementIndex );
			mVertexData[ baseIndex + 0 ] = -1.0f;
			mVertexData[ baseIndex + 1 ] = -1.0f;
		}
		mVertexDataChanged = true;
	}
}

void Mesh::setColor( size_t vertexIndex, int colorIndex, const Vec4 &color )
{
	size_t elementIndex = mVertexLayout.getFloatIndex( VertexElement::Color4f, colorIndex );
	if( elementIndex >= 0 )
	{
		size_t baseIndex = vertexIndex * mVertexLayout.getFloatCount();
		baseIndex += elementIndex;
		mVertexData[ baseIndex + 0 ] = color.mX;
		mVertexData[ baseIndex + 1 ] = color.mY;
		mVertexData[ baseIndex + 2 ] = color.mZ;
		mVertexData[ baseIndex + 3 ] = color.mW;
		mVertexDataChanged = true;
	}
}

void Mesh::setAllVertexAttributes( size_t vertexIndex, const std::vector<float>& vertexAttributes )
{
	size_t floatCount = mVertexLayout.getFloatCount();
	if( floatCount != vertexAttributes.size() )
		throw std::invalid_argument( "Cannot set all vertex attributes because given vector contains wrong amount of attributes." );
	if( vertexIndex >= mVertexCount )
		throw std::invalid_argument( "Given vertex value is bigger than total number of vertices in the mesh." );

	// Get the index of the vertex inside the mesh. Then copy the vertex data from
	// input vector to 
	size_t baseIndex = vertexIndex * floatCount;
	size_t floatSize = sizeof( mVertexData[ 0 ] );
	float* meshBufferPtr = mVertexData.data() + baseIndex;;
	const float* sourceVector = vertexAttributes.data();
	memcpy( static_cast< void* >( meshBufferPtr ), static_cast< const void* >( sourceVector ), ( floatCount * floatSize ) );
}

void Mesh::setFace( size_t faceIndex, size_t vertexIndex1, size_t vertexIndex2, size_t vertexIndex3 )
{
	size_t baseIndex = faceIndex * 3;
	mIndexData[ baseIndex + 0 ] = static_cast< GLuint >( vertexIndex1 );
	mIndexData[ baseIndex + 1 ] = static_cast< GLuint >( vertexIndex2 );
	mIndexData[ baseIndex + 2 ] = static_cast< GLuint >( vertexIndex3 );

	mIndexDataChanged = true;
}

void Mesh::getFace( size_t faceIndex, size_t& outVIndex1, size_t& outVIndex2, size_t& outVIndex3 ) const
{
	size_t baseIndex = faceIndex * 3;
	outVIndex1 = mIndexData[ baseIndex + 0 ];
	outVIndex2 = mIndexData[ baseIndex + 1 ];
	outVIndex3 = mIndexData[ baseIndex + 2 ];
}

size_t Mesh::getFaceCount() const
{
	return mFaceCount;
}

size_t Mesh::getVertexCount() const
{
	return mVertexCount;
}

size_t Mesh::addVertex()
{
	//assert( mVertexCount < 32767 );

	// Add vertex by adding as many values to the vertex array as single vertex has floats.
	size_t floatCount = mVertexLayout.getFloatCount();
	for( size_t i = 0; i < floatCount; ++i )
		mVertexData.push_back( 0.0f );

	// Update the mesh's vertex count.
	size_t vertexIndex = mVertexCount;
	mVertexCount++;
	return vertexIndex;
}

size_t Mesh::addFace( size_t vertexIndex1, size_t vertexIndex2, size_t vertexIndex3 )
{
	// Add room for new face indices.
	mIndexData.push_back( static_cast< GLuint >( vertexIndex1 ) );
	mIndexData.push_back( static_cast< GLuint >( vertexIndex2 ) );
	mIndexData.push_back( static_cast< GLuint >( vertexIndex3 ) );

	// Update the mesh's face count.
	size_t faceIndex = mFaceCount;
	mFaceCount++;
	return faceIndex;
}

void Mesh::getRenderPassInfo( RenderPassInfo &renderPassInfo ) const
{
	// Set the Vertex Array Object if mesh has one.
	renderPassInfo.setVAO( GL_TRIANGLES, mVAO, static_cast< GLsizei >( mIndexData.size() ) );
	renderPassInfo.mFaceWindingOrder = mFaceWinding == FaceWinding::EClockwise ? GL_CW : GL_CCW;
}

void Mesh::moveVertices( const Vec3 &moveAmount )
{
	// Move every vertex position by give move amount.
	for( size_t i = 0; i < mVertexCount; ++i )
	{
		Vec3 vertexPos = getPosition( i );
		vertexPos += moveAmount;
		setPosition( i, vertexPos );
	}
}

void Mesh::rotateVertices( const Matrix3x3 &rotMat )
{
	// TODO: This should have generic implementation that would process all element
	// types.

	// Rotate every vertex by given rotation amount.
	for( size_t i = 0; i < mVertexCount; ++i )
	{
		Vec3 vertexPos = getPosition( i );
		vertexPos = rotMat * vertexPos;
		setPosition( i, vertexPos );
	}

	// Rotate normal vectors by given rotation amount.
	if( mVertexLayout.getFloatIndex( VertexElement::Normal3f ) != -1 )
	{
		Matrix3x3 invRot = rotMat.inversed();
		for( size_t i = 0; i < mVertexCount; ++i )
		{
			Vec3 normalVec;
			getNormal( i, normalVec );
			normalVec = invRot * normalVec;
			setNormal( i, normalVec );
		}
	}

	// Rotate tangents.
	if( mVertexLayout.getFloatIndex( VertexElement::Tangent3f ) != -1 )
	{
		for( size_t i = 0; i < mVertexCount; ++i )
		{
			Vec3 tangentVec;
			getVec3Attribute( i, VertexElement::Tangent3f, 0, tangentVec );
			tangentVec = rotMat * tangentVec;
			setVec3Attribute( i, VertexElement::Tangent3f, 0, tangentVec );
		}
	}

	// Rotate bitangents.
	if( mVertexLayout.getFloatIndex( VertexElement::Bitangent3f ) != -1 )
	{
		for( size_t i = 0; i < mVertexCount; ++i )
		{
			Vec3 bitangentVec;
			getVec3Attribute( i, VertexElement::Bitangent3f, 0, bitangentVec );
			bitangentVec = rotMat * bitangentVec;
			setVec3Attribute( i, VertexElement::Bitangent3f, 0, bitangentVec );
		}
	}
}

GLuint Mesh::getVAO() const
{
	return mVAO;
}

void Mesh::uploadToGPU()
{
	RengineError::check( "Error state before uploading a mesh" );
	// Create GPU memory buffers if necessary.
	if( mGL_Buffers.size() == 0 )
	{
		// Calculate the smallest and biggest Y-coordinates.
		mMinY = std::numeric_limits<float>::max();
		mMaxY = std::numeric_limits<float>::min();
		for( size_t i = 0; i < mVertexCount; ++i )
		{
			Vec3 v = getPosition( i );
			if( v.mY < mMinY )
				mMinY = v.mY;
			if( v.mY > mMaxY )
				mMaxY = v.mY;
		}

		// Create vertex and index buffers.
		GLuint vbo = 0;
		GLuint ibo = 0;
		mGL_Buffers.resize( 2, 0 );
		glGenBuffers( 1, &vbo );
		glGenBuffers( 1, &ibo );
		mGL_Buffers[ BufferIndex::VBO ] = vbo;
		mGL_Buffers[ BufferIndex::IBO ] = ibo;

		// Define the buffer usage.
		GLenum bufferUsage = GL_STATIC_DRAW;

		// Create Vertex Array Object (VAO) buffer.Activate the VAO buffer so that following Vertex Buffer Object and
		// Index Buffer Object get associated with to the VAO.
		glGenVertexArrays( 1, &mVAO );
//		RengineError::clearError();
		RengineError::check( "Failed to create VAO for a mesh." );
		GLASSERT( glBindVertexArray( mVAO ) );

		// Bind the Vertex Buffer Object (VBO) and upoload initial data to it.
		size_t bufferSizeBytes = mVertexCount * mVertexLayout.getSizeBytes();
		GLASSERT( glBindBuffer( GL_ARRAY_BUFFER, mGL_Buffers[ BufferIndex::VBO ] ) );
		glBufferData( GL_ARRAY_BUFFER, bufferSizeBytes, mVertexData.data(), bufferUsage );
		RengineError::check( "Failed to upload mesh data to vertex buffer." );

		// Bind element array and upload index data to it.
		GLASSERT( glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mGL_Buffers[ BufferIndex::IBO ] ) );
		glBufferData( GL_ELEMENT_ARRAY_BUFFER, mFaceCount * 3 * sizeof( GLuint ), mIndexData.data(), GL_STATIC_DRAW );
		RengineError::check( "Failed to upload index data to buffer object." );

		// Set vertex attribute pointers.
		GLsizei stride = static_cast< GLsizei >( mVertexLayout.getSizeBytes() );
		GLboolean normalized = GL_FALSE;
		GLenum attribType = GL_FLOAT;
		unsigned long long offsetPtrVal = 0;
		for( size_t i = 0; i < mVertexLayout.getElementCount(); ++i )
		{
			GLvoid *offsetPtr = reinterpret_cast< GLvoid * >( offsetPtrVal );
			GLint elementFloatCount = static_cast< GLint >( VertexLayout::getFloatCountOfElement( mVertexLayout[ i ] ) );
			GLASSERT( glEnableVertexAttribArray( static_cast< GLuint >( i ) ) );
			GLASSERT( glVertexAttribPointer(
				static_cast< GLuint >( i ),
				elementFloatCount,
				attribType,
				normalized,
				stride,
				offsetPtr ) );
			offsetPtrVal += VertexLayout::getElementSizeBytes( mVertexLayout[ i ] );
		}

		// Now the VAO is ready. Unbind it.
		GLASSERT( glBindVertexArray( 0 ) );
		GLASSERT( glBindBuffer( GL_ARRAY_BUFFER, 0 ) );
		GLASSERT( glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 ) );
	}
	else
	{

		// GPU buffer exists already. Check if data needs to be uploaded there.
		if( mVertexDataChanged || mIndexDataChanged )
		{
			// Upload changed vertex data.
			if( mVertexDataChanged )
			{
				// Vertex data contains changes. Upload them to GPU. When updating the vertex buffer object, we
				// should not bind VAO. Instead we will just bind the Vertex Buffer Object and upload the
				// vertex data there.
				GLASSERT( glBindBuffer( GL_ARRAY_BUFFER, mGL_Buffers[ BufferIndex::VBO ] ) );

				// Upload the vertex data.
				size_t bufferSizeBytes = mVertexCount * mVertexLayout.getSizeBytes();
				GLASSERT( glBufferSubData( GL_ARRAY_BUFFER, 0, bufferSizeBytes, mVertexData.data() ) );

				// Unbind the Vertex Buffer Object.
				glBindBuffer( GL_ARRAY_BUFFER, 0 );
			}

			// Upload changed index data.
			if( mIndexDataChanged )
			{
				// Bind the Index Buffer Object.
				GLASSERT( glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mGL_Buffers[ BufferIndex::IBO ] ) );

				// Upload new content to the buffer.
				size_t bufferSizeBytes = mFaceCount * 3 * sizeof( GLint );
				GLASSERT( glBufferSubData( GL_ELEMENT_ARRAY_BUFFER, 0, bufferSizeBytes, mIndexData.data() ) );

				// Unbind the Index Buffer Object.
				glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 );
			}
		}
	}

	// No need to upload anything anymore.
	mVertexDataChanged = false;
	mIndexDataChanged = false;
	RengineError::check( "Error state after uploading a mesh" );
}

void Mesh::removeFromGPU()
{
	// Release the VBO and IBO.
	if( mGL_Buffers.size() > 0 )
	{
		GLuint bufferCount = static_cast< GLuint >( mGL_Buffers.size() );
		GLASSERT( glDeleteBuffers( bufferCount, mGL_Buffers.data() ) );
		mGL_Buffers.clear();
	}

	// Release the VAO.
	if( mVAO != 0 )
	{
		glDeleteVertexArrays( 1, &mVAO );
		mVAO = 0;
	}
}

MeshInfo &Mesh::accessMeshInfo()
{
	return mMeshInfo;
}

const MeshInfo& Mesh::accessMeshInfo() const
	{
	return mMeshInfo;
	}

const VertexLayout &Mesh::getVertexLayout() const
{
	return mVertexLayout;
}

void Mesh::calculateTangents()
{
	// Ensure that this mesh has normals.
	if( mVertexLayout.hasElement( VertexElement::Normal3f ) == false )
		throw std::invalid_argument( "Mesh does not have slot attribute for normals." );
	if( facesHaveSharedVertices() )
		throw std::runtime_error( "Mesh has at least 2 faces that share vertices." );

	// Go through all faces.
	size_t facecount = getFaceCount();
	for( size_t index = 0; index < facecount; index++ )
	{
		// Get the vertex indices of index'th face.
		size_t vindex1 = 0;
		size_t vindex2 = 0;
		size_t vindex3 = 0;
		getFace( index, vindex1, vindex2, vindex3 );

		// Get the vertex positions and normals of index'th face.
		Vec3 v1 = getPosition( vindex1 );
		Vec3 v2 = getPosition( vindex2 );
		Vec3 v3 = getPosition( vindex3 );
		Vec3 vnormal;
		getNormal( vindex1, vnormal );

		// Calculate the tangent. It is vector from vindex1 to vindex2. Calculate also the bitangent.
		// We can find bitangent by rotating the tangent vector around the normal vector for 90 degrees.
		Vec3 vtangent = v2 - v1;
		Matrix3x3 rotMat = Quaternion::newAxisRotation( 90, vnormal ).toRotationMatrix3();
		Vec3 vbitangent = rotMat * vtangent;
		setVec3Attribute( vindex1, VertexElement::Tangent3f, 0, vtangent );
		setVec3Attribute( vindex1, VertexElement::Tangent3f, 0, vbitangent );
		setVec3Attribute( vindex2, VertexElement::Tangent3f, 0, vtangent );
		setVec3Attribute( vindex2, VertexElement::Tangent3f, 0, vbitangent );
		setVec3Attribute( vindex3, VertexElement::Tangent3f, 0, vtangent );
		setVec3Attribute( vindex3, VertexElement::Tangent3f, 0, vbitangent );
	}
}

bool Mesh::facesHaveSharedVertices() const
{
	// Go faces through one by one and start adding vertex indices to a set. If vertex index already
	// exists in the set, we know that there are at least 2 faces that share a vertex.
	std::set<size_t> processedIndices;
	size_t facecount = getFaceCount();
	for( size_t index = 0; index < facecount; index++ )
	{
		// Get the vertex indices of index'th face.
		size_t vindex1 = 0;
		size_t vindex2 = 0;
		size_t vindex3 = 0;
		getFace( index, vindex1, vindex2, vindex3 );

		// Check if any of the vertex indices have already been part of faces.
		if( processedIndices.find( vindex1 ) != processedIndices.end() )
			return true;
		if( processedIndices.find( vindex2 ) != processedIndices.end() )
			return true;
		if( processedIndices.find( vindex3 ) != processedIndices.end() )
			return true;

		// Include vertex indices to the set of processed vertex indices.
		processedIndices.insert( vindex1 );
		processedIndices.insert( vindex2 );
		processedIndices.insert( vindex3 );
	}

	// We didn't find shared vertices.
	return false;
}

void Mesh::render()
{
	// Bind Vertex Array Object.
	GLASSERT( glBindVertexArray( mVAO ) );

	// Start rendering.
	GLsizei drawCount = static_cast< GLsizei >( mIndexData.size() );
	GLenum drawMode = GL_TRIANGLES;
	GLenum elementType = GL_UNSIGNED_INT;
	GLASSERT( glFrontFace( FaceWinding2VertexOrder( mFaceWinding ) ) );
	GLASSERT( glDrawElements( drawMode, drawCount, elementType, 0 ) );
}

void Mesh::mergeMaterialAttributes( const Material& material )
{
	// Copy the solid color from material to the vertex attributes.
	if( mVertexLayout.hasElement( VertexElement::Color3f ) )
	{
		// Set the solid color to the vertices.
		Vec3 materialColor = material.getSolidColor().asVec3();
		for( size_t vertexIndex = 0; vertexIndex < mVertexCount; vertexIndex++ )
			setVec3Attribute( vertexIndex, VertexElement::Color3f, 0, materialColor );
	}

	// Copy the texture ID to mesh if material has enabled textures.
	if( mVertexLayout.hasElement( VertexElement::TextureIndex1f ) )
	{
		if( material.getEnabledTexturesCount() > 0 )
		{
			// Material has enabled textures. We actually support only 1 texture at the moment.
			if( material.getEnabledTexturesCount() > 2 )
			{
				// Too many textures. Throw an error.
				std::string message = "Cannot merge material to mesh. Material has " +
						std::to_string( material.getEnabledTexturesCount() ) +
						" textures but we support only 2 texture.";
				throw RengineError( message );
			}

			// Assume that the enabled texture is at index 0.
			setFloatAttributes( 0, mVertexCount, VertexElement::TextureIndex1f, 0, 0.0f );
		}
		else
		{
			// No textures. Set the 'TextureIndex' attributes to -1 indicating that solid color should be used
			// instead of texture.
			setFloatAttributes( 0, mVertexCount, VertexElement::TextureIndex1f, 0, KDisabledTextureIndexValue );
		}
	}

	// Copy the material specular exponent value to the mesh.
	if( mVertexLayout.hasElement( VertexElement::Specular1f ) )
	{
		setFloatAttributes( 0, mVertexCount, VertexElement::Specular1f, 0, material.getSpecularColor().specularExponent );
	}
}
