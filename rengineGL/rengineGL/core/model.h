
#pragma once

#include <memory>
#include <string>
#include <vector>

#include "../math/aabb.h"

// Forward declarations.
class ModelManager;
class Mesh;
class Material;

/// <summary>
/// One 3D model that can be added to the scene, could consist of multiple sections.
/// Each section is an instance of this class. Single ModelSection combines mesh and material together.
/// </summary>
class ModelSection
{
public:
	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="meshPtr">The mesh that will be associated with this model section.</param>
	/// <param name="materialPtr">The material that will be used when rendering the mesh.</param>
	ModelSection( std::shared_ptr<Mesh> meshPtr, std::shared_ptr<Material> materialPtr );

	/// <summary>
	/// Tells if this model section doesn't have real mesh and material data.
	/// </summary>
	/// <returns>Returns 'true' if there are no mesh and material.</returns>
	bool isEmpty() const;

public:
	/// <summary>The mesh of this section.</summary>
	std::shared_ptr<Mesh> mesh;

	/// <summary>The material of this section.</summary>
	std::shared_ptr<Material> material;
};

/// <summary>
/// Model represents a 3D model. It may contain multiple meshes that each are rendered with different material.
/// The scene will contain instances of Renderable-class and those Renderable objects can refer to a
/// Model instances.
/// </summary>
class Model
{
public:
	/// <summary>
	/// Constructs new Model.
	/// </summary>
	/// <param name="modelId">ID of the new model.</param>
	/// <param name="manager">Reference to ModelManager that will "own" us.</param>
	Model(const std::string &modelId, ModelManager &manager);

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~Model();
	Model(const Model &) = delete;
	Model& operator = (const Model &) = delete;

public:
	/// <summary>
	/// Adds new Mesh and Material section to this model.
	/// </summary>
	/// <param name="mesh">The mesh object.</param>
	/// <param name="material">The material to be used when rendering the mesh.</param>
	void addSection( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material );

	/// <summary>
	/// Removes a section from this model.
	/// </summary>
	/// <param name="secIndex">Index of the section that will be removed.</param>
	void removeSection( size_t secIndex );

	/// <summary>
	/// Returns the number of sections this model has.
	/// </summary>
	/// <returns>Number of sections this model has.</returns>
	size_t getSectionCount() const;

	/// <summary>
	/// Returns the section.
	/// </summary>
	/// <param name="secIndex">Index of the section that will be returned.</param>
	/// <returns>The section.</returns>
	ModelSection getSection( size_t secIndex ) const;

	/// <summary>
	/// Returns the ID of this model.
	/// </summary>
	/// <returns>ID of this model.</returns>
	const std::string& getId() const;

	/// <summary>
	/// Returns the bounding box that will contain all the vertices of this model.
	/// </summary>
	/// <returns>Axis Aligned Bounding Box of this model.</returns>
	const Aabb& getAabb() const;

	/// <summary>
	/// Recalculates the bounding box so that all the vertices are inside the box.
	/// </summary>
	void updateAabb();

protected:

	/// <summary>
	/// Searches for model section that is using the provided material.
	/// </summary>
	/// <param name="material">Pointer to material that we look for.</param>
	/// <returns>
	/// The model section that is using the material. The isEmpty() of returned model section
	/// will return 'true' if model section with given material does not exist.
	/// </returns>
	ModelSection findModelSectionByMaterial( const std::shared_ptr<Material> &material ) const;

private:
	/// <summary>Type definition of the section container.</summary>
	typedef std::vector<ModelSection> SectionContainer;

	/// <summary>Reference to the model manager that owns this model.</summary>
	ModelManager &mManager;

	/// <summary>ID of this model.</summary>
	std::string mId;

	/// <summary>Contains all the sections that belong to this model.</summary>
	SectionContainer mModelSections;

	/// <summary>Bounding box and encloses all the vertices of this model.</summary>
	Aabb mAabb;
};
