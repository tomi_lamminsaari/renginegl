
#include "glpointlight.h"

GLStructs::GLPointLightBuffer::GLPointLightBuffer()
{
	for( size_t index = 0; index < KLightFloatCount; ++index )
	{
		mLightBuffer[ index ] = 0.0f;
	}
}

void GLStructs::GLPointLightBuffer::setLightPosition( size_t lightIndex, const Vec3 &lightPos )
{
	// Don't set lights that have bigger index than this buffer has storage.
	if( lightIndex >= KMaxLightSourceCount )
		return;

	// Set the light position.
	size_t baseIndex = lightIndex * KLightSourceFloatCount;
	mLightBuffer[ baseIndex + KSubIndexPosX ] = static_cast< GLfloat >( lightPos.mX );
	mLightBuffer[ baseIndex + KSubIndexPosY ] = static_cast< GLfloat >( lightPos.mY );
	mLightBuffer[ baseIndex + KSubIndexPosZ ] = static_cast< GLfloat >( lightPos.mZ );
}

void GLStructs::GLPointLightBuffer::setLightColor( size_t lightIndex, const Rgba &lightCol )
{
	// Don't set lights that have bigger index than this buffer has storage.
	if( lightIndex >= KMaxLightSourceCount )
		return;

	// Set the light color.
	size_t baseIndex = lightIndex * KLightSourceFloatCount;
	mLightBuffer[ baseIndex + KSubIndexColR ] = static_cast< GLfloat >( lightCol.red() );
	mLightBuffer[ baseIndex + KSubIndexColG ] = static_cast< GLfloat >( lightCol.green() );
	mLightBuffer[ baseIndex + KSubIndexColB ] = static_cast< GLfloat >( lightCol.blue() );
}

void GLStructs::GLPointLightBuffer::setLightIntensity( size_t lightIndex, float lightIntensity )
{
	// Don't set lights that have bigger index than this buffer has storage.
	if( lightIndex >= KMaxLightSourceCount )
		return;

	// Set the light intensity.
	size_t baseIndex = lightIndex * KLightSourceFloatCount;
	mLightBuffer[ baseIndex + KSubIndexIntensity ] = static_cast< GLfloat >( lightIntensity );
}

void GLStructs::GLPointLightBuffer::setSpecularIntensity( size_t lightIndex, float specularIntensity )
{
	if( lightIndex >= KMaxLightSourceCount )
		return;

	// Set the specular intensity of this lightsource.
	size_t baseIndex = lightIndex * KLightSourceFloatCount;
	mLightBuffer[ baseIndex + KSubIndexSpecularIntensity ] = static_cast< GLfloat >( specularIntensity );
}

size_t GLStructs::GLPointLightBuffer::getLightSourceCount() const
{
	return KMaxLightSourceCount;
}

GLsizei GLStructs::GLPointLightBuffer::getFloatCount() const
{
	return KLightFloatCount;
}
