
#pragma once

#include <GL/glew.h>
#include <vector>
#include "../defines.h"
#include "../../math/vec3.h"
#include "../../utils/rgba.h"

namespace GLStructs
{
	/// <summary>
	/// GLPointLightBuffer class operates as the light source databuffer that gets uploaded to the shader
	/// when starting the rendering.
	/// </summary>
	class GLPointLightBuffer
	{
	public:
		static const unsigned int KSubIndexPosX = 0;
		static const unsigned int KSubIndexPosY = 1;
		static const unsigned int KSubIndexPosZ = 2;
		static const unsigned int KSubIndexColR = 3;
		static const unsigned int KSubIndexColG = 4;
		static const unsigned int KSubIndexColB = 5;
		static const unsigned int KSubIndexIntensity = 6;
		static const unsigned int KSubIndexSpecularIntensity = 7;
		static constexpr unsigned int KLightSourceFloatCount = 8;  // Lightsource consists of 8 floats.
		static constexpr unsigned int KLightFloatCount = KMaxLightSourceCount * KLightSourceFloatCount;

		/// <summary>
		/// Initializes empty light buffer.
		/// </summary>
		GLPointLightBuffer();

		/// <summary>
		/// Sets the light position of a specific light source.
		/// </summary>
		/// <param name="lightIndex">Index of the lightsource to set.</param>
		/// <param name="lightPos">The position of the lightsourvce.</param>
		void setLightPosition( size_t lightIndex, const Vec3 &lightPos );

		/// <summary>
		/// Sets the color of the light source.
		/// </summary>
		/// <param name="lightIndex">Index of the lightsource to set.</param>
		/// <param name="lightColor">Color of the lightsource.</param>
		void setLightColor( size_t lightIndex, const Rgba &lightColor );

		/// <summary>
		/// Sets the intensity of a light source.
		/// </summary>
		/// <param name="lightIndex">Index of the lightsource to set.</param>
		/// <param name="lightIntensity">Intensity of the light source.</param>
		void setLightIntensity( size_t lightIndex, float lightIntensity );

		/// <summary>
		/// Sets the intensity of specular light effect.
		/// </summary>
		/// <param name="lightIndex">Index of the lightsource to set.</param>
		/// <param name="specularIntensity">Multiplier for specular light effect.</param>
		void setSpecularIntensity( size_t líghtIndex, float specularIntensity );

		/// <summary>
		/// Gets number of lightsources there is room for in this buffer object.
		/// </summary>
		/// <returns>Number of lightsources this lightsource buffer has room for.</returns>
		size_t getLightSourceCount() const;

		/// <summary>
		/// Returns the number of floats this buffer has.
		/// </summary>
		/// <returns>Number of floats this buffer has.</returns>
		GLsizei getFloatCount() const;

	public:

		/// <summary>The float buffer where light data will be stored.</summary>
		GLfloat mLightBuffer[ KLightFloatCount ];
	};

}
