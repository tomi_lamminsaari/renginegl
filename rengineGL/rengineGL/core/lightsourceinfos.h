
#pragma once

#include <GL/glew.h>
#include "../math/vec3.h"
#include "../utils/rgba.h"

/// <summary>
/// Contains the directional light information.
/// </summary>
class DirectionalLightSourceInfo
{
public:
	/// <summary>
	/// Default constructor.
	/// </summary>
	DirectionalLightSourceInfo();

	/// <summary>
	/// Constructor that takes all the light source parameters.
	/// </summary>
	/// <param name="lightDir">Direction of the directional light.</param>
	///
	DirectionalLightSourceInfo(
		const Vec3 &lightDir,
		const Rgba &lightColor,
		GLfloat lightIntensity
	);

	/// <summary>
	/// Destructor.
	/// </summary>
	~DirectionalLightSourceInfo();

	/// <summary>
	/// A copy constructor.
	/// </summary>
	DirectionalLightSourceInfo( const DirectionalLightSourceInfo &obj );

	/// <summary>
	/// An assignment operator.
	/// </summary>
	DirectionalLightSourceInfo &operator=( const DirectionalLightSourceInfo &obj );

	/// <summary>
	/// Tells if this light source info object contains initialzed data.
	/// </summary>
	/// <returns>Returns true if light info has been initialized.</returns>
	bool isInitialized() const;

	/// <summary>
	/// Erases all light source information and set this object to be uninitialized.
	/// </summary>
	void reset();

public:
	bool mInitialized;
	Vec3 mLightDir;
	Rgba mLightColor;
	GLfloat mLightIntensity;
};

/// <summary>
/// Contains point light source information.
/// </summary>
class PointLightSourceInfo
{
public:
	PointLightSourceInfo();
	PointLightSourceInfo(
		const Vec3 &pos,
		const Rgba &lightColor,
		GLfloat lightIntensity );
	~PointLightSourceInfo();
	PointLightSourceInfo( const PointLightSourceInfo &obj );
	PointLightSourceInfo &operator=( const PointLightSourceInfo &obj );

public:
	/// <summary>
	/// Tells if this light source info object contains initialzed data.
	/// </summary>
	/// <returns>Returns true if light info has been initialized.</returns>
	bool isInitialized() const;

	/// <summary>
	/// Erases all light source information and set this object to be uninitialized.
	/// </summary>
	void reset();

public:
	bool mInitialized;
	Vec3 mLightSourcePos;
	Rgba mLightColor;
	GLfloat mLightIntensity;
};
