
#pragma once

#include <memory>
#include <string>
#include <tuple>
#include <vector>
#include "../material.h"
#include "../rendercore.h"

/// <summar>
/// Defines the description of multi textured materials.
/// With MultiTextureMaterialDescription it is possible to define materials with multiple textures and how many
/// times they wrap around when UV-coordinates go from 0.0 to 1.0.
/// </summary>
class MultiTextureMaterialDescription
{
public:
	/// <summary>Maximum number of textures supported by multi-textured blend map.</summary>
	static const int KMultiTextureCount = 4;

	/// <summar>
	/// Default constructor.
	/// </summar>
	MultiTextureMaterialDescription();

	/// <summary>
	/// Copy constructor.
	/// </summar>
	MultiTextureMaterialDescription( const MultiTextureMaterialDescription &obj );

	/// <summar>
	/// Destructor.
	/// </summar>
	~MultiTextureMaterialDescription();

	/// <summary>
	/// An assignment operator.
	/// </summary>
	MultiTextureMaterialDescription &operator=( const MultiTextureMaterialDescription &obj );

	/// <summary>
	/// Sets one of the multi texture textures.
	/// </summary>
	/// <param name="textureIndex">Index of the texture to set.</param>
	/// <param name="textureId">ID of the texture to use.</param>
	/// <param name="tileCount">Number of times the texture gets wrapped when UV goes from 0.0 to 1.0.</param>
	void setTexture(
		std::size_t textureIndex,
		const std::string &textureId,
		float tileCount );

	/// <summary>
	/// Returns the texture item data.
	/// </summary>
	/// <param name="textureIndex">Index of the texture whose data will be returned.</param>
	/// <param name="textureID">Pointer to string that receives the texture item data.</param>
	/// <param name="textureTileCount">Pointer to float that receives the texture item data.</param>
	void getTextureItem( std::size_t textureIndex, std::string *textureID, float *textureTileCount ) const;

	/// <summary>
	/// Sets the blend texture that defines how much each of the each multi texture gets
	/// blended to the final fragment color.
	/// </summary>
	/// <param name="blendTextureId">ID of the blend map texture.</param>
	void setBlendMap( const std::string &blendTextureId );

	/// <summary>
	/// Returns the blend map texture ID.
	/// </summary>
	/// <returns>The blend map texture ID.</returns>
	const std::string &getBlendMap() const;

	/// <summary>
	/// Sets the shader program that should be used for rendering this material.
	/// </summary>
	/// <param name="shaderId">The logical name of the shader.</param>
	void setShaderProgramId( const std::string &shaderId );

	/// <summary>
	/// Returns the ID of the shader program.
	/// </summary>
	/// <returns>ID of the shader program.</returns>
	const std::string &getShaderProgramId() const;

private:
	/// <summary>
	/// Type definition for a multi-texture item. There are following fields.
	/// - ID of the texture.
	/// - Number of times the texture will be tiled.
	/// </summary>
	typedef std::tuple<std::string, float > MultiTextureItem;

private:
	std::vector< MultiTextureItem > mTextures;
	std::string mBlendTextureId;
	std::string mShaderProgramId;
};

/// <summary>
/// TextureRepeatParams contains special texture wrapping and repeating parameters
/// that allows us to define how many times the texture gets wrapped per certain
/// distance unit.
///
/// TODO: This is somewhat redundant with UVTextureRange class!
/// </summary>
class TextureRepeatParams
{
public:
	TextureRepeatParams();
	TextureRepeatParams( float repeatPerUnitU, float repeatPerUnitV );
	~TextureRepeatParams();
	TextureRepeatParams( const TextureRepeatParams &obj );
	TextureRepeatParams &operator=( const TextureRepeatParams &obj );

	float getTextureUForWith( float widthOfMesh ) const;
	float getTextureVForHeight( float heightOfMesh ) const;

private:
	float mRepeatPerUnitU;
	float mRepeatPerUnitV;
};

/// <summary>
/// A class with utility function that set material properties.
/// </summary>
class MaterialUtility
{
public:

	/// <summary>
	/// Converts the given material to match the given material description.
	/// </summary>
	/// <param name="material">The material to be modified.</param>
	/// <param name="matDesc">The material description.</param>
	/// <returns>Returns true if successful.</returns>
	static bool applyToMaterial(
		std::shared_ptr<Material> material,
		const MultiTextureMaterialDescription &matDesc );
};
