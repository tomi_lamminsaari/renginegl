
#include <exception>
#include "../shaderprogram.h"
#include "materialutility.h"

MultiTextureMaterialDescription::MultiTextureMaterialDescription()
{
	// Allocate space for multi-textured material textures.
	MultiTextureItem item0 = std::make_tuple( "", 1.0f );
	mTextures.resize( KMultiTextureCount, item0 );
}

MultiTextureMaterialDescription::~MultiTextureMaterialDescription()
{

}

void MultiTextureMaterialDescription::setTexture(
	std::size_t textureIndex,
	const std::string &textureId,
	float tileCount )
{
	MultiTextureItem item = std::make_tuple( textureId, tileCount );
	mTextures[ textureIndex ] = item;
}

void MultiTextureMaterialDescription::getTextureItem( std::size_t textureIndex, std::string *textureID, float *textureTileCount ) const
{
	// Check input parameters.
	if( textureID == nullptr )
		throw std::invalid_argument( "MultiTextureMaterial::getTextureItem(): textureID parameter must not be nullptr." );
	if( textureTileCount == nullptr )
		throw std::invalid_argument( "MultiTextureMaterial::getTextureItem(): textureTileCount parameter must not be nullptr." );

	// Set the return values.
	*textureID = std::get<0>( mTextures[ textureIndex ] );
	*textureTileCount = std::get<1>( mTextures[ textureIndex ] );
}

void MultiTextureMaterialDescription::setBlendMap( const std::string &blendTextureId )
{
	mBlendTextureId = blendTextureId;
}

const std::string &MultiTextureMaterialDescription::getBlendMap() const
{
	return mBlendTextureId;
}

void MultiTextureMaterialDescription::setShaderProgramId( const std::string &shaderId )
{
	mShaderProgramId = shaderId;
}

const std::string &MultiTextureMaterialDescription::getShaderProgramId() const
{
	return mShaderProgramId;
}

///
/// TextureRepeatParams class.
///

TextureRepeatParams::TextureRepeatParams() :
	mRepeatPerUnitU( 1.0f ),
	mRepeatPerUnitV( 1.0f )
{

}

TextureRepeatParams::TextureRepeatParams( float repeatPerUnitU, float repeatPerUnitV ) :
	mRepeatPerUnitU( repeatPerUnitU ),
	mRepeatPerUnitV( repeatPerUnitV )
{

}

TextureRepeatParams::~TextureRepeatParams()
{

}

TextureRepeatParams &TextureRepeatParams::operator=( const TextureRepeatParams &obj )
{
	if( this != &obj )
	{
		mRepeatPerUnitU = obj.mRepeatPerUnitU;
		mRepeatPerUnitV = obj.mRepeatPerUnitV;
	}
	return *this;
}

float TextureRepeatParams::getTextureUForWith( float width ) const
{
	return 1.0f;
}

float TextureRepeatParams::getTextureVForHeight( float height ) const
{
	return 1.0f;
}

///
/// MaterialUtility class.
///

bool MaterialUtility::applyToMaterial(
	std::shared_ptr<Material> material,
	const MultiTextureMaterialDescription &matDesc )
{

	// Set the shader program.
	material->setShaderProgramById( matDesc.getShaderProgramId() );

	// Set the textures from material description to the actual material.
	// Set also the tile count uniforms.
	// The terrain material will have the dedicated texture slots:
	// 0 = Blendmap
	// 1 = Terrain texture 1
	// 2 = Terrain texture 2
	// 3 = Terrain texture 3
	// 4 = Terrain texture 4
	std::string tileCountUniforms[] =
	{
		ShaderProgram::KUniformName_Texture0TileCount,
		ShaderProgram::KUniformName_Texture1TileCount,
		ShaderProgram::KUniformName_Texture2TileCount,
		ShaderProgram::KUniformName_Texture3TileCount
	};
	for( std::size_t i = 0; i < MultiTextureMaterialDescription::KMultiTextureCount; ++i )
	{
		std::string textureId = "";
		float textureTileCount = 1.0f;
		matDesc.getTextureItem( i, &textureId, &textureTileCount );

		// Set the texture to material.
		material->setTexture( Material::KCustomTextureIndex0 + i, textureId );

		// Set the number of times the texture will be tiled.
		material->setFloatUniform( tileCountUniforms[ i ], textureTileCount );
	}

	// Set the blend map to the material. The blend map defines how much different textures get mixed
	// to the final result.
	const std::string &blendMapID = matDesc.getBlendMap();
	material->setTexture( static_cast< std::size_t >( Material::KBlendMapTextureIndex ), blendMapID );

	return true;
}
