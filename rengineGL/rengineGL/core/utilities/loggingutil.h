#pragma once

#include <string>

class RenderCore;
class Vec3;

/// <summary>
/// Class that contains logging helper functions.
/// </summary>
class LoggingUtil
	{
	public:
		/// <summary>
		/// Prints log line and Vec3 position to the log. "<message> vec3=[x, y, z]".
		/// </summary>
		/// <param name="core">The rendering core object.</param>
		/// <param name="id">ID of the log line to print to the terminal.</param>
		/// <param name="message">The body of the message line.</param>
		/// <param name="pos">The position to print to the log.</param>
		static void write( RenderCore &core, int id, const std::string &message, const Vec3 &pos );
	};
