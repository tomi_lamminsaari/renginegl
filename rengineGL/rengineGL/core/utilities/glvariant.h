
#pragma once

#include <GL/glew.h>

/// <summary>
/// Special type that can contain either GLfloat value or GLint value.
/// </summary>
class GLVariant
{
public:
	/// <summary>Possible datatypes of the value.</summary>
	enum class GLType { Unspecified, Float, Integer };

	/// <summary>
	/// Default constructor. Defines an unspecified value.
	/// </summary>
	GLVariant();

	/// <summary>
	/// Constructs a variant type for GLfloat value.
	/// </summary>
	/// <param name="floatValue">The value as GLfloat type.</param>
	GLVariant( GLfloat floatValue );

	/// <summary>
	/// Constructs a variant type for GLint value.
	/// </summary>
	/// <param name="intValue">THe value as GLint type.</param>
	GLVariant( GLint intValue );

	/// <summary>
	/// A copy constructor.
	/// </summary>
	/// <param name="obj">Another GLVariant that will be copied.</param>
	GLVariant( const GLVariant &obj );

	/// <summary>
	/// Destructor.
	/// </summary>
	~GLVariant();

	/// <summary>
	/// An assignment operator.
	/// </summary>
	/// <param name="obj">Another GLVariant to be assigned to this object.</param>
	GLVariant &operator=( const GLVariant &obj );

	/// <summary>
	/// An assignment operator for assigning a GLfloat value to us.
	/// </summary>
	/// <param name="floatValue">The value to assign.</param>
	/// <returns>Reference to us.</returns>
	GLVariant &operator=( GLfloat floatValue );

	/// <summary>
	/// An assignment operator for assigning an int value.
	/// </summary>
	/// <param name="intValue">The GLint value to assign.</param>
	/// <returns>Reference to us.</returns>
	GLVariant &operator=( GLint intValue );

	/// <summary>
	/// Returns the float value. Throws an error if we're not containing a float value.
	/// </summary>
	/// <returns>The float value.</returns>
	GLfloat asFloat() const;

	/// <summary>
	/// Returns the int value. Throws an error if we're not containing an integer.
	/// </summary>
	/// <returns>The integer value.</returns>
	GLint asInt() const;

	/// <summary>
	/// Returns the type of data this variant is containing.
	/// </summary>
	/// <returns>Type of data we're containing.</returns>
	GLType getType() const;

private:
	/// <summary>Type of this value.</summary>
	GLType mType;

	/// <summary>The float value.</summary>
	GLfloat mFloatValue;

	/// <summary>The int value.</summary>
	GLint mIntValue;
};
