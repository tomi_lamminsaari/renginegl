
#include "frametimemeasurement.h"

void FrameTimeMeasurement::startTimingEvent( TimeSubEvent subEvent )
{
    switch( subEvent )
    {
    case TimeSubEvent::ShadowmapPass:
        mShadowmapStart = std::chrono::high_resolution_clock::now();
        mShadowmapEnd = mShadowmapStart;
        break;
    case TimeSubEvent::GeometryPass:
        mGeometryPassStart = std::chrono::high_resolution_clock::now();
        mGeometryPassEnd = mGeometryPassStart;
        break;
    case TimeSubEvent::ShadingPass:
        mShadingPassStart = std::chrono::high_resolution_clock::now();
        mShadingPassEnd = mShadingPassStart;
        break;
    default:
        mFrameStart = std::chrono::high_resolution_clock::now();
        mFrameEnd = mFrameStart;
        mShadowmapStart = mFrameStart;
        mShadowmapEnd = mFrameStart;
        mGeometryPassStart = mFrameStart;
        mGeometryPassEnd = mFrameStart;
        mShadingPassStart = mFrameStart;
        mShadingPassEnd = mFrameStart;
    }
}

void FrameTimeMeasurement::endTimingEvent( TimeSubEvent subEvent )
{
    switch( subEvent )
    {
    case TimeSubEvent::ShadowmapPass:
        mShadowmapEnd = std::chrono::high_resolution_clock::now();
        break;
    case TimeSubEvent::GeometryPass:
        mGeometryPassEnd = std::chrono::high_resolution_clock::now();
        break;
    case TimeSubEvent::ShadingPass:
        mShadingPassEnd = std::chrono::high_resolution_clock::now();
        break;
    default:
        mFrameEnd = std::chrono::high_resolution_clock::now();
        break;
    }
}

int64_t FrameTimeMeasurement::getDurationUs( TimeSubEvent subEvent ) const
{
    switch( subEvent )
    {
    case TimeSubEvent::ShadowmapPass:
        return std::chrono::duration_cast< std::chrono::microseconds >( mShadowmapEnd - mShadowmapStart ).count();
    case TimeSubEvent::GeometryPass:
        return std::chrono::duration_cast< std::chrono::microseconds >( mGeometryPassEnd - mGeometryPassStart ).count();
    case TimeSubEvent::ShadingPass:
        return std::chrono::duration_cast< std::chrono::microseconds >( mShadingPassEnd - mShadingPassStart ).count();
    default:
        return std::chrono::duration_cast< std::chrono::microseconds >( mFrameEnd - mFrameStart ).count();
    }
}

int64_t FrameTimeMeasurement::getDurationMs( TimeSubEvent subEvent ) const
{
    switch( subEvent )
    {
    case TimeSubEvent::ShadowmapPass:
        return std::chrono::duration_cast< std::chrono::milliseconds >( mShadowmapEnd - mShadowmapStart ).count();
    case TimeSubEvent::GeometryPass:
        return std::chrono::duration_cast< std::chrono::milliseconds >( mGeometryPassEnd - mGeometryPassStart ).count();
    case TimeSubEvent::ShadingPass:
        return std::chrono::duration_cast< std::chrono::milliseconds >( mShadingPassEnd - mShadingPassStart ).count();
    default:
        return std::chrono::duration_cast< std::chrono::milliseconds >( mFrameEnd - mFrameStart ).count();
    }
}
