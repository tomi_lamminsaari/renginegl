
#include <stdexcept>
#include "glvariant.h"

GLVariant::GLVariant() :
	mType( GLType::Unspecified ),
	mFloatValue( 0.0f ),
	mIntValue( 0 )
{

}

GLVariant::GLVariant( GLfloat floatVal ) :
	mType( GLType::Float ),
	mFloatValue( floatVal ),
	mIntValue( 0 )
{

}

GLVariant::GLVariant( GLint intVal ) :
	mType( GLType::Integer ),
	mFloatValue( 0.0f ),
	mIntValue( intVal )
{

}

GLVariant::GLVariant( const GLVariant &obj ) :
	mType( obj.mType ),
	mFloatValue( obj.mFloatValue ),
	mIntValue( obj.mIntValue )
{

}

GLVariant::~GLVariant()
{

}

GLVariant &GLVariant::operator=( const GLVariant &obj )
{
	if( this != &obj )
	{
		mType = obj.mType;
		mFloatValue = obj.mFloatValue;
		mIntValue = obj.mIntValue;
	}
	return *this;
}

GLVariant &GLVariant::operator=( GLfloat floatVal )
{
	mType = GLType::Float;
	mFloatValue = floatVal;
	mIntValue = 0;
	return *this;
}

GLVariant &GLVariant::operator=( GLint intVal )
{
	mType = GLType::Integer;
	mFloatValue = 0.0f;
	mIntValue = intVal;
	return *this;
}

GLfloat GLVariant::asFloat() const
{
	if( mType != GLType::Float )
		throw std::logic_error( "GLVariant is not holding a float value." );
	return mFloatValue;
}

GLint GLVariant::asInt() const
{
	if( mType != GLType::Integer )
		throw std::logic_error( "GLVariant is not holding an integer value." );
	return mIntValue;
}

GLVariant::GLType GLVariant::getType() const
{
	return mType;
}
