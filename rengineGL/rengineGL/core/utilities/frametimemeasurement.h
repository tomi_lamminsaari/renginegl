#pragma once

#include <chrono>

/// <summary>
/// FrameTimeMeasurement is an utility class that can be used for measuring the
/// the time it takes to render certain stages during the frame.
/// </summary>
class FrameTimeMeasurement
{
public:
    /// <summary>
    /// TimeSubEvent enumerates the different sub events we can measure.
    /// </summary>
    enum class TimeSubEvent {
        ShadowmapPass,
        GeometryPass,
        ShadingPass,
        FullFrame
    };

    /// <summary>
    /// Starts timing the event.
    /// </summary>
    /// <param name="subEvent">Type of event you start timing. In the beginning of frame
    /// you should use 'TimeSubEvent::FullFrame'.</param>
    void startTimingEvent( TimeSubEvent subEvent );

    /// <summary>
    /// Ends timing an event.
    /// </summary>
    /// <param name="subEvent">Type of event you stop timing.</param>
    void endTimingEvent( TimeSubEvent subEvent );

    /// <summary>
    /// Gets duration of measured event in microseconds. Remember to call startTimingEvent() and
    /// endTimingEvent() methods first.
    /// </summary>
    /// <param name="subEvent">Type of event whoose duration you want to get.</param>
    /// <returns>Duration of event in microseconds.</returns>
    int64_t getDurationUs( TimeSubEvent subEvent ) const;

    /// <summary>
    /// Gets duration of measured event in milliseconds. Remember to call startTimingEvent() and
    /// endTimingEvent() methods first.
    /// </summary>
    /// <param name="subEvent">Type of event whoose duration you want to get.</param>
    /// <returns>Duration of event in milliseconds.</returns>
    int64_t getDurationMs( TimeSubEvent subEvent ) const;

private:
    std::chrono::steady_clock::time_point mFrameStart;
    std::chrono::steady_clock::time_point mFrameEnd;
    std::chrono::steady_clock::time_point mShadowmapStart;
    std::chrono::steady_clock::time_point mShadowmapEnd;
    std::chrono::steady_clock::time_point mGeometryPassStart;
    std::chrono::steady_clock::time_point mGeometryPassEnd;
    std::chrono::steady_clock::time_point mShadingPassStart;
    std::chrono::steady_clock::time_point mShadingPassEnd;
};
