/**
* Contains utility functions for handling OpenGL error codes.
*/

#pragma once

#include <string>
#include <GL/glew.h>

namespace GLErrors
{

	/// <summary>
	/// Returns the textual representation of given error code.
	/// </summary>
	/// <param name="glErrorCode">The error code.</param>
	/// <returns>A string containing the error in textual format.</returns>
	std::string getErrorText( GLenum glErrorCode );

};
