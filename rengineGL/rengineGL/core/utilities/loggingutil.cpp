
#include <sstream>
#include "loggingutil.h"
#include "../rendercore.h"
#include "../../math/vec3.h"

void LoggingUtil::write( RenderCore &core, int id, const std::string &message, const Vec3 &pos )
{
	std::ostringstream ss;
	ss << message << " Vec3( " << pos.mX << ", " << pos.mY << ", " << pos.mZ << ")";
	core.printTerminalMessage( id, ss.str() );
}
