
#include "lightsourcecollection.h"
#include "lightsources.h"
#include "rengineexception.h"
#include "scene.h"

LightSourceCollection::LightSourceCollection( Scene &scene ) :
	mScene( scene )
{

}

LightSourceCollection::~LightSourceCollection()
{

}

std::shared_ptr<LightSource> LightSourceCollection::createDirectionalLight(
		const Vec3& lightDir,
		const Rgba& lightColor,
		float lightIntensity )
{
	if( mDirectionalLight.get() )
	{
		throw RengineError( "Light ID '" + std::string( BuiltIn::LIGHT_SUN ) + "' exists already." );
	}

	// Create directional light.
	mDirectionalLight = std::make_shared<LightSource>( mScene.getCore() );
	mDirectionalLight->makeDirectionalLightSource( lightDir, lightColor, lightIntensity );
	return mDirectionalLight;
}

std::shared_ptr<LightSource> LightSourceCollection::createLightSource( const std::string &lightId )
{
	// Add lightsource to the collection unless another lightsource with same ID already exists.
	std::shared_ptr<LightSource> lightsource;
	if( lightIdExists( lightId ) )
	{
		// Lightsource exists. Throw an error.
		throw RengineError( "Light ID '" + lightId + "' exists already." );
	}
	else
	{
		// Lightsource does not exist. Add it now.
		lightsource = std::make_shared<LightSource>( mScene.getCore() );
		mLights[ lightId ] = lightsource;
	}
	return lightsource;
}

void LightSourceCollection::destroyLightSource( const std::string &lightsourceId )
{
	if( lightsourceId == BuiltIn::LIGHT_SUN )
	{
		// Destroying directional light.
		mDirectionalLight = nullptr;
	}
	else
	{
		// Find the lightsource by its ID and remove it from the collection. LightSource is
		// owned by a smart pointer so removing it from the collection will delete it.
		auto iter = mLights.find( lightsourceId );
		if( iter != mLights.end() )
			mLights.erase( iter );
	}
}

std::shared_ptr<LightSource> LightSourceCollection::getLightSource( const std::string &lightId )
{
	if( lightId == BuiltIn::LIGHT_SUN )
	{
		return mDirectionalLight;
	}

	// Find the lightsource that matches the given ID.
	auto iter = mLights.find( lightId );
	if( iter != mLights.end() )
		return iter->second;

	// Light was not found. Returns null pointer.
	return std::shared_ptr<LightSource>();
}

std::vector< std::shared_ptr<LightSource> > LightSourceCollection::getLightSources() const
{
	std::vector< std::shared_ptr<LightSource> > lights;
	lights.reserve( mLights.size() + 1 );

	// Add directional light source if one exists.
	if( mDirectionalLight.get() )
	{
		lights.push_back( mDirectionalLight );
	}

	// Add other light sources.
	auto iter = mLights.begin();
	while( iter != mLights.end() )
	{
		// Get the pointer of the light source and push it to the output vector.
		lights.push_back( iter->second );

		// Move to next light source.
		iter++;
	}
	return lights;
}

bool LightSourceCollection::lightIdExists( const std::string &lightId ) const
{
	if( lightId == BuiltIn::LIGHT_SUN )
		return mDirectionalLight.get() != nullptr;
	if( mLights.find( lightId ) == mLights.end() )
	{
		return false;
	}
	return true;
}
