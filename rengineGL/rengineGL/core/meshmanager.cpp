
#include <limits>
#include "../math/quaternion.h"
#include "../math/size3.h"
#include "renderutils/skybox.h"
#include "mesh.h"
#include "meshmanager.h"
#include "meshcreation/meshcreation.h"

MeshManager::MeshManager( RenderCore& core ) :
	ManagerBase( core )
{

}

MeshManager::~MeshManager()
{
	// Remove the GPU resources.
	removeFromGPU();

	// Destroy the meshes.
	mMeshes.clear();
}

void MeshManager::cleanup()
{
	removeFromGPU();
	mMeshes.clear();
}

std::shared_ptr<Mesh> MeshManager::createMesh( const std::string& meshId )
{
	// Check if mesh with given ID already exists.
	if( mMeshes.find( meshId ) != mMeshes.end() )
		throw std::invalid_argument( "Mesh with id '" + meshId + "' exists already." );

	// Create mesh and add it to the container.
	std::shared_ptr<Mesh> mesh( new Mesh() );
	mesh->setId( meshId );
	mMeshes[ meshId ] = mesh;
	return mesh;
}

void MeshManager::destroyMesh( const std::string& meshId )
{
	auto iter = mMeshes.find( meshId );
	if( iter != mMeshes.end() )
	{
		// We found the mesh from the container. Release the mesh resources and remove the
		// mesh from the mesh container.
		std::shared_ptr<Mesh> mesh = iter->second;
		mesh->removeFromGPU();
		mMeshes.erase( iter );
	}
}

std::shared_ptr<Mesh> MeshManager::findMesh( const std::string& meshId ) const
{
	// Returns the mesh if we can find it by ID. Return nullptr if we can't find it.
	auto iter = mMeshes.find( meshId );
	if( iter != mMeshes.end() )
		return iter->second;
	return nullptr;
}

bool MeshManager::initializeDefaultMeshes()
{
	// Create unit cube.
	VertexLayout layout1( { VertexElement::Position3f } );
	std::shared_ptr<Mesh> mesh = MeshCreation::createCuboidNotTextured(
			mCore, BuiltIn::MESH_UNIT_CUBE, layout1, Size3( 1.0f ), true );

	// Create mesh for showing the texture on screen.
	mesh = nullptr;
	VertexLayout layout2( { VertexElement::Position3f, VertexElement::Texture2f } );
	mesh = MeshCreation::createQuad( mCore, BuiltIn::MESH_TEXTURE_PREVIEW, layout2, FaceSides::SingleSide, 0.45f, 0.45f,
			MeshCreation::UVTextureRange::empty() );
	mesh->moveVertices( Vec3( 0.74f, 0.74f, 0.0f ) );	// Move the texture preview mesh a bit aside from the center of the screen.

	// Create default skybox mesh.
	mesh = nullptr;
	mesh = SkyBox::initSkyBoxMesh( mCore, BuiltIn::MESH_SKYBOX );

	// Create fullscreen quad for deferred shading rendering.
	VertexLayout deferredShadingLayout( { VertexElement::Position3f, VertexElement::Texture2f } );
	mesh = MeshCreation::createQuad( mCore, BuiltIn::MESH_DEFERREDSHADING_QUAD, deferredShadingLayout, FaceSides::SingleSide, 2.0f, 2.0f,
			MeshCreation::UVTextureRange::repeatTexture( 1.0f, 1.0f ) );

	return true;
}

void MeshManager::uploadToGPU()
{
	// Upload all meshes to the GPU.
	RengineError::check( "Error state before uploading meshes" );
	auto iter = mMeshes.begin();
	while( iter != mMeshes.end() )
	{
		iter->second->uploadToGPU();
		iter++;
	}
	RengineError::check( "Error state after uploading meshes" );
}

void MeshManager::removeFromGPU()
{
	// Unload all meshes from GPU memory.
	auto iter = mMeshes.begin();
	while( iter != mMeshes.end() )
	{
		iter->second->removeFromGPU();
		iter++;
	}
}

std::shared_ptr<Mesh> MeshManager::createMesh_Grid( const std::string& meshId,
	const VertexLayout& vertexLayout,
	size_t verticesHorizontal,
	size_t verticesVertical,
	float w,
	float h,
	float texRepeatU,
	float texRepeatV )
{
	// Create and initialize the vertices.
	size_t vertexCount = verticesHorizontal * verticesVertical;
	std::shared_ptr<Mesh> mesh = createMesh( meshId );
	mesh->initialize( vertexLayout, vertexCount, 0 );
	mesh->accessMeshInfo().setGridInfo( static_cast< int >( verticesHorizontal ), static_cast< int >( verticesVertical ) );
	int textureCount = 0;
	for( size_t i = 0; i < vertexLayout.getElementCount(); ++i )
		if( vertexLayout[i] == VertexElement::Texture2f )
			textureCount++;

	// Set the vertex coordinates with even spacing.
	float vertexDeltaX = w / static_cast< float >( verticesHorizontal );
	float vertexDeltaZ = h / static_cast< float >( verticesVertical );
	float uvDeltaU = texRepeatU / static_cast< float >( verticesHorizontal );
	float uvDeltaV = texRepeatV / static_cast< float >( verticesVertical );
	bool setTextureCoords = vertexLayout.getFloatIndex( VertexElement::Texture2f, 0 ) != -1 ? true : false;
	bool setVertexNormals = vertexLayout.getFloatIndex( VertexElement::Normal3f, 0 ) != -1 ? true : false;
	float startX = -w / 2.0f;
	float startZ = -w / 2.0f;
	float endX = w / 2.0f;
	float endZ = w / 2.0f;
	Vec3 pos( startX, 0.0f, startZ );
	Vec2 uv( 0.0f, 0.0f );
	size_t currentVertexCountInRow = 0;
	for( size_t index = 0; index < vertexCount; index++ )
	{
		// Set the vertex position.
		mesh->setPosition( index, pos );

		// Set vertex texture coordiante.
		if( setTextureCoords )
		{
			for( int vi = 0; vi < textureCount; ++vi )
			{
				mesh->setTexture2D( index, vi, uv );
			}
		}

		// Set vertex normal.
		if( setVertexNormals )
			mesh->setNormal( index, Vec3( 0.0f, 1.0f, 0.0f ) );

		// Update the vertex coordinates for next round.
		pos.mX += vertexDeltaX;
		uv.mX += uvDeltaU;
		currentVertexCountInRow++;
		if( currentVertexCountInRow >= verticesHorizontal )
		{
			// Coordinate has reached the right edge. Move x-coordinate back to the left
			// and move the Z-coordinate to next grid line.
			pos.mZ += vertexDeltaZ;
			pos.mX = startX;
			uv.mY += uvDeltaV;
			uv.mX = 0.0f;
			currentVertexCountInRow = 0;
		}
	}

	// Crete the faces.
	for( size_t z = 0; z < verticesVertical - 1; z++ )
	{
		for( size_t x = 0; x < verticesHorizontal - 1; x++ )
		{
			// Construct two faces.
			size_t baseIndex = ( z * verticesHorizontal ) + x;
			mesh->addFace( baseIndex + 0, baseIndex + 1, baseIndex + verticesHorizontal );
			mesh->addFace( baseIndex + verticesHorizontal, baseIndex + 1, baseIndex + verticesHorizontal + 1 );
		}
	}

	return mesh;
}

