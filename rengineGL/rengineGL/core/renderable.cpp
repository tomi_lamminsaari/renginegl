
#include <assert.h>
#include "../math/matrix4x4.h"
#include "renderutils/framebufferobject.h"
#include "renderutils/shadowrenderer.h"
#include "camera.h"
#include "material.h"
#include "materialmanager.h"
#include "mesh.h"
#include "meshmanager.h"
#include "model.h"
#include "modelmanager.h"
#include "node.h"
#include "renderable.h"
#include "rendercore.h"
#include "renderpassinfo.h"
#include "renderstatistics.h"
#include "shaderprogram.h"

Renderable::Renderable( RenderCore &core ) :
	SceneNodeObject( core ),
	mCore( core ),
	mModel( nullptr ),
	mOwnModel( false )
{

}

Renderable::~Renderable()
{

}

void Renderable::setId( const std::string& id )
{
	mId = id;
}

const std::string& Renderable::getId() const
{
	return mId;
}

void Renderable::prepareRender( Camera& camera )
{
	if( mModel == nullptr || mHidden )
		return;

	// Reset render-time attributes to their default ones.
	mIsOutsideCameraView = false;

	// Detect if object is visible in camera. We project the bounding box corner coordinates
	// to the screen and check if they are outside of visible screen or not. If all corner
	// coordinates have negative Z-value, it means that they are behind the camera so they
	// can't be seen.
	Matrix4x4 modelMatrix;
	modelMatrix.setIdentity();
	if( mParentNode != nullptr )
		modelMatrix = mParentNode->getTransformationMatrix();
	Matrix4x4 viewProjectionMatrix = camera.getViewProjMatrix();
	Matrix4x4 modelViewProjectionMatrix = viewProjectionMatrix * modelMatrix;
	std::vector<Vec3> aabbcorners = mModel->getAabb().getCorners();
	std::vector<Vec4> aabbCornersProjected( aabbcorners.size() );
	bool allBehindCamera = true;
	for( size_t i = 0; i < aabbcorners.size(); ++i )
	{
		Vec4 screenCoord = modelViewProjectionMatrix * Vec4( aabbcorners[ i ], 1.0f );
		aabbCornersProjected[ i ] = screenCoord;

		// Is this Z-coordinate somewhere in front of camera? If it is, it means that
		// not every corner is behind the camera.
		if( screenCoord.mZ > 0.0f )
			allBehindCamera = false;
	}
	if( allBehindCamera == false )
	{
		// Some of the corners are potentially visible because their Z-coordinates are positive. Check if
		// they are in front of camera frustum. We projected the coordinates to clipping space so if
		// their x- or y-coordinates are within range [-w, w], it means that they are inside the
		// the visible part of the screen.
		int outsideLeftScreen = 0;
		int outsideRightScreen = 0;
		for( const Vec4 &v : aabbCornersProjected )
		{
			if( v.mX < -v.mW )
				outsideLeftScreen++;
			if( v.mX > v.mW )
				outsideRightScreen++;

		}
		if( outsideLeftScreen == 8 || outsideRightScreen == 8 )
			mIsOutsideCameraView = true;
	}
	else
	{
		// Every corner is behind camera so this definetily will not be visible to the camera.
		mIsOutsideCameraView = true;
	}
}

void Renderable::render( Camera& camera, SceneGlobalRenderPassInfo *globalRenderPassInfo )
{
	// Don't attempt to render if there is no renderable or if this is outside of camera view or
	// if it's hidden.
	if( mModel == nullptr || mIsOutsideCameraView == true || mHidden )
	{
		return;
	}

	// Prepare the translation matrices
	Matrix4x4 modelMatrix;
	modelMatrix.setIdentity();
	if( mParentNode )
	{
		modelMatrix = mParentNode->getTransformationMatrix();
	}
	Matrix4x4 viewProjectionMatrix = camera.getViewProjMatrix();
	Matrix4x4 modelViewProjectionMatrix = viewProjectionMatrix * modelMatrix;
	Matrix4x4 modelViewMatrix = camera.getViewMatrix() * modelMatrix;

	Matrix3x3 modelRotationMat;
	modelMatrix.getRotationMatrix( modelRotationMat );

	// Render the meshes of the model.
	size_t sectionCount = mModel->getSectionCount();
	for( size_t i = 0; i < sectionCount; ++i )
	{
		ModelSection section = mModel->getSection( i );

		// Get the vertex array object from mesh.
		RenderPassInfo renderPassInfo( globalRenderPassInfo );
		section.mesh->getRenderPassInfo( renderPassInfo );

		// Bind Vertex Array Object.
		GLASSERT( glBindVertexArray( renderPassInfo.mVAO ) );

		// Apply the material. We use the overriding material if one was provided. At least
		// when rendering the shadow map pass we use the special shadow material.
		std::shared_ptr<ShaderProgram> shader;
		std::shared_ptr<Material> mat = globalRenderPassInfo->getOverrideMaterial();
		if( mat == nullptr )
		{
			// Overriding material not set. Use material from the object section.
			mat = section.material;
		}

		// Start using the material if we have one.
		if( mat != nullptr )
		{
			// Apply the material. This will bind the correct material based textures to the texture units.
			// This will also activate the material specific shader program.
			mat->apply();
			shader = mat->getShaderProgram();
		}

		// Set shader input uniforms.
		if( shader != nullptr )
		{
			// Camera uniforms.
			shader->setCameraUniforms( camera.getPosition(), &modelViewProjectionMatrix, &modelViewMatrix, &modelMatrix );

			// Light related uniforms.
			if( globalRenderPassInfo->mAmbientLightEnabled )
			{
				// Ambient light enabled. Set ambient light data.
				shader->setAmbientLight(
					globalRenderPassInfo->mAmbientLightColor,
					globalRenderPassInfo->mAmbientLightIntensity );
			}
		}

		// Enable rendering options.
		glEnable( GL_DEPTH_TEST );
		glEnable( GL_CULL_FACE );
		GLASSERT( glFrontFace( renderPassInfo.mFaceWindingOrder ) );

		// Render the mesh.
		GLASSERT( glDrawElements( renderPassInfo.mDrawMode, renderPassInfo.mDrawCount, renderPassInfo.mElementType, 0 ) );

		// Update the rendering statistics.
		mCore.getActiveStats().increaseRenderPassCount();

		// Unbind everything.
		GLASSERT( glBindVertexArray( 0 ) );
		GLASSERT( glBindBuffer( GL_ARRAY_BUFFER, 0 ) );
		GLASSERT( glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, 0 ) );
		if( mat != nullptr )
		{
			mat->unapply();
		}
	}
}

void Renderable::renderSimplifiedMaterial(
	const Camera& camera,
	ShaderProgram& currentProgram,
	SceneGlobalRenderPassInfo* globalRenderPassInfo )
{
	// Don't attempt to render if there is no renderable or if this is outside of camera view or
	// if it's hidden.
	if( mModel == nullptr || mIsOutsideCameraView == true || mHidden )
	{
		return;
	}

	// Set the camera uniforms to the shader program.
	Matrix4x4 modelMatrix;
	modelMatrix.setIdentity();
	if( mParentNode )
		modelMatrix = mParentNode->getTransformationMatrix();
	Matrix4x4 viewProjectionMatrix = camera.getViewProjMatrix();
	Matrix4x4 modelViewProjectionMatrix = viewProjectionMatrix * modelMatrix;
	Matrix4x4 modelViewMatrix = camera.getViewMatrix() * modelMatrix;
	Matrix3x3 modelRotationMat;
	modelMatrix.getRotationMatrix( modelRotationMat );
	currentProgram.setCameraUniforms( camera.getPosition(), &modelViewProjectionMatrix, &modelViewMatrix, &modelMatrix );

	// Render the meshes of the model.
	size_t sectionCount = mModel->getSectionCount();
	for( size_t secIndex = 0; secIndex < mModel->getSectionCount(); secIndex++ )
	{
		// Get the VAO of the model section and bind it.
		ModelSection section = mModel->getSection( secIndex );
		RenderPassInfo renderPassInfo( globalRenderPassInfo );
		section.mesh->getRenderPassInfo( renderPassInfo );
		if( renderPassInfo.mVAO == 0 )
			continue;

		// Bind Vertex Array Object.
		GLASSERT( glBindVertexArray( renderPassInfo.mVAO ) );

		// Bind the textures if needed.
		section.material->applyTextures();

		// Render the mesh.
		GLASSERT( glFrontFace( renderPassInfo.mFaceWindingOrder ) );
		GLASSERT( glDrawElements( renderPassInfo.mDrawMode, renderPassInfo.mDrawCount, renderPassInfo.mElementType, 0 ) );
		mCore.getActiveStats().increaseRenderPassCount();

		// Unbind the VAO.
		GLASSERT( glBindVertexArray( 0 ) );
	}
}

void Renderable::setVisibilityInCamera( bool visibility )
{
	mIsOutsideCameraView = !visibility;
}

void Renderable::setGeometryObjects( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material )
{
	assert( mesh );
	assert( material );

	// Check if mesh can be used with this renderable.
	if( validateMesh( mesh ) == false )
		throw std::invalid_argument( "Can't use given mesh. This renderable does not support the mesh's vertex attribute layout." );

	// Remove the previously owned model.
	mModel = nullptr;

	// Create new model that will not be added to the model manager. This model will only be used by this
	// renderable.
	mModel.reset( new Model( "", mCore.getModelManager() ) );

	// Add new section to the model that combines the mesh and material together.
	mModel->addSection( mesh, material );
}

void Renderable::setGeometryIds( const std::string &meshId, const std::string &materialId )
{
	// Get the mesh and material objects.
	std::shared_ptr<Mesh> mesh = mCore.getMeshManager().findMesh( meshId );
	std::shared_ptr<Material> material = mCore.getMaterialManager().findMaterial( materialId );
	setGeometryObjects( mesh, material );
}

void Renderable::setModel( const std::string &modelId )
{
	mModel = nullptr;

	// Find the model from model manager.
	std::shared_ptr<Model> model = mCore.getModelManager().getModel( modelId );
	assert( model );
	if( model == nullptr )
		throw std::invalid_argument( "Renderable can't find model with given model id: " + modelId );

	// Validate the meshes.
	for( size_t sectionIndex = 0; sectionIndex < model->getSectionCount(); ++sectionIndex )
	{
		// Check if we support the given mesh.
		auto modelSection = model->getSection( sectionIndex );
		if( validateMesh( modelSection.mesh ) == false )
			throw std::invalid_argument( "Can't use mesh. This renderable does not support the mesh vertex attribute layout of of given model mesh." );
	}

	// Store the model object.
	mModel = model;
	mOwnModel = false;
}

std::shared_ptr<Model> Renderable::getModel() const
{
	return mModel;
}

Renderable::Type Renderable::getType() const
{
	return Type::NormalRenderable;
}

bool Renderable::validateMesh( std::shared_ptr<Mesh>& mesh ) const
{
	// By default we accept all kinds of meshes.
	return true;
}
