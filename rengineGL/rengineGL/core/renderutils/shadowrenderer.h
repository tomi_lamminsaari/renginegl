
#pragma once

#include <memory>
#include "framebufferobject.h"
#include "../camera.h"
#include "../lightsourcecollection.h"

// Forward declarations.
class RenderCore;

/// <summary>
/// A helper object that can render a shadowmap of the scene to a texture. The shadowmap
/// is suitable for rendering directional lights.
/// <para>
/// By default the light direction is 'Vec3( 0.0f, -1.0f, 1.0f )' and light cone center is
/// in world origo at 'Vec3(0.0f, 0.0f, 0.0f)'. To change these, you should call setLightFocusPos()-method.
/// </para>
/// </summary>
class ShadowRenderer
{
public:

	/// <summary>Default width of the shadowmap texture.</summary>
	static const int KShadowMapTextureWidth = 2048;

	/// <summary>Default height of the shadowmap texture.</summary>
	static const int KShadowMapTextureHeight = 2048;

	/// <summary>Theorethical distance of directional light source.</summary>
	static const float KDirectionalLightSourceDistance;

	/// <summary>Specifies the distance from near plane how far the shadows will be rendered.</summary>
	static const float KMaxShadowRenderDistance;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="core">Render engine's main object.</param>
	ShadowRenderer( RenderCore &core );

	/// <summary>
	/// Destructor.
	/// </summary>
	~ShadowRenderer();

	/// <summary>
	/// Initializes the shadow renderer. Creates the shadow textures and framebuffer objects that are
	/// needed when doing the shadow mapping.
	/// </summary>
	/// <param name="rendererName">Unique name of this shadow renderer.</param>
	/// <returns>Returns true if initialization was successful.</returns>
	bool initialize( const std::string &rendererName );

	/// <summary>
	/// Uninitializes the shadow render. Releases all the GPU resources allocated for the shadow mapping.
	/// </summary>
	void uninitialize();

	/// <summary>
	/// Sets this ShadowRenderer to be the render target. Once called, all rendering will go to
	/// this renderer shadow map texture.
	/// </summary>
	void bindAsRenderTarget();

	/// <summary>
	/// Stops this ShadowRenderer to be the render target. All rendering will go to
	/// default frame buffer window.
	/// </summary>
	void unbindAsRenderTarget();

	/// <summary>
	/// Binds the depth buffer texture of this shadow renderer to specified texture unit.
	/// </summary>
	/// <param name="glTextureUnit">Texture unit where to bind the shadow map texture.</param>
	void bindShadowMapTexture( GLenum glTextureUnit );

	/// <summary>
	/// Updates the region from where the shadows will be rendered.
	/// </summary>
	/// <param name="mainCamera">Main rendering camera. This specifies the region in world that
	/// is visible in the view.</param>
	/// <param name="lightDir">Direction of directional light.</param>
	void updateShadowRegion( const Camera& mainCamera, const Vec3& lightDir );

	/// <summary>
	/// Returns the camera that will be used when rendering the shadow map.
	/// </summary>
	/// <returns>Pointer to light source camera that will be used when rendering the shadow map.</returns>
	std::shared_ptr<Camera> getCamera();

	/// <summary>
	/// Returns the ID of the shadow material this ShadowRenderer is supposed to use.
	/// </summary>
	/// <returns>ID of the shadow material.</returns>
	std::string getShadowMaterial() const;

	/// <summary>
	/// Returns the shadowmap texture.
	/// </summary>
	/// <returns>Pointer to shadowmap texture.</returns>
	std::shared_ptr<Texture> getShadowMapTexture() const;

	/// <summary>
	/// Returns the distance of the directinal light from the center of the visible scene.
	/// </summary>
	/// <returns>Returns the directional light distance.</returns>
	float getDirectionalLightDistance() const;

private:

	/// <summary>The render engine's main object.</summary>
	RenderCore &mCore;

	/// <summary>Name of this shadow renderer</summary>
	std::string mRendererName;

	/// <summary>The framebuffer object that is used when rendering shadowmap to the texture.</summary>
	std::shared_ptr< FrameBufferObject > mShadowFBO;

	/// <summary>The camera at the position of lightsource that is used when rendering the shadowmap.</summary>
	std::shared_ptr<Camera> mLightSourceCamera;

	/// <summary>The size and location of current viewport.</summary>
	GLint mViewportParams[ 4 ];

	/// <summary>The direction of the light when rendering the shadowmap of directional light.</summary>
	Vec3 mLightDir;

	/// <summary>How far away the lightsource of directional light is.</summary>
	float mDirectionalLightDistance;
};
