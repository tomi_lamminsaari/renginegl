
#include "skybox.h"
#include "../../math/size3.h"
#include "../meshcreation/meshcreation.h"
#include "../rendercore.h"
#include "../scene.h"
#include "../mesh.h"
#include "../meshmanager.h"
#include "../renderable.h"
#include "../texturemanager.h"

SkyBox::SkyBox( RenderCore &core ) :
    mCore( core ),
    mEnabled( false ),
    mSkyBoxRenderable( nullptr ),
    mMaterial( BuiltIn::MATERIAL_SKYBOX  )
{

}

SkyBox::~SkyBox()
{

}

void SkyBox::setCenterPosition( const Vec3 &cameraPos )
{
    mSkyBoxPosition = cameraPos;

    // If we have renderable, set it's position to the camera's position.
    if( mSkyBoxRenderable != nullptr )
    {
        mSkyBoxRenderable->setPosition( cameraPos );
    }
}

const Vec3 &SkyBox::getCenterPosition() const
{
    return mSkyBoxPosition;
}

void SkyBox::setEnabled( bool enabled )
{
    // Either create the renderable or destroy the renderable that renders the
    // skybox contents.
    if( enabled )
    {
        // Verify that we have material set.
        if( mMaterial == "" )
            throw std::runtime_error( "Cannot enable SkyBox when material has not been set." );

        // SkyBox is enabled. Make sure we have the renderable.
        if( mSkyBoxRenderable == nullptr )
        {
            // No SkyBox renderable. Create it now.
            mSkyBoxRenderableId = mCore.generateId( RenderCore::IDGenerator::Renderable );
            mSkyBoxRenderable = mCore.getScene()->createRenderable( mSkyBoxRenderableId );
            mSkyBoxRenderable->setGeometryIds( BuiltIn::MESH_SKYBOX, mMaterial );
        }
    }
    else
    {
        // SkyBox is disabled. Destroy the skybox renderable.
        if( mSkyBoxRenderable != nullptr )
        {
            mCore.getScene()->destroyRenderable( mSkyBoxRenderableId );
            mSkyBoxRenderable = nullptr;
            mSkyBoxRenderableId = "";
        }
    }

    mEnabled = enabled;
}

bool SkyBox::isEnabled() const
{
    return mEnabled;
}

void SkyBox::setMaterial( const std::string &material )
{
    mMaterial = material;
}

std::shared_ptr<Renderable> SkyBox::getRenderable()
{
    return mSkyBoxRenderable;
}

std::shared_ptr<Mesh> SkyBox::initSkyBoxMesh( RenderCore &core, const std::string &skyboxMeshId )
{
    // Create the cube mesh for skybox.
    MeshManager &meshman = core.getMeshManager();
    VertexLayout layout3( { VertexElement::Position3f, VertexElement::Texture2f } );
    std::shared_ptr<Mesh> mesh = MeshCreation::createCuboidNotTextured(
        core, BuiltIn::MESH_SKYBOX, layout3, Size3( 1200.0f ), false );

    // Init the skybox vertex coordinates.
    TextureManager::TextureGridParams texGridParams( 1024, 768, 8 );
    texGridParams.setLeftAndBottomOffsets( 1, 1 );
    texGridParams.setRowsAndColumns( 4, 3 );

    // Front faces.
    auto texCoords = texGridParams.calculateTextureCornerCoordinates( 1, 1 );
    mesh->setTexture2D( 0, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 1, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 2, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 3, 0, texCoords[ 3 ] );

    // Right faces.
    texCoords = texGridParams.calculateTextureCornerCoordinates( 2, 1 );
    mesh->setTexture2D( 4, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 5, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 6, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 7, 0, texCoords[ 3 ] );

    // Back faces.
    texCoords = texGridParams.calculateTextureCornerCoordinates( 3, 1 );
    mesh->setTexture2D( 8, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 9, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 10, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 11, 0, texCoords[ 3 ] );

    // Left faces.
    texCoords = texGridParams.calculateTextureCornerCoordinates( 0, 1 );
    mesh->setTexture2D( 12, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 13, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 14, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 15, 0, texCoords[ 3 ] );

    // Up faces.
    texCoords = texGridParams.calculateTextureCornerCoordinates( 1, 2 );
    mesh->setTexture2D( 16, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 17, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 18, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 19, 0, texCoords[ 3 ] );

    // Down faces.
    texCoords = texGridParams.calculateTextureCornerCoordinates( 1, 0 );
    mesh->setTexture2D( 20, 0, texCoords[ 0 ] );
    mesh->setTexture2D( 21, 0, texCoords[ 1 ] );
    mesh->setTexture2D( 22, 0, texCoords[ 2 ] );
    mesh->setTexture2D( 23, 0, texCoords[ 3 ] );

    // The skybox is rendered in inverted order so that visible faces are inside the skybox
    // cube.
    mesh->setVertexOrder( Mesh::FaceWinding::ECounterClockwise );
    return mesh;
}
