
#pragma once

#include <string>
#include <gl\glew.h>

// Forward declarations.
class RenderCore;
class Texture;

/// <summary>
/// An utility class for creating and managing a frame buffer objects that can be used as render targets.
/// </summary>
class FrameBufferObject
{
public:
	typedef unsigned int AttachmentField;
	static const AttachmentField KColorAttachment = 1;
	static const AttachmentField KDepthAttachment = 2;

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="core">The main render engine object.</param>
	FrameBufferObject( RenderCore &core );

	/// <summary>
	/// Destructor.
	/// </summary>
	~FrameBufferObject();

	FrameBufferObject( const FrameBufferObject &obj ) = delete;
	FrameBufferObject &operator=( const FrameBufferObject &obj ) = delete;

public:

	/// <summary>
	/// Sets the unique ID of this frame buffer object.
	/// </summary>
	/// <param name="id">ID of this frame buffer object.</param>
	void setId( const std::string &id );

	/// <summary>
	/// Returns the unique ID of this frame buffer object.
	/// </summary>
	/// <returns>ID of this frame buffer object.</returns>
	const std::string &getId() const;

	/// <summary>
	/// Initializes the frame buffer object to have given size and given elements. Allocates the
	/// resources from GPU memory to accomodate the frame buffer's texture and depth buffers.
	/// </summary>
	/// <param name="attachmentBits">
	/// The bitfield that defines which attachment buffer (such as color and depth)
	/// get attached to the frame buffer object. Different attachments can be or'ed together.
	/// </param>
	/// <param name="width">Width of the render target bitmap.</param>
	/// <param name="height">Height of the render target bitmap.</param>
	/// <returns>Returns true if successful.</returns>
	bool initialize( AttachmentField attachmentBits, int width, int height );

	/// <summary>
	/// Releases the GPU resources allocated for this frame buffer object.
	/// </summary>
	void uninitialize();

	/// <summary>
	/// Activates this framebuffer. Set this to be the rendering target.
	/// </summary>
	void setAsRenderTarget();

	/// <summary>
	/// Deactivates this framebuffer. This framebuffer will not be rendering target anymore.
	/// </summary>
	void removeAsRenderTarget();

	/// <summary>
	/// Binds this framebuffer's color and depth buffers to OpenGL texture units.
	/// </summary>
	/// <param name="colorAttachmentTexUnit">
	/// Texture unit where the color attachment texture will be bound to. Pass zero to not bind color texture.
	/// </param>
	/// <param name="depthAttachmentTexUnit">
	/// Texture unit where the depth attachment texture will be bound to. Pass zero to not bind depth texture.
	/// </param>
	void setAsSourceTexture( GLenum colorAttachmentTexUnit, GLenum depthAttachmentTexUnit );

	/// <summary>
	/// Tells if this framebuffer has been initialized.
	/// </summary>
	/// <returns>Returns true if this has been initialized.</returns>
	bool isInitialized() const;

	/// <summary>
	/// Returns the texture that works as framebuffer's color buffer.
	/// </summary>
	/// <returns>Color texture.</returns>
	std::shared_ptr<Texture> getColorAttachmentTexture() const;

	/// <summary>
	/// Returns the texture that works as framebuffer's depth buffer.
	/// </summary>
	/// <returns>Depth texture.</returns>
	std::shared_ptr<Texture> getDepthAttachmentTexture() const;

private:

	/// <summary>Reference to the engine core.</summary>
	RenderCore &mCore;

	/// <summary>Unique ID of this framebuffer object.</summary>
	std::string mId;

	/// <summary>Width of this framebuffer.</summary>
	GLsizei mWidth;

	/// <summary>Height of this framebuffer.</summary>
	GLsizei mHeight;

	/// <summary>The framebuffer object.</summary>
	GLuint mFBO;

	/// <summary>The texture that will be used as colorbuffer if that is enabled.</summary>
	std::shared_ptr<Texture> mColorAttachmentTex;

	/// <summary>The texture that will be used as depthbuffer if that is enabled.</summary>
	std::shared_ptr<Texture> mDepthAttachmentTex;

	/// <summary>Tells if this framebuffer object has already been bound as rendering target.</summary>
	bool mSetAsRenderTarget;

	/// <summary>The viewport size that was active when setting this framebuffer as rendering target.</summary>
	GLint mPreviousViewPort[ 4 ];

	/// <summary>The current frame buffer attachments.</summary>
	int mAttachmentBits;
};
