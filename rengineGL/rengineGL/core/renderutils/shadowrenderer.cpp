
#include <sstream>
#include "../defines.h"
#include "../rendercore.h"
#include "../scene.h"
#include "../texture.h"
#include "../utilities/loggingutil.h"
#include "shadowrenderer.h"

const float ShadowRenderer::KDirectionalLightSourceDistance = 500.0f;
const float ShadowRenderer::KMaxShadowRenderDistance = 700.0f;

ShadowRenderer::ShadowRenderer( RenderCore &core ) :
	mCore( core ),
	mDirectionalLightDistance( KDirectionalLightSourceDistance ),
	mLightSourceCamera( new Camera() )
{
	memset( mViewportParams, 0, sizeof( mViewportParams ) );
	mLightSourceCamera->setProjectionParameters( 
			-KShadowMapRegionWidth / 2.0f,
			KShadowMapRegionWidth / 2.0f,
			KShadowMapRegionBreadth / 2.0f,
			-KShadowMapRegionBreadth / 2.0f,
			1.0f,
			KMaxShadowRenderDistance );
	mLightSourceCamera->setOrthogonalProjection();

	mLightDir = Vec3( 0.0f, -1.0f, 1.0f ).normalized();
}

ShadowRenderer::~ShadowRenderer()
{
	if( mShadowFBO )
		mCore.destroyFBO( mShadowFBO->getId() );
}

bool ShadowRenderer::initialize( const std::string &rendererName )
{
	mRendererName = rendererName;
	mShadowFBO = mCore.createFBO( mRendererName );
	bool fboSuccess = mShadowFBO->initialize(
			FrameBufferObject::KDepthAttachment, KShadowMapTextureWidth, KShadowMapTextureHeight );
	return fboSuccess;
}

void ShadowRenderer::uninitialize()
{
	if( mShadowFBO )
		mCore.destroyFBO( mShadowFBO->getId() );
	mShadowFBO = std::shared_ptr< FrameBufferObject >();
}

void ShadowRenderer::bindAsRenderTarget()
{
	// Take the current viewport.
	glGetIntegerv( GL_VIEWPORT, mViewportParams );

	// If initialized, set our own framebuffer to be the render target.
	if( mShadowFBO )
		mShadowFBO->setAsRenderTarget();
}

void ShadowRenderer::unbindAsRenderTarget()
{
	// If initialized, stop our framebuffer to be the render target.
	if( mShadowFBO )
		mShadowFBO->removeAsRenderTarget();

	// Restore the old viewport.
	GLASSERT( glViewport( mViewportParams[ 0 ], mViewportParams[ 1 ], mViewportParams[ 2 ], mViewportParams[ 3 ] ) );
}

void ShadowRenderer::bindShadowMapTexture( GLenum glTextureUnit )
{
	if( mShadowFBO && mShadowFBO->getDepthAttachmentTexture() != nullptr )
	{
		// Bind texture to OpenGL texture unit.
		GLASSERT( glActiveTexture( glTextureUnit ) );
		GLASSERT( glBindTexture( GL_TEXTURE_2D,
				mShadowFBO->getDepthAttachmentTexture()->getOpenGLTextureObject() ) );
		GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ) );
		GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ) );
	}
}

void ShadowRenderer::updateShadowRegion( const Camera& mainCamera, const Vec3& lightDir )
{
	auto frustumCoords = mainCamera.getFrustumWorldCoordinates();
	Vec3 nearCenter = ( frustumCoords[4] + frustumCoords[6] ) * 0.5f;
	Vec3 lightFocusPoint = nearCenter;

	mLightDir = lightDir;
	Vec3 lightCamPos = lightFocusPoint - ( mLightDir * mDirectionalLightDistance );
	mLightSourceCamera->setCameraVectors( lightCamPos, lightFocusPoint, Vec3( 0.0f, 1.0f, 0.0f ) );
	mLightSourceCamera->updateMatrices();
}

std::shared_ptr<Camera> ShadowRenderer::getCamera()
{
	return mLightSourceCamera;
}

std::string ShadowRenderer::getShadowMaterial() const
{
	return BuiltIn::MATERIAL_SHADOWMAP;
}

std::shared_ptr<Texture> ShadowRenderer::getShadowMapTexture() const
{
	if( mShadowFBO )
		return mShadowFBO->getDepthAttachmentTexture();
	return nullptr;
}

float ShadowRenderer::getDirectionalLightDistance() const
{
	return mDirectionalLightDistance;
}
