
#include <map>
#include <sstream>
#include "framebufferobject.h"
#include "../defines.h"
#include "../rendercore.h"
#include "../texture.h"
#include "../texturemanager.h"
#include "../textureparameters.h"
#include "../utilities/glerrors.h"

FrameBufferObject::FrameBufferObject( RenderCore &core ) :
	mCore( core ),
	mWidth( 0 ),
	mHeight( 0 ),
	mFBO( 0 ),
	mSetAsRenderTarget( false ),
	mColorAttachmentTex( nullptr ),
	mDepthAttachmentTex( nullptr ),
	mAttachmentBits( 0 )
{
	// Clear the old viewport buffer to default values.
	memset( mPreviousViewPort, 0, sizeof( mPreviousViewPort ) );
}

FrameBufferObject::~FrameBufferObject()
{
	uninitialize();
}

void FrameBufferObject::setId( const std::string &id )
{
	mId = id;
}

const std::string &FrameBufferObject::getId() const
{
	return mId;
}

bool FrameBufferObject::initialize( AttachmentField attachmentBits, int width, int height )
{
	// Release the current framebuffer and textures if initialize() is called again.
	uninitialize();

	// Store the parameters.
	mAttachmentBits = attachmentBits;
	mWidth = width;
	mHeight = height;

	// Create color buffer texture if requested.
	if( ( attachmentBits & KColorAttachment ) != 0 )
	{
		// Generate the texture for color attachment.
		std::string colorAttachmentTexId = mCore.generateId( RenderCore::IDGenerator::Texture );
		TextureParameters colorTexParams( static_cast< GLsizei >( width ), static_cast< GLsizei >( height ),
				TextureParameters::Format::Rgb );
		mColorAttachmentTex = mCore.getTextureManager().createTextureWithParams( colorAttachmentTexId, colorTexParams );
	}

	// Create depth buffer texture if requested.
	if( ( attachmentBits & KDepthAttachment ) != 0 )
	{
		// Create texture for depth data.
		std::string depthAttachmentTexId = mCore.generateId( RenderCore::IDGenerator::Texture );
		TextureParameters depthTexParams( static_cast< GLsizei >( width ), static_cast< GLsizei >( height ),
				TextureParameters::Format::Depth );
		depthTexParams.setGlParameterf( GL_TEXTURE_MIN_FILTER, GL_LINEAR );
		depthTexParams.setGlParameterf( GL_TEXTURE_MAG_FILTER, GL_LINEAR );
		depthTexParams.setGlParameteri( GL_TEXTURE_COMPARE_MODE, GL_COMPARE_REF_TO_TEXTURE );
		//depthTexParams.setGlParameteri( GL_DEPTH_TEXTURE_MODE, GL_INTENSITY );
		depthTexParams.setGlParameteri( GL_TEXTURE_COMPARE_FUNC, GL_LEQUAL );
		depthTexParams.setGlParameterf( GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
		depthTexParams.setGlParameterf( GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
		mDepthAttachmentTex = mCore.getTextureManager().createTextureWithParams( depthAttachmentTexId, depthTexParams );
		mDepthAttachmentTex->setMipMaps( false );
		mDepthAttachmentTex->setWrapMode( Texture::Wrap::ClampToEdge, Texture::Wrap::ClampToEdge );
	}

	// Create the framebuffer.
	GLASSERT( glGenFramebuffers( 1, &mFBO ) );
	if( mFBO == 0 )
		throw std::runtime_error( "FrameBufferObject::intialize() failed to create framebuffer." );
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, mFBO ) );

	// Bind color buffer if requested.
	if( mColorAttachmentTex != 0 )
	{
		// Bind color buffer and enable drawing to it.
	}
	else
	{
		// Disable writing to color buffer.
		glDrawBuffer( GL_NONE );
	}

	// Bind depth buffer if requested.
	if( mDepthAttachmentTex != 0 )
	{
		GLASSERT( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D,
				mDepthAttachmentTex->getOpenGLTextureObject(), 0 ) );
	}
	else
	{
		// TODO:
	}

	// Check if everything is ok.
	GLenum status = glCheckFramebufferStatus( GL_FRAMEBUFFER );
	if( status != GL_FRAMEBUFFER_COMPLETE )
	{
		// Framebuffer creation failed. Throw an error.
		std::string errorCodeString = GLErrors::getErrorText( status );
		throw std::runtime_error( "Framebuffer object initialization failed. Error: " + errorCodeString );
	}

	// Unbind the framebuffer.
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );

	return true;
}

void FrameBufferObject::uninitialize()
{
	// Release color attachment texture.
	if( mColorAttachmentTex != nullptr )
	{
		mCore.getTextureManager().destroyTexture( mColorAttachmentTex->getId() );
		mColorAttachmentTex = nullptr;
	}

	// Release depth attachment texture.
	if( mDepthAttachmentTex != nullptr )
	{
		mCore.getTextureManager().destroyTexture( mDepthAttachmentTex->getId() );
		mDepthAttachmentTex = nullptr;
	}

	// Release the framebuffer object.
	if( mFBO != 0 )
	{
		GLASSERT( glDeleteFramebuffers( 1, &mFBO ) );
		mFBO = 0;
	}

	mAttachmentBits = 0;
	mWidth = 0;
	mHeight = 0;
}

void FrameBufferObject::setAsRenderTarget()
{
	// Allowing using only initialized framebuffer object.
	if( mFBO != 0 )
	{
		// Make sure that this framebuffer has not been set as render target already.
		if( mSetAsRenderTarget )
			throw std::runtime_error( "Framebuffer has been set as render target already." );
		mSetAsRenderTarget = true;

		// Take the previous viewport information.
		glGetIntegerv( GL_VIEWPORT, mPreviousViewPort );

		// Set the framebuffer as rendering target. Set the viewport to match the framebuffer
		// texture size.
		GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, mFBO ) );
		GLASSERT( glViewport( 0, 0, mWidth, mHeight ) );
	}
	else
	{
		throw std::runtime_error( "Attempting to activate framebuffer that has not been initialized." );
	}
}

void FrameBufferObject::removeAsRenderTarget()
{
	// Set the default framebuffer back.
	if( mSetAsRenderTarget )
	{
		// Set the default framebuffer. Restore the default viewport.
		GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
		GLASSERT( glViewport( mPreviousViewPort[ 0 ], mPreviousViewPort[ 1 ], mPreviousViewPort[ 2 ], mPreviousViewPort[ 3 ] ) );

		// Release the "set as rendering target" flag.
		mSetAsRenderTarget = false;
	}
	else
	{
		throw std::runtime_error( "Framebuffer has not been set as rendering target so can't unset." );
	}
}

void FrameBufferObject::setAsSourceTexture( GLenum colorAttachmentTexUnit, GLenum depthAttachmentTexUnit )
{
	// Bind the color attachment texture.

}

bool FrameBufferObject::isInitialized() const
{
	return mFBO != 0;
}

std::shared_ptr<Texture> FrameBufferObject::getColorAttachmentTexture() const
{
	return mColorAttachmentTex;
}

std::shared_ptr<Texture> FrameBufferObject::getDepthAttachmentTexture() const
{
	return mDepthAttachmentTex;
}
