
#pragma once

#include <string>
#include "../../math/vec3.h"
#include "../model.h"

// Forward declarations.
class Camera;
class Mesh;
class RenderCore;
class Renderable;

/// <summary>
/// SkyBox can render the sky and the horizon to the view that stays always equally far from the camera.
/// </summary>
class SkyBox
{
public:
    SkyBox( RenderCore &core );
    ~SkyBox();
    SkyBox( const SkyBox &obj ) = delete;
    SkyBox &operator=( const SkyBox &obj ) = delete;

public:
    void setCenterPosition( const Vec3 &cameraPos );
    const Vec3 &getCenterPosition() const;
    void setEnabled( bool enabled );
    bool isEnabled() const;
    void setMaterial( const std::string &skyMaterial );
    std::shared_ptr<Renderable> getRenderable();
    void renderSkyBox( Camera *camera );

    /// <summary>
    /// Initializes and creates skybox mesh and adds it to the meshmanager.
    /// <param name="core">The render engine.</param>
    /// <param name="skyboxMeshId">ID of the skybox mesh to create.</param>
    /// <returns>Pointer to new skybox mesh.</returns>
    /// </summary>
    static std::shared_ptr<Mesh> initSkyBoxMesh( RenderCore &core, const std::string &skyboxMeshId );

private:
    /// <summary>Declares if skybox has been enabled and therefor rendered.</summary>
    bool mEnabled;

    /// <summary>The material this skybox uses to render the skybox.</summary>
    std::string mMaterial;

    /// <summary>Reference to the rendering core's parimary object.</summary>
    RenderCore &mCore;

    /// <summary>The center position of the skybox when redering the view.</summary>
    Vec3 mSkyBoxPosition;

    /// <summary>The renderable object that is used for rendering the skybox contents.</summary>
    std::shared_ptr<Renderable> mSkyBoxRenderable;

    /// <summary>ID of the skybox's renderable object.</summary>
    std::string mSkyBoxRenderableId;
};