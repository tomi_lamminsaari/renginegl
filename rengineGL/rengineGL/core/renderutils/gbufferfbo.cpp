
#include <boost/format.hpp>
#include "../defines.h"
#include "gbufferfbo.h"

constexpr uint32_t KBufferIndexDiffuseSpecular = 0;
constexpr uint32_t KBufferIndexNormals = 1;
constexpr uint32_t KBufferIndexPositions = 2;
constexpr uint32_t KBufferIndexDepth = 3;

std::unique_ptr<GBufferFBO> GBufferFBO::createGBufferFBO( RenderCore& core, uint32_t bufferWidth, uint32_t bufferHeight )
{
	if( bufferWidth > 4096 || bufferHeight > 4096 )
		throw std::invalid_argument( ( boost::format( "Invalid size for GBufferFBO %1%x%2%." ) % bufferWidth % bufferHeight ).str() );

	std::unique_ptr<GBufferFBO> buffer = std::make_unique<GBufferFBO>( core );
	buffer->initializeWithSize( bufferWidth, bufferHeight );
	return buffer;
}

GBufferFBO::GBufferFBO( RenderCore& core ) :
	mCore( core ),
	mBufferWidth( 0 ),
	mBufferHeight( 0 ),
	mFrameBufferGL( 0 ),
	mDepthBufferGL( 0 ),
	mGBufferTextures( { 0, 0, 0 } ),
	mPreviousViewPort( { 0, 0, 0, 0 } ),
	mIsRenderTarget( false )
{

}

GBufferFBO::~GBufferFBO()
{
	uninitialize();
}

void GBufferFBO::setAsRenderTarget( GLbitfield clearAttachments )
{
	// Make sure we have initialized this FBO.
	if( mFrameBufferGL == 0 )
		throw RengineError( "Attempting to set uninitialized GBufferFBO as render target." );

	// Take the previous viewport information.
	glGetIntegerv( GL_VIEWPORT, mPreviousViewPort.data() );

	// Bind the FBO to use.
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, mFrameBufferGL ) );

	// Set the viewport to match target FBO size.
	GLASSERT( glViewport( 0, 0, static_cast< GLsizei >( mBufferWidth ), static_cast< GLsizei >( mBufferHeight ) ) );

	// Clear the framebuffer if that is requested.
	if( clearAttachments != 0 )
	{
		glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
		glClear( clearAttachments );
	}
	mIsRenderTarget = true;	
}

void GBufferFBO::removeAsRenderTarget()
{
	// Has this been properly intialized.
	if( mFrameBufferGL == 0 )
		throw RengineError( "Attempting to remove uninitialized GBufferFBO from render target." );

	// Is this FBO currently bound to be the target.
	if( ! mIsRenderTarget )
		throw RengineError( "Attempting to remove GBufferFBO from render target that has not been set to be render target." );

	// Restore the default framebuffer.
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );
	GLASSERT( glViewport( mPreviousViewPort[ 0 ], mPreviousViewPort[ 1 ], mPreviousViewPort[ 2 ], mPreviousViewPort[ 3 ] ) );
}

void GBufferFBO::initializeWithSize( uint32_t bufferWidth, uint32_t bufferHeight )
{
	mBufferWidth = static_cast< GLsizei >( bufferWidth );
	mBufferHeight = static_cast< GLsizei >( bufferHeight );

	// Create framebuffer.
	GLASSERT( glGenFramebuffers( 1, &mFrameBufferGL ) );
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, mFrameBufferGL ) );

	// Create textures for G-buffers.
	GLASSERT( glGenTextures( static_cast< GLsizei >( mGBufferTextures.size() ), mGBufferTextures.data() ) );

	// 1st the Diffuse color buffer + specular coefficient.
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexDiffuseSpecular ] ) );
	GLASSERT( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, mBufferWidth, mBufferHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, nullptr ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0 ) );
	GLASSERT( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexDiffuseSpecular ], 0 ) );

	// 2nd the Normals buffer
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexNormals ] ) );
	GLASSERT( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, mBufferWidth, mBufferHeight, 0, GL_RGBA, GL_FLOAT, nullptr ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0 ) );
	GLASSERT( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexNormals ], 0 ) );

	// 3rd the Positions buffer.
	GLASSERT( glBindTexture( GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexPositions ] ) );
	GLASSERT( glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA16F, mBufferWidth, mBufferHeight, 0, GL_RGBA, GL_FLOAT, nullptr ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST ) );
	GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 0 ) );
	GLASSERT( glFramebufferTexture2D( GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, mGBufferTextures[ KBufferIndexPositions ], 0 ) );

	// Prepare the depth buffer.
	GLASSERT( glGenRenderbuffers( 1, &mDepthBufferGL ) );
	GLASSERT( glBindRenderbuffer( GL_RENDERBUFFER, mDepthBufferGL ) );
	GLASSERT( glRenderbufferStorage( GL_RENDERBUFFER, GL_DEPTH_COMPONENT, mBufferWidth, bufferHeight ) );
	GLASSERT( glFramebufferRenderbuffer( GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, mDepthBufferGL ) );
	
	// Initialize 3 color attachments to the new framebuffer.
	std::array<GLenum, 3> drawbuffers{ GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	GLASSERT( glDrawBuffers( static_cast< GLsizei >( drawbuffers.size() ), drawbuffers.data() ) );
	GLenum framebufferStatus = glCheckFramebufferStatus( GL_FRAMEBUFFER );

	// Restore the original framebuffer.
	GLASSERT( glBindFramebuffer( GL_FRAMEBUFFER, 0 ) );

	// Throw an error if framebuffer creation failed.
	if( framebufferStatus != GL_FRAMEBUFFER_COMPLETE )
		throw std::runtime_error( "Framebuffer initialization failed." );
}

void GBufferFBO::uninitialize()
{
	glDeleteTextures( static_cast< GLsizei >( mGBufferTextures.size() ), mGBufferTextures.data() );
	mGBufferTextures = std::array< GLuint, 3 >( { 0, 0, 0 } );
	glDeleteFramebuffers( 1, &mFrameBufferGL );
	mFrameBufferGL = 0;
	glDeleteRenderbuffers( 1, &mDepthBufferGL );
	mDepthBufferGL = 0;
}

void GBufferFBO::setId( const std::string& gbufferId )
{
	mId = gbufferId;
}

const std::string GBufferFBO::getId() const
{
	return mId;
}

void GBufferFBO::bindAsSourceTexture( TargetTexture fboTexture, GLenum textureUnit )
{
	if( mFrameBufferGL == 0 )
		throw RengineError( "Cannot bind GBufferFBO as source texture. GBufferFBO object has not been initialized." );

	// Determine the texture.
	GLuint textureObject = 0;
	switch( fboTexture )
	{
	case TargetTexture::NormalsTexture:
		textureObject = mGBufferTextures[ KBufferIndexNormals ];
		break;

	case TargetTexture::DiffuseSpecularTexture:
		textureObject = mGBufferTextures[ KBufferIndexDiffuseSpecular ];
		break;

	case TargetTexture::PositionsTexture:
		textureObject = mGBufferTextures[ KBufferIndexPositions ];
		break;

	case TargetTexture::DepthTexture:
		assert( false );  // TODO: Remove this branch?
		//textureObject = mGBufferTextures[ KBufferIndexDepth ];
		break;
	default:
		throw std::invalid_argument( "Unknown target texture received when setting source bitmap." );
	}
	GLASSERT( glActiveTexture( textureUnit ) );
	GLASSERT( glBindTexture( GL_TEXTURE_2D, textureObject ) );
}
