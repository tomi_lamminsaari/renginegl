
#pragma once

// Forward declarations.
class RenderCore;
class Texture;

/// <summary>
/// A collection of utility functions for rendering some primitive things like lines and textured quads.
/// </summary>
class RenderPrimitives
{
public:

	/// <summary>
	/// Draws untransformed textured quad. Useful to draw a quad to the screen to see
	/// the actual contents of a texture.
	/// </summary>
	/// <param name="core">The render engine's core object.</param>
	/// <param name="texture">The texture to be rendered.</param>
	static void renderUntransformedTexturedQuad(
		RenderCore &core,
		const std::shared_ptr<Texture> &texture );
};
