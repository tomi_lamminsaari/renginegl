
#include <GL/glew.h>
#include "../defines.h"
#include "../material.h"
#include "../mesh.h"
#include "../meshmanager.h"
#include "../rendercore.h"
#include "../shaderprogram.h"
#include "../shadermanager.h"
#include "../texture.h"
#include "../texturemanager.h"
#include "renderprimitives.h"

void RenderPrimitives::renderUntransformedTexturedQuad(
	RenderCore &core,
	const std::shared_ptr<Texture> &texture )
{
	std::shared_ptr<Mesh> quadMesh = core.getMeshManager().findMesh( BuiltIn::MESH_TEXTURE_PREVIEW );
	std::shared_ptr<ShaderProgram> program = core.getShaderManager().findShaderProgram( BuiltIn::SHADER_UNTRANSFORMED_TEXTURE );
	if( quadMesh->getVAO() == 0 )
		return;

	// Prepare the drawing by binding the texture, shader and array buffers. Also
	// other rendering options will be initialized.
	texture->activateTexture( Material::KCustomTextureIndex0 );
	program->activateProgram();
	GLASSERT( glDisable( GL_DEPTH_TEST ) );
	GLASSERT( glFrontFace( GL_CW ) );
	GLASSERT( glDisable( GL_CULL_FACE ) );
	GLASSERT( glBindVertexArray( quadMesh->getVAO() ) );

	// Darw the quad.
	GLASSERT( glDrawElements( GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0 ) );

	// Unset everything.
	GLASSERT( glBindVertexArray( 0 ) );
	program->deactivateProgram();
}