
#pragma once

#include <GL/glew.h>
#include <array>
#include <memory>
#include <stdexcept>
#include "../rengineexception.h"

// Forward declarations.
class RenderCore;

/// <summary>
/// GBufferFBO is a framebuffer object that works as a rendering target when using
/// deferred shading mode for rendering. GBufferFBO contains rendering targets
/// for position-map, normal-map, diffuse color and specular intensitư.
/// <para>
/// This G-buffer implementation consists of following output texture structure.
/// - The 'NormalsTexture' texture receives the normal vectors (as RGB).
/// - The 'DiffuseSpecularTexture' texture works as combined albeido/specular coefficient texture. The
/// RGB-contains the albeido color from of the fragment and alpha-contains the specular coefficient.
/// - The 'PositionsTexture' receives the world position of the fragment.
/// </para>
/// <para>
/// TODO: This GBufferFBO should be combined with FrameBufferObject class so that we wouldn't
/// need two separate framebuffer implementations.
/// </para>
/// </summary>
class GBufferFBO
{
public:

	enum class TargetTexture { NormalsTexture, DiffuseSpecularTexture, PositionsTexture, DepthTexture };

	/// <summary>
	/// Constructs new buffer object but allows caller to define the size of it.
	/// </summary>
	/// <param name="core">The render engine's core object.</param>
	/// <param name="bufferWidth">Width of the framebuffer textures.</param>
	/// <param name="bufferHeight">Height of the framebuffer textures.</param>
	/// <returns>New instance of framebuffer.</param>
	/// <exception cref="std::invalid_argument">Thrown, if given buffer width or height are too large.</param>
	static std::unique_ptr<GBufferFBO> createGBufferFBO( RenderCore& core, uint32_t bufferWidth, uint32_t bufferHeight );

	GBufferFBO( RenderCore& core );
	~GBufferFBO();
	GBufferFBO( const GBufferFBO& ) = delete;
	GBufferFBO& operator=( const GBufferFBO& ) = delete;

public:
	/// <summary>
	/// Sets this FBO to be the render target.
	/// </summary>
	/// <param name="clearAttachments">The framebuffer attachments to be cleared when FBO is set as rendertarget.</param>
	/// <exception cred="RengineError">Thrown, if FBO has not been properly initialized.</exception>
	void setAsRenderTarget( GLbitfield clearAttachments );

	/// <summary>
	/// Removes this FBO from being the render target.
	/// </summary>
	/// <exception cred="RengineError">
	/// Thrown, if FBO has not been properly initialized. Also, if this FBO has not been set as render target.
	/// </exception>
	void removeAsRenderTarget();

	/// <summary>
	/// Returns the width of this framebuffer.
	/// </summary>
	/// <returns>Width of framebuffer.</returns>
	uint32_t getWidth() const;

	/// <summary>
	/// Returns the height of this framebuffer.
	/// </summary>
	/// <returns>Height of framebuffer.</returns>
	uint32_t getHeight() const;

	/// <summary>
	/// Sets the ID of the GBuffer object.
	/// </summary>
	/// <param name="gbufferId">The ID of this GBuffer.</param>
	void setId( const std::string& gbufferId );

	/// <summary>
	/// Returns the ID of this GBUffer object.
	/// </summary>
	/// <returns>The ID of this GBuffer object.</returns>
	const std::string getId() const;

	/// <summary>
	/// Binds the textures of this FBO to texture units so that rendering output of this
	/// FBO can be used as input texture for next draw calls.
	/// </summary>
	/// <param name="fboTexture">The subtexture item we have.</param>
	/// <param name="textureUnit">The OpenGL texture unit where to bind the texture to.</param>
	void bindAsSourceTexture( TargetTexture fboTexture, GLenum textureUnit );

private:
	/// <summary>
	/// Initializes this framebuffer object and creates all the target textures for it.
	/// </summary>
	/// <param name="bufferWidth">Width of the textures.</param>
	/// <param name="bufferHeight">Height of the textures.</param>
	/// <exception cref="RengineError">Thrown, if we fail to create all the required resources.</exception>
	void initializeWithSize( uint32_t bufferWidth, uint32_t bufferHeight );

	/// <summary>
	/// Release the allocated framebufer objects.
	/// </summary>
	void uninitialize();

private:
	/// <summary>Reference to render engine's core object.</summary>
	RenderCore& mCore;

	/// <summary>Width of the textures used by this framebuffer object.</summary>
	uint32_t mBufferWidth;

	/// <summary>Height of the textures used by this framebuffer object.</summary>
	uint32_t mBufferHeight;

	/// <summary>The framebuffer object ID.</summary>
	GLuint mFrameBufferGL;

	GLuint mDepthBufferGL;

	/// <summary>The rendering target textures of this FBO.</summary>
	std::array<GLuint, 3> mGBufferTextures;

	/// <summary>Previous viewport settings when activating this GBufferFBO.</summary>
	std::array<GLsizei, 4> mPreviousViewPort;

	/// <summary>A flag that tells if this FBO has been set as render target.</summary>
	bool mIsRenderTarget;

	/// <summary>ID of this GBuffer object.</summary>
	std::string mId;
};
