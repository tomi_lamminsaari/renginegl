
#include "lightsources.h"
#include "renderpassinfo.h"

/*****************************************************************************
* class SceneGlobalRenderPassInfo
*****************************************************************************/

SceneGlobalRenderPassInfo::SceneGlobalRenderPassInfo() :
	mAmbientLightEnabled( false ),
	mSpecularLightEnabled( false ),
	mAmbientLightColor( 1.0f, 1.0f, 1.0f ),
	mAmbientLightIntensity( 0.1f ),
	mDiffuseLightPos( 0.0f, 1.0f, 0.0f ),
	mDiffuseLightColor( 1.0f, 1.0f, 1.0f ),
	mDiffuseLightIntensity( 0.5f ),
	mSpecularLightColor( Rgba::White ),
	mSpecularLightIntensity( 1.0f ),
	mOverrideMaterial( nullptr )
{

}

SceneGlobalRenderPassInfo::~SceneGlobalRenderPassInfo()
{

}

SceneGlobalRenderPassInfo::SceneGlobalRenderPassInfo( const SceneGlobalRenderPassInfo &obj ) :
	mAmbientLightEnabled( obj.mAmbientLightEnabled ),
	mSpecularLightEnabled( obj.mSpecularLightEnabled ),
	mAmbientLightColor( obj.mAmbientLightColor ),
	mAmbientLightIntensity( obj.mAmbientLightIntensity ),
	mDiffuseLightPos( obj.mDiffuseLightPos ),
	mDiffuseLightColor( obj.mDiffuseLightColor ),
	mDiffuseLightIntensity( obj.mDiffuseLightIntensity ),
	mSpecularLightColor( obj.mSpecularLightColor ),
	mSpecularLightIntensity( obj.mSpecularLightIntensity ),
	mOverrideMaterial( obj.mOverrideMaterial )
{

}

SceneGlobalRenderPassInfo &SceneGlobalRenderPassInfo::operator = ( const SceneGlobalRenderPassInfo &obj )
{
	if( this != &obj )
	{
		mAmbientLightEnabled = obj.mAmbientLightEnabled;
		mSpecularLightEnabled = obj.mSpecularLightEnabled;
		mAmbientLightColor = obj.mAmbientLightColor;
		mAmbientLightIntensity = obj.mAmbientLightIntensity;
		mDiffuseLightPos = obj.mDiffuseLightPos;
		mDiffuseLightColor = obj.mDiffuseLightColor;
		mDiffuseLightIntensity = obj.mDiffuseLightIntensity;
		mSpecularLightColor = obj.mSpecularLightColor;
		mSpecularLightIntensity = obj.mSpecularLightIntensity;
		mOverrideMaterial = obj.mOverrideMaterial;
	}
	return *this;
}

void SceneGlobalRenderPassInfo::setAmbientLight( const LightColorInfo &ambientLight )
{
	mAmbientLightColor = ambientLight.getColor();
	mAmbientLightIntensity = ambientLight.getIntensity();
	mAmbientLightEnabled = true;
}

void SceneGlobalRenderPassInfo::setDiffuseLight( const Rgba &diffuseColor, const Vec3 &lightPos, GLfloat lightIntensity )
{
	mDiffuseLightColor = diffuseColor;
	mDiffuseLightPos = lightPos;
	mDiffuseLightIntensity = lightIntensity;
}

void SceneGlobalRenderPassInfo::setSpecularLight( const LightColorInfo &specularLight )
{
	mSpecularLightColor = specularLight.getColor();
	mSpecularLightIntensity = specularLight.getIntensity();
	mSpecularLightEnabled = true;
}

void SceneGlobalRenderPassInfo::setOverrideMaterial( std::shared_ptr<Material> overrideMaterial )
{
	mOverrideMaterial = overrideMaterial;
}

std::shared_ptr<Material> SceneGlobalRenderPassInfo::getOverrideMaterial() const
{
	return mOverrideMaterial;
}

/*****************************************************************************
* class RenderPassInfo
*****************************************************************************/

RenderPassInfo::RenderPassInfo( SceneGlobalRenderPassInfo *globalRenderPass ) :
	mGlobalRenderPass( globalRenderPass ),
	mVAO( 0 ),
	mDrawMode( GL_TRIANGLES ),
	mElementType( GL_UNSIGNED_INT ),
	mDrawCount( 0 ),
	mVBO( 0 ),
	mIBO( 0 ),
	mFaceWindingOrder( GL_CCW )
{

}

RenderPassInfo::~RenderPassInfo()
{

}

RenderPassInfo::RenderPassInfo( const RenderPassInfo &obj ) :
	mVAO( obj.mVAO ),
	mDrawMode( obj.mDrawMode ),
	mElementType( obj.mElementType ),
	mDrawCount( obj.mDrawCount ),
	mVBO( obj.mVBO ),
	mIBO( obj.mIBO )
{

}

RenderPassInfo &RenderPassInfo::operator = ( const RenderPassInfo &obj )
{
	if( &obj != this )
	{
		mVAO = obj.mVAO;
		mDrawMode = obj.mDrawMode;
		mElementType = obj.mElementType;
		mDrawCount = obj.mDrawCount;
		mVBO = obj.mVBO;
		mIBO = obj.mIBO;
	}
	return *this;
}

void RenderPassInfo::setVAO( GLenum drawMode, GLuint vao, GLsizei drawCount )
{
	mVAO = vao;
	mDrawMode = drawMode;
	mDrawCount = drawCount;
}
