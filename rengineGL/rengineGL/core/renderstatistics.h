
#pragma once

#include <vector>

class RenderStatistics
{
public:
	RenderStatistics();
	~RenderStatistics();
	RenderStatistics( const RenderStatistics &obj );
	RenderStatistics& operator= ( const RenderStatistics &obj );

public:
	void increaseRenderPassCount();
	void increaseProgramChangeCount();
	void includeAabbZ( float z );
	size_t getRenderPassCount() const;
	size_t getProgramChangeCount() const;
	void reset();
		
private:
	size_t mRenderPassCount;
	float mAabbSmallestZ;
	float mAabbLargestZ;
	size_t mProgramChangeCount;
};
