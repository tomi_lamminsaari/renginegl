
#include <rapidxml/rapidxml.hpp>
#include "../utils/conversionutils.h"
#include "../utils/rapidxmlutils.h"
#include "../utils/stringutils.h"
#include "defines.h"
#include "rendercore.h"
#include "rengineexception.h"
#include "texture.h"
#include "texturemanager.h"

const std::string TextureManager::KTextureAttribMipmapYes = "yes";
const std::string TextureManager::KTextureAttribMipmapNo = "no";
const std::string TextureManager::KTextureWrapRepeat = "repeat";
const std::string TextureManager::KTextureWrapMirroredRepeat = "mirror";
const std::string TextureManager::KTextureWrapClampToEdge = "clamptoedge";
const std::string TextureManager::KTextureWrapClampToBorder = "clamptoborder";

//
// class TextureGridParams
//
TextureManager::TextureGridParams::TextureGridParams( unsigned int w, unsigned int h, unsigned int guardZoneWidth ) :
	mWidth( w ),
	mHeight( h ),
	mGridColumns( 1 ),
	mGridRows( 1 ),
	mTextureGuardZoneWidth( guardZoneWidth ),
	mOffsetX( 0 ),
	mOffsetY( 0 )
{
	if( w == 0 )
		throw std::invalid_argument( "Texture width cannot be zero." );
	if( h == 0 )
		throw std::invalid_argument( "Texture height cannot be zero." );
}

void TextureManager::TextureGridParams::setLeftAndBottomOffsets( int offsetX, int offsetY )
{
	mOffsetX = offsetX;
	mOffsetY = offsetY;
}

void TextureManager::TextureGridParams::setRowsAndColumns( unsigned int columnCount, unsigned int rowCount )
{
	mGridColumns = columnCount;
	mGridRows = rowCount;
}

std::vector< Vec2 > TextureManager::TextureGridParams::calculateTextureCornerCoordinates(
	unsigned int gridColumnIndex,
	unsigned int gridRowIndex ) const
{
	float subTextureWidth = static_cast< float >( mWidth ) / static_cast< float >( mGridColumns );
	float subTextureHeight = static_cast< float >( mHeight ) / static_cast< float >( mGridRows );

	// Calculate texture X-coordinates.
	float leftX = subTextureWidth * static_cast< float >( gridColumnIndex );
	float rightX = leftX + subTextureWidth - mTextureGuardZoneWidth;
	leftX += static_cast< float >( mOffsetX );

	// Calculate texture Y-coordiantes. In textures the top Y has bigger value than bottom Y.
	float bottomY = subTextureHeight * static_cast< float >( gridRowIndex );
	float topY = bottomY + subTextureHeight - static_cast< float >( mTextureGuardZoneWidth );
	bottomY += static_cast< float >( mOffsetY );

	// Scale the coordinates to range 0.0 - 1.0.
	float fullWidth = static_cast< float >( mWidth );
	float fullHeight = static_cast< float >( mHeight );
	std::vector< Vec2 > coords;
	coords.push_back( Vec2( leftX / fullWidth, bottomY / fullHeight ) );
	coords.push_back( Vec2( rightX / fullWidth, bottomY / fullHeight ) );
	coords.push_back( Vec2( rightX / fullWidth, topY / fullHeight ) );
	coords.push_back( Vec2( leftX / fullWidth, topY / fullHeight ) );
	return coords;
}

//
// class TextureManager
//
TextureManager::TextureManager( RenderCore &renderCore ) :
	ManagerBase( renderCore ),
	mAllTexturesUploaded( false )
{

}

TextureManager::~TextureManager()
{
	removeFromGPU();
	mTextures.clear();
}

void TextureManager::cleanup()
{
	removeFromGPU();
	mTextures.clear();
}

std::shared_ptr<Texture> TextureManager::createTexture( const std::string& textureId )
{
	// Check if texture with given ID exists already.
	std::shared_ptr<Texture> existingTexture = findTexture( textureId );
	if( existingTexture != nullptr )
	{
		// Texture with ID exists already. This is an error situation because we can't create new texture.
		return nullptr;
	}

	// Create new texture.
	std::shared_ptr<Texture> texture( new Texture( *this ) );
	texture->setId( textureId );
	mTextures[ textureId ] = texture;
	return texture;
}

std::shared_ptr<Texture> TextureManager::createTexture( const std::string& textureId, const std::string& textureFile )
{
	// Check if texture with given ID exists already.
	std::shared_ptr<Texture> existingTexture = findTexture( textureId );
	if( existingTexture != nullptr )
	{
		return existingTexture;
	}

	std::shared_ptr<Texture> texture( new Texture( *this ) );
	texture->setId( textureId );
	texture->setTextureFilePath( textureFile );
	mTextures[ textureId ] = texture;
	return texture;
}

std::shared_ptr<Texture> TextureManager::createTextureWithParams(
	const std::string& textureId,
	const TextureParameters& texParams )
{
	std::shared_ptr<Texture> existingTexture = findTexture( textureId );
	if( existingTexture != nullptr )
	{
		// Texture exists already.
		return nullptr;
	}

	// Create new texture an initialize it.
	std::shared_ptr<Texture> texture( new Texture( *this ) );
	texture->setId( textureId );
	bool success = texture->initializeWithParameters( texParams );
	mTextures[ textureId ] = texture;
	return texture;
}

void TextureManager::destroyTexture( const std::string& textureId )
{
	auto iter = mTextures.find( textureId );
	if( iter != mTextures.end() )
	{
		// Remove the texture from container.
		mTextures.erase( iter );
	}
}

std::shared_ptr<Texture> TextureManager::findTexture( const std::string& textureId ) const
{
	auto iter = mTextures.find( textureId );
	if( iter != mTextures.end() )
	{
		return iter->second;
	}
	return nullptr;
}

void TextureManager::notifyTextureChanged( std::shared_ptr<Texture>& texture )
{
	mAllTexturesUploaded = false;
}

size_t TextureManager::loadTexturesFromListFile( const std::string& textureListFile )
{
	// Read the texture list xml-file to memory.
	std::string textBufferOut;
	if( mCore.loadTextFile( textureListFile, textBufferOut ) == false )
	{
		std::string errorMessage = "Failed to read texture list from file: " + textureListFile;
		throw RengineError( errorMessage );
	}

	// Parse the textures list.
	std::unique_ptr< rapidxml::xml_document<> > rapidXmlDoc( new rapidxml::xml_document<>() );
	rapidXmlDoc->parse<0>( const_cast< char* >( textBufferOut.c_str() ) );

	// Find the root node.
	rapidxml::xml_node<>* rootNode = rapidXmlDoc->first_node( "textures" );

	// Iterate all texture-nodes through.
	size_t loadedTexturesCount = 0;
	for( rapidxml::xml_node<>* textureNode = rootNode->first_node( "texture" );
		textureNode != nullptr;
		textureNode = textureNode->next_sibling() )
	{
		// Get the texture parameters from XML document.
		std::string texId = rapidxmlutils::getAttributeValue( textureNode, "id" );
		std::string texFile = rapidxmlutils::getAttributeValue( textureNode, "file" );
		std::string texMipmap = rapidxmlutils::getAttributeValueWitDefault( textureNode, "mipmap", KTextureAttribMipmapNo );
		std::string texWrap = rapidxmlutils::getAttributeValueWitDefault( textureNode, "wrap", KTextureWrapRepeat );
		std::string texBorderColor = rapidxmlutils::getAttributeValueWitDefault( textureNode, "borderColor", "0,0,0,255" );

		// Tokenize the border color.
		Rgba borderColor = ConversionUtils::convertToRgba( texBorderColor );

		// Create the texture.
		std::shared_ptr<Texture> texture = createTexture( texId, texFile );

		// Set the texture properties that were read from the XML-document.
		if( texMipmap == KTextureAttribMipmapYes )
			texture->setMipMaps( true );
		else
			texture->setMipMaps( false );
		texture->setWrapMode( Texture::Wrap::Repeat, Texture::Wrap::Repeat );
		if( texWrap == KTextureWrapClampToBorder )
			texture->setWrapMode( Texture::Wrap::ClampToBorder, Texture::Wrap::ClampToBorder );
		else if( texWrap == KTextureWrapMirroredRepeat )
			texture->setWrapMode( Texture::Wrap::MirroredRepeat, Texture::Wrap::MirroredRepeat );
		else if( texWrap == KTextureWrapClampToEdge )
			texture->setWrapMode( Texture::Wrap::ClampToEdge, Texture::Wrap::ClampToEdge );
		texture->setBorderColor( borderColor );

		// Increase the loaded textures count.
		loadedTexturesCount++;
	}
	return loadedTexturesCount;
}

void TextureManager::uploadToGPU()
{
	// If all texture uploaded already, no need to do anything more.
	if( mAllTexturesUploaded )
		return;

	for( auto& idTexPair : mTextures )
	{
		idTexPair.second->uploadToGPU();
	}

	// All textures uploaded.
	mAllTexturesUploaded = true;
}

void TextureManager::removeFromGPU()
{
	// Release the GPU resources allocated for the textures.
	// This will not destroy the texture objects themselves.
	for( auto& idTexPair : mTextures )
		idTexPair.second->removeFromGPU();
	mAllTexturesUploaded = false;
}
