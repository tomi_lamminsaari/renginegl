
#include "rengineexception.h"
#include <cassert>

namespace {
	std::string convertToErrorString( GLenum errorCode )
	{
		if( errorCode == GL_NO_ERROR )
			return "";
		else if( errorCode == GL_INVALID_ENUM )
			return "GL_INVALID_ENUM";
		else if( errorCode == GL_INVALID_VALUE )
			return "GL_INVALID_VALUE";
		else if( errorCode == GL_INVALID_OPERATION )
			return "GL_INVALID_OPERATION";
		else if( errorCode == GL_INVALID_FRAMEBUFFER_OPERATION )
			return "GL_INVALID_FRAMEBUFFER_OPERATION";
		else if( errorCode == GL_OUT_OF_MEMORY )
			return "GL_OUT_OF_MEMORY";
		else if( errorCode == GL_STACK_UNDERFLOW )
			return "GL_STACK_UNDERFLOW";
		else if( errorCode == GL_STACK_OVERFLOW )
			return "GL_STACK_OVERFLOW";
		else if( errorCode == GL_INVALID_INDEX )
			return "GL_INVALID_INDEX";
		return "UNSPECIFIED ERROR";
	}
}
// class RengineError

void RengineError::check( const std::string &errorMessage )
{
	// Get the error code and throw an error if error has been set.
	GLenum openGlError = glGetError();
	if( openGlError != GL_NO_ERROR )
	{
		throw RengineError( errorMessage, openGlError );
	}
}

void RengineError::clearError()
{
	while( glGetError() != GL_NO_ERROR )
	{
		// Just loop until error is cleared.
	}
}

RengineError::RengineError( const std::string &what_arg ) :
	std::runtime_error( what_arg ),
	mGLError( GL_NO_ERROR )
{
	assert( false );
}

RengineError::RengineError( const char *what_arg ) :
	std::runtime_error( what_arg ),
	mGLError( GL_NO_ERROR )
{
	assert( false );
}

RengineError::RengineError( const std::string &what_arg, GLenum errorCode ) :
	std::runtime_error( what_arg ),
	mGLError( errorCode )
{
	std::string errorString = ::convertToErrorString( errorCode );
	assert( false );
}

void RengineError::setOpenGLError( GLenum errorCode )
{
	mGLError = errorCode;
}

GLenum RengineError::getOpenGLError() const
{
	return mGLError;
}


// SDLError class

SDLError::SDLError( const std::string& what_arg ) :
	runtime_error( what_arg )
{

}

SDLError::SDLError( const char* what_arg ) :
	runtime_error( what_arg )
{

}
