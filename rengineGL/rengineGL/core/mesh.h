
#pragma once

#include <string>
#include <vector>
#include <GL/glew.h>
#include "defines.h"
#include "meshinfo.h"
#include "rengineexception.h"
#include "vertexlayout.h"
#include "../math/vec2.h"
#include "../math/vec3.h"
#include "../math/vec4.h"

// Forward declarations.
class Material;
class Matrix3x3;
class Matrix4x4;
class RenderPassInfo;

/// <summary>
/// Mesh class defines the 3D geometry for object. One object can consist of multiple meshes.
/// </summary>
class Mesh
{
public:

	enum BufferIndex
	{
		VBO = 0,
		IBO = 1,
		BufferCount = 2
	};

	enum class FaceWinding
	{
		EClockwise,
		ECounterClockwise
	};

	/// <summary>Defines the usage mode how we are intending to use the mesh and vertex buffers.</summary>
	enum class BufferUsage
	{
		/// <summary>Mesh gets created once and then uploaded to the GPU memory.</summary>
		StaticMesh
	};

	/// <summary>
	/// Creates a new mesh with certain amount of vertices and faces.
	/// </summary>
	/// <param name="vertexLayout">The vertex attribute layout.</param>
	/// <param name="vertexCount">Number of vertices.</param>
	/// <param name="faceCount">Number of faces.</param>
	/// <returns>Pointer to created Mesh object.</returns>
	static std::unique_ptr<Mesh> createMesh( const VertexLayout &vertexLayout, size_t vertexCount, size_t faceCount );

	/// <summary>
	/// Constructor.
	/// </summary>
	Mesh();

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~Mesh();

	Mesh( const Mesh &obj ) = delete;
	Mesh &operator = ( const Mesh &obj ) = delete;

	/// <summary>
	/// Sets the ID of this mesh.
	/// </summary>
	/// <param name="meshId">ID of this mesh.</param>
	void setId( const std::string &meshId );

	/// <summary>
	/// Returns the ID of this mesh.
	/// </summary>
	/// <returns>The ID of this mesh.</returns>
	const std::string &getId() const;

	/// <summary>
	/// Checks if vertex layout of this mesh matches with given vertex layout.
	/// </summary>
	/// <param name="vertexLayout">The vertex layout we check against with.</param>
	/// <returns>Returns true if vertex layout of this mesh matches with given layout object.</returns>
	bool vertexLayoutEquals( const VertexLayout& vertexLayout ) const;

	/// <summary>
	/// Allocates the memory buffers for vertex data. This does not allocate the buffer data from GPU yet. Instead
	/// this allocates the buffers from RAM. The user can then set the vertex data to these buffers and construct
	/// the actual geometry. When geometry is ready, you should call 'uploadToGPU()' to allocate the actual GPU memory
	/// for the mesh and to upload the vertex data to it.
	/// </summary>
	/// <param name="vertexLayout">List of vertex attributes each vertex will have.</param>
	/// <param name="vertexCount">Number of vertices to allocate.</param>
	/// <param name="faceCount">Number of faces to allocate.</param>
	void initialize( const VertexLayout& vertexLayout, size_t vertexCount, size_t faceCount );

	/// <summary>
	/// Initializes the vertex with provided set of vertex attributes. The actual vertices and faces must be
	/// added to the mesh with 'addVertex()' and 'addFace()' methods later.
	/// </summmary>
	/// <param name="vertexLayout">List of vertex attributes each vertex will have.</param>
	void initialize( const VertexLayout &vertexLayout );

	/// <summary>
	/// Set the usage mode how we are planning to use the buffer. This must be called before UploadToGPU() has
	/// been called. Or you must call removeFromGPU() first.
	/// </summary>
	/// <param name="usage">The usage mode.</summary>
	void setBufferUsage( BufferUsage usage );

	/// <summary>
	/// Sets the vertex order the faces have. This defines which direction is the front face.
	/// </summary>
	/// <param name="order">The vertex order in faces.</param>
	void setVertexOrder( FaceWinding order );

	/// <summary>
	/// Gets the vertex order the faces have.
	/// </summary>
	/// <returns>Vertex order of the faces.</returns>
	FaceWinding getVertexOrder() const;

	/// <summary>
	/// Sets the vertex position data to the vertex buffer. Does not change the data in GPU buffer. After changing
	/// the vertex data, you must call 'uploadToGPU()' to copy the changes to GPU memory.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="pos">The vertex position.</param>
	void setPosition( size_t vertexIndex, const Vec3 &pos );

	/// <summary>
	/// Returns the vertex position from vertex buffer.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex whose position will be returned.</param>
	/// <returns>Position of the vertex.</returns>
	Vec3 getPosition( size_t vertexIndex ) const;

	/// <summary>
	/// Sets the vertex normal vector. Does not change the data in GPU buffer. After changing the vertex data, you
	/// must call 'uploadToGPU()' to copy the changes to the GPU memory.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="vec">The normal vector to set. The vector will be normalized before storing to the buffer.</param>
	void setNormal( size_t vertexIndex, const Vec3 &vec );

	/// <summary>
	/// Returns the vertex normal vector.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex whose normal will be returned.</param>
	/// <param name="normalOut">Receives the normal vector if this mesh has normal vectors.</param>
	/// <returns>
	/// Returns true if this mesh has normal vectors. If this returns 'false', the 'normalOut' did not receive the normal vector.
	/// </returns>
	bool getNormal( size_t vertexIndex, Vec3 &normalOut ) const;

	/// <summary>
	/// Sets the generic vec3 vertex attribute data. Available vertex attributes depend on VertexLayout that
	/// was given to this mesh when initialized.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="element">The type of element to set.</param>
	/// <param name="elementIndex">
	/// Index of the same element types in case the VertexLayout contains multiple instances of the
	/// same vertex element. Normally there is only one instance of each vertex element so this value
	/// is typically 0.
	/// </param>
	/// <param name="vec">The vector to set.</param>
	void setVec3Attribute( size_t vertexIndex, VertexElement element, int elementIndex, const Vec3 &vec );

	/// <summary>
	/// Returns the generic vec3 vertex attribute data.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex.</param>
	/// <param name="element">The vertex attribute element to return.</param>
	/// <param name="elementIndex">
	/// Index of the vertex element in cause there are multiple instances of the same vertex element. Typically
	/// there is only one instance of each vertex elements so this parameter is 0.
	/// </param>
	/// <param name="outVec">Receives the vec3 type vertex attribute data.</param>
	/// <returns>
	/// Returns 'true' if mesh contains the requested vertex attribute. Returns 'false' if requested
	/// attribute does not exist.
	/// </returns>
	bool getVec3Attribute( size_t vertexIndex, VertexElement element, int elementIndex, Vec3 &outVec ) const;

	/// <summary>
	/// Sets same vec3 attribute to all vertices of the the mesh that are within given range.
	/// </summary>
	/// <param name="startIndex">First index of the vertex where to set the value.</param>
	/// <param name="vertexCount">Number of consequtive vertices where to set the same vec3 value.</param>
	/// <param name="element">The vertex element to process.</param>
	/// <param name="elementIndex">Index of the element to set if there are more than 1 of the same element types.</param>
	/// <param name="val">The value to set.</param>
	void setVec3Attributes( size_t startIndex, size_t vertexCount, VertexElement element, int elementIndex, const Vec3& val );

	/// <summary>
	/// Sets a float vertex attribute.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="element">The vertex element to process.</param>
	/// <param name="elementIndex">Index of the element to set if there are more than 1 of the same element types.</param>
	/// <param name="val">The value to set.</param>
	void setFloatAttribute( size_t vertexIndex, VertexElement element, int elementIndex, float val );

	/// <summary>
	/// Returns the float attribute from the mesh.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="element">The vertex element to process.</param>
	/// <param name="elementIndex">Index of the element to set if there are more than 1 of the same element types.</param>
	/// <param name="val">Receives the value from the mesh.</param>
	/// <returns>Returns 'true' if mesh contains the requested vertex attribute.</returns>
	bool getFloatAttribute( size_t vertexIndex, VertexElement element, int elementIndex, float& outVal ) const;

	/// <summary>
	/// Sets the float attribute for a range of vertices.
	/// </summary>
	/// <param name="startIndex">First index of the vertex where to set the value.</param>
	/// <param name="vertexCount">Number of consequtive vertices where to set the same vec3 value.</param>
	/// <param name="element">The vertex element to process.</param>
	/// <param name="elementIndex">Index of the element to set if there are more than 1 of the same element types.</param>
	/// <param name="val">The value to set.</param>
	void setFloatAttributes( size_t startIndex, size_t vertexCount, VertexElement element, int elementIndex, float val );

	/// <summary>
	/// Sets the vertex related texture coordinate.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="textureIndex">Index of the texture whose coordinate we are going to set.</param>
	/// <param name="texCoord">The texture coordinate.</param>
	void setTexture2D( size_t vertexIndex, int textureIndex, const Vec2 &texCoord );

	/// <summary>
	/// Disables the given texture coordinates. Effectively sets all texture coordinates
	/// to value (-1.0f, -1.0f).
	/// </summary>
	/// <param name="textureIndex">Index of the texture coordinate disable.</param>
	void setTextureCoordinatesToDisabled( int textureIndex );

	/// <summary>
	/// Sets the vertex related color value.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex to set.</param>
	/// <param name="colorIndex">Index of the color to set.</param>
	/// <param name="color">The RGBA color to set.</param>
	void setColor( size_t vertexIndex, int colorIndex, const Vec4 &color );

	/// <summary>
	/// Sets all the vertex attributes at once. The given vector must contain
	/// exactly same amount of floats as the VertexLayout object that was used
	/// when this mesh was initialized.
	/// </summary>
	/// <param name="vertexIndex">Index of the vertex that will be set.</param>
	/// <param name="vertexAttributes">Vector that contains all vertex attributes of a single vertex.</param>
	void setAllVertexAttributes( size_t vertexIndex, const std::vector<float>& vertexAttributes );

	/// <summary>
	/// Sets the vertex indices that construct a face.
	/// </summary>
	/// <param name="faceIndex">Index of the face to set.</param>
	/// <param name="vertexIndex1">Index of the first vertex of the face.</param>
	/// <param name="vertexIndex2">Index of the second vertex of the face.</param>
	/// <param name="ertexIndex3">Index of the third vertex of the face.</param>
	void setFace( size_t faceIndex, size_t vertexIndex1, size_t vertexIndex2, size_t vertexIndex3 );

	/// <summary>
	/// Gets the vertex indices that constructs the given face.
	/// </summary>
	/// <param name="faceIndex">Index of the face to return.</param>
	/// <param name="outVIndex1">Receives the index of the first face vertex.</param>
	/// <param name="outVIndex2">Receives the index of the second face vertex.</param>
	/// <param name="outVIndex3">Receives the index of the third face vertex.</param>
	void getFace( size_t faceIndex, size_t &outVIndex1, size_t &outVIndex2, size_t &outVIndex3 ) const;

	/// <summary>
	/// Returns the number of faces this mesh has.
	/// </summary>
	/// <returns>Number of faces this mesh has.</returns>
	size_t getFaceCount() const;

	/// <summary>
	/// Gets number of vertices the mesh has.
	/// </summary>
	/// <returns>Number of vertices.</returns>
	size_t getVertexCount() const;

	/// <summary>
	/// Adds new vertex to this mesh. This should be used only before the mesh gets uploaded to the
	/// GPU memory.
	/// </summary>
	/// <returns>Returns the index of the new vertex.</returns>
	size_t addVertex();

	/// <summary>
	/// Adds new face to theis mesh. This should be used only before the mesh gets uploaded to the
	/// GPU memory.
	/// </summary>
	/// <param name="vertexIndex1">Index of the first vertex of the face.</param>
	/// <param name="vertexIndex2"> Index of the second vertex of the face.</param>
	/// <param name="vertexIndex3">Index of the third vertex of the face.</param>
	/// <returns>Index of the new created face.</returns>
	size_t addFace( size_t vertexIndex1, size_t vertexIndex2, size_t vertexIndex3 );

	/// <summary>
	/// Fills the mesh's Vertex Array Object and draw mode data to the provided RenderPassInfo
	/// object. With these data it is possible to init the correct buffer to be drawn to the screen.
	/// </summary>
	/// <param name="renderPassInfo">The vertex buffer data will be written to this object.</param>
	void getRenderPassInfo( RenderPassInfo &renderPassInfo ) const;

	/// <summary>
	/// Moves the positions of every vertex in this mesh to given direction.
	/// </summary>
	/// <param name="moveAmount">Movement amount.</param>
	void moveVertices( const Vec3 &moveAmount );

	/// <summary>
	/// Rotates the vertives by given rotation matrix.
	/// </summary>
	/// <param name="rotMat">The rotation matrix that defines the amount of rotation.</param>
	void rotateVertices( const Matrix3x3 &rotMat );

	/// <summary>
	/// Returns the Vertex-Array-Object of this mesh.
	/// </summary>
	/// <returns>The OpenGL Vertex-Array-Object identifier. 0 if VAO hasn't been created yet.</returns>
	GLuint getVAO() const;

	/// <summary>
	/// Uploads the vertex and face data to the GPU memory. Constructs the GPU buffers during the first call. Any subsequent
	/// calls will upload changed vertex data to the GPU. If nothing has changed, this will do nothing. Mesh keeps track on
	/// changed vertex index ranges and attempts to minimize the vertex ranges that need to be copied to the GPU.
	/// </summary>
	/// <exception cref="RengineError">Thrown if there are failures to allocate OpenGL buffers</exception>
	void uploadToGPU();

	/// <summary>
	/// Removes the mesh data from GPU memory and releases the GPU resources.
	/// </summary>
	void removeFromGPU();

	/// <summary>
	/// Returns editable recference to the MeshInfo data.
	/// </summary>
	/// <returns>Reference to the MeshInfo of this mesh.</returns>
	MeshInfo &accessMeshInfo();

	/// <summary>
	/// Returns un-editable reference to the MeshInfo data.
	/// </summary>
	/// <returns>Un-editable reference to the MeshInfo of this mesh.</returns>
	const MeshInfo &accessMeshInfo() const;

	/// <summary>
	/// Returns the vertex layout this mesh uses.
	/// </summary>
	/// <returns>The mesh layout.</returns>
	const VertexLayout &getVertexLayout() const;

	/// <summary>
	/// Goes through all the faces and calculate tangents and bitangents of the vertices.
	/// This function works only for meshes where faces don't share vertices with other
	/// faces. The mesh must have attributes for vertex normals or operation will fail.
	/// </summary>
	/// <exception cref="std::invalid_argument">Thrown if mesh does not have attribute for vertex normals.</exception>
	/// <exception cref="std::runtime_error">Thrown if there are at least 2 faces that share vertices.</exception>
	void calculateTangents();

	/// <summary>
	/// Tells if this mesh has faces that share vertices.
	/// </summary>
	/// <returns>Returns 'true' if at least 2 faces share a vertex.</returns>
	bool facesHaveSharedVertices() const;

	/// <summary>
	/// Renders the mesh. This expects that shaders, uniforms, textures, etc. have been setup already
	/// and only thing left is to bind Vertex Array Object and start rendering.
	/// </summary>
	void render();

	/// <summary>
	/// Copies some of the material data from given material and bakes it to the vertex attributes of this
	/// mesh.
	/// </summary>
	/// <param name="material">The material where to take values from.</param>
	void mergeMaterialAttributes( const Material& material );

private:

private:

	/// <summary>The vertex attribute order how each vertex in this mesh will be arranged.</summary>
	VertexLayout mVertexLayout;

	/// <summary>ID of this mesh.</summary>
	std::string mId;

	/// <summary>Number of vertices this mesh has.</summary>
	size_t mVertexCount;

	/// <summary>Number of faces this mesh has.</summary>
	size_t mFaceCount;

	/// <summary>The raw buffer in system memory that contains the vertex data.</summary>
	std::vector<GLfloat> mVertexData;

	/// <summary>The index buffer data that is in system memory.</summary>
	std::vector<GLuint> mIndexData;

	/// <summary>Indicates if vertex data has changed and it must be uploaded to the GPU again.</summary>
	bool mVertexDataChanged;

	/// <summary>Indicates if index buffer data has changed and it must be uploaded to the GPU memory.</summary>
	bool mIndexDataChanged;

	/// <summary>Contains the OpenGL vertex and index buffer object IDs.</summary>
	std::vector<GLuint> mGL_Buffers;

	/// <summary>The Vertex Array Object that binds the vertex object and index object together.</summary>
	GLuint mVAO;

	/// <summary>Are the indices of a faces traverced clockwise or counter-clockwise.</summary>
	FaceWinding mFaceWinding;

	/// <summary>Extra metadata of the mesh.</summary>
	MeshInfo mMeshInfo;

	/// <summary>Smallest Y coordinate.</summary>
	float mMinY;

	/// <summary>Biggest Y cordinate.</summary>
	float mMaxY;

	/// <summary>How we are intending to use the vertex buffer.</summary>
	BufferUsage mBufferUsage;
};
