
#pragma once

#include "../utils/rgba.h"

/// <summary>
/// LightColorInfo combines the light color and light intensity together.
/// The light color is an Rgba object and intensity is a float.
/// </summary>
class LightColorInfo
{
public:
	/// <summary>
	/// Default constructor. Construct a LightColorInfo object that has
	/// black color and intensity 1.0.
	/// </summary>
	LightColorInfo();

	/// <summary>
	/// Construct a LightColorInfo object with given color and intensity values.
	/// </summary>
	/// <param name="col">The color to set.</param>
	/// <param name="intensity">The intensity.</param>
	LightColorInfo( const Rgba &col, float intensity );

	/// <summary>
	/// Destructor.
	/// </summary>
	~LightColorInfo();

	/// <summary>
	/// A copy constructor.
	/// </summary>
	LightColorInfo( const LightColorInfo &obj );

	/// <summary>
	/// An assignment operator.
	/// </summary>
	LightColorInfo &operator=( const LightColorInfo &obj );

public:
	/// <summary>
	/// Set the color.
	/// </summary>
	/// <param name="col">The color to be set.</param>
	void setColor( const Rgba &col );

	/// <summary>
	/// Returns the color.
	/// </summary>
	/// <returns>Returns the color.</returns>
	const Rgba &getColor() const;

	/// <summary>
	/// Sets the intensity of the light.
	/// </summary>
	/// <param name="intensity">The intensity of the light.</param>
	void setIntensity( float intensity );

	/// <summary>
	/// Returns the intensity of the light.
	/// </summary>
	/// <returns>The intensity of the light.</returns>
	float getIntensity() const;

	/// <summary>
	/// Returns a combined color where color values have been multiplied with intensity.
	/// </summary>
	/// <returns>Light color when color has been adjusted by intensity.</returns>
	Rgba getColorIntensityCombined() const;

private:
	/// <summary>The color of the light.</summary>
	Rgba mColor;

	/// <summary>Intensity of the light.</summary>
	float mIntensity;
};
