
#pragma once

#include <GL/glew.h>
#include "defines.h"
#include "lightcolorinfo.h"
#include "../math/vec3.h"
#include "../utils/rgba.h"
#include "glstructs/glpointlight.h"

// Forward declarations.
class Material;
class LightSource;
class ShadowRenderer;

/// <summary>
/// Contains the generic rendering parameters such as light source positions.
/// </summary>
class SceneGlobalRenderPassInfo
	{
	public:
		SceneGlobalRenderPassInfo();
		~SceneGlobalRenderPassInfo();
		SceneGlobalRenderPassInfo( const SceneGlobalRenderPassInfo & obj );
		SceneGlobalRenderPassInfo& operator = ( const SceneGlobalRenderPassInfo &obj );

	public:

		/// <summary>
		/// Sets the color and amount of ambient light there is during the rendering.
		/// <summary>
		/// <param name="ambientLight">The ambient light's color and amount.</param>
		void setAmbientLight( const LightColorInfo &ambientLight );

		void setDiffuseLight(
			const Rgba &lightColor,
			const Vec3 &lightPos,
			GLfloat lightIntensity );

		/// <summary>
		/// Set the color and intensity of the specular light.
		/// </summary>
		/// <param name="specularLight">Specular light's color and intensity.</param>
		void setSpecularLight( const LightColorInfo &specularLight );

		/// <summary>
		/// Sets the overriding material. If set, the rendering operations will use this
		/// material instead of object's own material in rendering.
		/// </summary>
		/// <param name="overrideMaterial">Pointer to overriding material. Pass nullptr to disable overriding material.</param>
		void setOverrideMaterial( std::shared_ptr<Material> overrideMaterial );

		/// <summary>
		/// Returns the currently set overriding material.
		/// </summary>
		/// <returns>Pointer to overriding material. Nullptr if overriding material is not in use.</returns>
		std::shared_ptr<Material> getOverrideMaterial() const;

	public:
		bool mAmbientLightEnabled;
		bool mSpecularLightEnabled;

		GLfloat mAmbientLightIntensity;
		Rgba mAmbientLightColor;

		Vec3 mDiffuseLightPos;
		Rgba mDiffuseLightColor;
		GLfloat mDiffuseLightIntensity;

		Rgba mSpecularLightColor;
		GLfloat mSpecularLightIntensity;

		/// <summary>The overriding material. This will be used instead of object's own material.</summary>
		std::shared_ptr<Material> mOverrideMaterial;
	};

/// <summary>
/// RenderPassInfo collects the mesh, material, etc. data together so that we can easily setup the OpenGL pipeline.
/// </summary>
class RenderPassInfo
	{
	public:
		RenderPassInfo( SceneGlobalRenderPassInfo *globalRenderPass );
		~RenderPassInfo();
		RenderPassInfo(const RenderPassInfo &obj);
		RenderPassInfo& operator = (const RenderPassInfo &obj);

	public:
		void setVAO(GLenum drawMode, GLuint vao, GLsizei drawCount);

	public:
		SceneGlobalRenderPassInfo *mGlobalRenderPass;

		GLuint mVAO;
		GLenum mDrawMode;
		GLenum mElementType;
		GLsizei mDrawCount;
		GLuint mVBO;
		GLuint mIBO;
		GLenum mFaceWindingOrder;
	};
