
#pragma once

#include <map>
#include <memory>
#include <string>
#include "../math/vec3.h"
#include "lightcolorinfo.h"
#include "renderpassinfo.h"
#include "renderutils/skybox.h"
#include "particles/particledefines.h"
#include "utilities/frametimemeasurement.h"

// Forward declarations.
class Camera;
class LightSourceCollection;
class Node;
class OverlayRenderable;
class ParticleSystem;
class LightSource;
class Renderable;
class RenderCore;
class ShadowRenderer;
class Texture;

/// <summary>
/// Scene is the collection of 3D renderables that are going to be rendered.
/// <para>
/// Scene can contain multiple 3D objects. The only way an object could get rendered to the screen
/// is that it had been attached to the scene. Attaching object to a scene happens by using the root
/// node of the scene.
/// </para>
/// </summary>
class Scene : public std::enable_shared_from_this< Scene >
{
public:

	static std::shared_ptr<Scene> createScene( RenderCore& core );

	~Scene();
	Scene( const Scene& obj ) = delete;
	Scene& operator = ( const Scene& obj ) = delete;

public:

	/// <summary>
	/// Creates new renderable and adds it to this scene. New renderable will also have its own Node so that
	/// we can easily move the renderable by moving and rotating its parent node.
	/// </summary>
	/// <param name="renderableId">ID of the new renderable.</param>
	/// <param name="parentNode">
	/// The node where renderable gets attached to. If null, the renderable is attached to scene's root node.
	/// </param>
	/// <returns>Pointer to new renderable.</returns>
	std::shared_ptr<Renderable> createRenderable(
		const std::string& renderableId,
		std::shared_ptr<Node> parentNode = nullptr );

	/// <summary>
	/// Create new overlay renderable. Overlay renderables will be rendered at last stage so they
	/// do not cast shadows or get rendered to the depth buffer either
	/// </summary>
	/// <param name="renderableId">ID of the renderable.</param>
	/// <returns>Pointer to new OverlayRenderable object.</returns>
	std::shared_ptr<OverlayRenderable> createOverlayRenderable( const std::string& renderableId );

	/// <summary>
	/// Destroyes the renderable from the scene.
	/// </summar>
	/// <param name="renderableId">ID of the renderable to destroy.</param>
	void destroyRenderable( const std::string& renderableId );

	/// <summary>
	/// Creates a particle system and attaches it to scene.
	/// </summary>
	/// <param name="particleSystemId">ID of the particle system that gets spawned.</param>
	/// <param name="particleType">Type of particle system to spawn.</param>
	/// <param name="parentNode">
	/// Pointer to scene node where this gets attached to. If nullptr, the particle system gets
	/// attached to scene's root node.
	/// </param>
	/// <returns>
	/// Pointer to particle system. Ownership is not transferred. This could potentially be nullptr because
	/// there is an upper limit how many particle systems the render engine can handle at once.
	/// </returns>
	ParticleSystem* createParticleSystem(
		const std::string& particleSystemId,
		ParticleSystemType particleType,
		std::shared_ptr<Node> parentNode = nullptr );

	/// <summary>
	/// Destroys the particle system that has given name.
	/// </summary>
	/// <param name="particleSystemId">ID of the particle system to destroy.</param>
	void destroyParticleSystem( const std::string& particleSystemId );

	/// <summary>
	/// Creates new lightsource and adds it to this scene. New lightsource will also have its own Node so
	/// we can easily move and rotate it by moving and rotating the node. Created lightsource has
	/// not been initialized yet so it has no type yet.
	/// </summary>
	/// <param name="lightsourceId">Unique ID of the lightsource.</param>
	/// <param name="parentNode">
	/// The node where lightsource gets attached to. If nullptr, the lightsource gets
	/// attached to scene's root node.
	/// </param>
	/// <returns>Returns pointer to created lightsource object.</returns>
	std::shared_ptr<LightSource> createLightSource(
		const std::string& lightsourceId,
		std::shared_ptr<Node> parentNode = nullptr );

	/// <summary>
	/// Destroys the lightsource.
	/// </summary>
	/// <param name="lightsourceId">ID of the lightsource to destroy.</param>
	void destroyLightSource( const std::string& lightsourceId );

	/// <summary>
	/// Sets the color and amount of ambient light there is in this scene.
	/// </summary>
	/// <param name="ambient">Ambient light's color and amount.</param>
	void setAmbientLight( const LightColorInfo& ambient );

	/// <summary>
	/// Returns the color and amount of ambient light there is in this scene.
	/// </summary>
	/// <returns>Color and amount of ambient light.</returns>
	const LightColorInfo& getAmbientLight() const;

	/// <summary>
	/// Searches for a renderable from the scene.
	/// </summary>
	/// <param name="renderableId">ID of the renderable to search for.</param>
	/// <returns>Pointer to renderable. Nullptr if matching renderable is not found.</returns>
	std::shared_ptr<Renderable> findRenderable( const std::string& renderableId ) const;

	/// <summary>
	/// Returns the scene's root node. All the renderable's that get renderable must be attached to a node.
	/// By default all renderable's get attached to this root node but you can reattach the renderable to
	/// other nodes as well.
	/// </summary>
	/// <returns>Pointer to root node. Ownership is not transferred.</returns>
	std::shared_ptr<Node> getRootNode() const;

	/// <summary>
	/// Prepares the scene for rendering. Detects the renderables that will get rendered.
	/// </summary>
	/// <param name="camera">Pointer to camera object that defines the viewing direction.</param>
	void prepareRender( std::shared_ptr<Camera>& camera );

	/// <summary>
	/// Renders the renderables to the active rendering target.
	/// </summary>
	/// <param name="camera">The camera that defines the viewing direction.</param>
	void render( std::shared_ptr<Camera>& camera );

	/// <summary>
	/// Returns the render engine's core object.
	/// </summary>
	/// <returns>Reference to core object.</returns>
	RenderCore& getCore();

	/// <summary>
	/// Returns the lightsource that affects that has given name.
	/// </summary>
	/// <param name="lightId">ID of the light to get.</param>
	/// <returns>Pointer to diffuse lightsource. Null if there is no light source with given index number.</returns>
	std::shared_ptr<LightSource> findLightSource( const std::string& lightId ) const;

	/// <summary>
	/// Gets the shadow renderer object by index.
	/// </summary>
	/// <param name="index">Index of the shadow renderer to get.</param>
	/// <returns>Pointer to shadowrenderer.</returns>
	std::shared_ptr< ShadowRenderer > getShadowRenderer( size_t index );

	/// <summary>
	/// Returns the skybox of the scene.
	/// </summary>
	/// <returns>The skybox object.</returns>
	SkyBox& getSkyBox();

	/// <summary>
	/// Enables or disables the shadow map debug rendering to the screen.
	/// </summary>
	/// <param name="enabled">Pass 'true' to make shadowmap visible to the screen.</param>
	void setDebugDrawShadowmap( bool enabled );

	/// <summary>
	/// Tells if shadowmap is drawn to the screen for debug purposes.
	/// </summary>
	/// <returns>Returns 'true' if shadowmap will be rendered as a small rect to the screen.</returns>
	bool isDebugDrawShadowmapEnabled() const;

	/// <summary>
	/// Gets the frame times. This can be called after the rendering has been done to get
	/// details about rendering times.
	/// </summary>
	/// <returns>Rendering times collection.</returns>
	FrameTimeMeasurement getFrameTimeMeasurements() const;

private:

	/// <summary>
	/// Renders the skybox to the frame buffer if skybox has been enabled.
	/// </summary>
	/// <param name="camera">Pointer to camera that is being used in the rendering.</param>
	void renderSkyBox( std::shared_ptr<Camera>& camera );

	/// <summary>
	/// Renders the shadows to a shadow map texture.
	/// </summary>
	/// <param name="camera">The camera that defines the viewing direction.</param>
	void renderShadows( std::shared_ptr<Camera>& camera );

	/// <summary>
	/// Renders the graphics to default frame buffer with deferred shading mode.
	/// </summary>
	/// <param name="camera">The camera that defines the viewing point.</param>
	void renderObjectsDeferredShading( std::shared_ptr<Camera>& camera );

	/// <summary>
	/// Renders all the overlay objects.
	/// </summary>
	void renderOverlayObjects();

private:
	/// <summary>Typedefinition for collection of renderables.</summary>
	typedef std::vector< std::shared_ptr<Renderable> > RenderableContainer;

private:

	Scene( RenderCore& core );
	void initilizeScene();

	/// <summary>
	/// Sorts the given renderables container so that renderables are in alphabethical order
	/// based on their IDs.
	/// </summary>
	/// <param name="renderableContainer">The container to sort.</param>
	void sortRenderableContainer( RenderableContainer& renderableContainer );

	/// <summary>
	/// Finds the renderable from the renderable container.
	/// </summary>
	/// <returns>Iterator to found object. Iterates points to container's end() iterator if renderable ID was not found.</returns>
	RenderableContainer::const_iterator findRenderableFromContainer(
		const RenderableContainer& renderableContainer,
		const std::string& renderableId ) const;

private:

	/// <summary>The render engine's core object.</summary>
	RenderCore& mCore;

	/// <summary>The scene's root node.</summary>
	std::shared_ptr<Node> mRootNode;

	/// <summary>The container that contains all the renderables.</summary>
	RenderableContainer mRenderables;

	/// <summary>The container that contains all the overlay renderables.</summary>
	std::map< std::string, std::shared_ptr<OverlayRenderable> > mOverlayRenderables;

	/// <summary>Lightsource that defines the location and properties of diffuse light source.</summary>
	std::shared_ptr< LightSourceCollection > mLightSourceCollection;

	/// <summary>The skybox that renders the sky and horizon.</summary>
	SkyBox mSkyBox;

	/// <summary>Available shadow renderers.</summary>
	std::vector< std::shared_ptr< ShadowRenderer > > mShadowRenderers;

	/// <summary>An object that contains generic rendering parameters light lights.</summary>
	SceneGlobalRenderPassInfo mGlobalRenderPassInfo;

	/// <summary>Color and amount of ambient light in the scene.</summary>
	LightColorInfo mAmbientLight;

	/// <summary>Indicates if we want to draw the shadowmap to the screen for debug purposes.</summmary>
	bool mRenderShadowMap;

	/// <summary>Object that can keep time on frame rendering times.</summary>
	FrameTimeMeasurement mFrameTimeMeasurement;
};
