
#pragma once

#include <mutex>
#include <map>
#include "managerbase.h"

// Forward declarations.
class Model;
class RenderCore;

/// <summary>
/// ModelManager is an object that stores and manages all the 3D models.
/// </summary>
class ModelManager : public ManagerBase
{
public:
	ModelManager( RenderCore& core );
	virtual ~ModelManager();
	ModelManager( const ModelManager& ) = delete;
	ModelManager& operator = ( const ModelManager& ) = delete;

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup
	/// </summary>
	virtual void cleanup() override;

public:

	/// <summary>
	/// Creates new model with given ID.
	/// </summary>
	/// <param name="modelId">ID of the new model. Must be unique.</param>
	/// <returns>Pointer to new model object.</returns>
	std::shared_ptr<Model> createModel( const std::string& modelId );

	/// <summary>
	/// Adds the given model to this model manager.
	/// </summary>
	/// <param name="model">
	/// Pointer to the model that will be added. The ID of the model must be unique.</param>
	/// <returns>Returns 'true' if model was added. </returns>
	bool addModel( std::shared_ptr<Model> model );

	/// <summary>
	/// Deletes a model that has given ID.
	/// </summary>
	/// <param name="modelId">ID of the model that will be deleted.</param>
	void deleteModel( const std::string& modelId );

	/// <summary>
	/// Returns the model that has given ID.
	/// </summary>
	/// <param name="modelId">ID of the model to be returned.</param>
	/// <returns>Pointer to the model. Nullpointer if model with given ID does not exist.</returns>
	std::shared_ptr<Model> getModel( const std::string& modelId ) const;

	/// <summary>
	/// Returns the number of models there are in this model manager.
	/// </summary>
	/// <returns>Number of models there are.</returns>
	size_t getModelCount() const;

	/// <summary>
	/// Returns a vector where all the model IDs of the created models are.
	/// </summary>
	/// <returns>A vector that contains the existing model IDs.</returns>
	std::vector< std::string > getModelIds() const;

private:
	/// <summary>Container type of the model container.</summary>
	typedef std::map< std::string, std::shared_ptr<Model> > ModelContainer;

	/// <summary>Container where the models are being stored.</summary>
	ModelContainer mModels;
};
