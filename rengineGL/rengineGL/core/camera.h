
#pragma once

#include <vector>
#include "../math/aabb.h"
#include "../math/matrix4x4.h"
#include "../math/quaternion.h"
#include "../math/vec3.h"

/// <summary>
/// Camera class represents the 3D world camera that defines the point of view from where the scene gets rendered.
/// </summary>
class Camera
{
public:
	const static unsigned int KLeftParamIndex = 0;
	const static unsigned int KRightParamIndex = 1;
	const static unsigned int KTopParamIndex = 2;
	const static unsigned int KBottomParamIndex = 3;
	const static unsigned int KNearParamIndex = 4;
	const static unsigned int KFarParamIndex = 5;

	/// <summary>
	/// Constructor.
	/// </summary>
	Camera();

	/// <summary>
	/// Disabled copy constructor.
	/// </summary>
	Camera( const Camera &obj ) = delete;

	/// <summary>
	/// Destructor.
	/// </summary>
	~Camera();

	/// <summary>
	/// Disabled assignment operator.
	/// </summary>
	Camera& operator = ( const Camera &obj ) = delete;

	/// <summary>
	/// Sets the projection parameters.
	/// </summary>
	/// <param name="l">Left edge of projection.</param>
	/// <param name="r">Right edge of projection.</param>
	/// <param name="t">Top edge of projection.</param>
	/// <param name="b">Bottom edge of projection.</param>
	/// <param name="n">Near plane of the projection.</param>
	/// <param name="f">Far place of the projection.</param>
	void setProjectionParameters( float l, float r, float t, float b, float n, float f );

	/// <summary>
	/// Enables the perspective projection to the camera with parameters that have been set
	/// by setProjectionParameters() function.
	/// </summary>
	void setPerspProjection();

	/// <summary>
	/// Enables the orthogonal projection to the camera with parameters that have been set
	/// by setProjectionParameters() function.
	/// </summary>
	void setOrthogonalProjection();

	/// <summary>
	/// Sets the position of this camera.
	/// </summary>
	/// <param name="pos">Position of this camera.</param>
	void setPosition( const Vec3 &pos );

	/// <summary>
	/// Returns the position of this camera.
	/// </summary>
	/// <returns>Position of this camera.</returns>
	const Vec3& getPosition() const;

	/// <summary>
	/// Sets the position in the world where this camera looks at.
	/// </summary>
	/// <param name="pos">Position of the camera's focus point.</param>
	void setLookAt( const Vec3 &pos );

	/// <summary>
	/// Returns the camera's focus point.
	/// </summary>
	/// <returns>Camera's look at point.</returns>
	const Vec3& getLookAt() const;

	/// <summary>
	/// Rotates the look at point around the camera's position with given amount.
	/// </summary>
	/// <param name="quat">Amount of rotation.</param>
	void rotateLookAt( const Quaternion &quat );

	/// <summary>
	/// Returns the vector that points to the direction the camera looks at.
	/// </summary>
	/// <returns>Front vector.</returns>
	Vec3 getFrontVec() const;

	/// <summary>
	/// Returns the vector that points up from the camera orientation.
	/// </summary>
	/// <returns>Vector that points up from camera's current orientation.</returns>
	Vec3 getUpVec() const;

	/// <summary>
	/// Returns the vector that shows right from the camera's orientation.
	/// </summary>
	/// <returns>Vector that points to the right from camera.</returns>
	Vec3 getRightVec() const;

	/// <summary>
	/// Calculates all the projection matrices of the camera. This must be called after moving and
	/// translating the camera and before applying the camera matrices to the shaders for rendering.
	/// </summary>
	void updateMatrices();

	/// <summary>
	/// Multiplies the view and projection matrices. Returns the View x Projection matrix.
	/// </summary>
	/// <returns>The View x Projection matrix.</returns>
	const Matrix4x4& getViewProjMatrix() const;

	/// <summary>
	/// Returns the view matrix. With view matrix you can translates the coordinates from objects
	/// local coordinates to the world coordinates.
	/// </summary>
	/// <returns>Thhe view matrix.</returns>
	const Matrix4x4& getViewMatrix() const;

	/// <summary>
	/// Sets all camera vectors.
	/// </summary>
	/// <param name="pos">Position of the camera.</param>
	/// <param name="lookAt">The coordinate where the camera looks at.</param>
	/// <param name="up">Vector that defines the "up" direction of the camera.</param>
	void setCameraVectors(
		const Vec3 &pos,
		const Vec3 &lookAt,
		const Vec3 &up );

	/// <summary>
	/// Moves camera forward or backwards.
	/// </summary>
	/// <param name="units">
	/// Number of coordinate units to move. Positive numbers move forwards. Negative numbers move backwards.
	/// </param>
	void moveForward( float units );

	/// <summary>
	/// Moves camera sideways.
	/// </summary>
	/// <param name="units">
	/// Number of coordinate units to move. Positive numbers move right. Negative numbers move left.
	/// </param>
	void moveSideways(float units);

	/// <summary>
	/// Moves camera to specic direction.
	/// </summary>
	/// <param name="dir">Direction and amout of camera movement.</param>
	/// <param name="moveLookAt">
	/// Pass true to move the camera's look at coordinate during moving.
	/// Pass false to move only the camera. It still looks at the same old position so
	/// view is rotating as the camera moves.
	/// </param>
	void move(const Vec3 &dir, bool moveLookAt);

	/// <summary>
	/// Returns the view frustum's world coordinates. The returned vector contains
	/// 8 coordinates. The frustum vertices are in following indices:
	/// 0 = far bottom left, 1 = far bottom right, 2 = far top right, 3 = far top left
	/// 4 = near bottom left, 5 = near bottom right, 6 = near top right, 7 = near top left
	/// </summary>
	/// <returns>The view frustum's corners' world coordinates.</returns>
	std::vector< Vec3 > getFrustumWorldCoordinates() const;

	/// <summary>
	/// Returns the view frustum's world coordinates as an Aabb object.
	/// </summary>
	/// <returns>Axis Aligned Bounding Box of the view frustum's coordinates.</returns>
	Aabb getFrustCoordinatesAsAabb() const;

private:
	/// <summary>Current position of the camera in world coordinates.</summary>
	Vec3 mPosition;

	/// <summary>World coordinate location the camera is looking at.</summary>
	Vec3 mLookAt;

	/// <summary>Orientation of the camera.</summary>
	Quaternion mOrientation;

	Vec3 mFrontVecTrans;
	Vec3 mRightVecTrans;
	Vec3 mUpVecTrans;

	Matrix4x4 mProjectionMat;
	Matrix4x4 mViewMatrix;
	Matrix4x4 mViewProjMatrix;
	
	/// <summary>The camera parameters that were used when setting up the frustum or orthogonal projection.</summary>
	std::vector< float > mParams;
};