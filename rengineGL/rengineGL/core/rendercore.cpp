
#include <sstream>
#include <fstream>
#include <GL/glew.h>
#include <assert.h>
#include "camera.h"
#include "defines.h"
#include "materialmanager.h"
#include "meshmanager.h"
#include "modelmanager.h"
#include "particles/particlemanager.h"
#include "shadermanager.h"
#include "texturemanager.h"
#include "rendercore.h"
#include "scene.h"
#include "renderutils/framebufferobject.h"
#include "renderutils/gbufferfbo.h"
#include "../core/animators/animatorbase.h"

RenderCore::RenderCore() :
	mMaterialManager( nullptr ),
	mMeshManager( nullptr ),
	mModelManager( nullptr ),
	mShaderManager( nullptr ),
	mTextureManager( nullptr ),
	mParticleManager( nullptr ),
	mScene( nullptr ),
	mHostApp( nullptr ),
	mClearColor( 0.2f, 0.2f, 0.4f, 1.0f ),
	mScreenRect( Vec2(), 320, 200 ),
	mIDCounterMaterial( 1 ),
	mIDCounterMesh( 1 ),
	mIDCounterModel( 1 ),
	mIDCounterShader( 1 ),
	mIDCounterTexture( 1 ),
	mIDCounterRenderable( 1 ),
	mIDCounterLightSource( 1 ),
	mIDCounterFBO( 1 ),
	mIDCounterAnimator( 1 ),
	mIDCounterParticles( 1 )
{

}

RenderCore::~RenderCore()
{
	assert( mMaterialManager == nullptr );
	assert( mMeshManager == nullptr );
	assert( mModelManager == nullptr );
	assert( mShaderManager == nullptr );
	assert( mTextureManager == nullptr );
	assert( mParticleManager == nullptr );
	assert( mScene == nullptr );
	assert( mCamera == nullptr );
}

void RenderCore::initialize( IRenderCoreHost *host )
{
	// Initialize general members.
	mHostApp = host;

	// Create the managers.
	mMaterialManager.reset( new MaterialManager( *this ) );
	mMeshManager.reset( new MeshManager( *this ) );
	mModelManager.reset( new ModelManager( *this ) );
	mShaderManager.reset( new ShaderManager( *this ) );
	mTextureManager.reset( new TextureManager( *this ) );
	mParticleManager.reset( new ParticleManager( *this ) );


	// Initialize the built-in rendering resources.
	bool success = true;
	success = mShaderManager->initializeDefaultShaders();
	assert( success == true );
	success = mMaterialManager->initializeDefaultMaterials();
	assert( success == true );
	success = mMeshManager->initializeDefaultMeshes();
	assert( success == true );
	mParticleManager->intializeParticleSystemsPool();

	// Create scene.
	mScene = Scene::createScene( *this );

	// Create camera.
	mCamera.reset( new Camera );
	mCamera->setCameraVectors(
		Vec3( 0.0f, 0.0f, -2.0f ),
		Vec3( 0.0f, 0.0f, 0.0f ),
		Vec3( 0.0f, -1.0f, 0.0f ) );
}

void RenderCore::uninitialize()
{
	// Destroy scene.
	mScene.reset();

	// Destroy framebuffer objects.
	for( auto &fboItem : mFrameBufferObjects )
	{
		// TODO: Remove the unitialize(). Instead just do uninitialiation in destructor.
		fboItem.second->uninitialize();
	}
	mFrameBufferObjects.clear();
	mGBufferObjects.clear();

	// Destroy all managers.
	if( mParticleManager )
		mParticleManager->cleanup();
	mParticleManager.reset();

	if( mModelManager )
		mModelManager->cleanup();
	mModelManager.reset();

	if( mTextureManager )
		mTextureManager->cleanup();
	mTextureManager.reset();

	if( mMaterialManager )
		mMaterialManager->cleanup();
	mMaterialManager.reset();

	if( mMeshManager )
		mMeshManager->cleanup();
	mMeshManager.reset();

	if( mShaderManager )
		mShaderManager->cleanup();
	mShaderManager.reset();
	mScene.reset();
	mCamera.reset();
}

void RenderCore::update( double secsSincePrevious )
{
	// Update the animators.
	auto iter = mAnimators.begin();
	while( iter != mAnimators.end() )
	{
		// Update the animators.
		iter->second->update( secsSincePrevious );

		// Move to next iterm.
		iter++;
	}

	// Update the particlesystems.
	mParticleManager->update( secsSincePrevious );
}

void RenderCore::render()
{
	RengineError::clearError();
	try
	{
		// Start rendering by clearing the statistics.
		mActiveStats.reset();

		// Make sure all resources are in GPU memory.
		uploadAllToGPU();

		// Render the scenes.
		GLASSERT( glViewport( 0, 0, static_cast< GLsizei >( mScreenRect.getWidth() ), static_cast< GLsizei >( mScreenRect.getHeight() ) ) );
		GLASSERT( glClearColor( mClearColor.mX, mClearColor.mY, mClearColor.mZ, mClearColor.mW ) );
		GLASSERT( glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT ) );
		GLASSERT( glClearDepth( 1.0f ) );
		GLASSERT( glEnable( GL_MULTISAMPLE ) );

		// First prepare the scene to the rendering.
		Vec3 cp = mCamera->getPosition();
		mCamera->updateMatrices();
		mScene->prepareRender( mCamera );

		// Then do the actual rendering.
		mShaderManager->startRenderingRound();
		mScene->render( mCamera );
		mShaderManager->endRenderingRound();

		size_t progcount = mActiveStats.getProgramChangeCount();
	}
	catch( std::exception & )
	{
		assert( false );
		throw;
	}
}

std::shared_ptr<Scene> RenderCore::getScene() const
{
	return mScene;
}

void RenderCore::setViewport( int w, int h )
{
	mScreenRect.setTl( Vec2( 0.0f, 0.0f ) );
	mScreenRect.setBr( Vec2( static_cast< float >( w ), static_cast< float >( h ) ) );
	if( mCamera )
	{
		float aspect = w / h;
		float l = -0.6f;
		float r = 0.6f;
		float t = 0.6f / aspect;
		float b = -0.6f / aspect;
		float n = 1.0f;
		float f = 4000.0f;
		mCamera->setProjectionParameters( l, r, t, b, n, f );
		mCamera->setPerspProjection();
	}
}

std::shared_ptr<Camera> RenderCore::getCamera() const
{
	return mCamera;
}

MaterialManager &RenderCore::getMaterialManager()
{
	return *mMaterialManager;
}

MeshManager &RenderCore::getMeshManager()
{
	return *mMeshManager;
}

ModelManager &RenderCore::getModelManager()
{
	return *mModelManager;
}

ShaderManager &RenderCore::getShaderManager()
{
	return *mShaderManager;
}

TextureManager &RenderCore::getTextureManager()
{
	return *mTextureManager;
}

ParticleManager &RenderCore::getParticleManager()
{
	return *mParticleManager;
}

std::string RenderCore::generateId( IDGenerator generatorType )
{
	int idValue = 0;
	std::string baseName = "";
	{
		// Block do synchronizing the ID generation.
		std::lock_guard<std::mutex> lock( mIDGeneratorMutex );
		switch( generatorType )
		{
		case IDGenerator::Material:
			baseName = "mat#";
			idValue = mIDCounterMaterial++;
			break;
		case IDGenerator::Mesh:
			baseName = "mes#";
			idValue = mIDCounterMesh++;
			break;
		case IDGenerator::Model:
			baseName = "mod#";
			idValue = mIDCounterModel++;
			break;
		case IDGenerator::Shader:
			baseName = "sha#";
			idValue = mIDCounterShader++;
			break;
		case IDGenerator::Texture:
			baseName = "tex#";
			idValue = mIDCounterTexture++;
			break;
		case IDGenerator::Renderable:
			baseName = "rend#";
			idValue = mIDCounterRenderable++;
			break;
		case IDGenerator::LightSource:
			baseName = "light#";
			idValue = mIDCounterLightSource++;
			break;
		case IDGenerator::FrameBufferObject:
			baseName = "fbo#";
			idValue = mIDCounterFBO++;
			break;
		case IDGenerator::Animator:
			baseName = "ani#";
			idValue = mIDCounterAnimator++;
			break;
		case IDGenerator::Particles:
			baseName = "par#";
			idValue = mIDCounterParticles++;
			break;
		default:
			abort();
		}
	}

	// Generate the ID by adding the ID to the base name.
	std::ostringstream oss;
	oss << baseName;
	oss << idValue;
	return oss.str();
}

std::string RenderCore::getAbsolutePath( const std::string &relativePath )
{
	const std::string KAssetsPrefix = "assets://";
	std::string fullPath = "";
	size_t pos = relativePath.find( KAssetsPrefix );
	if( pos == 0 )
	{
		// Given path begins with "assets://"
		std::string endPart = relativePath.substr( KAssetsPrefix.length() );

		// In debug builds the assets are located "../../../Assets" folder.
#ifdef _DEBUG
		std::string assetsPath = "../Assets/";
#else
		std::string assetsPath = "../Assets/";
#endif
		fullPath = assetsPath + endPart;

	}
	else
	{
		// No prefix-beginning. Assume that file is full absolute path already.
		fullPath = relativePath;
	}
	return fullPath;
}

bool RenderCore::loadTextFile( const std::string &relativePath, std::string &textBufferOut )
{
	std::string fullPath = getAbsolutePath( relativePath );

	// Open the file for reading.
	std::ifstream ifs;
	ifs.open( fullPath );
	if( !ifs || ifs.good() == false )
	{
		return false;
	}

	// Read the contents to a stringstream.
	std::stringstream ss;
	ss << ifs.rdbuf();

	// Get the loaded data from the stringstream.
	textBufferOut = ss.str();
	return true;
}

RenderStatistics &RenderCore::getActiveStats()
{
	return mActiveStats;
}

void RenderCore::showMessage( const std::string &messageText )
{
	// Attempt to show the message by delegating the showing to the hosting application.
	if( mHostApp != nullptr )
		mHostApp->showErrorMessage( messageText );
}

void RenderCore::printTerminalMessage( int lineId, const std::string &message )
{
	if( mHostApp != nullptr )
		mHostApp->printToTerminalWindow( lineId, message );
}

void RenderCore::addAnimator( const std::string &animatorId, std::shared_ptr< AnimatorBase > animator )
{
	mAnimators.insert( std::make_pair( animatorId, animator ) );
}

RengineConfig& RenderCore::getConfig()
{
	return mConfig;
}

void RenderCore::uploadAllToGPU()
{
	mMeshManager->uploadToGPU();
	mShaderManager->uploadToGPU();
	mTextureManager->uploadToGPU();
}

std::shared_ptr< FrameBufferObject > RenderCore::createFBO( const std::string &fboName )
{
	std::shared_ptr< FrameBufferObject > fbo( new FrameBufferObject( *this ) );
	fbo->setId( fboName );

	// Add new framebuffer only if it doesn't exist already.
	auto iter = mFrameBufferObjects.find( fboName );
	if( iter == mFrameBufferObjects.end() )
	{
		// Framebuffer doesn't exist yet. Add it to our collection.
		mFrameBufferObjects[ fboName ] = fbo;
	}
	else
	{
		// Framebuffer with ID already exists. This is an error.
		std::string errMessage = "An FBO with name '" + fboName + "' already exists.";
		throw std::runtime_error( errMessage );
	}

	return fbo;
}

void RenderCore::destroyFBO( const std::string &fboName )
{
	auto iter = mFrameBufferObjects.find( fboName );
	if( iter != mFrameBufferObjects.end() )
	{
		// Framebuffer object exists. Destroy it.
		mFrameBufferObjects.erase( iter );
	}
}

std::shared_ptr< FrameBufferObject > RenderCore::getFBO( const std::string &fboName ) const
{
	auto iter = mFrameBufferObjects.find( fboName );
	if( iter != mFrameBufferObjects.end() )
	{
		// Framebuffer object was found. Return it.
		return iter->second;
	}

	// Not found. Return a null pointer.
	return std::shared_ptr< FrameBufferObject >();
}

std::shared_ptr<GBufferFBO> RenderCore::createGBuffer( const std::string& gbufferName, uint32_t bufferWidth, uint32_t bufferHeight )
{
	std::shared_ptr< GBufferFBO > gbufferFBO;
	auto iter = mGBufferObjects.find( gbufferName );
	if( iter == mGBufferObjects.end() )
	{
		// Crete GBuffer.
		gbufferFBO = GBufferFBO::createGBufferFBO( *this, bufferWidth, bufferHeight );
		gbufferFBO->setId( gbufferName );
		mGBufferObjects[ gbufferName ] = gbufferFBO;
	}
	else
	{
		throw std::invalid_argument( "GBuffer '" + gbufferName + "' already exists." );
	}
	return gbufferFBO;
}

void RenderCore::destroyGBuffer( const std::string& gbufferName )
{
	auto iter = mGBufferObjects.find( gbufferName );
	if( iter != mGBufferObjects.end() )
		mGBufferObjects.erase( iter );
}

std::shared_ptr< GBufferFBO > RenderCore::getGBuffer( const std::string& gbufferName ) const
{
	auto iter = mGBufferObjects.find( gbufferName );
	if( iter != mGBufferObjects.end() )
		return iter->second;
	else
		return std::shared_ptr< GBufferFBO >();
}
