
#pragma once

#include <map>
#include <string>
#include "rengineexception.h"

class RengineConfig
{
public:
    RengineConfig();

    void loadConfiguration( const std::string& configFilePath );
    void setProperty( const std::string& propertyName, bool propValue );
    bool getBooleanOrDefault( const std::string& propertyName, bool defaultValue ) const;
    bool getBoolean( const std::string& propertyName ) const;

private:
    std::map< std::string, bool > mBooleanProperties;
};