#pragma once

#include <memory>
#include "../math/matrix4x4.h"
#include "../math/vec3.h"

// Forward declaration.
class Node;
class RenderCore;

/// <summary>
/// SceneNodeObject is a baseclass of an object that can be attached to a scene node.
/// </summary>
class SceneNodeObject
	{
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		SceneNodeObject( RenderCore &core );

		/// <summary>
		/// Destructor.
		/// </summary>
		virtual ~SceneNodeObject();
		SceneNodeObject( const SceneNodeObject &obj ) = delete;
		SceneNodeObject& operator=( const SceneNodeObject &obj ) = delete;

	public:
		/// <summary>
		/// Sets the parent node of this object.
		/// </summary>
		/// <param name="parentNode">Pointer to our parent node.</param>
		void setParentNode( std::shared_ptr<Node> parentNode );

		/// <summary>
		/// Returns the parent node of this object.
		/// </summary>
		/// <returns>Pointer to parent node.</returns>
		std::shared_ptr<Node> getParentNode() const;

		/// <summary>
		/// Returns the transformation matrix for transforming this renderable to world coordinates.
		/// </summary>
		/// <returns>Transformation matrix for transforming all vertex coordinates to world coordinates.</returns>
		Matrix4x4 getTransformationMatrix() const;

		/// <summary>
		/// Sets this object to be hidden or visible.
		/// </summary>
		/// <param name="hidden">Pass 'true' to make this object hidden. 'False' to make this visible.</param>
		void setHidden( bool hidden );

		/// <summary>
		/// Tells if this object is currently hidden.
		/// </summary>
		/// <returns>Returns 'true' if this object is hidden.</returns>
		bool isHidden() const;

		/// <summary>
		/// Enables or disables the shadow casting from this object. If shadow casting is disabled,
		/// this object is excluded from the shadowmap rendering pass.
		/// </summary>
		/// <param name="castShadows">
		/// Set to 'true' to include this object to shadowmap rendering pass. False to exclude
		/// this object from shadow rendering pass.
		/// </param>
		void setShadowCasting( bool castShadows );

		/// <summary>
		/// Tells if this object can cast shadows.
		/// </summary>
		/// <returns>Returns 'true' if this object can cast shadows.</returns>
		bool isCastingShadows() const;

		/// <summary>
		/// Set the position of the parent scene node. Throws an error if parent node has not been set.
		/// </summary>
		/// <param name="pos">Position for the parent scene node.</param>
		void setPosition( const Vec3 &pos );

		/// <summary>
		/// Returns the position of the parent node. Throws an error if parent node has not been set.
		/// </summary>
		/// <returns>Position where the parent node has been positioned to.</returns>
		Vec3 getPosition() const;

	protected:
		/// <summary>The render engine's core object.</summary>
		RenderCore &mCore;

		/// <summary>The parent node.</summary>
		std::shared_ptr<Node> mParentNode;

		/// <summary>Is this object currently hidden.</summary>
		bool mHidden;

		/// <summary>Can this object cast shadows.</summary>
		bool mCastShadows;
	};
