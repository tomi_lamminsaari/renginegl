
#include <assert.h>
#include <boost/format.hpp>
#include "lightsourcecollection.h"
#include "lightsources.h"
#include "materialmanager.h"
#include "mesh.h"
#include "meshmanager.h"
#include "node.h"
#include "overlayrenderable.h"
#include "renderable.h"
#include "rendercore.h"
#include "renderstatistics.h"
#include "scene.h"
#include "shadermanager.h"
#include "shaderprogram.h"
#include "meshcreation/quadmesh.h"
#include "meshcreation/uvtexturerange.h"
#include "particles/particlemanager.h"
#include "renderutils/renderprimitives.h"
#include "renderutils/shadowrenderer.h"
#include "renderutils/gbufferfbo.h"
#include "utilities/loggingutil.h"


std::shared_ptr<Scene> Scene::createScene( RenderCore& core )
{
	std::shared_ptr<Scene> scene( new Scene( core ) );
	scene->initilizeScene();
	return scene;
}

Scene::Scene( RenderCore& core ) :
	mCore( core ),
	mRootNode( new Node ),
	mLightSourceCollection( new LightSourceCollection( *this ) ),
	mSkyBox( core ),
	mAmbientLight( Rgba( 0.6f, 0.7f, 1.0f ), 0.4f ),
	mRenderShadowMap( false )
{
	// Reserve some capacity to the renderable containers.
	mRenderables.reserve( 2048 );
}

void Scene::initilizeScene()
{
	// Create the scene's global directional lightsource. This
	// casts shadows.
	auto sunlight = mLightSourceCollection->createDirectionalLight(
			Vec3( -0.5f, -1.0f, 1.0f ), Rgba::White, 0.2f );

	// Initialize shadow renderers.
	static_assert( KMaxShadowCasterCount == 3, "Adjust shadow renderer instance creation here." );
	mShadowRenderers.push_back( std::shared_ptr< ShadowRenderer >( new ShadowRenderer( mCore ) ) );
	for( auto& shadowRenderer : mShadowRenderers )
	{
		// Create ID for the shadow renderer and create an instance of it.
		std::string rendererName = mCore.generateId( RenderCore::IDGenerator::FrameBufferObject );
		shadowRenderer->initialize( rendererName );
	}

	// Initialize the framebuffers for deferred shading.
	mCore.createGBuffer( BuiltIn::GBUFFER_FBO, 1600, 1200 );
}

Scene::~Scene()
{

}

std::shared_ptr<Renderable> Scene::createRenderable( const std::string& renderableId, std::shared_ptr<Node> parentNode )
{
	// Check if renderable with given ID already exists.
	auto existingRenderable = findRenderable( renderableId );
	assert( existingRenderable == nullptr );
	if( existingRenderable )
		throw std::runtime_error( "Renderable creation failed. Renderable id '" + renderableId + "' already exists." );

	// Create the renderable and add it to an array. Sort the
	std::shared_ptr<Renderable> renderable( new Renderable( mCore ) );
	renderable->setId( renderableId );
	mRenderables.push_back(renderable );
	sortRenderableContainer( mRenderables );

	// Create node for the renderable so that we can move it. Attach this renderable specific node
	// to the parent node or scene's root node.
	std::shared_ptr<Node> childNode = nullptr;
	if( parentNode != nullptr )
		childNode = parentNode->createSubNode();
	else
		childNode = mRootNode->createSubNode();
	childNode->attachRenderable( renderable );

	return renderable;
}

std::shared_ptr<OverlayRenderable> Scene::createOverlayRenderable( const std::string& renderableId )
{
	// Check if ID is already in use.
	auto existingIter = mOverlayRenderables.find( renderableId );
	if( existingIter != mOverlayRenderables.end() )
	{
		// Renderable ID already in use.
		assert( existingIter != mOverlayRenderables.end() );
		auto msgFormat = boost::format( "Failed to create OverlayRenderable. ID %1% is already in use." ) % renderableId.c_str();
		throw std::invalid_argument( msgFormat.str() );
	}

	// Create the renderable.
	std::shared_ptr<OverlayRenderable> renderable = std::make_shared<OverlayRenderable>( mCore );
	mOverlayRenderables[ renderableId ] = renderable;
	return renderable;
}

void Scene::destroyRenderable( const std::string& renderableId )
{
	// Search from regular renderables containers.
	bool foundFromRegular = false;
	RenderableContainer::const_iterator iter = findRenderableFromContainer( mRenderables, renderableId );
	if( iter != mRenderables.end() )
	{
		// Detach renderable's node from its parent node.
		std::shared_ptr<Renderable> renderable = *iter;
		std::shared_ptr<Node> parentNode = renderable->getParentNode();
		parentNode->detachFromParent();

		// Detach the renderable from its node.
		parentNode->detachRenderable( renderable );

		// Remove the renderable.
		mRenderables.erase( iter );

		// We found from regular renderables, no need to search from other renderables.
		foundFromRegular = true;
	}

	// Seach from OverlayRenderable container if necessary.
	if( foundFromRegular == false )
	{
		auto iter3 = mOverlayRenderables.find( renderableId );
		if( iter3 != mOverlayRenderables.end() )
		{
			// Remove the renderable.
			mOverlayRenderables.erase( iter3 );
		}
	}
}

ParticleSystem* Scene::createParticleSystem(
	const std::string& particleSystemId,
	ParticleSystemType particleType,
	std::shared_ptr<Node> parentNode )
{
	// Get particle system from particle manager. This will activate the particle system. It is possible that
	// our free particle system pool is empty. In that case we don't receive particle system but a nullpointer
	// instead.
	ParticleSystem* spawnedSystem = mCore.getParticleManager().getFreeParticleSystem( particleType );

	// Add that particle system to scene to make it visible.
	if( spawnedSystem != nullptr )
	{
		spawnedSystem->attachToScene( shared_from_this(), parentNode );
	}
	return spawnedSystem;
}

void Scene::destroyParticleSystem( const std::string& particleSystemId )
{
}

std::shared_ptr<LightSource> Scene::createLightSource(
	const std::string& lightsourceId,
	std::shared_ptr<Node> parentNode )
{
	// Create the lightsource object.
	auto lightSourcePtr = mLightSourceCollection->createLightSource( lightsourceId );

	// Create the node and attach lightsource with it.
	// Create node for the renderable so that we can move it. Attach this renderable specific node
	// to the parent node or scene's root node.
	std::shared_ptr<Node> childNode;
	if( parentNode != nullptr )
		childNode = parentNode->createSubNode();
	else
		childNode = mRootNode->createSubNode();
	childNode->attachLightSource( lightSourcePtr );
	return lightSourcePtr;
}

void Scene::destroyLightSource( const std::string& lightsourceId )
{
	mLightSourceCollection->destroyLightSource( lightsourceId );
}

void Scene::setAmbientLight( const LightColorInfo& ambient )
{
	mAmbientLight = ambient;
}

const LightColorInfo& Scene::getAmbientLight() const
{
	return mAmbientLight;
}

std::shared_ptr<Renderable> Scene::findRenderable( const std::string& renderableId ) const
{
	// Search from regular renderables and then from batch renderables.
	auto iterRegular = findRenderableFromContainer( mRenderables, renderableId );
	if( iterRegular != mRenderables.end() && ! ( renderableId < ( *iterRegular )->getId() ) )
		return *iterRegular;
	return nullptr;
}

std::shared_ptr<Node> Scene::getRootNode() const
{
	return mRootNode;
}

void Scene::prepareRender( std::shared_ptr<Camera>& camera )
{
	// Not needed yet.
}

void Scene::render( std::shared_ptr<Camera>& camera )
{
	mFrameTimeMeasurement.startTimingEvent( FrameTimeMeasurement::TimeSubEvent::FullFrame );

	// Rendering of scene consists of following render passes:
	// - Shadow pass
	// - Objects pass

	// Render shadows to shadowmap.
	renderShadows( camera );

	// Render Skybox first.
	renderSkyBox( camera );

	// Render objects with deferred shading mode.
	renderObjectsDeferredShading( camera );

	// Render overlay renderables.
	renderOverlayObjects();

	// Debug renderings.
	if( mRenderShadowMap )
	{
		std::shared_ptr< ShadowRenderer > shadowRenderer = getShadowRenderer( 0 );
		std::shared_ptr<Texture> depthTex = shadowRenderer->getShadowMapTexture();
		RenderPrimitives::renderUntransformedTexturedQuad( mCore, depthTex );
	}

	mFrameTimeMeasurement.endTimingEvent( FrameTimeMeasurement::TimeSubEvent::FullFrame );
}

RenderCore& Scene::getCore()
{
	return mCore;
}

std::shared_ptr<LightSource> Scene::findLightSource( const std::string& lightId ) const
{
	return mLightSourceCollection->getLightSource( lightId );
}

std::shared_ptr< ShadowRenderer > Scene::getShadowRenderer( size_t index )
{
	if( index >= 0 && index < mShadowRenderers.size() )
		return mShadowRenderers[ index ];
	return std::shared_ptr< ShadowRenderer >( nullptr );
}

SkyBox& Scene::getSkyBox()
{
	return mSkyBox;
}

void Scene::setDebugDrawShadowmap( bool enabled )
{
	mRenderShadowMap = enabled;
}

bool Scene::isDebugDrawShadowmapEnabled() const
{
	return mRenderShadowMap;
}

FrameTimeMeasurement Scene::getFrameTimeMeasurements() const
{
	return mFrameTimeMeasurement;
}

void Scene::renderSkyBox( std::shared_ptr<Camera>& camera )
{
	// Don't render anything if skybox has been disabled.
	if( mSkyBox.isEnabled() == false )
		return;

	// Position the skybox to the location where the camera is.
	mSkyBox.setCenterPosition( camera->getPosition() );

	// Render the skybox. Disable depth buffer rendering.
	GLASSERT( glDisable( GL_DEPTH_TEST ) );

	// Skybox is a special renderable that is normally hidden. Enable
	// the skybox renderable before rending it.
	auto skyboxRenderable = mSkyBox.getRenderable();
	skyboxRenderable->setHidden( false );
	skyboxRenderable->prepareRender( *camera );

	// Render the skybox contents.
	SceneGlobalRenderPassInfo skyboxRenderPassInfo;
	skyboxRenderable->render( *camera, &skyboxRenderPassInfo );

	// Set skybox back to hidden.
	skyboxRenderable->setHidden( true );

	// Enable the depth buffer usage.
	GLASSERT( glEnable( GL_DEPTH_TEST ) );
}

void Scene::renderShadows( std::shared_ptr<Camera>& camera )
{
	// Enable the shadow texture to be the render target.
	std::shared_ptr< ShadowRenderer > shadowRenderer = getShadowRenderer( 0 );
	if( shadowRenderer == nullptr )
		return;
	mFrameTimeMeasurement.startTimingEvent( FrameTimeMeasurement::TimeSubEvent::ShadowmapPass );
	shadowRenderer->bindAsRenderTarget();

	// Clear the shadowmap buffer and set the shadow rendering camera to the
	// position of the lightsource.
	std::shared_ptr<LightSource> sunlight = findLightSource( BuiltIn::LIGHT_SUN );

	// Prepare the shadow rendering pass.
	GLASSERT( glClear( GL_DEPTH_BUFFER_BIT ) );
	GLASSERT( glCullFace( GL_BACK ) );

	// Skip rendering of shadows if we do not have global shadow casting
	// lightsource.
	if( sunlight != nullptr )
	{
		// Position the shadow render pass camera.
		shadowRenderer->updateShadowRegion( *mCore.getCamera(), sunlight->getDirectionalLightDirection() );

		//sunlight->setVisibleSceneCenterPosition( Vec3( 0.0f, 0.0f, 0.0f ) );
		std::shared_ptr<Camera> lightSourceCamera = shadowRenderer->getCamera();

		// Render all objects with our special shadow renderer.
		std::shared_ptr<Material> overrideMaterial = mCore.getMaterialManager().findMaterial( shadowRenderer->getShadowMaterial() );
		SceneGlobalRenderPassInfo shadowRenderPassInfo;
		shadowRenderPassInfo.setOverrideMaterial( overrideMaterial );

		// Render all renderables.
		auto iter = mRenderables.begin();
		while( iter != mRenderables.end() )
		{
			// Render the renderable if visibility conditions match.
			auto& renderable = *iter;
			if( renderable->isHidden() == false &&
				renderable->isCastingShadows() == true )
			{
				// Not hidden. Render this renderable.
				renderable->setVisibilityInCamera( true );
				renderable->render( *lightSourceCamera, &shadowRenderPassInfo );
			}

			// Move to next renderable.
			iter++;
		}
	}

	// Restore the default framebuffer.
	GLASSERT( glCullFace( GL_BACK ) );
	shadowRenderer->unbindAsRenderTarget();
	mFrameTimeMeasurement.endTimingEvent( FrameTimeMeasurement::TimeSubEvent::ShadowmapPass );
}

void Scene::renderObjectsDeferredShading( std::shared_ptr<Camera>& camera )
{
	// Initialize the lightsource vector.
	mFrameTimeMeasurement.startTimingEvent( FrameTimeMeasurement::TimeSubEvent::GeometryPass );
	auto activeLightSources = mLightSourceCollection->getLightSources();
	mGlobalRenderPassInfo.setAmbientLight( mAmbientLight );
	mGlobalRenderPassInfo.setSpecularLight( LightColorInfo( Rgba( 1.0f, 1.0f, 1.0f ), 0.1f ) );

	// Geometry pass.
	// ---------------
	// Render the geometry, normals and diffuse colors to G-Buffer.

	// Activate the GBuffer to be the rendering target.
	auto gbuffer = mCore.getGBuffer( BuiltIn::GBUFFER_FBO );
	gbuffer->setAsRenderTarget( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );

	// Activate the shader for geometry pass. Set the camera uniforms to the shader.
	auto shaderProgramNormal = mCore.getShaderManager().findShaderProgram( BuiltIn::SHADER_DEFERREDSHADING_GEOMETRYPASS );
	glEnable( GL_DEPTH_TEST );
	glEnable( GL_CULL_FACE );

#if 1

	// Start rendering single renderables.
	shaderProgramNormal->activateProgram();

	// Render all regular renderables.
	for( auto& renderable : mRenderables )
	{
		renderable->prepareRender( *camera );
		renderable->renderSimplifiedMaterial( *camera, *shaderProgramNormal, &mGlobalRenderPassInfo );
	}
#endif

	// The geometry pass is complete. Unbind the GBuffer from being render target.
	gbuffer->removeAsRenderTarget();

	// Render SkyBox mesh.
	renderSkyBox( camera );

	mFrameTimeMeasurement.endTimingEvent( FrameTimeMeasurement::TimeSubEvent::GeometryPass );
	mFrameTimeMeasurement.startTimingEvent( FrameTimeMeasurement::TimeSubEvent::ShadingPass );

	// Shading pass.
	// -----------------
	// Render entire screen as a quad and produce the final lighting. For that we first set the
	// lighting pass shader. Then bind the GBuffer textures to be source textures and finally we render the
	// fullscreen mesh.

	// Activate shading pass shader.
	auto lightingPassShader = mCore.getShaderManager().findShaderProgram( BuiltIn::SHADER_DEFERREDSHADING_LIGHTINGPASS );
	lightingPassShader->activateProgram();

	// Set the lightsource arrays to shading pass shader.
	size_t lightIndex = 0;
	for( auto ls : activeLightSources )
	{
		std::string lightUniform = "uLightSources[" + std::to_string( lightIndex ) + "]";
		if( ls->getType() == LightSource::LightType::Point )
		{
			lightingPassShader->setUniformVec3( lightUniform + ".pos", ls->getPosition() );
			lightingPassShader->setUniformVec3( lightUniform + ".color", ls->getLightColor().asVec3() );
			lightingPassShader->setUniformFloat( lightUniform + ".intensity", ls->getIntensity() );
		}
		else
		{
			// TODO: Maybe the directional light should be provided as separate uniform instead of one
			// ligh in ligts-array. Mainly because with directional light the '.pos' member does have
			// the direction of light and not actual light position.
			assert( lightIndex == 0 );
			lightingPassShader->setUniformVec3( lightUniform + ".pos", ls->getDirectionalLightDirection() );
			lightingPassShader->setUniformVec3( lightUniform + ".color", ls->getLightColor().asVec3() );
			lightingPassShader->setUniformFloat( lightUniform + ".intensity", ls->getIntensity() );
		}
		lightIndex++;
	}

	// Bind the G-Buffer textures to the texture units.
	gbuffer->bindAsSourceTexture( GBufferFBO::TargetTexture::DiffuseSpecularTexture, GL_TEXTURE0 );
	gbuffer->bindAsSourceTexture( GBufferFBO::TargetTexture::NormalsTexture, GL_TEXTURE1 );
	gbuffer->bindAsSourceTexture( GBufferFBO::TargetTexture::PositionsTexture, GL_TEXTURE2 );
	std::shared_ptr< ShadowRenderer > shadowRenderer = getShadowRenderer( 0 );
	if( shadowRenderer != nullptr )
	{
		shadowRenderer->bindShadowMapTexture( GL_TEXTURE3 );
		auto lightSourceCamera = shadowRenderer->getCamera();
		Matrix4x4 lightVP = lightSourceCamera->getViewProjMatrix();
		lightingPassShader->setShadowMappingLightMatrix( lightVP );
	}

	// Set the camera position.
	lightingPassShader->setUniformVec3( "uEyePos", camera->getPosition() );
	lightingPassShader->setAmbientLight( mAmbientLight.getColorIntensityCombined(), 0.0f );

	// Render the fullscreen quad.
	auto quadMesh = mCore.getMeshManager().findMesh( BuiltIn::MESH_DEFERREDSHADING_QUAD );
	glEnable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	quadMesh->render();

	// Rendering complete.
	glDisable( GL_BLEND );
	glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );
	mFrameTimeMeasurement.endTimingEvent( FrameTimeMeasurement::TimeSubEvent::ShadingPass );
}

void Scene::renderOverlayObjects()
{
	// Disable depth buffer usage.
	glDisable( GL_DEPTH_TEST );
	glDepthMask( GL_FALSE );
	glDisable( GL_CULL_FACE );

	// Render overlay renderables one by one.
	for( auto renderablePair : mOverlayRenderables )
	{
		renderablePair.second->render( &mGlobalRenderPassInfo );
	}

	glEnable( GL_DEPTH_TEST );
	glDepthMask( GL_TRUE );
}

void Scene::sortRenderableContainer( RenderableContainer& renderableContainer )
{
	// Sort the container.
	std::sort( std::begin( renderableContainer ), std::end( renderableContainer ),
		[]( const std::shared_ptr<Renderable>& r1, const std::shared_ptr<Renderable>& r2 )
		{
			return r1->getId() < r2->getId();
		} );
}

Scene::RenderableContainer::const_iterator Scene::findRenderableFromContainer(
	const RenderableContainer& renderableContainer,
	const std::string& renderableId
) const
{
	auto lowerBound = std::lower_bound( std::begin( renderableContainer ), std::end( renderableContainer ), renderableId,
		[]( const std::shared_ptr<Renderable>& r1, const std::string& searchId )
		{
			return r1->getId() < searchId;
		} );
	return lowerBound;
}