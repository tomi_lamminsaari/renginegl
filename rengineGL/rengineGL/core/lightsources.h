
#pragma once

#include <GL/glew.h>
#include <string>
#include "scenenodeobject.h"
#include "lightsourceinfos.h"
#include "../math/vec3.h"
#include "../utils/rgba.h"

// Forward declarations.
class Scene;
class Renderable;

/**
* @class ShadowInfo
* @brief Contains the generic information about lightsource's shadow casting capabilities.
*/
class ShadowInfo
	{
	public:
		ShadowInfo();
		~ShadowInfo();
		ShadowInfo( const ShadowInfo &obj );
		ShadowInfo& operator=( const ShadowInfo &obj );

	public:
		void setEnabled( bool enabled );
		bool isEnabled() const;

	private:
		bool mEnabled;
	};



/**
* @class LightSource
* @brief This class represents a point light source.
*/
class LightSource : public SceneNodeObject
	{
	public:
		/** Defines the possible type of light source. */
		enum class LightType
			{
			Uninitialized, Point, Directional, Spotlight
			};

		/**
		* Constructs a lightsource. It will be located to the origo and has
		* intensity of 1.0.
		* @param core The render engine's core object.
		*/
		LightSource( RenderCore &core );

		/**
		* Destructor.
		*/
		~LightSource();

		LightSource( const LightSource &obj ) = delete;
		LightSource& operator = ( const LightSource &obj ) = delete;

	public:

		/**
		* Sets the intensity of this light.
		* @param lightIntensity Intensity of the light.
		*/
		void setIntensity( GLfloat lightIntensity );

		/**
		* Returns the intensity of this light.
		* @return Intensity of this light.
		*/
		GLfloat getIntensity() const;

		/**
		* Sets the color of the lightsource.
		* @param lightColor Color of this light.
		*/
		void setLightColor( const Rgba &lightColor );

		/**
		* Returns the color of this lightsource.
		* @return Color of this lightsource.
		*/
		const Rgba& getLightColor() const;

		LightType getType() const;

		/**
		* Converts this lightsource to be a directional light source.
		* @param lightDir Direction of the light rays.
		* @param lightColor Color of the light that comes from this light source.
		* @param lightIntensity The intensity of the light.
		*/
		void makeDirectionalLightSource(
			const Vec3 &lightDir,
			const Rgba &lightColor,
			float lightIntensity );

		/**
		* Converts this light source to be a point light source. The light source will be positioned
		* to certain position and emit light to every direction.
		*
		* The point light sources can't cast shadows currently.
		* @param pos The position of this lightsource in relation to the parent node.
		* @param lightColor Color of the light this light source emits.
		* @param lightIntensity The intensitory of the light.
		*/
		void makePointLightSource(
			const Vec3 &pos,
			const Rgba &lightColor,
			float lightIntensity );

		/**
		* Converts this light source to be spot light source.
		*
		* NOT IMPLEMENTED YET!
		*/
		void makeSpotLightSource(
			const Vec3 &pos,
			const Rgba &lightColor );

		/**
		* Gets light direction from lightsource position to given reference point.
		* If lightsource is plain directional light, the direction is same for all
		* reference points.
		* @param point The reference point we calculate the direction vector to.
		* @return Direction vector of the light as unit vector.
		*/
		Vec3 getLightDirection( const Vec3 &point ) const;

		/// <summary>
		/// Returns the light direction. Throws an exception if light source is not
		/// directional light by type.
		/// </summary>
		/// <returns>Direction</returns>
		Vec3 getDirectionalLightDirection() const;

		void setLightDirection( const Vec3 &lightDir );

		void showInScene( std::shared_ptr<Scene> scene );

	private:
		DirectionalLightSourceInfo mDirectionalLightInfo;
		PointLightSourceInfo mPointLightInfo;
		LightType mLightType;
		std::shared_ptr<Scene> mScene;
		std::string mRenderableId;
		std::shared_ptr<Renderable> mRenderable;
	};
