
#pragma once

#include <iostream>
#include <GL/glew.h>
#include <assert.h>
#include <string>
#include <vector>

inline void checkGlError()
{
	GLenum _glerror = glGetError(); 
	if( _glerror != GL_NO_ERROR ) {		
		std::string errorText;
		if( _glerror == GL_INVALID_ENUM )
			errorText = "GL_INVALID_ENUM";
		else if( _glerror == GL_INVALID_VALUE )
			errorText = "GL_INVALID_VALUE";
		else if( _glerror == GL_INVALID_OPERATION )
			errorText = "GL_INVALID_OPERATION";
		else if( _glerror == GL_INVALID_FRAMEBUFFER_OPERATION )
			errorText = "GL_INVALID_FRAMEBUFFER_OPERATION";
		else if( _glerror == GL_OUT_OF_MEMORY )
			errorText = "GL_OUT_OF_MEMORY";
		else if( _glerror == GL_STACK_UNDERFLOW )
			errorText = "GL_STACK_UNDERFLOW";
		else if( _glerror == GL_STACK_OVERFLOW )
			errorText = "GL_STACK_OVERFLOW";
		else if( _glerror == GL_INVALID_INDEX )
			errorText = "GL_INVALID_INDEX";
		std::cerr << "glGetError(): " << errorText << std::endl;
		assert( _glerror == GL_NO_ERROR );
	}
}

// Define assertion macro that calls a function and checks the OpenGL errors and expects them to be GL_NO_ERROR.
#if defined(_DEBUG)
#define GLASSERT( glfunc ) { \
	glfunc; \
    checkGlError(); \
}

#else
#define GLASSERT( glfunc ) glfunc;
#endif

// Assertion macro that checks if there are active OpenGL errors.
#if defined(_DEBUG)
#define GLASSERT_EMPTY() { \
	checkGlError(); \
}
#else
#define GLASSERT_EMPTY()
#endif

// Clear OpenGL errors.
#define GLCLEARERROR() { \
    while( glGetError() != GL_NO_ERROR ) { } \
}

/// <summary>Maximum number of active lightsources the scene can have at once.</summary>
const size_t KMaxLightSourceCount = 6;

/// <summary>Maximum number of shadow casting lightsources the scene can have at once.</summary>
const size_t KMaxShadowCasterCount = 3;

/// <summary>The distance from lightsource where light attenuation starts.</summary>
const float KLightAttenuationDistance = 30.0f;

/// <summary>Width of the world region where shadowmap affects.</summary>
const float KShadowMapRegionWidth = 500.0f;

/// <summary>Breadth of world region where shadowmap affects.</summary>
const float KShadowMapRegionBreadth = 500.0f;

/// <summary>Number of water sprinkler particle systems we have by default available in particlesystem pool.</summary>
const unsigned int KDefaultParticleSprinklerPoolSize = 10;

/// <summary>Number of spark particle systems we have by default available in particlesystem pool.</summary>
const unsigned int KDefaultParticleSparksPoolSize = 10;

/// <summary>Defines if faces are created double sided or single sided.</summary>
enum class FaceSides { SingleSide, DoubleSide };

/// <summary>Maximum number of vertices one batch renderable's mesh can contain.</summary>
const size_t KMaxBatchVertexBufferSize = 700000;

namespace BuiltIn
{

	// Default texture names
	
	/// <summary>Name of built-in default flat normal map.</summary>
	const char TEXTURE_DEFAULT_NORMAL_MAP[] = "default-normals.png";

	// Default material names

	/// <summary>Name of built-in black material.</summary>
	const char MATERIAL_BLACK[] = "_mat001";

	/// <summary>Name of built-in material that can change color dynamically through material's color property.</summary>
	const char MATERIAL_COLOR[] = "_mat002";

	/// <summary>Name of built-in material where each vertex has separate color.</summary>
	const char MATERIAL_COLOR_VERTEX[] = "_mat003";

	/// <summary>Name of built-in material where each vertex has also texture coordinates.</summary>
	const char MATERIAL_TEXTURE_VERTEX[] = "_mat004";

	/// <summary>Name of built-in material that supports texture and light model.</summary>
	const char MATERIAL_TEXTURE_VERTEX_LIGHTS[] = "_mat005";

	/// <summary>Name of built-in material that is used for rendering diffuse light source cube.</summary>
	const char MATERIAL_DIFFUSE_LIGHT_SOURCE[] = "_mat007";

	/// <summary>Name of built-in shadow mapping material.</summary>
	const char MATERIAL_SHADOWMAP[] = "_mat008";

    /// <summary>Name of built-in skybox material.</summary>
    const char MATERIAL_SKYBOX[] = "_mat009";

	/// <summary>Name of built-in particle system material.</summary>
	const char MATERIAL_PARTICLESYSTEM[] = "_mat010";

	/// <summary>Name of built-in batch renderer material.</summary>
	const char MATERIAL_BATCH_RENDERER[] = "_mat011";

	/// <summary>Name of built-in material for rendering untransformed textured mesh.</summary>
	const char MATERIAL_UNTRANSFORMED_TEXTURE[] = "_mat013";

	// Default mesh names.

	/// <summary>Mesh with unit cube geometry.</summary>
	const char MESH_UNIT_CUBE[] = "_unitcube";

	/// <summary>Mesh for rendering full screen texture.</summary>
	const char MESH_TEXTURE_PREVIEW[] = "_mesh001";

    /// <summary>Mesh for default skybox.</summary>
    const char MESH_SKYBOX[] = "_mesh002";

	/// <summary>Fullscreen quad for deferred shading rendering.</summary>
	const char MESH_DEFERREDSHADING_QUAD[] = "_mesh003";


	// Default light sources.

	/// <summary>Default directional lightsource.</summary>
	const char LIGHT_SUN[] = "_light#sun";


	// Default shader names.

	/// <summary>Name of built-in black shader.</summary>
	const char SHADER_BLACK[] = "_sha001";

	 /// <summary>Name of built-in shader that has color coming from material.</summary>
	const char SHADER_COLOR[] = "_sha002";
	
	/// <summary>Name of built-in shader that has color for each vertex.</summary>
	const char SHADER_COLOR_VERTEX[] = "_sha003";

	/// <summary>Name of built-in shader that inputs vertex coordinate and 1 texture coordinate.</summary>
	const char SHADER_TEXTURE_VERTEX[] = "_sha004";

	/// <summary>Name of built-in shader that inputs vertex coordinate and 1 texture coordinate. It also has the basic light model.</summary>
	const char SHADER_TEXTURE_VERTEX_LIGHTS[] = "_sha005";

	/// <summary>Name of built-in shader for wavefront meshes without textures.</summary>
	const char SHADER_POS_NORM_LIGHTS[] = "_sha007";

	/// <summary>Name of built-in shadow map rendering shader.</summary>
	const char SHADER_SHADOWMAP[] = "_sha010";

	/// <summary>Name of built-in shader for simple untransformed texture rendering.</summary>
	const char SHADER_UNTRANSFORMED_TEXTURE[] = "_sha011";

	/// <summary>Name of the built-in shader that uses special specular light blending texture.</summary>
	const char SHADER_WAVEFRONT_TEXTURE_SPECULAR_TEXTURE[] = "_sha012";

	/// <summary>Name of the built-in shader that takes diffuse color from uTexture0 and normal map from uTexture1</summary>
	const char SHADER_WAVEFRONT_TEXTURE_NORMAL_TEXTURE[] = "_sha013";

	/// <summary>Name of the built-in default particle system shader.</summary>
	const char SHADER_PARTICLESYSTEM[] = "_sha014";

	/// <summary>Name of the built-in batch renderer shader.</summary>
	const char SHADER_BATCH_RENDERER[] = "_sha015";

	/// <summary>Name of the built-in Wavefront object shader.</summary>
	const char SHADER_WAVEFRONT_TEX_SOLID[] = "_sha016";

	/// <summary>Name of the shader for geometry pass of deferred shading rendering.</summary>
	const char SHADER_DEFERREDSHADING_GEOMETRYPASS[] = "_sha017";

	/// <summary>Name of the shader for lighting pass of deferred shading rendering.</summary>
	const char SHADER_DEFERREDSHADING_LIGHTINGPASS[] = "_sha018";

	// <summary>Name of shader for batched rendering with deferred shading geometry pass.</summary>
	const char SHADER_DEFERREDSHADING_GEOMETRYPASS_BATCHED[] = "_sha019";

	// Framebuffer names.

	/// <summary>GBuffer FBO that is used rendering with deferred shading mode.</summary>
	const char GBUFFER_FBO[] = "_gbuffer";
}
