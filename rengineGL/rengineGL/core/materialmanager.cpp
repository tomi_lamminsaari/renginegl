
#include <assert.h>
#include "defines.h"
#include "material.h"
#include "materialmanager.h"

MaterialManager::MaterialManager( RenderCore& core ) :
	ManagerBase( core )
{

}

MaterialManager::~MaterialManager()
{
	mMaterials.clear();
}

void MaterialManager::cleanup()
{
	mMaterials.clear();
}

bool MaterialManager::initializeDefaultMaterials()
{
	// Construct black material.
	std::shared_ptr<Material> mat = createMaterial( BuiltIn::MATERIAL_BLACK );
	mat->setShaderProgramById( BuiltIn::SHADER_BLACK );

	// Construct material for dynamic colors.
	mat = createMaterial( BuiltIn::MATERIAL_COLOR );
	mat->setShaderProgramById( BuiltIn::SHADER_COLOR );

	// Construct material for simple texture mapping.
	mat = createMaterial( BuiltIn::MATERIAL_TEXTURE_VERTEX );
	mat->setShaderProgramById( BuiltIn::SHADER_TEXTURE_VERTEX );

	// Construct material for diffuse light source cube.
	mat = createMaterial( BuiltIn::MATERIAL_DIFFUSE_LIGHT_SOURCE );
	mat->setShaderProgramById( BuiltIn::SHADER_COLOR );

	// Construct the shadow mapping material.
	mat = createMaterial( BuiltIn::MATERIAL_SHADOWMAP );
	mat->setShaderProgramById( BuiltIn::SHADER_SHADOWMAP );

	// Construct the SkyBox material-
	mat = createMaterial( BuiltIn::MATERIAL_SKYBOX );
	mat->setShaderProgramById( BuiltIn::SHADER_TEXTURE_VERTEX );
	mat->setTexture( 0, "custom-skybox" );

	// Construct sparks material.
	mat = createMaterial( BuiltIn::MATERIAL_PARTICLESYSTEM );
	mat->setShaderProgramById( BuiltIn::SHADER_PARTICLESYSTEM );

	// Construct batch renderer material.
	mat = createMaterial( BuiltIn::MATERIAL_BATCH_RENDERER );
	mat->setShaderProgramById( BuiltIn::SHADER_BATCH_RENDERER );

	// Construct material for untransformed texture mesh.
	mat = createMaterial( BuiltIn::MATERIAL_UNTRANSFORMED_TEXTURE );
	mat->setShaderProgramById( BuiltIn::SHADER_UNTRANSFORMED_TEXTURE );

	return true;
}

std::shared_ptr<Material> MaterialManager::createMaterial( const std::string& materialId )
{
	std::shared_ptr<Material> existingMaterial = findMaterial( materialId );
	assert( existingMaterial == nullptr );
	if( existingMaterial )
		throw std::invalid_argument( "Material with ID '" + materialId + "' already exists." );

	// Create the material.
	std::shared_ptr<Material> mat( new Material( *this, materialId ) );
	mMaterials[ materialId ] = mat;
	return mat;
}

std::shared_ptr<Material> MaterialManager::cloneMaterial( const std::string& materialId, const std::string& existingMaterialId )
{
	// Get the existing material instance.
	std::shared_ptr<Material> oldMaterial = findMaterial( existingMaterialId );
	if( !oldMaterial )
		throw std::invalid_argument( "Cannot clone material '" + existingMaterialId + "'. Material does not exist." );

	// Create new material instance.
	std::shared_ptr<Material> newMaterial = createMaterial( materialId );

	// Clone the material properties from old material to new material.
	if( newMaterial )
		newMaterial->cloneProperties( *oldMaterial.get() );
	return newMaterial;
}

void MaterialManager::destroyMaterial( const std::string& materialId )
{
	auto iter = mMaterials.find( materialId );
	if( iter != mMaterials.end() )
		mMaterials.erase( iter );
}

std::shared_ptr<Material> MaterialManager::findMaterial( const std::string& materialId ) const
{
	auto iter = mMaterials.find( materialId );
	if( iter != mMaterials.end() )
		return iter->second;
	return nullptr;
}
