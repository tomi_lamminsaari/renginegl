
#include "defines.h"
#include "lightsources.h"
#include "node.h"
#include "renderable.h"
#include "rendercore.h"
#include "scene.h"

LightSource::LightSource( RenderCore &core ) :
	SceneNodeObject( core ),
	mLightType( LightType::Uninitialized ),
	mScene( nullptr ),
	mRenderable( nullptr )
{

}

LightSource::~LightSource()
{

}

void LightSource::setIntensity( const GLfloat lightIntensity )
{
	switch( mLightType )
	{
	case LightType::Directional:
		mDirectionalLightInfo.mLightIntensity = lightIntensity;
		break;
	case LightType::Point:
		mPointLightInfo.mLightIntensity = lightIntensity;
		break;
	default:
		break;
	}
}

GLfloat LightSource::getIntensity() const
{
	GLfloat intensity = 0.0f;
	switch( mLightType )
	{
	case LightType::Directional:
		intensity = mDirectionalLightInfo.mLightIntensity;
		break;

	case LightType::Point:
		intensity = mPointLightInfo.mLightIntensity;
		break;

	case LightType::Spotlight:
		intensity = 1.0f;
		break;
	}
	return intensity;
}

void LightSource::setLightColor( const Rgba &lightColor )
{
	switch( mLightType )
	{
	case LightType::Directional:
		mDirectionalLightInfo.mLightColor = lightColor;
		break;

	case LightType::Point:
		mPointLightInfo.mLightColor = lightColor;
		break;
	}
}

const Rgba &LightSource::getLightColor() const
{
	switch( mLightType )
	{
	case LightType::Directional:
		return mDirectionalLightInfo.mLightColor;

	case LightType::Point:
		return mPointLightInfo.mLightColor;
	}
	return Rgba::White;
}

void LightSource::makeDirectionalLightSource(
	const Vec3 &lightDir,
	const Rgba &lightColor,
	float lightIntensity
)
{
	mLightType = LightType::Directional;
	mDirectionalLightInfo = DirectionalLightSourceInfo( lightDir, lightColor, lightIntensity );
}

void LightSource::makePointLightSource(
	const Vec3 &pos,
	const Rgba &lightColor,
	float lightIntensity )
{
	mLightType = LightType::Point;
	mPointLightInfo = PointLightSourceInfo( pos, lightColor, lightIntensity );
	setPosition( pos );
}

Vec3 LightSource::getLightDirection( const Vec3 &point ) const
{
	// Based on light source type calculate the direction of the light
	// in given position.
	if( mLightType == LightType::Directional )
		return mDirectionalLightInfo.mLightDir;
	else if( mLightType == LightType::Point )
		return point - mPointLightInfo.mLightSourcePos;
	return Vec3( 0.0f, 1.0f, 0.0f );
}

Vec3 LightSource::getDirectionalLightDirection() const
{
	if( mLightType == LightType::Directional )
		return mDirectionalLightInfo.mLightDir;
	throw RengineError( "Light source is not a directional light." );
}

void LightSource::setLightDirection( const Vec3 &lightDir )
{
	if( mLightType == LightType::Directional )
		mDirectionalLightInfo.mLightDir = lightDir.normalized();
}

LightSource::LightType LightSource::getType() const
{
	return mLightType;
}

void LightSource::showInScene( std::shared_ptr<Scene> scene )
{
	// If detached from scene, remove the renderable.
	if( mScene != nullptr && mRenderableId.length() > 0 )
	{
		mScene->destroyRenderable( mRenderableId );
		mRenderableId = "";
		mScene = nullptr;
		mRenderable = nullptr;
	}

	// Store the scene.
	mScene = scene;

	// Don't attempt to show uninitialized light source.
	if( mLightType == LightType::Uninitialized )
		return;

	// Create renderable for the light.
	if( mScene != nullptr && mLightType != LightType::Directional )
	{
		// The new renderable will be attached to same node where this lightsource
		// is attached to. This way the lightsource renderable will move together
		// with the lightsource.
		mRenderableId = scene->getCore().generateId( RenderCore::IDGenerator::Mesh );
		mRenderable = scene->createRenderable( mRenderableId, mParentNode );
		mRenderable->setGeometryIds( BuiltIn::MESH_UNIT_CUBE, BuiltIn::MATERIAL_DIFFUSE_LIGHT_SOURCE );
		mRenderable->setShadowCasting( false );
	}
}
