
#pragma once

#include <map>
#include <memory>
#include <mutex>
#include <vector>
#include "../math/rect2d.h"
#include "../math/vec4.h"
#include "rengineconfig.h"
#include "renderstatistics.h"

// Forward declarations
struct ALLEGRO_DISPLAY;
struct ALLEGRO_FONT;
class AnimatorBase;
class Camera;
class FrameBufferObject;
class GBufferFBO;
class MaterialManager;
class Mesh;
class MeshManager;
class ModelManager;
class ParticleManager;
class Scene;
class ShaderManager;
class TextureManager;

/// <summary>
/// An interface to host environment the RenderCore may use to get data from hosting application.
/// </summary>
class IRenderCoreHost
	{
	public:
		/// <summary>
		/// Called by RenderCore when an error message should be shown. The hosting application is allowed
		/// to ignore this call and return immediately without showing any messages.
		/// </summary>
		/// <param name="messageText">The error message to show.</param>
		virtual void showErrorMessage( const std::string &messageText ) = 0;

		/// <summary>
		/// Prints the given line to terminal window. Each printed line has its own ID. This way multiple prints
		/// with the same ID will be print to the terminal window only once.
		/// </summary>
		/// <param name="lineId">ID of the line.</param>
		/// <param name="message">The message text to be print.</param>
		virtual void printToTerminalWindow( int lineId, const std::string &message ) = 0;
	};

/// <summary>
/// The most central class of the 3D render engine.
/// <para>
/// RenderCore class the core class of the engine. It provides access to all resource managers, such as
/// MeshManager and ShaderManager. It also contains the methods for rendering the current scene.
/// </para>
/// </summary>
class RenderCore
{
public:
	/// <summary>IDs of different ID generators the RenderCore object provides.</summary>
	enum class IDGenerator { Material, Mesh, Model, Shader, Texture, Renderable, LightSource, FrameBufferObject, Animator, Particles };

public:
	/// <summary>
	/// Constructor.
	/// </summary>
	RenderCore();

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~RenderCore();

	RenderCore(const RenderCore &obj) = delete;
	RenderCore& operator = (const RenderCore &obj) = delete;

public:

	/// <summary>
	/// Initializes the render engine. Loads and creates all the the relevant built-in resources.
	/// </summary>
	/// <param name="host">Pointer to host interface. Can be null.</param>
	/// <exception cref="RengineError">
	/// Thrown if rendering engine initialization fails rengineGL engine related reason.
	/// </exception>
	/// <exception cref="AllegroError">
	/// Thrown if rendering engine initialization fails because of an error to access some
	/// Allegro library specific resource.
	/// </exception>
	void initialize( IRenderCoreHost *host );

	/// <summary>
	/// Uninitializes the render engine. Releases and unloads all the resouces from GPU memory.
	/// </summary>
	void uninitialize();

	/// <summary>
	/// Updates the dynamic elements such as animators.
	/// </summary>
	/// <param name="secsSincePrevious">Number of seconds from previous call.</param>
	void update( double secsSincePrevious );

	/// <summary>
	/// Renders the current scene when called.
	/// </summary>
	void render();

	/// <summary>
	/// Returns the scene-object.
	/// </summary>
	/// <returns>Pointer to scene object. Ownership is not transferred.</returns>
	std::shared_ptr<Scene> getScene() const;

	/// <summary>
	/// Sets the rendering view to given screen shape.
	/// </summary>
	/// <param name="w">Width of the view where to render.</param>
	/// <param name="h">Height of the view where to render.</param>
	void setViewport(int w, int h);

	/// <summary>
	/// Returns the current camera that will be used when rendering the scene.
	/// </summary>
	/// <returns>Pointer to current camera. Ownership is not transferred.</returns>
	std::shared_ptr<Camera> getCamera() const;

	/// <returns>Returns access to MeshManager.</returns>
	MeshManager& getMeshManager();

	/// <summary>
	/// Returns access to MaterialManager.
	/// </summary>
	/// <returns>Returns access to MaterialManager.</returns>
	MaterialManager& getMaterialManager();

	/// <summary>Returns access to ModelManager.</summary>
	/// <returns>Returns access to ModelManager.</returns>
	ModelManager& getModelManager();

	/// <summary>Returns access to ShaderManager.</summary>
	/// <returns>Returns access to ShaderManager.</returns>
	ShaderManager& getShaderManager();

	/// <summary>Returns access to TextureManager.</summary>
	/// <returns>Returns access to TextureManager.</returns>
	TextureManager& getTextureManager();

	/// <summary>Returns access to ParticleManager.</summary>
	/// <returns>Returns access to ParticleManager.</returns>
	ParticleManager& getParticleManager();

	/// <summary>
	/// Generates new unique ID for textures, shaders, meshes, etc.
	/// </summary>
	/// <param name="generatorType">Type of ID to be generated.</param>
	/// <returns>New ID for requested resource.</returns>
	std::string generateId(IDGenerator generatorType);

	/// <summary>
	/// Returns the absolute path in current runtime environment. If given path begins with "assets://" scheme, this
	/// function maps the path to absolute file path from where the file can be loaded.
	/// </summary>
	/// <param name="relativePath">Logical path to the file.</param>
	/// <returns>File path from where the file can be actually loaded from.</returns>
	std::string getAbsolutePath(const std::string &relativePath);

	/// <summary>
	/// Loads text file from file to a string object.
	/// </summary>
	/// <param name="relativePath">Relative file path from where to load the file.</param>
	/// <param name="textBufferOut">Receives the loaded text data.</param>
	/// <returns>Returns true if loading was successful. Returns false if loading fails.</returns>
	bool loadTextFile(const std::string &relativePath, std::string &textBufferOut);

	/// <summary>
	/// Returns active rendering statistics. Different render engine components can log rendering
	/// statistics like mesh counts, shader program changes, etc. to this statistics object.
	/// </summary>
	/// <returns>Reference to statistics object.</returns>
	RenderStatistics& getActiveStats();

	/// <summary>
	/// An message dialog will be shown in the UI when calling this function.
	/// </summary>
	/// <param name="messageText">The message shown in the dialog.</param>
	void showMessage( const std::string &messageText );

	/// <summary>
	/// Prints debug line to terminal window.
	/// </summary>
	/// <param name="lineId">Unique line identifier. Using same identifier twice will overwrite the previous line.</param>
	/// <param name="message">The line to print.</param>
	void printTerminalMessage( int lineId, const std::string &message );

	/// <summary>
	/// Creates new framebuffer object that can be used as rendering target. The returned frame buffer object instance
	/// is not initialized yet so the caller must initialize it before it can be used.
	/// </summary>
	/// <param name="fboName">Unique name of the framebuffer object.</param>
	/// <returns>Pointer to new framebuffer object.</returns>
	std::shared_ptr< FrameBufferObject > createFBO( const std::string &fboName );

	/// <summary>
	/// Destroys the framebuffer object.
	/// </summary>
	/// <param name="fboName">Name of the framebuffer to be destroyed.</param>
	void destroyFBO( const std::string &fboName );

	/// <summary>
	/// Returns a framebuffer object.
	/// </summary>
	/// <param name="fboName">Name of the framebuffer object to return.</param>
	/// <returns>Pointer to matching framebuffer object. Nullptr if not found.</returns>
	std::shared_ptr< FrameBufferObject > getFBO( const std::string &fboName ) const;

	/// <summary>
	/// Creates new GBuffer framebuffer.
	/// </summary>
	/// <param name="gbufferName">Name of the GBuffer object.</param>
	/// <param name="bufferWidth">Width of the GBuffer that gets created.</param>
	/// <param name="bufferHeight">Height of the GBuffer that gets created.</param>
	/// <returns>Pointer to new GBuffer object.</returns>
	std::shared_ptr<GBufferFBO> createGBuffer( const std::string& gbufferName, uint32_t bufferWidth, uint32_t bufferHeight );

	/// <summary>
	/// Name of the GBuffer that should be removed.
	/// </summary>
	/// <param name="gbufferName">Name of the GBuffer.</param>
	void destroyGBuffer( const std::string& gbufferName );

	/// <summary>
	/// Returns an existing GBuffer object.
	/// </summary>
	/// <param name="gbufferName">Name of the GBuffer to return.</param>
	/// <returns>Pointer to GBuffer. Nullptr if GBuffer was not found.</returns>
	std::shared_ptr< GBufferFBO > getGBuffer( const std::string& gbufferName ) const;

	/// <summary>
	/// Adds new animator.
	/// </summary>
	/// <param name="animatorId">ID of the animator.</param>
	/// <param name="animator">Smart pointer to the animator object.</param>
	void addAnimator( const std::string &animatorId, std::shared_ptr< AnimatorBase> animator );

	/// <summary>
	/// Gets the generic configuration data.
	/// </summary>
	/// <returns>The configuration data.</returns>
	RengineConfig& getConfig();

private:
	void uploadAllToGPU();

private:

	/// <summary>The material manager instance.</summary>
	std::unique_ptr<MaterialManager> mMaterialManager;

	/// <summary>The mesh manager instance.</summary>
	std::unique_ptr<MeshManager> mMeshManager;

	/// <summary>The model manager instance.</summary>
	std::unique_ptr<ModelManager> mModelManager;

	/// <summary>The shader manager instance.</summary>
	std::unique_ptr<ShaderManager> mShaderManager;

	/// <summary>The texture manager instance.</summary>
	std::unique_ptr<TextureManager> mTextureManager;

	/// <summary>The particle manager instance.</summary>
	std::unique_ptr<ParticleManager> mParticleManager;

	/// <summary>The master scene of this rendering engine.</summary>
	std::shared_ptr<Scene> mScene;

	/// <summary>The primary camera that will be used when rendering the scene contents to framebuffer.</summary>
	std::shared_ptr<Camera> mCamera;

	/// <summary>The color to clear the screen when starting the rendering.</summary>
	Vec4 mClearColor;

	/// <summary>The viewport of the screen where to render.</summary>
	Rect2D mScreenRect;

	/// <summary>The statistics object we use to keep track on some rendering statistics.</summary>
	RenderStatistics mActiveStats;

	/// <summary>Pointer to application object that provides access to outside application environment.</summary>
	IRenderCoreHost *mHostApp;

	/// <summary>Collection of created framebuffer objects.</summary>
	std::map< std::string, std::shared_ptr< FrameBufferObject > > mFrameBufferObjects;

	/// <summary>Collection of created GBuffers objects</summary>
	std::map< std::string, std::shared_ptr< GBufferFBO > > mGBufferObjects;

	/// <summary>Collection of animator object that will be animated by the rendering engine.</summary>
	std::map< std::string, std::shared_ptr< AnimatorBase > > mAnimators;

	/// <summary>An object that contains the configurations.</summary>
	RengineConfig mConfig;

	ALLEGRO_FONT* mCourierFont;

	/// <summary>Mutex that protects the access to ID generators.</summary>
	std::mutex mIDGeneratorMutex;
	int mIDCounterMaterial;
	int mIDCounterMesh;
	int mIDCounterModel;
	int mIDCounterShader;
	int mIDCounterTexture;
	int mIDCounterRenderable;
	int mIDCounterLightSource;
	int mIDCounterFBO;
	int mIDCounterAnimator;
	int mIDCounterParticles;
};