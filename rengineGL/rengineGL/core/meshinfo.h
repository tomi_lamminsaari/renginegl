
#pragma once

#include <map>
#include <string>

/// <summary>
/// MeshInfo contains extra metadata that can be attached to Mesh objects.
/// As an example the created grid meshes contain MeshInfo objects that contain the information about
/// the grid meshes themselves. Also information if mesh is a "cone" or "cuboid" can be storaged to the
/// mesh as MeshInfo object.
/// </summary>
class MeshInfo
	{
	public:
		static const std::string KBoolIsGrid;
		static const std::string KIntGridWidth;
		static const std::string KIntGridHeight;
		static const std::string KIntConeVerticesCount;
		static const std::string KStringMeshShape;

	public:
		/// <summary>
		/// Constructs an empty MeshInfo object.
		/// </summary>
		MeshInfo();

		/// <summary>
		/// A copy constructor.
		/// </summary>
		MeshInfo( const MeshInfo &obj );

		/// <summary>
		/// Destructor.
		/// </summary>
		~MeshInfo();

		/// <summary>
		/// An assignment operator.
		/// </summary>
		MeshInfo& operator=( const MeshInfo &obj );

	public:

		/// <summary>
		/// Sets the grid data to this info. When grid meshes get created, those meshes will have MeshInfo members
		/// that have set the grid details like this. Once this has been called, the grid flag is set. Use 'clearGridInfo()'
		/// function to erase the grid information from the object.
		/// <para>
		/// Calling this function will set the following metadata values:
		/// - KBoolIsGrid set to true.
		/// - KIntGridWidth set to given 'verticesWidth' parameter.
		/// - KIntGridHeight set to given 'verticesHeight' parameter.
		/// </para>
		/// </summary>
		/// <param name="verticesWidth">Width of the grid mesh. Number of vertices there are along the width axis.</param>
		/// <param name="verticesHeight">Height of the grid mesh. Number of vertices there are along the height axis.</param>
		void setGridInfo( int verticesWidth, int verticesHeight );

		/// <summary>
		/// Clears the grid related metadata properties.
		/// </summary>
		void clearGridInfo();

		/// <summary>
		/// Adds extra integer data to this info. This data can later be queried with 'getIntInfo()' function.
		/// </summary>
		/// <param name="infoId">An unique ID of the data item to set.</param>
		/// <param name="intData">The data value itself that will be set to this MeshInfo instance.</param>
		void addIntInfo( const std::string& infoId, int intData );

		/// <summary>
		/// Adds extra boolean data to this info. This data can later be queried with 'getBoolInfo()' function.
		/// </summary>
		/// <param name="infoId">An unique ID of the boolean to set.</param>
		/// <param name="boolData">The boolean value itself.</param>
		void addBoolInfo( const std::string& infoId, bool boolData );

		/// <summary>
		/// Adds string data to this info. This data can later be queried with 'getStringInfo()' function.
		/// </summary>
		/// <param name="infoId">>An unique ID of the string value to set.</param>
		/// <param name="stringData">The string value itself.</param>
		void addStringInfo( const std::string& infoId, const std::string& stringData );

		/// <summary>
		/// Tells if the grid is actually a grid-mesh. This means that KBoolIsGrid metadata property has been set.
		/// </summary>
		/// <returns>Returns 'true' if the mesh is a grid mesh.</returns>
		bool isGrid() const;

		/// <summary>
		/// Returns the grid related details. Width and height of the grid. Number of vertices there are along
		/// the width and height axises.
		/// </summary>
		/// <param name="outWidthVertices">Receives the width of the mesh.</param>
		/// <param name="outHeightVertices">Receives the height of the mesh.</param>
		void getGridInfo( int& outWidthVertices, int& outHeightVertices ) const;

		/// <summary>
		/// Returns the int values that have been stored earlier with 'addIntInfo()' function.
		/// </summary>
		/// <param name="infoId">An unique ID of the data item to return.</param>
		/// <param name="outValue">Receives the value of the int value. Receives 0 if data does not exist.</param>
		/// <returns>Tells if requested value was found.</returns>
		bool getIntInfo( const std::string& infoId, int& outValue ) const;

		/// <summary>
		/// Returns the boolean values that have been stored earlier with 'addBoolInfo()' function.
		/// </summary>
		/// <param name="infoId">An unique ID of the boolean property to return.</param>
		/// <param name="outValue">Receives the boolean value stored earlier. Receives 'false' if data does not exist.</param>
		/// <returns>Tells if requested value was found.</returns>
		bool getBoolInfo( const std::string& infoId, bool& outValue ) const;

		/// <summary>
		/// Returns the string value that has been stored earlier with 'addStringInfo()' function.
		/// </summary>
		/// <param name="infoId">Unique ID of the string data to return.</param>
		/// <param name="outValue">Receives the string value stored earlier. Receives an empty string if data does not exist.</param>
		/// <returns>Tells if request value was found.</returns>
		bool getStringInfo( const std::string& infoId, std::string& outValue ) const;

	private:
		/// <summary>Will contain all metadata parameters that are integer type.</summary>
		std::map<std::string, int> mIntData;

		/// <summary>Will contain all metadata parameters that are boolean type.</summary>
		std::map<std::string, bool> mBoolData;

		/// <summary>Will contain all string type metadata parameters.</summary>
		std::map<std::string, std::string> mStringData;
	};