
#include <assert.h>
#include "material.h"
#include "materialmanager.h"
#include "rendercore.h"
#include "shadermanager.h"
#include "shaderprogram.h"
#include "texture.h"
#include "texturemanager.h"

Material::Material( MaterialManager &manager, const std::string &id ) :
	mMaterialManager( manager ),
	mId( id ),
	mShaderProgram( nullptr ),
	mSolidColor( 0.0f, 0.0f, 0.0f, 1.0f ),
	mSpecularColor( Rgba::White, 32.0f ),
	mSpecularReflectTexture()
{
	// Allocate slots for textures.
	TextureData tex;
	mTextures.resize( KMaxTextureCount, tex );
}

Material::~Material()
{

}

void Material::apply() const
{
	// Apply material by activating the shader program first.
	if( mShaderProgram != nullptr )
		mShaderProgram->activateProgram();

	assert( mShaderProgram != nullptr );

	// Setup the texture units.
	applyTextures();

	// Setup special specular/reflection texture.
	if( mSpecularReflectTexture.enabled &&
		mShaderProgram->hasUniform( ShaderProgram::KUniformName_TextureSpecular ) )
	{
		// Shader program has specular/reflection texture uniform.
		if( mSpecularReflectTexture.texture == nullptr )
		{
			// Texture not cached yet. Find it from the texture manager.
			mSpecularReflectTexture.texture = mMaterialManager.getCore().getTextureManager().findTexture(
					mSpecularReflectTexture.textureId );
		}
	}

	// Setup common material specific uniforms.

	// Common matrices.
	auto iter1 = mMat4Uniforms.begin();
	while( iter1 != mMat4Uniforms.end() )
	{
		mShaderProgram->setUniformMat4x4( iter1->first, iter1->second );
		iter1++;
	}

	// Set material specific generic properties. These includes properties like solid and
	// specular colors.
	mShaderProgram->setUniformVec4( ShaderProgram::KUniformName_Color, mSolidColor.asVec4() );
	mShaderProgram->setSpecularColor( mSpecularColor.specularColor, mSpecularColor.specularExponent, true );

	// Set the float uniforms to the application.
	auto floatUniformsIter = mFloatUniforms.begin();
	while( floatUniformsIter != mFloatUniforms.end() )
	{
		// Set the float uniform value to the shader program.
		mShaderProgram->setUniformFloat( floatUniformsIter->first, floatUniformsIter->second );

		// Move to next uniform.
		floatUniformsIter++;
	}
}

void Material::unapply() const
{

}

void Material::applyTextures() const
{
	// Bind the textures.
	bool manytex = false;
	size_t textureUnitNumber = KCustomTextureIndex0;
	for( TextureData& texData : mTextures )
	{
		if( texData.enabled )
		{
			// This texture has been enabled. Get the texture pointer.
			std::shared_ptr<Texture>& texture = texData.texture;
			if( texture == nullptr )
			{
				// No cached texture yet. Find it from the texture manager and store the pointer for
				// future use.
				texture = mMaterialManager.getCore().getTextureManager().findTexture( texData.textureId );
				texData.texture = texture;
			}

			// Activate the texture unit.
			if( texture != nullptr )
			{
				// Bind the texture to the texture unit.
				texture->activateTexture( textureUnitNumber );
			}
		}

		textureUnitNumber++;
	}
}

std::shared_ptr<ShaderProgram> Material::getShaderProgram() const
{
	return mShaderProgram;
}

const std::string &Material::getShaderProgramId() const
{
	return mShaderProgramId;
}

void Material::setShaderProgramById( const std::string &shaderId )
{
	ShaderManager &shaman = mMaterialManager.getCore().getShaderManager();
	std::shared_ptr<ShaderProgram> program = shaman.findShaderProgram( shaderId );
	assert( program != nullptr );
	mShaderProgram = program;
	mShaderProgramId = shaderId;
}

const std::string &Material::getId() const
{
	return mId;
}

void Material::setTexture( size_t textureNumber, const std::string &textureId )
{
	assert( textureNumber < KMaxTextureCount );

	mTextures[ textureNumber ].enabled = true;
	mTextures[ textureNumber ].textureId = textureId;
	mTextures[ textureNumber ].texture = nullptr;
}

void Material::removeTexture( size_t textureNumber )
{
	assert( textureNumber < KMaxTextureCount );

	mTextures[ textureNumber ].enabled = false;
	mTextures[ textureNumber ].textureId = "";
	mTextures[ textureNumber ].texture = nullptr;
}

const TextureData& Material::getTextureData( size_t textureNumber ) const
{
	assert( textureNumber < KMaxTextureCount );

	return mTextures[ textureNumber ];
}

void Material::setSolidColor( const Rgba &solidColor )
{
	mSolidColor = solidColor;
}

const Rgba &Material::getSolidColor() const
{
	return mSolidColor;
}

void Material::setSolidColorDisabledValue()
{
	mSolidColor.forceSetValues( -1.0f, -1.0f, -1.0f, 0.0f );
}

void Material::setSpecularColor( const SpecularColor &specularColor )
{
	mSpecularColor = specularColor;
}

const SpecularColor &Material::getSpecularColor() const
{
	return mSpecularColor;
}

void Material::setFloatUniform( const std::string &uniformName, float uniformValue )
{
	mFloatUniforms[ uniformName ] = uniformValue;
}

void Material::setSpecularReflectionTexture( bool enabled, const std::string &textureName )
{
	mSpecularReflectTexture.enabled = enabled;
	mSpecularReflectTexture.textureId = textureName;
	mSpecularReflectTexture.texture.reset();
}

Texture* Material::resolveTexturePointer( size_t textureIndex )
{
	TextureData& texData = mTextures[ textureIndex ];
	if( texData.enabled )
	{
		// Texture unit has been enabled. Find the texture object unless the
		// texture object has already been resolved.
		// This texture has been enabled. Get the texture pointer.
		std::shared_ptr<Texture>& texture = texData.texture;
		return texture.get();
	}
	return nullptr;
}

size_t Material::getEnabledTexturesCount() const
{
	// Go all textures through and calculate how many of them has been enabled.
	size_t enabledCount = 0;
	for( const TextureData& texData : mTextures )
	{
		if( texData.enabled )
			enabledCount++;
	}
	return enabledCount;
}

void Material::cloneProperties( const Material &otherMaterial )
{
	mShaderProgramId = otherMaterial.mShaderProgramId;
	mMat4Uniforms = otherMaterial.mMat4Uniforms;
	mVec3Uniforms = otherMaterial.mVec3Uniforms;
	mFloatUniforms = otherMaterial.mFloatUniforms;
	mSolidColor = otherMaterial.mSolidColor;
	mShaderProgram = otherMaterial.mShaderProgram;
	mSpecularColor = otherMaterial.mSpecularColor;
	for( size_t i = 0; i < mTextures.size(); ++i )
	{
		mTextures[ i ] = otherMaterial.mTextures[ i ];
	}
}
