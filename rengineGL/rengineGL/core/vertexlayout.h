
#pragma once

#include <vector>

/// <summary>
/// A type that defines the vertex attribute types.
/// </summary>
enum class VertexElement
{
	Position3f,
	Normal3f,
	Texture2f,
	Color4f,
	Tangent3f,
	Bitangent3f,
	Color3f,
	TextureIndex1f,
	Specular1f
};

/// <summary>
/// VertexLayout class defines the vertex attributes that each mesh vertex will have.
/// </summary>
class VertexLayout
{
public:

	/// <summary>Index of normal map texture attributes in default mesh structure.</summary>
	static const size_t KNormalmapTextureIndex = 1;

	/// <summary>
	/// Returns the number of bytes the given vertex attribute type consumes.
	/// </summary>
	/// <param name="element">The vertex element to check for.</param>
	/// <returns>Number of bytes the given vertex attribute consumes.</returns>
	static size_t getElementSizeBytes( VertexElement element );

	/// <summary>
	/// Returns the number of float-primitives the given vertex attribute requires.
	/// </summary>
	/// <param name="element">The vertex element to check for.</param>
	/// <returns>Number of bytes the vertex element occupiens.</returns>
	static size_t getFloatCountOfElement( VertexElement element );

	/// <summary>
	/// Gets the default vertex layout that can be used with deferred shading rendering mode.
	/// </summary>
	/// <returns>The default VertexLayout data.</returns>
	static VertexLayout getDefaultLayout_DeferredShading();
	
	/// <summary>
	/// Default constructor. Constructs empty vertex layout structure.
	/// </summary>
	VertexLayout();

	/// <summary>
	/// Constructs the VertexLayout object and initializes it with elements from given vetor.
	/// </summary>
	/// <param name="vertexElementOrder"The order the vertex elements will be in vertex buffer.</param>
	VertexLayout( const std::vector<VertexElement> &vertexElementOrder );

	/// <summary>
	/// Copy constructor.
	/// </summary>
	VertexLayout( const VertexLayout &obj );

	/// <summary>
	/// Destructor.
	/// </summary>
	~VertexLayout();

	/// <summary>
	/// An assignment operator.
	/// </summary>
	VertexLayout &operator = ( const VertexLayout& obj );

	/// <summary>
	/// Equality comparison operator.
	/// </summary>
	bool operator==( const VertexLayout& obj ) const;

	/// <summary>
	/// Indexing operator.
	/// </summary>
	/// <param name="index">Index of the vertex element to access.</param>
	/// <returns>The element.</returns>
	VertexElement operator[] ( size_t index ) const;

	/// <summary>
	/// Returns the total number of bytes the vertex requires when created with this VertexLayout.
	/// </summary>
	/// <returns>Number of bytes a vertex will consume.</returns>
	size_t getSizeBytes() const;

	/// <summary>
	/// Returns the number of 'floats' a vertex will require.
	/// </summary>
	/// <returns>Number of float the vertex will consume.</returns>
	size_t getFloatCount() const;

	/// <summary>
	/// Returns the number of vertex attributes the VertexLayout object has.
	/// </summary>
	/// <returns>Number vertex attributes the object has.</returns>
	size_t getElementCount() const;

	/// <summary>
	/// Returns the index of first 'float' that belongs requested vertex attribute.
	/// </summary>
	/// <param name="element">The vertex attribute to search for.</param>
	/// <param name="instanceOfType">
	/// A vertex could have multiple same kind of vertex attributes. This defines
	/// how "manyeth" same vertex attribute we should take into account.
	/// </param>
	/// <returns>
	/// Index of the float where 'element' vertex attribute starts. -1 if given vertex element
	/// doesn't belong to this VertexLayout.
	/// </returns>
	int getFloatIndex(VertexElement element, int instanceOfType = 0) const;

	/// <summary>
	/// Tells if layout contains the given vertex element.
	/// </summary>
	/// <param name="element">The vertex element whose existence will be checked.</param>
	/// <returns>Returns 'true' if vertex element exists in this layout.</returns>
	bool hasElement( VertexElement element ) const;

private:
	/// <summary>The container of the vertex attribute types.</summary>
	std::vector<VertexElement> mVertexElementOrder;

	/// <summary>Number of bytes one vertex will consume.</summary>
	size_t mVertexSizeBytes;

	/// <summary>Number of floats the vertex has.</summary>
	size_t mFloatCount;
};