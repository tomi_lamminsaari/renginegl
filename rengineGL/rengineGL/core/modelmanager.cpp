
#include <assert.h>
#include "model.h"
#include "modelmanager.h"

ModelManager::ModelManager( RenderCore &core ) :
	ManagerBase( core )
{

}

ModelManager::~ModelManager()
{
	// Empty the model container.
	mModels.clear();
}

void ModelManager::cleanup()
{
	mModels.clear();
}

std::shared_ptr<Model> ModelManager::createModel( const std::string &modelId )
{
	// Attempt to find model with given ID. IDs should be unique so there shouldn't be
	// model already with given ID.
	std::shared_ptr<Model> existingModel = getModel( modelId );
	assert( existingModel == nullptr );
	if( existingModel )
		throw std::invalid_argument( "Cannot create model '" + modelId + "'. Model ID already exists." );

	// Create new model and add it to the collection.
	std::shared_ptr<Model> newModel( new Model( modelId, *this ) );
	mModels[ modelId ] = newModel;
	return newModel;
}

bool ModelManager::addModel( std::shared_ptr<Model> model )
{
	// Attempt to find model with given ID. IDs should be unique so there shouldn't be
	// model already with given ID.
	std::shared_ptr<Model> existingModel = getModel( model->getId() );
	assert( existingModel == nullptr );
	if( existingModel )
		return false;

	// Add model to the collection.
	mModels[ model->getId() ] = model;
	return true;
}

void ModelManager::deleteModel( const std::string &modelId )
{
	// Find the iterator that points to the location where the model should be.
	// Remove model if integer is valid.
	auto iter = mModels.find( modelId );
	if( iter != mModels.end() )
		mModels.erase( iter );
}

std::shared_ptr<Model> ModelManager::getModel( const std::string &modelId ) const
{
	// Find the model and return pointer to it.
	auto iter = mModels.find( modelId );
	if( iter != mModels.end() )
		return iter->second;
	return nullptr;
}

size_t ModelManager::getModelCount() const
{
	return mModels.size();
}

std::vector<std::string> ModelManager::getModelIds() const
{
	std::vector<std::string> modelIdVec;
	auto iter = mModels.begin();
	while( iter != mModels.end() )
	{
		modelIdVec.push_back( iter->first );
		iter++;
	}
	return modelIdVec;
}
