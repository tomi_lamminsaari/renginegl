
#include <exception>
#include "scenenodeobject.h"
#include "node.h"


SceneNodeObject::SceneNodeObject( RenderCore &core ) :
	mCore( core ),
	mParentNode( nullptr ),
	mHidden( false ),
	mCastShadows( true )
{

}

SceneNodeObject::~SceneNodeObject()
{

}

void SceneNodeObject::setParentNode( std::shared_ptr<Node> parentNode )
{
	mParentNode = parentNode;
}

std::shared_ptr<Node> SceneNodeObject::getParentNode() const
{
	return mParentNode;
}

Matrix4x4 SceneNodeObject::getTransformationMatrix() const
{
	Matrix4x4 transMatrix;
	transMatrix.setIdentity();
	if( mParentNode != nullptr )
		transMatrix = mParentNode->getTransformationMatrix();
	return transMatrix;
}

void SceneNodeObject::setHidden( bool hidden )
{
	mHidden = hidden;
}

bool SceneNodeObject::isHidden() const
{
	return mHidden;
}

void SceneNodeObject::setShadowCasting( bool castShadows )
{
	mCastShadows = castShadows;
}

bool SceneNodeObject::isCastingShadows() const
{
	return mCastShadows;
}

void SceneNodeObject::setPosition( const Vec3 &pos )
{
	if( !mParentNode )
		throw std::invalid_argument( "Can't set node position because parent node has not been set." );
	mParentNode->setPosition( pos );
}

Vec3 SceneNodeObject::getPosition() const
{
	if( !mParentNode )
		throw std::invalid_argument( "Can't get parent node position because parent node has not been set." );
	return mParentNode->getPosition();
}
