
#pragma once

#include <stdexcept>
#include <GL/glew.h>

/// <summary>
/// RengineError is the exception class of the exceptions that are rengineGL specific errors.
/// </summary>
class RengineError : public std::runtime_error
{
public:

	/// <summary>
	/// Checks if there is an OpenGL error flag set and throws an error if it is. This will use
	// the glGetError() function to check the error and throw an error if it returns other than
	// GL_NO_ERROR. The exception object will contain the given error message and the OpenGL error
	// code as well.
	/// </summary>
	/// <param name="errorMessage">
	/// The error message to be set to the exception object if there is an active error.
	/// </param>
	/// <exception cref = "RengineError">Thrown if there is an OpenGL error flags has been set.</exception>
	static void check( const std::string &errorMessage );

	/// <summary>
	/// Clears the OpenGL error and ignores them all. After this the glGetError() no
	/// longer return any error.
	/// </summary>
	static void clearError();

	/// <summary>
	/// Constructs an error object with given error message. The OpenGL error will be set to GL_NO_ERROR.
	/// </summary>
	/// <param id="what_arg">A string containing the error message.</param>
	explicit RengineError( const std::string &what_arg );

	/// <summary>
	/// Constructs an error object with given error message. The OpenGL error will be set to GL_NO_ERROR.
	/// </summary>
	/// <param name="what_arg">A C string containing the error message.</param>
	explicit RengineError( const char *what_arg );

	/// <summary>
	/// Constructs an error object with given error message and with given OpenGL error code.
	/// </summary>
	/// <param name="what_arg">The error message</param>
	/// <param name="errorCode">The OpenGL error code to set.</param>
	RengineError( const std::string &what_arg, GLenum errorCode );

	/// <summary>
	/// Sets the OpenGL error code.
	/// </summary>
	/// <param name="errorCode">The OpenGL error code to be set.</param>
	void setOpenGLError( GLenum errorCode );

	/// <summary>
	/// Returns the OpenGL error code this exception object carries.
	/// </summary>
	/// <returns>The OpenGL error code.</returns>
	GLenum getOpenGLError() const;

private:
	/// <summary>Possible OpenGL error code.</summary>
	GLenum mGLError;
};

/// <summary>
/// SDLError is an exception type the rengineGL throws if it encounters an error
/// originating from SDL2 programming library.
/// </summary>
class SDLError : public std::runtime_error
{
public:
	explicit SDLError( const std::string& what_arg );
	explicit SDLError( const char* what_arg );
};
