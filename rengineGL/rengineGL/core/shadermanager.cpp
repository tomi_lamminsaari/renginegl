
#include <assert.h>
#include <stdexcept>
#include <system_error>
#include "defines.h"
#include "rendercore.h"
#include "shadermanager.h"
#include "shaderprogram.h"
#include "glstructs/glpointlight.h"
#include "../utils/conversionutils.h"

ShaderManager::ShaderManager( RenderCore &core ) :
	ManagerBase( core )
{

}

ShaderManager::~ShaderManager()
{
	mShaders.clear();
}

void ShaderManager::cleanup()
{
	removeFromGPU();
	mShaders.clear();
}

bool ShaderManager::initializeDefaultShaders()
{
	std::shared_ptr<ShaderProgram> tempShader;
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_BLACK, "assets://built-in/shader-solid-black.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_COLOR, "assets://built-in/shader-solid-color.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_TEXTURE_VERTEX, "assets://built-in/shader-single-texture.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_SHADOWMAP, "assets://built-in/shader-shadow-mapping.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_UNTRANSFORMED_TEXTURE, "assets://built-in/shader-untransformed-textured-quad.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_WAVEFRONT_TEXTURE_SPECULAR_TEXTURE, "assets://built-in/shader-wavefront-texture-speculartexture.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_WAVEFRONT_TEXTURE_NORMAL_TEXTURE, "assets://built-in/shader-wavefront-texture-normalmap.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_PARTICLESYSTEM, "assets://built-in/shader-particles.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_BATCH_RENDERER, "assets://built-in/shader-batchrenderer.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_WAVEFRONT_TEX_SOLID, "assets://built-in/shader-wavefront-tex-solid.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_DEFERREDSHADING_GEOMETRYPASS, "assets://built-in/deferredshadingshaders/shader-geometrypass.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_DEFERREDSHADING_GEOMETRYPASS_BATCHED, "assets://built-in/deferredshadingshaders/shader-geometrypass-batched.glsl" );
	tempShader = loadSingleShaderProgram( BuiltIn::SHADER_DEFERREDSHADING_LIGHTINGPASS, "assets://built-in/deferredshadingshaders/shader-shadingpass.glsl" );
	uploadToGPU();
	return true;
}

std::shared_ptr<ShaderProgram> ShaderManager::createShaderProgram(
	const std::string &programId,
	const std::string &vshSource,
	const std::string &fshSource )
{
	std::shared_ptr<ShaderProgram> program( new ShaderProgram( *this ) );
	program->setProgramId( programId );
	program->setSourceCodes( vshSource, fshSource );
	mShaders[ programId ] = program;
	return program;
}

std::shared_ptr<ShaderProgram> ShaderManager::createSingleShaderProgram(
	const std::string &programId,
	const std::string &shaderSource )
{
	// Replace the template variables from the source code with current runtime variables.

	// Create the shader program object.
	std::shared_ptr<ShaderProgram> program( new ShaderProgram( *this ) );
	program->setProgramId( programId );
	program->setSingleSourceCode( shaderSource );
	mShaders[ programId ] = program;
	return program;
}

std::shared_ptr<ShaderProgram> ShaderManager::loadSingleShaderProgram(
	const std::string &programId,
	const std::string &shaderPath )
{
	// Load the shader file from the filesystem.
	std::string shaderSource;
	if( !mCore.loadTextFile( shaderPath, shaderSource ) )
	{
		return nullptr;
	}

	// Process include statements.
	shaderSource = processIncludeStatements( mCore, shaderSource );

	// Replace the common variables with runtime information.
	ShaderTemplateMap templateParameterMap;
	templateParameterMap[ "%LIGHTSOURCE_COUNT%" ] = ConversionUtils::convertToString( KMaxLightSourceCount );
	templateParameterMap[ "%LIGHTSOURCE_FLOAT_COUNT%" ] = ConversionUtils::convertToString( GLStructs::GLPointLightBuffer::KLightSourceFloatCount );
	templateParameterMap[ "%LIGHT_ATTENUATION%" ] = ConversionUtils::convertToString( KLightAttenuationDistance, 2 );
	shaderSource = replaceAllTemplateParameters( shaderSource, templateParameterMap );

	// Create the shader program from the loaded source code.
	auto shaderProgram = createSingleShaderProgram( programId, shaderSource );
	shaderProgram->setShaderFilePath( shaderPath );
	return shaderProgram;
}

void ShaderManager::destroyShaderProgram( const std::string &programId )
{
	auto iter = mShaders.find( programId );
	if( iter != mShaders.end() )
	{
		iter->second->removeFromGPU();
		mShaders.erase( iter );
	}
}

std::shared_ptr<ShaderProgram> ShaderManager::findShaderProgram( const std::string &programId ) const
{
	auto iter = mShaders.find( programId );
	if( iter != mShaders.end() )
	{
		return iter->second;
	}
	return nullptr;
}

void ShaderManager::uploadToGPU()
{
	RengineError::check( "Error state before starting to build shaders" );
	auto iter = mShaders.begin();
	while( iter != mShaders.end() )
	{
		iter->second->uploadToGPU();
		iter++;
	}
	RengineError::check( "Error state after building shaders" );
}

void ShaderManager::removeFromGPU()
{
	auto iter = mShaders.begin();
	while( iter != mShaders.end() )
	{
		iter->second->removeFromGPU();
		iter++;
	}
}

void ShaderManager::startRenderingRound()
{
	mUsedShaders.clear();
}

void ShaderManager::endRenderingRound()
{
#ifdef PRINT_SHADER_USAGE
	for( auto program : mUsedShaders )
	{
		std::cout << "Shader name: " << program->getProgramId() << ", file: " << program->getShaderFilePath() << std::endl;
	}
#endif
}

void ShaderManager::recordShaderUsage( const ShaderProgram* program )
{
	mUsedShaders.insert( program );
}

std::vector<std::string> ShaderManager::loadShaderSource(
	const std::string &vshPath,
	const std::string &fshPath )
{
	std::vector<std::string> sourceVector;

	// Load vertex shader source.
	std::string vertexShaderSource = "";
	if( mCore.loadTextFile( vshPath, vertexShaderSource ) == false )
	{
		return sourceVector;
	}

	// Load fragment shader source.
	std::string fragmentShaderSource = "";
	if( mCore.loadTextFile( fshPath, fragmentShaderSource ) == false )
	{
		return sourceVector;
	}

	// Process #include-directives in vertex shader file.
	const std::string KIncludeDirective = "#include \"";
	size_t includePos = vertexShaderSource.find( KIncludeDirective );
	while( includePos != std::string::npos )
	{
		// Expect next character to be quotation mark. Start reading from 1 character after that.
		size_t includeFileNameStart = includePos + KIncludeDirective.length();
		size_t includeFileNameEnd = vertexShaderSource.find( "\"", includeFileNameStart );
		if( includeFileNameEnd == std::string::npos )
			return sourceVector;

		// Get the filename.
		size_t includeFileNameLength = includeFileNameEnd - includeFileNameStart;
		std::string fileToInclude = vertexShaderSource.substr( includeFileNameStart, includeFileNameLength );
		std::string includedFileContents = loadSingleShaderFile( fileToInclude );
		size_t replaceLength = includeFileNameEnd - includePos + 1;
		vertexShaderSource.replace( includePos, replaceLength, includedFileContents );
	}

	// Process #include directives in fragment shader file.

	// Put sources to the vector.
	sourceVector.push_back( vertexShaderSource );
	sourceVector.push_back( fragmentShaderSource );
	return sourceVector;
}

std::string ShaderManager::loadSingleShaderFile( const std::string &shaderPath )
{
	std::string fileContents = "";
	if( mCore.loadTextFile( shaderPath, fileContents ) == false )
	{
		// Loading failed.
		assert( fileContents.size() > 0 );
		throw std::exception( "Failed to load shader file." );
	}
	return fileContents;
}

std::string ShaderManager::replaceAllTemplateParameters(
	const std::string &shaderSource,
	const std::map< std::string, std::string > templateVars )
{
	// Replace all occurences of template variables.
	std::string source = shaderSource;
	auto iter = templateVars.begin();
	while( iter != templateVars.end() )
	{
		source = replaceTemplateParameter( source, iter->first, iter->second );
		iter++;
	}

	return source;
}

std::string ShaderManager::replaceTemplateParameter(
	const std::string &shaderSource,
	const std::string &templateVariable,
	const std::string &replaceWith )
{
	std::string modifiedSource = shaderSource;

	// Search and replace the substring as long as all occurences of the substring have been
	// replaced with other string.
	size_t foundIndex = modifiedSource.find( templateVariable );
	while( foundIndex != std::string::npos )
	{
		modifiedSource = modifiedSource.replace( foundIndex, templateVariable.length(), replaceWith );
		foundIndex = modifiedSource.find( templateVariable );
	}

	return modifiedSource;
}

std::string ShaderManager::processIncludeStatements(
	RenderCore &core,
	const std::string &shaderSource )
{
	std::string modifiedSource = shaderSource;
	const std::string KIncludeStatement = "#include \"";

	// Search as many times as there are no '#include "filename"' elements left.
	size_t foundIndex = modifiedSource.find( KIncludeStatement );
	while( foundIndex != std::string::npos )
	{
		// Extract the filename from the quotation marks.
		size_t filenameStartPos = foundIndex + KIncludeStatement.length();
		size_t filenameEndPos = modifiedSource.find( '"', filenameStartPos );
		std::string filename = modifiedSource.substr( filenameStartPos, ( filenameEndPos - filenameStartPos ) );

		// Load the included file.
		std::string includedContents;
		if( core.loadTextFile( filename, includedContents ) == false )
		{
			// Loading failed.
			std::string errMessage = "Included file '" + filename + "' does not exist!";
			throw std::runtime_error( errMessage );
		}
		else
		{
			modifiedSource.replace( modifiedSource.begin() + foundIndex,
					modifiedSource.begin() + filenameEndPos + 1,
					includedContents );
		}

		// Process next include.
		foundIndex = modifiedSource.find( KIncludeStatement );
	}
	return modifiedSource;
}
