
#include "rengineconfig.h"

RengineConfig::RengineConfig()
{

}

void RengineConfig::loadConfiguration( const std::string& configFilePath )
{

}

void RengineConfig::setProperty( const std::string& propertyName, bool propertyValue )
{
    mBooleanProperties[ propertyName ] = propertyValue;
}

bool RengineConfig::getBooleanOrDefault( const std::string& propertyName, bool defaultValue ) const
{
    auto iter = mBooleanProperties.find( propertyName );
    if( iter == mBooleanProperties.end() )
    {
        return defaultValue;
    }
    return iter->second;
}

bool RengineConfig::getBoolean( const std::string& propertyName ) const
{
    auto iter = mBooleanProperties.find( propertyName );
    if( iter == mBooleanProperties.end() ) {
        throw RengineError( "Failed to get boolean property: " + propertyName );
    }
    return iter->second;
}
