
#pragma once

#include <map>
#include "managerbase.h"

// Forward declarations.
class Material;
class RenderCore;
class RenderStatistics;

/// <summary>
/// Manages the collection of materials.
/// </summary>
class MaterialManager : public ManagerBase
{
public:
	MaterialManager( RenderCore& core );
	virtual ~MaterialManager();
	MaterialManager( const MaterialManager& obj ) = delete;
	MaterialManager& operator = ( const MaterialManager& obj ) = delete;

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup
	/// </summary>
	virtual void cleanup() override;

public:

	/// <summary>
	/// Initializes the built-in materials that are always available.
	/// </summary>
	/// <returns>Returns true if intialization is successful.</returns>
	bool initializeDefaultMaterials();

	/// <summary>
	/// Creates new material object.
	/// </summary>
	/// <param name="materialId">Unique ID of the new material.</param>
	/// <returns>New material object.</returns>
	std::shared_ptr<Material> createMaterial( const std::string& materialId );

	/// <summary>
	/// Creates new material from existing material. Clones the existing material and creates new
	/// material with new ID.
	/// </summary>
	/// <param name="materialId">ID of the new material to be created.</param>
	/// <param name="existingMaterialId">ID of the material to be cloned.</param>
	/// <returns>New material object.</returns>
	std::shared_ptr<Material> cloneMaterial(
		const std::string& materialId,
		const std::string& existingMaterialId );

	/// <summary>
	/// Destroys a material that matches with given ID.
	/// </summary>
	/// <param name="materialId">ID of the material to be destroyed.</param>
	void destroyMaterial( const std::string& materialId );

	/// <summary>
	/// Returns the material that matches with given ID.
	/// </summary>
	/// <param name="materialId">ID of the material.</param>
	/// <returns>
	/// Pointer to material instance. Nullptr if no material matches with given ID.
	/// </returns>
	std::shared_ptr<Material> findMaterial( const std::string& materialId ) const;

private:
	/// <summary>Container for the material objects.</summary>
	typedef std::map<std::string, std::shared_ptr<Material> > MaterialContainer;

	/// <summary>Material container</summary>
	MaterialContainer mMaterials;
};
