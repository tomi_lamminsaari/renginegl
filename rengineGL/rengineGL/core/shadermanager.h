
#pragma once

#include <map>
#include <memory>
#include <set>
#include <string>
#include <vector>
#include "managerbase.h"

// Forward declarations.
class ShaderProgram;

/// <summary>
/// ShaderManager can load, create, store and provide access to shader programs.
/// </summary>
class ShaderManager : public ManagerBase
{
public:
	ShaderManager(RenderCore &core);
	virtual ~ShaderManager();
	ShaderManager(const ShaderManager &obj) = delete;
	ShaderManager& operator = (const ShaderManager &obj) = delete;

	typedef std::map< std::string, std::string > ShaderTemplateMap;

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup
	/// </summary>
	virtual void cleanup() override;

public:

	/// <summary>
	/// Initializes all the built-in shaders.
	/// </summary>
	/// <returns>Returns true on success.</returns>
	bool initializeDefaultShaders();

	/// <summary>
	/// Creates new shader program from given source codes.
	/// </summary>
	/// <param name="programId">ID of the shader to create. Must be unique.</param>
	/// <param name="vshSource">Vertex shader source code.</param>
	/// <param name="fshSource">Fragment shader source code.</param>
	/// <returns>Created shader program.</returns>
	std::shared_ptr<ShaderProgram> createShaderProgram(
		const std::string &programId,
		const std::string &vshSource,
		const std::string &fshSource);

	/// <summary>
	/// Creates new shader program from a single shader source. The source data
	/// must contain valid vertex shader and fragment shaders.
	/// </summary>
	/// <param name="programId">ID of the shader program to create. Must be unique.</param>
	/// <param name="shaderSource">The shader sources.</param>
	/// <returns>Created shader program.</returns>
	std::shared_ptr<ShaderProgram> createSingleShaderProgram(
		const std::string &programId,
		const std::string &shaderSource );

	/// <summary>
	/// Creates new shader program and loads the shader source from given file. The is
	/// supposed to contain both the vertex shader and fragment shader programs.
	/// </summary>
	/// <param name="programId">ID of the new program.</param>
	/// <param name="shaderSourcePath">The file path to the shader program source.</param>
	/// <returns>Created shader program.</returns>
	std::shared_ptr<ShaderProgram> loadSingleShaderProgram(
		const std::string &programId,
		const std::string &shaderSourcePath );

	/// <summary>
	/// Destroys the shader program.
	/// </summary>
	/// <param name="programId">ID of the shader program to destroy.</param>
	void destroyShaderProgram( const std::string &programId );

	/// <summary>
	/// Returns the shader program that matches with given ID.
	/// </summary>
	/// <param name="programId">ID of the program to return.</param>
	/// <returns>Pointer to shader program. Nullptr if program was not found.</returns>
	std::shared_ptr<ShaderProgram> findShaderProgram( const std::string &programId ) const;

	/// <summary>
	/// Compiles all the programs. Allocate the GPU resources for them.
	/// </summary>
	void uploadToGPU();

	/// <summary>
	/// Releases all the GPU resources allocated for the shader programs.
	/// </summary>
	void removeFromGPU();

	/// <summary>
	/// This method should be called in the beginning of each frame rendering. Clears the
	/// statistics regarding the shaders.
	/// </summary>
	void startRenderingRound();

	/// <summary>
	/// Notifies that frame rendering has ended. The ShaderManager can now collect some
	/// rendering statistics.
	/// </summary>
	void endRenderingRound();

	/// <summary>
	/// Stores information that given shader has been used.
	/// </summary>
	/// <param name="shader">Pointer to the shader that has been used.</param>
	void recordShaderUsage( const ShaderProgram* shader );

private:

	/// <summary>
	/// Loads vertex and fragment shader sources from given files and returns them in a vector.
	/// </summary>
	/// <param name="vshPath">File path of vertex shader source.</param>
	/// <param name="fshPath">File path of fragment shader source.</param>
	/// <returns>
	/// Vector that contains vertex shader source in index 0 and fragment shader in index 1.
	/// Vector has zero length if loading fails.
	/// </returns>
	std::vector< std::string > loadShaderSource(
		const std::string &vshPath,
		const std::string &fshPath);

	/// <summary>
	/// Loads single shader file.
	/// </summary>
	/// <param name="shaderPath">Full path to the shader file.</param>
	/// <returns>The shader source.</returns>
	std::string loadSingleShaderFile( const std::string &shaderPath );

	/// <summary>
	/// Replaces all template variables with certain substrings.
	/// This is useful for providing some compilation time run environment related
	/// information from application to the shader program.
	/// </summary>
	/// <param name="shaderSource">The string containing the vertex and fragment shaders.</param>
	/// <param name="templateVars">A collection of template variable names and their values.</param>
	/// <returns>
	/// Returns a shader source where all template variables have been replaced with
	/// their values from 'templateVar' container.
	/// </returns>
	static std::string replaceAllTemplateParameters(
		const std::string &shaderSource,
		const std::map< std::string, std::string > templateVars
	);

	/// <summary>
	/// Replaces the given template parameter with another value. This is useful when
	/// replacing some generic constant variables with values from current runtime
	/// environment.
	/// </summary>
	/// <param name="shaderSource">The source of the shader file.</param>
	/// <param name="templateVariable">The template variable to look for.</param>
	/// <param name="replaceWith">The data to be placed in place of template variable.</param>
	/// <returns>
	/// Modified shader source where the template variable has been replaced with other data.
	/// </returns>
	static std::string replaceTemplateParameter(
		const std::string &shaderSource,
		const std::string &templateVariable,
		const std::string &replaceWith
	);

	/// <summary>
	/// Processes all include statements from the given shader.
	/// </summary>
	/// <param name="core">Reference to render engine's core object.</param>
	/// <param name="shaderSource">The shader code that may or may not contain include statements.</param>
	/// <returns>Altered shader source that contains the included contents.</returns>
	static std::string processIncludeStatements(
		RenderCore &core,
		const std::string &shaderSource
	);

private:

	/// <summary>Container type that holds all the shaders.</summary>
	typedef std::map< std::string, std::shared_ptr<ShaderProgram> > ProgramContainer;

	/// <summary>Contains all the shaders that have been loaded and created by this shader manager.</summary>
	ProgramContainer mShaders;

	/// <summary>Set of shaders that have been used in frame rendering.</summary>
	std::set<const ShaderProgram*> mUsedShaders;
};