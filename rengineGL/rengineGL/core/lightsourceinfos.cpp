
#include "lightsourceinfos.h"

DirectionalLightSourceInfo::DirectionalLightSourceInfo() :
	mInitialized( false ),
	mLightDir( 0.0f, -1.0f, 1.0f ),
	mLightColor( 1.0f, 1.0f, 1.0f, 1.0f ),
	mLightIntensity( 1.0f )
{

}

DirectionalLightSourceInfo::DirectionalLightSourceInfo(
	const Vec3 &lightDir,
	const Rgba &lightColor,
	GLfloat lightIntensity
) :
	mInitialized( true ),
	mLightDir( lightDir.normalized() ),
	mLightColor( lightColor ),
	mLightIntensity( lightIntensity )
{

}

DirectionalLightSourceInfo::~DirectionalLightSourceInfo()
{

}

DirectionalLightSourceInfo::DirectionalLightSourceInfo( const DirectionalLightSourceInfo &obj ) :
	mInitialized( obj.mInitialized ),
	mLightDir( obj.mLightDir ),
	mLightColor( obj.mLightColor ),
	mLightIntensity( obj.mLightIntensity )
{

}

DirectionalLightSourceInfo &DirectionalLightSourceInfo::operator=( const DirectionalLightSourceInfo &obj )
{
	if( this != &obj )
	{
		mInitialized = obj.mInitialized;
		mLightDir = obj.mLightDir;
		mLightColor = obj.mLightColor;
		mLightIntensity = obj.mLightIntensity;
	}
	return *this;
}

bool DirectionalLightSourceInfo::isInitialized() const
{
	return mInitialized;
}

void DirectionalLightSourceInfo::reset()
{
	mInitialized = false;
}


//
// class PointLightSourceInfo
//

PointLightSourceInfo::PointLightSourceInfo() :
	mInitialized( false ),
	mLightSourcePos( 0.0f, 0.0f, 0.0f ),
	mLightColor( 1.0f, 1.0f, 1.0f, 1.0f ),
	mLightIntensity( 1.0f )
{

}

PointLightSourceInfo::PointLightSourceInfo(
	const Vec3 &pos,
	const Rgba &lightColor,
	GLfloat lightIntensity
) :
	mInitialized( true ),
	mLightColor( lightColor ),
	mLightIntensity( lightIntensity )
{

}

PointLightSourceInfo::~PointLightSourceInfo()
{

}

PointLightSourceInfo::PointLightSourceInfo( const PointLightSourceInfo &obj ) :
	mInitialized( obj.mInitialized ),
	mLightColor( obj.mLightColor ),
	mLightIntensity( obj.mLightIntensity )
{

}

PointLightSourceInfo &PointLightSourceInfo::operator=( const PointLightSourceInfo &obj )
{
	if( this != &obj )
	{
		mInitialized = obj.mInitialized;
		mLightColor = obj.mLightColor;
		mLightIntensity = obj.mLightIntensity;
	}
	return *this;
}

bool PointLightSourceInfo::isInitialized() const
{
	return mInitialized;
}

void PointLightSourceInfo::reset()
{
	mInitialized = false;
}
