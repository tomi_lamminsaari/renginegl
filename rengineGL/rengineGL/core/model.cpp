
#include "material.h"
#include "mesh.h"
#include "model.h"

// Class ModelSection

ModelSection::ModelSection( std::shared_ptr<Mesh> meshPtr, std::shared_ptr<Material> materialPtr ) :
	mesh( meshPtr ),
	material( materialPtr )
{

}

bool ModelSection::isEmpty() const
{
	if( mesh == nullptr || material == nullptr )
		return true;
	return false;
}

// Class Model

Model::Model( const std::string &modelId, ModelManager &manager ) :
	mManager( manager ),
	mId( modelId )
{

}

Model::~Model()
{

}

void Model::addSection( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material )
{
	// We will add the sections in shader order so that model sections with same shader
	// will be rendered sequentially. This should improve the performance slightly since there
	// could be few shader program changes less.
	const std::string &sectionShaderId = material->getShaderProgramId();
	auto sectionIterAddLoc = mModelSections.begin();
	while( sectionIterAddLoc != mModelSections.end() )
	{
		const ModelSection &existingModelSec = *sectionIterAddLoc;
		if( ( *sectionIterAddLoc ).material->getShaderProgramId() == sectionShaderId )
		{
			// This section index is using same shader. Add section to this location.
			break;
		}

		// Proceed to next model section.
		sectionIterAddLoc++;
	}

	// Add the new model section to found location or at the end of model sections vector.
	ModelSection sec( mesh, material );
	if( sectionIterAddLoc != mModelSections.end() )
		mModelSections.insert( sectionIterAddLoc, sec );
	else
		mModelSections.push_back( sec );

	// Expand the bounding box so that it contains the vertices from the added mesh as well.
	size_t vertexCount = mesh->getVertexCount();
	for( size_t i = 0; i < vertexCount; ++i )
	{
		mAabb.include( mesh->getPosition( i ) );
	}

	// Merge material properties to the mesh to make it support deferred shading.
	mesh->mergeMaterialAttributes( *material );
}

void Model::removeSection( size_t sectionIndex )
{
	mModelSections.erase( mModelSections.begin() + sectionIndex );
}

size_t Model::getSectionCount() const
{
	return mModelSections.size();
}

ModelSection Model::getSection( size_t sectionIndex ) const
{
	return mModelSections.at( sectionIndex );
}

const std::string &Model::getId() const
{
	return mId;
}

const Aabb &Model::getAabb() const
{
	return mAabb;
}

ModelSection Model::findModelSectionByMaterial( const std::shared_ptr<Material> &material ) const
{
	// Find the model section that uses the given material.
	for( const ModelSection &section : mModelSections )
	{
		if( section.material == material )
			return section;
	}

	// None of the model sections are using the given material.
	// We will return an empty model section.
	return ModelSection( nullptr, nullptr );
}

void Model::updateAabb()
{
	mAabb = Aabb();

	// Process all the sections this model has.
	for( const ModelSection &section : mModelSections )
	{
		// Don't process empty model section.
		if( section.isEmpty() )
			continue;

		// Expand the bounding box so that it will include all the vertices of
		// current model section.
		size_t meshVertexCount = section.mesh->getVertexCount();
		for( size_t vertexIndex = 0; vertexIndex < meshVertexCount; ++vertexIndex )
		{
			mAabb.include( section.mesh->getPosition( vertexIndex ) );
		}
	}
}
