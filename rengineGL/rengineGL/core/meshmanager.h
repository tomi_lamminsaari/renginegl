
#pragma once

#include <map>
#include <memory>
#include <string>
#include "managerbase.h"
#include "vertexlayout.h"

// Forward declarations.
class Mesh;

/// <summary>
/// MeshManager class can be used for storing and creating the mesh objects.
/// </summary>
class MeshManager : public ManagerBase
	{
public:
	MeshManager( RenderCore &core );
	virtual ~MeshManager();
	MeshManager( const MeshManager &obj ) = delete;
	MeshManager& operator = ( const MeshManager &obj ) = delete;

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup
	/// </summary>
	virtual void cleanup() override;

public:

	/// <summary>
	/// Creates new mesh with given ID. No vertices/faces are created yet.
	/// <param name="meshId">ID of the mesh to be created.</param>
	/// <returns>Pointer to new mesh.</returns>
	/// </summary>
	std::shared_ptr<Mesh> createMesh( const std::string &meshId );

	/// <summary>
	/// Destroys the mesh that has given ID.
	/// <param name="meshId">ID of the mesh to be deleted.</param>
	/// </summary>
	void destroyMesh( const std::string &meshId );

	/// <summary>
	/// Searches for a mesh that has given ID.
	/// <param name="meshId">ID of the mesh to look for.</param>
	/// <returns>Pointer to found mesh. Null pointer if mesh with ID does not exist.</returns>
	/// </summary>
	std::shared_ptr<Mesh> findMesh( const std::string &meshId ) const;

	/// <summary>
	/// Creates the built-in meshes.
	/// <returns>Returns 'true' if successful.</returns>
	/// </summary>
	bool initializeDefaultMeshes();

	/// <summary>
	/// Copies all the mesh data to GPU memory that has changed since previous upload.
	/// </summary>
	void uploadToGPU();

	/// <summary>
	/// Releases all the GPU memory that has been allocated for the meshes.
	/// </summary>
	void removeFromGPU();

public:
	
	/// <summary>
	/// Creates new grid mesh. Mesh vertices will be located symmetrically around the
	/// origo so that coordinate range will be [-w/2, 0, -w/2] - [w/2, 0, w/2]. If vertex attributes
	/// contain texture coordinate, their coordinates will be stretch between [0.0, 0.0]-[tw, th]. If 'tw' and
	/// 'th' are bigger that 1.0, the set wrapping mode will be used.
	/// </summary>
	/// <param name="meshId>ID of the new mesh.</param>
	/// <param name="vertexLayout">The vertex attributes the mesh vertices have.</param>
	/// <param name="verticesHorizontal">Number of vertices the mesh has horizontally.</param>
	/// <param name="verticesVertical">Number of vertices the mesh has vertically.</param>
	/// <param name="w">Width of the terrain mesh in world coordinates.</param>
	/// <param name="h">Height of the terrain mesh in world coordinates.</param>
	/// <param name="texRepeatU">Number of times texture gets repeated in U-direction. U-coordinate ranges from 0.0 to this.</param>
	/// <param name="texRepeatV>Number of times texture gets repeated in V-direction. V-coordinate ranges from 0.0 to this.</param>
	/// <returns>Pointer to created new mesh.</returns>
	std::shared_ptr<Mesh> createMesh_Grid(
		const std::string &meshId,
		const VertexLayout &vertexLayout,
		size_t verticesHorizontal,
		size_t verticesVertical,
		float w,
		float h,
		float texRepeatU,
		float texRepeatV );

private:
	/// <summary>Type definition for mesh container.</summary>
	typedef std::map< std::string, std::shared_ptr<Mesh> > MeshContainer;

	/// <summary>Contains all the meshes.</summary>
	MeshContainer mMeshes;
	};