
#pragma once

#include <map>
#include <GL/glew.h>
#include "utilities/glvariant.h"

/// <summary>
/// TextureParameters is a collection of OpenGL texture parameters that will be applied to the
/// texture when allocating GPU resources for it.
/// </summary>
class TextureParameters
{
public:

	/// <summary>Possible texture formats or purposes.</summary>
	enum class Format { Rgb, Rgba, Depth };

	/// <summary>
	/// Default constructor.
	/// </summary>
	TextureParameters();

	/// <summary>
	/// Constructs texture parameters for a texture with given size and format of the texture data.
	/// </summary>
	/// <param name="width">Width of the texture.</param>
	/// <param name="height">Height of the texture.</param>
	/// <param name="texFormat">Format of the texture.</param>
	/// </summary>
	TextureParameters( GLsizei width, GLsizei height, Format texFormat );

	/// <summary>
	/// Destructor.
	/// </summary>
	~TextureParameters();

public:
	/// <summary>
	/// Sets the size of the texture as well as internal format.
	/// </summary>
	/// <param name="width">Width of the texture.</param>
	/// <param name="height">Height of the texture.</param>
	/// <param name="texFormat">Format of the texture.</param>
	void setSize( GLsizei width, GLsizei height, Format texFormat );

	/// <summary>
	/// Returns the desired width of the texture.
	/// </summary>
	/// <returns>Width of the texture.</returns>
	GLsizei getWidth() const;

	/// <summary>
	/// Returns the desired height of the texture.
	/// </summary>
	/// <returns>Height of the texture.</returns>
	GLsizei getHeight() const;

	/// <summary>
	/// Returns the internal OpenGL format of the texture data. This is based on given format.
	/// </summary>
	/// <returns>Internal format of the texture.</returns>
	GLint getInternalFormat() const;

	/// <summary>
	/// Returns the data format of the source data based on generic format of this texture.
	/// </summary>
	/// <params name="sourceFormat">Receives the type of source data.</param>
	/// <param name="sourceType">Receives the type of the source data.</param>
	void getSourceFormat( GLenum* sourceFormat, GLenum* sourceType );

	/// <summary>
	/// Inserts GLfloat texture parameter to this collection. Overwrites previous value if same parameter
	/// name was set earlier. When applying these parameters, the glTexParameterf() function will be called.
	/// </summary>
	/// <param name="paramName">Name of the glTexParameterf parameter.</param>
	/// <param name="paramValue">The texture parameter value.</param>
	void setGlParameterf( GLenum paramName, GLfloat paramValue );

	/// <summary>
	/// Inserts GLint texture parameter to this collection. Overwrites previous value if same parameter
	/// name was set earlier. WHen applying these parameter, then glTexParameteri() function will be called.
	/// </summary>
	/// <param name="parameterName">Name of the glTextParameteri parameter.</param>
	/// <param name="paramValue">The texture parameter value.</param>
	void setGlParameteri( GLenum paramName, GLint paramValue );

	/// <summary>
	/// Applies the texture parameters to the texture object. The OpenGL texture must be bound before
	/// calling this function.
	/// </summary>
	void apply();

private:

	/// <summary>A collection of texture parameters. The values can be either GLfloat or GLint types.</summary>
	std::map< GLenum, GLVariant > mParameters;

	/// <summary>Width of the texture.</summary>
	GLsizei mWidth;

	/// <summary>Height of the texture.</summary>
	GLsizei mHeight;

	/// <summary>Internal data format of the texture.</summary>
	Format mFormat;
};
