
#pragma once

#include <map>
#include <string>
#include <vector>
#include "../math/matrix4x4.h"
#include "../math/vec3.h"
#include "../math/vec4.h"
#include "../utils/rgba.h"

// Forward declarations.
class MaterialManager;
class ShaderProgram;
class Texture;

/// <summary>
/// A structure that contains the associated texture ID and cached pointer to texture object.
/// </summary>
struct TextureData
{
	/// <summary>Indicates if this texture has been enabled.</summary>
	bool enabled;

	/// <summary>ID of the texture.</summary>
	std::string textureId;

	/// <summary>Pointer to the texture instance itself.</summary>
	std::shared_ptr<Texture> texture;

	/// <summary>
	/// Default constructor. Sets texturedata to be disabled and to refer
	/// none particular texture instance.
	/// </summary>
	TextureData() : enabled( false ), textureId( "" ), texture( nullptr ) { }

	/// <summary>
	/// An assignment operator.
	/// </summary>
	TextureData& operator= ( const TextureData &tex )
		{
		if( this != &tex )
			{
			enabled = tex.enabled;
			textureId = tex.textureId;
			texture = tex.texture;
			}
		return *this;
		}
};

/// <summary>
/// SpecularColor class defines the material's specular reflection data.
/// <para>
/// Material's specular highlight consists of two different components - the color and specular exponent.
/// </para>
/// </summary>
class SpecularColor
	{
	public:
		SpecularColor( const Rgba &specColor, float specExponent ) :
			specularColor( specColor ),
			specularExponent( specExponent )
			{
			}

	public:
		Rgba specularColor;
		float specularExponent;
	};

/// <summary>
/// Material class describes the colors, textures, shaders and shader parameters for a mesh.
/// <para>
/// When rendering a mesh, we must know which vertex/fragment shader and textures should be used for the
/// rendering. This class collects these texture, shader and colors definitions together. When you are
/// about to render the mesh, you can apply the material. It will prepare all the texture units, shader programs
/// and other properties so that correct rendering properties are effective when you are actually rendering the
/// mesh.
/// </para>
/// <para>
/// By default the ShaderProgram object supports KMaxTextureCount textures. This means that single material
/// can contain that many textures and they get bound to the texture units when material is activated. The
/// names of the texture uniforms in fragment shaders must follow the naming 'uTexture0', 'uTexture1', etc. See
/// ShaderProgram class for more detailed description.
/// </para>
/// </summary>
class Material
	{
public:
	/// <summary>Maximum number of textures single material could have.</summary>
	static const int KMaxTextureCount = 15;

	/// <summary>Index of first custom texture that can be freely used by developers.</summary>
	static const int KCustomTextureIndex0 = 0;

	/// <summary>Index of multi-textured materials that use blend map.</summary>
	static const int KBlendMapTextureIndex = 4;

	/// <summary>Index of shadowmap's texture slot.</summary>
	static const int KShadowMapTextureIndex = 16;

public:
	Material( MaterialManager &manager, const std::string &id );
	~Material();
	Material( const Material &obj ) = delete;
	Material& operator = ( const Material &obj ) = delete;

public:
	/// <summary>
	/// Applies this material. Activates the shader program. Prepares the texture units for
	/// rendering of this material. Sets material specific uniform values to the shader program.
	/// </summary>
	void apply() const;

	/// <summary>
	/// Deactivates the shader program. This should be called after rendering is complete.
	/// </summary>
	void unapply() const;

	/// <summary>
	/// Binds the material specific textures to texture units.
	/// </summary>
	void applyTextures() const;
	
	/// <summary>
	/// Returns the shader program that is associated with this material.
	/// </summary>
	/// <returns>Pointer to associated shader program. Ownership is not transferred.</returns>
	std::shared_ptr<ShaderProgram> getShaderProgram() const;

	/// <summary>
	/// Returns the ID of the shader program that has been associated to this material.
	/// </summary>
	/// <returns>Shader program ID.</returns>
	const std::string& getShaderProgramId() const;

	/// <summary>
	/// Sets the shader program this material uses. Reference to the shader program
	/// is done through the ID of the program.
	/// </summary>
	/// <param name="programId">
	/// ID of the shader program. This function expects that this program
	/// can be found from ShaderManager instance with this ID.
	/// </param>
	void setShaderProgramById( const std::string &programId );

	/// <summary>
	/// Returns the ID of this material.
	/// </summary>
	/// <returns>ID of this material.</returns>
	const std::string& getId() const;

	/// <summary>
	/// Sets the texture to this material.
	/// </summary>
	/// <param name="textureNumber">
	/// The texture number that will be set. This also defines the OpenGL texture unit
	/// ID where the texture gets bound to.
	/// </param>
	/// <param name="textureId">ID of texture to set.</param>
	void setTexture( size_t textureNumber, const std::string &textureId );

	/// <summary>
	/// Removes the texture from this material.
	/// </summary>
	/// <param name="textureNumber">The texture number that will be removed.</param>
	void removeTexture( size_t textureNumber );

	/// <summary>
	/// Returns the texture information from this material.
	/// </summary>
	/// <param name="textureNumber">Index of the texture to return.</param>
	/// <returns>Returns the texture information of textureNumber'th texture.</returns>
	const TextureData& getTextureData( size_t textureNumber ) const;

	/// <summary>
	/// Sets the solid material color.
	/// <param name="solidColor">The solid material color.</param>
	void setSolidColor( const Rgba &solidColor );

	/// <summary>
	/// Returns the solid material color.
	/// </summary>
	/// <returns>The solid material color.</returns>
	const Rgba& getSolidColor() const;

	/// <summary>
	/// Disables the solid color. Sets color to Vec3( -1.0f, -1.0f, -1.0f, 0.0f ).
	/// </summary>
	void setSolidColorDisabledValue();

	/// <summary>
	/// Sets the specular color values.
	/// </summary>
	/// <param name="specularColor">The specular color values.</param>
	void setSpecularColor( const SpecularColor &specularColor );

	/// <summary>
	/// Returns the specular color values.
	/// </summary>
	/// <returns>Specular color value.</returns>
	const SpecularColor& getSpecularColor() const;

	/// <summary>
	/// Sets float uniform constant. These will be set to the shader uniforms during
	/// render operation.
	/// </summary>
	/// <param name="uniformName">Name of the uniform to set.</param>
	/// <param name="uniformValue">Value of the uniform.</param>
	void setFloatUniform( const std::string &uniformName, float uniformValue );

	/// <summary>
	/// Sets the specular reflections texture.
	/// </summary>
	/// <param name="enabled">Pass 'true' to enable this texture. Pass 'false' to disable it.</param>
	/// <param name="textureName">Name of the texture to bind. Means nothing if parameter 'enabled' differs from false.</param>
	void setSpecularReflectionTexture( bool enabled, const std::string &textureName );

	/// <summary>
	/// Resolves the texture object that matches the texture ID that has been associated to certain
	/// texture index.
	/// </summary>
	/// <param name="textureIndex">The index of the texture to resolve.</param>
	/// <returns>
	/// Returns the pointer to texture that match with texture ID. Ownership is not transferred.
	/// </returns>
	Texture* resolveTexturePointer( size_t textureIndex );

	/// <summary>
	/// Tells, how manyu textures have been enabled in this material.
	/// </summary>
	/// <returns>Returns the number of textures have been enabled.</returns>
	size_t getEnabledTexturesCount() const;

private:
	/// <summary>
	/// Copies most of the properties from given other material object. ID of material is not
	/// copied from the other material.
	/// </summary>
	/// <param name="otherMaterial">Reference to material where to copy from.</param>
	void cloneProperties( const Material &otherMaterial );

private:
	MaterialManager &mMaterialManager;
	std::string mId;
	std::string mShaderProgramId;
	std::map<std::string, Matrix4x4> mMat4Uniforms;
	std::map<std::string, Vec3> mVec3Uniforms;
	std::map<std::string, float> mFloatUniforms;
	Rgba mSolidColor;
	std::shared_ptr<ShaderProgram> mShaderProgram;
	SpecularColor mSpecularColor;
	mutable std::vector< TextureData > mTextures;
	mutable TextureData mSpecularReflectTexture;

	friend class MaterialManager;
};
