
#pragma once

#include <mutex>

// Forward declarations.
class RenderCore;

/// <summary>
/// Base class for all manager objects (e.g. MeshManager, TextureManager, etc.)
/// </summary>
class ManagerBase
{
public:

	/// <summary>
	/// Constructs new manager object.
	/// </summary>
	/// <param name="core">Render engine's core object.</param>
	ManagerBase( RenderCore& core );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~ManagerBase();

	ManagerBase( const ManagerBase& ) = delete;
	ManagerBase& operator = ( const ManagerBase& ) = delete;

	/// <summary>
	/// Returns the render engine's core object.
	/// </summary>
	/// <returns>Render engine's core</returns>
	RenderCore& getCore();

	/// <summary>
	/// Returns the render engine's core object as const reference.
	/// </summary>
	/// <returns>Constant reference to the core object.</returns>
	const RenderCore& getCore() const;

	/// <summary>
	/// Called by render engine's core object when render engine will shut down. This function
	/// is supposed to release all the resources allocated by this manager.
	/// </summary>
	virtual void cleanup() = 0;

protected:
	/// <summary>
	/// Gets unique sequence number for new object. Usefull when generating new unique IDs for objects
	/// that get added to this manager.
	/// </summary>
	/// <returns>Unique sequence number.</returns>
	int generateUniqueID();

protected:

	/// <summary>Reference to render engine's core object.</summary>
	RenderCore & mCore;

private:
	/// <summary>Current ID counter value for creating unique identifiers.</summary>
	int mIDCounter;

	/// <summary>A mutex for thread safe ID generation.</summary>
	std::mutex mIDCounterMutex;
};