
#pragma once

#include "animatorbase.h"
#include "../../math/path3.h"

// Forward declarations.
class Node;

/**
* @class NodeTranslator
* @brief A class that can be used for animating Node-objects.
*/
class NodeTranslator : public AnimatorBase
{
public:
	NodeTranslator( RenderCore &core );
	virtual ~NodeTranslator();
	NodeTranslator( const NodeTranslator &obj ) = delete;
	NodeTranslator &operator=( const NodeTranslator &obj ) = delete;

public:
	/**
	* Sets the node we will be animating.
	* @param node Pointer to node object. Ownership is not transferred.
	*/
	void setNode( Node *node );

	void setPath( const Path3D &path );

protected:
	/** The Node we will be animating. */
	Node *mNode;

	/** The path this translator travels. */
	Path3D mPath;
};
