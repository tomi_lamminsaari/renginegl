
#include "lightcoloranimator.h"

LightColorKeyFrame::LightColorKeyFrame( const Vec3& col, double t ) :
    mColor( col ),
    mTime( t )
{

}

LightColorAnimator::LightColorAnimator( RenderCore& core ) :
    AnimatorBase( core ),
    mCurrent( Rgba::Black.asVec3() ),
    mDeltaPerSec( 0.0f, 0.0f, 0.0f ),
    mTimeUntilKeyFrameChange( 0.0 ),
    mKeyFrameIndex( 0 )
{
    mColors.push_back( LightColorKeyFrame( Rgba::Black.asVec3(), 0.0 ) );
}

LightColorAnimator::~LightColorAnimator()
{

}

void LightColorAnimator::update( double timeSincePrevious )
{
    if( mColors.size() == 1 )
    {
        // Only initial color. Don't animate anything.
        return;
    }

    mCurrent += ( mDeltaPerSec * timeSincePrevious );
    mTimeUntilKeyFrameChange -= timeSincePrevious;
    if( mTimeUntilKeyFrameChange < 0.0 ) {
        Vec3 currentColor = mColors[mKeyFrameIndex].mColor;
        mKeyFrameIndex++;
        if( mKeyFrameIndex >= mColors.size() )
        {
            mKeyFrameIndex = 0;
        }
        Vec3 targetColor = mColors[mKeyFrameIndex].mColor;
        mCurrent = currentColor;
        mTimeUntilKeyFrameChange = mColors[mKeyFrameIndex].mTime;

        // TODO: Light Color Animator is WIP!
    }
}

void LightColorAnimator::setInitialColor( const Rgba& col )
{

}

void LightColorAnimator::addColorKeyFrame( const Rgba& col, double colorTime )
{

}
