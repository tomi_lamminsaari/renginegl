#pragma once

#include <memory>
#include <vector>
#include "animatorbase.h"

// Forward declarations.
class LightSource;

class LightIntensityAnimatorPeriod
{
public:
    double mDuration;
    float mIntensity;

public:
    LightIntensityAnimatorPeriod( double duration, float intensity );
};

/// <summary>
/// LightIntensityAnimator can be used for animating light intensity
/// changes. It can make flickering lights.
/// </summary>
class LightIntensityAnimator : public AnimatorBase
{
public:
    /// <summary>
    /// Creates an animator that changes light intensity between 2 intensities.
    /// </summary>
    /// <param name="core">Reference to rendering engine.</param>
    /// <param name="light">Pointer to light source we are supposed to animate.</param>
    /// <param name="state1Duration">Duration of the first state in seconds.</param>
    /// <param name="state1Intensity">Intensity of the light in the first state.</param>
    /// <param name="state2Duration">Duration of the second state in seconds.</param>
    /// <param name="state2Intensity">Intensity of the light in the second state.</param>
    /// <returns>Pointer to newly created intensity animator.</returns>
    static std::shared_ptr<LightIntensityAnimator> create2StateAnimator(
            RenderCore& core,
            std::shared_ptr<LightSource> light,
            double state1Duration,
            float state1Intensity,
            double state2Duration,
            float state2Intensity );

    /// <summary>
    /// Constructs new LightIntensityAnimator.
    /// </summary>
    /// <param name="core">Reference to rendering engine.</param>
    LightIntensityAnimator( RenderCore& core );

    /// <summary>
    /// Destructor.
    /// </summary>
    virtual ~LightIntensityAnimator();

    LightIntensityAnimator( const LightIntensityAnimator& ) = delete;
    LightIntensityAnimator& operator=( const LightIntensityAnimator& ) = delete;

public:  // from AnimatorBase
    /// <summary>
    /// from AnimatorBase, update
    /// </summary>
    /// <param name="timeSincePrevious"></param>
    virtual void update( double timeSincePrevious ) override;

public:
    /// <summary>
    /// Sets the light source we are supposed to animate.
    /// </summary>
    /// <param name="light">Pointer to the light source.</param>
    void setLightSource( std::shared_ptr<LightSource> light );

    /// <summary>
    /// Adds new section to intensity animation sequence.
    /// </summary>
    /// <param name="duration">Duration of the new sequence.</param>
    /// <param name="intensity">Intensity of the light in this new section.</param>
    void addIntensityPeriod( double duration, float intensity );

private:
    std::shared_ptr<LightSource> mLight;
    std::vector<LightIntensityAnimatorPeriod> mKeyFrames;
    size_t mCurrentIndex;
    double mCurrentPeriodTime;
};
