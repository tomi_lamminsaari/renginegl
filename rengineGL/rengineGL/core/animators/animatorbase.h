#pragma once

// Forward declarations.
class RenderCore;

/// <summary>
/// Base class for all the property, value and node animators.
/// </summary>
class AnimatorBase
{
public:
	/// <summary>
	/// Constructs new animator.
	/// </summary>
	/// <param name="core">The main render engine object.</param>
	AnimatorBase( RenderCore &core );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~AnimatorBase();

	AnimatorBase( const AnimatorBase &obj ) = delete;
	AnimatorBase &operator=( const AnimatorBase &obj ) = delete;

public:
	/// <summary>
	/// Called by the rendering engine when it wants to animate us.
	/// </summary>
	/// <param name="timeSincePrevious">Amount of time passed since previous call.</param>
	virtual void update( double timeSincePrevious );

protected:

	/// <summary>The main render engine object.</summary>
	RenderCore &mCore;
};
