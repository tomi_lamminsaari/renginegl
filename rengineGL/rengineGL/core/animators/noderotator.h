#pragma once

#include <memory>
#include "animatorbase.h"
#include "../../math/quaternion.h"
#include "../../math/vec3.h"

// Forward declarations.
class Node;

/// <summary>
/// A node animator that rotates a node around an axis with certain speed.
/// </summary>
class NodeRotator : public AnimatorBase
{
public:
	/// <summary>
	/// Constructor new rotator but does not attach any node to it yet.
	/// </summary>
	/// <param name="core">The render engine's core object.</param>
	NodeRotator( RenderCore &core );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~NodeRotator();

	NodeRotator( const NodeRotator &obj ) = delete;
	NodeRotator &operator=( const NodeRotator &obj ) = delete;

public:
	/// <summary>
	/// From AnimatorBase class. The update function that animates the attached node.
	/// <summary>
	/// <param name="timeSincePrevious">Amount of time passed since previous update.</param>
	virtual void update( double timeSincePrevious ) override;

public:

	/// <summary>
	/// Attached the node to this animator.
	/// </summary>
	/// <param name="node">Pointer to node to attach to this animator.</param>
	void setNode( std::shared_ptr<Node> node );

	/// <summary>
	/// Sets the rotation speed and starting angle.
	/// </summary>
	/// <param name="axis">The axis this rotator rotates around.</param>
	/// <param name="angleDegs">The initial rotation angle.</param>
	/// <param name="revsPerSecond">Rotation speed in number of revolutions per second.</param>
	void setRotation( const Vec3 &axis, float angleDegs, float revsPerSecond );

protected:

	/// <summary>The node that has been attached to this rotator.</summary>
	std::shared_ptr<Node> mNode;

	/// <summary>The axis we rotate around.</summary>
	Vec3 mAxis;

	/// <summary>Current rotation angle. Changes as the rotation runs.</summary>
	float mAngleDegs;

	/// <summary>Rotation speed in revolutions per second.</summary>
	float mRevsPerSecond;
};
