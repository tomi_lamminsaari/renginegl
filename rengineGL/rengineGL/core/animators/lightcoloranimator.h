#pragma once

#include <vector>
#include "animatorbase.h"
#include "../math/vec3.h"
#include "../utils/rgba.h"

class LightColorKeyFrame
{
public:
    Vec3 mColor;
    double mTime;

    LightColorKeyFrame( const Vec3& col, double t );
};

class LightColorAnimator : public AnimatorBase
{
public:
    LightColorAnimator( RenderCore& core );
    virtual ~LightColorAnimator();

    LightColorAnimator( const LightColorAnimator& ) = delete;
    LightColorAnimator& operator=( const LightColorAnimator& ) = delete;

public:  // from AnimatorBase
    virtual void update( double timeSincePrevious ) override;

public:
    void setInitialColor( const Rgba& col );
    void addColorKeyFrame( const Rgba& col, double colorTime );

private:
    Vec3 mCurrent;
    Vec3 mDeltaPerSec;
    double mTimeUntilKeyFrameChange;
    size_t mKeyFrameIndex;
    std::vector<LightColorKeyFrame> mColors;
};
