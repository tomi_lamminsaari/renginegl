
#include "../node.h"
#include "backforthanimator.h"

BackAndForthAnimator::BackAndForthAnimator( RenderCore& core ) :
    AnimatorBase( core ),
    mCurrentPos( 0.0f, 0.0f, 0.0f ),
    mEndPoint1( 0.0f, 0.0f, 0.0f ),
	mEndPoint2( 0.0f, 0.0f, 0.0f ),
	mDurationUntilDirChange( 0.5 ),
    mDuration( 1.0 ),
	mMoveAmount( 0.0f, 0.0f, 0.0f ),
	mMovementDirection( MovementDirection::EP1ToEP2 )
    {

    }

BackAndForthAnimator::~BackAndForthAnimator()
    {

    }

void BackAndForthAnimator::setNode( std::weak_ptr<Node> node )
	{
	mNode = node;
	}

void BackAndForthAnimator::setMovementRange(
	const Vec3 &endpoint1,
	const Vec3 &endpoint2,
	const double &duration )
	{
	mDurationUntilDirChange = duration / 2.0;
	mCurrentPos = endpoint1;
	mEndPoint1 = endpoint1;
	mEndPoint2 = endpoint2;
	mMoveAmount = endpoint2 - endpoint1;
	mMoveAmount /= static_cast< float >( mDurationUntilDirChange );
	mDuration = duration;
	}

void BackAndForthAnimator::update( double timeStepSecs )
	{
	mDurationUntilDirChange -= timeStepSecs;
	if( mDurationUntilDirChange > 0.0 )
		{
		// Do not change direction yet. Just move the node.
		Vec3 amountOfMovement = mMoveAmount * static_cast< float >( timeStepSecs );
		mCurrentPos += amountOfMovement;
		}
	else
		{
		// Change the movement direction.
		if( mMovementDirection == MovementDirection::EP1ToEP2 )
			{
			// We were moving from endpoint1 to endpoint2. Change the direction.
			// Set the movement direction change to happen after certain time.
			mMovementDirection = MovementDirection::EP2ToEP1;
			mDurationUntilDirChange = mDuration / 2.0;
			mCurrentPos = mEndPoint2;
			mMoveAmount = mEndPoint1 - mEndPoint2;
			mMoveAmount /= static_cast< float >( mDurationUntilDirChange );
			}
		else if( mMovementDirection == MovementDirection::EP2ToEP1 )
			{
			// We were moving from endpoint2 to endpoint1. Change the direction.
			mMovementDirection = MovementDirection::EP1ToEP2;
			mDurationUntilDirChange = mDuration / 2.0;
			mCurrentPos = mEndPoint1;
			mMoveAmount = mEndPoint2 - mEndPoint1;
			mMoveAmount /= static_cast< float >( mDurationUntilDirChange );
			}
		}

	// Position the node to our position.
	std::shared_ptr<Node> node = mNode.lock();
	if( node )
		{
		node->setPosition( mCurrentPos );
		}
	}
