
#include "lightintensityanimator.h"
#include "../core/lightsources.h"

LightIntensityAnimatorPeriod::LightIntensityAnimatorPeriod( double duration, float intensity ) :
    mDuration( duration ),
    mIntensity( intensity )
{

}

std::shared_ptr<LightIntensityAnimator> LightIntensityAnimator::create2StateAnimator(
        RenderCore& core,
        std::shared_ptr<LightSource> light,
        double state1Duration,
        float state1Intensity,
        double state2Duration,
        float state2Intensity )
{
    auto animator = std::make_shared<LightIntensityAnimator>( core );
    animator->setLightSource( light );
    animator->addIntensityPeriod( state1Duration, state1Intensity );
    animator->addIntensityPeriod( state2Duration, state2Intensity );
    return animator;
}

LightIntensityAnimator::LightIntensityAnimator( RenderCore& core ) :
    AnimatorBase( core ),
    mCurrentIndex( 0 ),
    mCurrentPeriodTime( 0.0 )
{

}

LightIntensityAnimator::~LightIntensityAnimator()
{

}

void LightIntensityAnimator::update( double timeSincePrevious )
{
    // Exit if no intensity sequences to animate.
    if( mKeyFrames.size() == 0 ) {
        return;
    }

    mCurrentPeriodTime -= timeSincePrevious;
    if( mCurrentPeriodTime < 0.0 ) {
        mCurrentIndex++;
        if( mCurrentIndex >= mKeyFrames.size() ) {
            mCurrentIndex = 0;
        }
        mCurrentPeriodTime = mKeyFrames[mCurrentIndex].mDuration;
    }

    // Set the light intensity.
    if( mLight.get() ) {
        mLight->setIntensity( static_cast< GLfloat >( mKeyFrames[mCurrentIndex].mIntensity ) );
    }
}

void LightIntensityAnimator::setLightSource( std::shared_ptr<LightSource> light )
{
    mLight = light;
}

void LightIntensityAnimator::addIntensityPeriod( double duration, float intensity )
{
    mKeyFrames.push_back( LightIntensityAnimatorPeriod( duration, intensity ) );
    mCurrentIndex = 0;
    mCurrentPeriodTime = mKeyFrames[0].mDuration;
}
