
#include "nodetranslator.h"

NodeTranslator::NodeTranslator( RenderCore &core ) :
	AnimatorBase( core ),
	mNode( nullptr )
{

}

NodeTranslator::~NodeTranslator()
{

}

void NodeTranslator::setNode( Node *node )
{
	mNode = node;
}

void NodeTranslator::setPath( const Path3D &path )
{
	mPath = path;
}
