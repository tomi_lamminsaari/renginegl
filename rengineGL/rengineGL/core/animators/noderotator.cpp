
#include "noderotator.h"
#include "../node.h"

NodeRotator::NodeRotator( RenderCore &core ) :
	AnimatorBase( core ),
	mNode( nullptr ),
	mAngleDegs( 0.0f ),
	mRevsPerSecond( 1.0f )
{

}

NodeRotator::~NodeRotator()
{

}

void NodeRotator::update( double timeSincePrevious )
{
	// Rotate the angle.
	mAngleDegs += mRevsPerSecond * static_cast< float >( timeSincePrevious ) * 360.0f;
	if( mAngleDegs >= 360.0f )
		mAngleDegs -= 360.0f;
	if( mAngleDegs < 0.0f )
		mAngleDegs += 360.0f;

	// Set new rotation to the node.
	if( mNode != nullptr )
		mNode->setRotation( Quaternion::newAxisRotation( mAngleDegs, mAxis ) );
}

void NodeRotator::setNode( std::shared_ptr<Node> node )
{
	mNode = node;
}

void NodeRotator::setRotation( const Vec3 &axis, float angleDegs, float revsPerSecond )
{
	// Store the rotation parameters.
	mAxis = axis;
	mAngleDegs = angleDegs;
	mRevsPerSecond = revsPerSecond;

	// Set the rotation to the node if we already have one.
	if( mNode != nullptr )
	{
		mNode->setRotation( Quaternion::newAxisRotation( mAngleDegs, mAxis ) );
	}
}
