
#pragma once

#include <memory>
#include <vector>
#include "animatorbase.h"
#include "../../math/vec3.h"

// Forward declarations.
class Node;

/// <summary>
/// An animator that moves a node back and forth between 2 coordinates.
/// </summary>
class BackAndForthAnimator : public AnimatorBase
{
public:
	/// <summary>The possible movement directions. Either from endpoint1 to endpoint2 or vice versa.</summary>
	enum class MovementDirection { EP1ToEP2, EP2ToEP1 };

public:

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="core">The main render engine object.</param>
	BackAndForthAnimator( RenderCore &core );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~BackAndForthAnimator();

	BackAndForthAnimator( const BackAndForthAnimator &obj ) = delete;
	BackAndForthAnimator &operator=( const BackAndForthAnimator &obj ) = delete;

public:  // from AnimatorBase

	/// <summary>
	/// From AnimatorBase class.
	/// </summary>
	/// <param name="timeSincePrevious">Amount of time passed since previous call.</param>
	virtual void update( double timeSincePrevious ) override;

public:

	/// <summary>
	/// Sets the node this animator moves back and forth.
	/// </summary>
	/// <param name="node">Pointer to the node we will move.</param>
	void setNode( std::weak_ptr<Node> node );

	/// <summary>
	/// Sets the coordinate range where this animator moves the node back and forth.
	/// </summary>
	/// <param name="endpoint1">The first end of the movement.</param>
	/// <param name="endpoint2">The second end of the movement.</param>
	/// <param name="duration">
	/// How long (in seconds) it takes for the animator to move from endpoint1 to endpoint2 and back.
	/// </param>
	void setMovementRange(
		const Vec3 &endpoint1,
		const Vec3 &endpoint2,
		const double &duration
	);

private:

	/// <summary>Current position where we are in the interpolation.</summary>
	Vec3 mCurrentPos;

	/// <summary>The first end point of the movement range.</summary>
	Vec3 mEndPoint1;

	/// <summary>The second end point of the movement range.</summary>
	Vec3 mEndPoint2;

	/// <summary>Defines how long until we change the direction.</summary>
	double mDurationUntilDirChange;

	/// <summary>Duration that defines how long it takes to interpolate the position back and forth.</summary>
	double mDuration;

	/// <summary>Defines the direction and amount of movement. Points either towards endpoint1 or endpoint2.</summary>
	Vec3 mMoveAmount;

	/// <summary>Defines if we are moving from endpoint1 to endpoint2 or vice versa.</summary>
	MovementDirection mMovementDirection;

	/// <summary>The node we will move back and forth.</summary>
	std::weak_ptr<Node> mNode;
};
