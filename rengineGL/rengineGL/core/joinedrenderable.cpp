
#include "joinedrenderable.h"
#include "model.h"

JoinedRenderable::JoinedRenderable( RenderCore& core ) :
	Renderable( core )
{

}

JoinedRenderable::~JoinedRenderable()
{

}

void JoinedRenderable::joinWithRenderable( std::shared_ptr<Renderable> renderable )
{
	// Ensure that the meshes and materials of the input renderable can be merged with us.

	// Construct a mesh for ourselves.
	
	// Copy the mesh data.

	auto model = renderable->getModel();
	for( size_t sectionIndex = 0; sectionIndex < model->getSectionCount(); sectionIndex++ )
	{
		ModelSection modsec = model->getSection( sectionIndex );
		
	}

	// Hide the original renderable.
	renderable->setHidden( true );
}
