
#include "lightcolorinfo.h"

LightColorInfo::LightColorInfo() :
	mColor( Rgba::Black ),
	mIntensity( 1.0f )
{

}

LightColorInfo::LightColorInfo( const Rgba &col, float intensity ) :
	mColor( col ),
	mIntensity( intensity )
{

}

LightColorInfo::~LightColorInfo()
{

}

LightColorInfo::LightColorInfo( const LightColorInfo &obj ) :
	mColor( obj.mColor ),
	mIntensity( obj.mIntensity )
{

}

LightColorInfo &LightColorInfo::operator=( const LightColorInfo &obj )
{
	if( this != &obj )
	{
		mColor = obj.mColor;
		mIntensity = obj.mIntensity;
	}
	return *this;
}

void LightColorInfo::setColor( const Rgba &col )
{
	mColor = col;
}

const Rgba &LightColorInfo::getColor() const
{
	return  mColor;
}

void LightColorInfo::setIntensity( float intensity )
{
	mIntensity = intensity;
}

float LightColorInfo::getIntensity() const
{
	return mIntensity;
}

Rgba LightColorInfo::getColorIntensityCombined() const
{
	return Rgba( mColor.red() * mIntensity, mColor.green() * mIntensity, mColor.blue() * mIntensity );
}
