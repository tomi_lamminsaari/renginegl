
#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

// Forward declarations.
class LightSource;
class Rgba;
class Scene;
class Vec3;

/// <summary>
/// This class works as a collection for all the lightsources that exist in the scene.
/// </summary>
class LightSourceCollection
{
public:

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="scene">Reference to scene object that owns this collection.</param>
	LightSourceCollection( Scene &scene );

	/// <summary>
	/// Destructor.
	/// </summary>
	~LightSourceCollection();

	/// <summary>
	/// Disabled copy constructor.
	/// </summary>
	LightSourceCollection( const LightSourceCollection &obj ) = delete;

	/// <summary>
	/// Disabled assignment operator.
	/// </summary>
	LightSourceCollection& operator=( const LightSourceCollection &obj ) = delete;

public:

	/// <summary>
	/// Creates directional light. The name of the light will be <c>BuiltIn::LIGHT_SUN</c>.
	/// </summary>
	/// <param name="lightDir">Direction of light.</param>
	/// <param name="lightColor">Color of directional light.</param>
	/// <param name="lightIntensity">Intensity of light.</param>
	/// <returns></returns>
	/// <exception cref="RengineError">Thrown if directional light with name <c>BuiltIn::LIGHT_SUN</c>
	/// has been created already. Destroy previous light before calling this.</exception>
	std::shared_ptr<LightSource> createDirectionalLight(
			const Vec3& lightDir,
			const Rgba& lightColor,
			float lightIntensity );

	/// <summary>
	/// Creates new point light source and adds it to the collection with given name.
	/// </summary>
	/// <param name="lightId">ID of the new light source.</param>
	/// <returns>Pointer to new lightsource.</returns>
	std::shared_ptr<LightSource> createLightSource( const std::string &lightId );

	/// <summary>
	/// Destroys the lightsource.
	/// </summary>
	/// <param name="lightsourceId">ID of the lightsource that will be destroyed.</param>
	void destroyLightSource( const std::string &lightsourceId );
		
	/// <summary>
	/// Gets access to point light source that has the given ID.
	/// </summary>
	/// <param name="lightId">ID of the point light source to return.</param>
	/// <returns>Pointer to found lightsource. Nullptr if lightsource was not found.</returns>
	std::shared_ptr<LightSource> getLightSource( const std::string &lightId );

	/// <summary>
	/// Returns a vector that contains pointers to all light sources.
	/// </summary>
	/// <returns>A vector containing all the point light sources.</returns>
	std::vector< std::shared_ptr<LightSource> > getLightSources() const;

private:

	/// <summary>
	/// Checks if lightsource with given ID exists in the lightsource collection already.
	/// </summary>
	/// <param name="lightId">ID of the light to check.</param>
	/// <returns>Returns 'true' if light with given ID already exists.</returns>
	bool lightIdExists( const std::string &lightId ) const;

private:

	/// <summary>Reference to the owning scene object.</summary>
	Scene &mScene;

	/// <summary>The container for the point light sources.</summary>
	std::map< std::string, std::shared_ptr<LightSource> > mLights;

	/// <summary>Special directional lightsource. Basically the "sunlight" of the scene.</summary>
	std::shared_ptr< LightSource > mDirectionalLight;
};
