
#pragma once

#include <map>
#include <string>
#include <GL/glew.h>
#include <string>
#include "defines.h"
#include "glstructs/glpointlight.h"

// Forward declarations.
class Matrix4x4;
class Rgba;
class ShaderManager;
class Vec3;
class Vec4;


/// <summary>
/// ShaderProgram class encapsulates the management and interaction between C++ code and OpenGL
/// shader program. This class can also build the shader program from given shader source files.
/// It finds the common uniform variable locations from the shader program
/// and sets uniform values before rendering the meshes.
/// </summary>
class ShaderProgram
{
public:

	// List of uniform variables the render engine expects to be in shader programs.
	static const std::string KUniformName_M;  // mat4
	static const std::string KUniformName_MV;  // mat4
	static const std::string KUniformName_MVP;  // mat4
	static const std::string KUniformName_LightMVP;  // mat4
	static const std::string KUniformName_Color;  // vec4
	static const std::string KUniformName_EyePos;  // vec4
	static const std::string KUniformName_Texture0;  // texture
	static const std::string KUniformName_Texture1;  // texture
	static const std::string KUniformName_Texture2;  // texture
	static const std::string KUniformName_Texture3;  // texture
	static const std::string KUniformName_Texture4;  // texture
	static const std::string KUniformName_Texture5;  // texture
	static const std::string KUniformName_Texture6;  // texture
	static const std::string KUniformName_Texture7;  // texture
	static const std::string KUniformName_Texture8;  // texture
	static const std::string KUniformName_Texture9;  // texture
	static const std::string KUniformName_Texture10;  // texture
	static const std::string KUniformName_Texture11;  // texture
	static const std::string KUniformName_Texture12;  // texture
	static const std::string KUniformName_Texture13;  // texture
	static const std::string KUniformName_Texture14;  // texture
	static const std::string KUniformName_Texture15;  // texture
	static const std::string KUniformName_Texture0TileCount;  // float
	static const std::string KUniformName_Texture1TileCount;  // float
	static const std::string KUniformName_Texture2TileCount;  // float
	static const std::string KUniformName_Texture3TileCount;  // float
	static const std::string KUniformName_TextureSpecular;  // texture
	static const std::string KUniformName_TextureNormals;  // texture
	static const std::string KUniformName_TerrainTex0;  // texture
	static const std::string KUniformName_TerrainTex1;  // texture
	static const std::string KUniformName_TerrainTex2;  // texture
	static const std::string KUniformName_TerrainTex3;  // texture
	static const std::string KUniformName_BlendMap;  // texture
	static const std::string KUniformName_ShadowMap;  // texture
	static const std::string KUniformName_DiffuseLightDir;  // vec3
	static const std::string KUniformName_DiffuseLightPos;  // vec3
	static const std::string KUniformName_DiffuseLightColor;  // vec4
	static const std::string KUniformName_DiffuseLightIntensity;  // float
	static const std::string KUniformName_DiffuseLightArray;  // array of floats
	static const std::string KUniformName_AmbientLightColor;  // vec4
	static const std::string KUniformName_AmbientLightIntensity;  // float
	static const std::string KUniformName_SpecularColor;  // vec4
	static const std::string KUniformName_SpecularExponent;  // float
	static const std::string KUniformName_SpecularEnabled;  // int
	static const std::string KUniformName_OffsetPosition;  // vec3
	static const std::string KUniformName_LightsourcesArray;  // array of LightSource structs.

public:

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="shaderMan">Reference to the shader manager object that owns all the shader programs.</param>
	ShaderProgram( ShaderManager &shaderMan );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~ShaderProgram();

	ShaderProgram( const ShaderProgram &obj ) = delete;
	ShaderProgram &operator=( const ShaderProgram &obj ) = delete;

	/// <summary>
	/// Sets the ID of this program.
	/// </summary>
	/// <param name="id">ID of this program.</param>
	void setProgramId( const std::string &id );

	/// <summary>
	/// Returns the ID of this program.
	/// </summary>
	/// <returns>ID of this program.</returns>
	const std::string& getProgramId() const;

	/// <summary>
	/// Sets the filename where shader code was loaded from.
	/// </summary>
	/// <param name="filepath">Name of shader file.</param>
	void setShaderFilePath( const std::string& filepath );

	/// <summary>
	/// Returns the filename where shader source code was loaded from.
	/// </summary>
	/// <returns>Shader filename.</returns>
	const std::string& getShaderFilePath() const;

	/// <summary>
	/// Sets the vertex- and fragment shader source codes. This does not yet attempt to compile and link the
	/// program. You should call either uploadToGPU() or buildProgram() to do the actual building.
	/// </summary>
	/// <param name="vertexShaderCode">A string that contains the vertex shader source.</param>
	/// <param name="fragmentShaderCode">A string that contains the fragment shader source.</param>
	void setSourceCodes(const std::string &vertexShaderCode, const std::string &fragmentShaderCode);

	/// <summary>
	/// Creates new shader program from given shader source.
	/// <param name="shaderSource">
	/// The source data that must contain both the vertex shader and fragment
	/// shader. The vertex shader program must be included inside a "#ifdef VERTEX_SHADER" and
	/// "#ifdef FRAGMENT_SHADER" regions.
	/// </param>
	/// <param name="shaderSource">The shader source that contains both the vertex shader and fragment shader.</param>
	void setSingleSourceCode( const std::string &shaderSource );

	/// <summary>
	/// Attempts to compile the program.
	/// </summary>
	/// <returns>
	/// Returns 'true' if compilation is successful. Use getBuildError() method to get the build error.
	/// </returns>
	bool buildProgram();

	/// <summary>
	/// Returns the build error that was generated when this program was compiled. An empty string if
	/// compilation was successful.
	/// </summary>
	/// <returns>The compilation error message.</returns>
	const std::string& getBuildError() const;

	/// <summary>
	/// Tells if this program has been successfully compiled.
	/// </summary>
	/// <returns>Returns 'true' if successfully compiled.</returns>
	bool isReady() const;

	/// <summary>
	/// Takes this program into use in OpenGL rendering pipeline.
	/// </summary>
	void activateProgram() const;

	/// <summary>
	/// Removes this program from OpenGL rendering pipeline.
	/// </summary>
	void deactivateProgram() const;

	/// <summary>
	/// Compiles the OpenGL shader program from the source codes.
	/// </summary>
	void uploadToGPU();

	/// <summary>
	/// Releases the OpenGL resources that have been reserved for the program. Destroys
	/// the OpenGL shader program object.
	/// </summary>
	void removeFromGPU();

	/// <summary>
	/// Checks if shader application has uniform with given name.
	/// </summary>
	/// <param name="uniformName">Name of the uniform to check.</param>
	/// <returns>Returns 'true' if program has uniform of given name.</returns>
	bool hasUniform( const std::string &uniformName );

public:

	/// <summary>
	/// Sets camera related data shader application uniforms.
	/// </summary>
	/// <param name="eyePos">Current camera position.</param>
	/// <param name="modelViewProjectionMat">Pointer to camera's model-view-projection matrix.</param>
	/// <param name="modelViewMat">Pointer to camera's model-view matrix.</param>
	/// <param name="modelMat">Pointer to renderable's model matrix.</param>
	void setCameraUniforms(
		const Vec3 &eyePos,
		const Matrix4x4 *modelViewProjectionMat,
		const Matrix4x4 *modelViewMat,
		const Matrix4x4 *modelMat );

	/// <summary>
	/// Sets mat4 matrix to the uniform. Basically does nothing if uniform does not exist in shader code.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</param>
	/// <param name="mat4">The matrix to be set.</param>
	void setUniformMat4x4(
		const std::string &uniformName,
		const Matrix4x4 &mat4);

	/// <summary>
	/// Sets vec3 uniform. Does nothing if shader program does not have the uniform.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</summary>
	/// <param name="vec">The vector to be set.</param>
	void setUniformVec3(
		const std::string &uniformName,
		const Vec3 &vec);

	/// <summary>
	/// Sets vec4 uniform. Does nothing if shader program does not have the uniform.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</param>
	/// <param name="vec">The vector to be set.</param>
	void setUniformVec4(
		const std::string &uniformName,
		const Vec4 &vec);

	/// <summary>
	/// Sets float uniform. Does nothing if shader program does not have the uniform.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</param>
	/// <param name="floatValue">The value to be set.</param>
	void setUniformFloat(
		const std::string &uniformName,
		GLfloat floatValue );

	/// <summary>
	/// Sets float array uniform. Does nothing if program does not have the uniform.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</param>
	/// <param name="floatArrayPtr">Pointer to first float of the float array.</param>
	/// <param name="numOfFloat">Number of floats there are in array.</param>
	void setUniformFloatArray(
		const std::string &uniformName,
		const GLfloat *floatArrayPtr,
		GLsizei numOfFloats );

	/// <summary>
	/// Sets int uniform. Does nothing if shader program does not have the uniform.
	/// </summary>
	/// <param name="uniformName">Name of the uniform in GLSL code.</param>
	/// <param name="intValue">The value to be set.</param>
	void setUniformInt(
		const std::string &uniformName,
		GLint intValue );

	/// <summary>
	/// Sets the ambient light values to the ambient light uniform variables of this program.
	/// If shader program does not have light uniform variables, this function doesn't do anything.
	/// </summary>
	/// <param name="lightColor">Defines the color of the ambient light.</param>
	/// <param name="lightIntensity">Defines the ambient light intensity. Zero means black.</param>
	void setAmbientLight(
		const Rgba &lightColor,
		GLfloat lightIntensity );

	/// <summary>
	/// Sets the diffuser light values to the diffuse light uniform variables of this program.
	/// If shader program does not have uniform variable, this function doesn't do anything.
	/// </summary>
	/// <param name="lightColor">Color of the diffuse light.</param>
	/// <param name="lightDirection">Defines the location of diffuse lightsource.</param>
	/// <param name="lightIntensity">Defines the intensity of the light.</param>
	void setDiffuseLight(
		const Rgba &lightColor,
		const Vec3 &lightPosition,
		GLfloat lightIntensity );

	/// <summary>
	/// Sets the array of diffuse lights to a shader that supports multiple light sources.
	/// </summary>
	/// <param name="diffuseLightBuffer">Buffer object that contains the diffuse light sources.</param>
	void setDiffuseLightArray(
		const GLStructs::GLPointLightBuffer &diffuseLightBuffer );

	/// <summary>
	/// Sets the material's specular reflection properties.
	/// </summary>
	/// <param name="specColor">Material's specular color.</param>
	/// <param name="specExponent">Material's specular exponent.</param>
	/// <param name="specEnabled">Pass 1 to enable specular light. Pass 0 to disable specular light.</param>
	void setSpecularColor(
		const Rgba &specColor,
		GLfloat specExponent,
		GLint specEnabled );

	/// <summary>
	/// Sets the light source MVP matrix that is used for shadow mapping.
	/// </summary>
	/// <param name="lightMVP">The MVP matrix of the lightsource.</param>
	void setShadowMappingLightMatrix( const Matrix4x4 &lightMVP );

private:

	/// <summary>
	/// Compiles the shader program from source code.
	/// </summary>
	/// <param name="shaderObject">ID of the OpenGL shader program.</param>
	/// <param name="shaderSource">A string containing the shader source code.</param>
	/// <param name="compileErrorOut">A string that receives the compilation error if shader compilation fails.</param>
	/// <returns>Returns 'true' if compilation succeeds.</returns>
	bool compileShader( GLuint shaderObject, const std::string &shaderSource, std::string &compileErrorOut );

	/// <summary>
	/// Finds the uniform constant location indices of all known uniform variable names.
	/// </summary>
	void findUniformLocations();

	/// <summary>
	/// Finds the uniform location from the shader program.
	/// </summary>
	/// <param name="uniformName">Name of the uniform to search for.</param>
	/// <returns>Uniform location in shader. -1 if shader does not contain the given uniform.</returns>
	GLint getUniformLocation( const std::string& uniformName );

	/// <summary>
	/// Binds the 'texture' uniform location with texture units.
	/// </summary>
	void bindUniformLocationsToTextureUnits();

private:
	/// <summary>Type definition for the uniform container.</summary>
	typedef std::map< std::string, GLint > UniformMap;

	/// <summary>The shader manager who owns us.</summary>
	ShaderManager &mShaderManager;

	/// <summary>ID of this shader program.</summary>
	std::string mId;

	/// <summary>Optional filename where the shader was loaded from.</summary>
	std::string mShaderFileName;

	/// <summary>The OpenGL object ID of the vertex shader program.</summary>
	GLuint mVertexShaderProgram;

	/// <summary>The OpenGL object ID of the fragment shader program.</summary>
	GLuint mFragmentShaderProgram;

	/// <summary>The OpenGL object ID of the linked shader program.</summary>
	GLuint mShaderProgram;

	/// <summary>The vertex shader source code.</summary>
	std::string mVertexShaderCode;

	/// <summary>The fragment shader source code.</summary>
	std::string mFragmentShaderCode;

	/// <summary>The compiler/linker error when building the shader program from source codes.</summary>
	std::string mErrorMessage;

	/// <summary>TODO: To be removed.</summary>
	bool mMustDoCleanup;

	/// <summary>A flag that indicates if shader program is ready to be used.</summary>
	bool mProgramIsReady;

	/// <summary>Is this program currently activated.</summary>
	bool mProgramActivated;

	/// <summary>Collection of uniforms and their location numbers inside the built shader program.</summary>
	UniformMap mUniforms;
};
