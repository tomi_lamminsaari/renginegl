
#include "particlemanager.h"
#include "particlesystem.h"
#include "../materialmanager.h"
#include "../mesh.h"
#include "../meshmanager.h"
#include "../node.h"
#include "../rendercore.h"
#include "../renderable.h"
#include "../scene.h"

//
// class Configuration

#pragma region Configuration

ParticleSystem::Configuration::Configuration(
	unsigned int particleCount,
	const Vec3& gravity,
	ParticleSystemType systemType,
	const std::pair<double, double>& lifetime ) :
	mParticleCount( particleCount ),
	mGravity( gravity ),
	mType( systemType ),
	mLifetimeRange( lifetime )
{
	if( lifetime.first > lifetime.second )
		throw std::invalid_argument( "Invalid lifetime parameter. Maximum lifetime must not be smaller than minimum lifetime." );
}

ParticleSystem::Configuration::Configuration( const ParticleSystem::Configuration& obj ) :
	mParticleCount( obj.mParticleCount ),
	mGravity( obj.mGravity ),
	mType( obj.mType ),
	mLifetimeRange( obj.mLifetimeRange )
{

}

ParticleSystem::Configuration& ParticleSystem::Configuration::operator=( const ParticleSystem::Configuration& obj )
{
	if( this != &obj )
	{
		mParticleCount = obj.mParticleCount;
		mGravity = obj.mGravity;
		mType = obj.mType;
		mLifetimeRange = obj.mLifetimeRange;
	}
	return *this;
}

double ParticleSystem::Configuration::getRandomLifetime() const
{
	double diff = mLifetimeRange.second - mLifetimeRange.first;
	return mLifetimeRange.first + ( sDistribution( sGenerator ) * diff );
}

#pragma endregion

//
// class ParticleSystem

std::default_random_engine ParticleSystem::sGenerator;
std::uniform_real_distribution<float> ParticleSystem::sDistribution;

float ParticleSystem::generateRandom( float minFloat, float maxFloat )
{
	return minFloat + ( maxFloat - minFloat ) * sDistribution( sGenerator );
}

ParticleSystem::ParticleSystem(
	ParticleManager& manager,
	const Configuration& systemConfiguration,
	const ParticleMeshData& meshData ) :
	mManager( manager ),
	mConfig( systemConfiguration ),
	mMeshData( meshData ),
	mParticleMesh( nullptr ),
	mUnallocatedParticlePosition( Vec3( 1000000.0f, 1000000.0f, 1000000.0f ) ),
	mLifeLeft( 10.0 ),
	mScene( nullptr )
{

}

ParticleSystem::~ParticleSystem()
{
	// Destroy the mesh.
	if( mParticleMesh != nullptr )
	{
		mManager.getCore().getMeshManager().destroyMesh( mParticleMesh->getId() );
		mParticleMesh = nullptr;
	}

}

#pragma region Virtual functions

void ParticleSystem::activate()
{
	// Move all particles polygons in particle system mesh to "unallocated" location outside the view.
	for( unsigned int i = 0; i < mConfig.mParticleCount; ++i )
	{
		float rotAngle = sDistribution( sGenerator ) * 360.0f;
		setParticleVertices( i, mUnallocatedParticlePosition,
				Quaternion::newAxisRotation( rotAngle, Vec3( 0.0f, 1.0f, 0.0f ) ) );
	}

	// Set the initial lifetime for this particle system.
	mLifeLeft = getConfiguration().getRandomLifetime();

}

void ParticleSystem::deactivate()
{
	// No implementation needed.
}

void ParticleSystem::attachToScene( std::shared_ptr<Scene> scene, std::shared_ptr<Node> parentNode )
{
	// Don't allow re-attaching without first detaching the particle system.
	if( mScene != nullptr || mRenderable != nullptr )
		throw std::runtime_error( "ParticleSystem is already attached to the scene." );

	// Save the scene pointer.
	mScene = scene;

	// Make sure our mesh is valid.
	if( mParticleMesh == nullptr )
		throw std::runtime_error( "Can't add particlesystem to scene because mesh has not been defined." );

	// Generate the ID for the renderable and create it. Associate it with the our mesh
	// and particle system specific material.
	const ParticleMeshData& meshData = getParticleMeshData();
	mRenderableId = mManager.getCore().generateId( RenderCore::IDGenerator::Renderable );
	mRenderable = mScene->createRenderable( mRenderableId );
	mRenderable->setGeometryIds( mParticleMesh->getId(), meshData.getMaterialId() );
	mRenderable->getParentNode()->setPosition( Vec3( 0.0f, 20.0f, 0.0f ) );
}

void ParticleSystem::detachFromScene()
{
	// Destroy the renderable and set scene and renderable pointers to null.
	if( mScene != nullptr && mRenderableId != "" )
	{
		mScene->destroyRenderable( mRenderableId );
		mRenderableId = "";
	}

	mScene = nullptr;
	mRenderable = nullptr;
}

void ParticleSystem::addedToParticleManager()
{
	// Make sure we do not get added twice.
	if( mParticleMesh != nullptr )
		throw std::runtime_error( "Particle system has been added to the particle manager already." );

	// Define the mesh properties.
	unsigned int maxNumOfVertices = static_cast<unsigned int>( getParticleMeshData().getParticlePrototype().mVertices.size() ) *
			getConfiguration().mParticleCount;
	unsigned int maxNumOfFaces = static_cast<unsigned int>( getParticleMeshData().getParticlePrototype().mFaceIndices.size() ) / 3;  // 3 vertex indices per triangle face.
	maxNumOfFaces *= getConfiguration().mParticleCount;
	VertexLayout layout = getParticleMeshData().getParticlePrototype().getVertexLayout();

	// Create the mesh.
	std::string meshid = mManager.getCore().generateId( RenderCore::IDGenerator::Mesh );
	MeshManager& meshman = mManager.getCore().getMeshManager();
	mParticleMesh = meshman.createMesh( meshid );
	mParticleMesh->initialize( layout, maxNumOfVertices, maxNumOfFaces );

	// Initialize the particle meshes coordinates and faces.
	createParticleMesh();
}

void ParticleSystem::removeFromParticleManager()
{
	// Release the mesh created for this particle manager.
	if( mParticleMesh != nullptr )
	{
		MeshManager& meshman = mManager.getCore().getMeshManager();
		std::string meshId = mParticleMesh->getId();
		mParticleMesh = nullptr;
		meshman.destroyMesh( meshId );
	}
}

#pragma endregion

const ParticleSystem::Configuration& ParticleSystem::getConfiguration() const
{
	return mConfig;
}

const ParticleMeshData& ParticleSystem::getParticleMeshData() const
{
	return mMeshData;
}

void ParticleSystem::setParticleSystemPosition( const Vec3& pos )
{
	if( mRenderable != nullptr )
	{
		mRenderable->getParentNode()->setPosition( pos );
	}
}

std::shared_ptr<Node> ParticleSystem::getSceneNode() const
{
	std::shared_ptr<Node> sceneNode = nullptr;
	if( mRenderable != nullptr )
		sceneNode = mRenderable->getParentNode();
	return sceneNode;
}

bool ParticleSystem::update( double timeStepSecs )
{
	for( size_t index = 0; index < mParticles.size(); ++index )
	{
		if( mParticles[ index ].getParticleIndex() != -1 )
		{
			// This is an active particle. Update it.
			bool dead = mParticles[ index ].update( timeStepSecs );
			if( dead )
			{
				// This particle has just dead. Add it to the pool of free particles.
				releaseParticle( static_cast<int>( index ) );
			}
		}
	}

	// Spawn new particles.
	scheduleParticleSpawn( timeStepSecs );

	// Handle the lifetime.
	bool systemIsDead = false;
	mLifeLeft -= timeStepSecs;
	if( mLifeLeft < 0.0 )
		systemIsDead = true;
	return systemIsDead;
}

void ParticleSystem::setParticlePosition( unsigned int particleIndex, const Vec3& particlePos )
{
	mParticles[ particleIndex ].setPosition( particlePos );
}

int ParticleSystem::spawnParticle( const Vec3& particlePos, const Vec3& velocity, double timeToLive )
{
	// First, get next free particle.
	int particleIndex = allocateParticle();

	// If we found a free particle, bring it to life.
	if( particleIndex != -1 )
	{
		// Position this particle to the position.
		mParticles[ particleIndex ].startParticleLife( particleIndex, timeToLive, particlePos );
		mParticles[ particleIndex ].setVelocity( velocity );
	}
	return particleIndex;
}

void ParticleSystem::createParticleMesh()
{
	const ParticleMeshData::ParticlePrototype& proto = mMeshData.getParticlePrototype();
	mParticles.clear();
	
	// Loop all particles through.
	unsigned int faceIndex = 0;
	mParticles.reserve( mConfig.mParticleCount );
	for( unsigned int i = 0; i < mConfig.mParticleCount; ++i )
	{
		// Create the particle object.
		Particle p( *this );
		mParticles.push_back( p );
		mUnallocatedParticleIndices.push_back( i );

		// Create the polygon for i'th particle.
		size_t baseVertexIndex = proto.mVertices.size() * i;
		for( size_t vertexIndex = 0; vertexIndex < proto.mVertices.size(); vertexIndex++ )
		{
			// Position the vertex polygon to initial location.
			const ParticleMeshData::Vertex& pv = proto.mVertices[ vertexIndex ];
			mParticleMesh->setPosition( static_cast<size_t>( baseVertexIndex ) + vertexIndex,
					pv.mPos + mUnallocatedParticlePosition );

			// Set additional vertex attributes.
			if( proto.mVertexAttributeBits & ParticleMeshData::ParticlePrototype::KRgbaAttributeBit )
				mParticleMesh->setColor( baseVertexIndex + vertexIndex, 0, pv.mColorAttribute.asVec4() );
			if( proto.mVertexAttributeBits & ParticleMeshData::ParticlePrototype::KFloatAttributeBit )
				throw std::runtime_error( "Float attribute has not been implemented yet." );
		}
		for( size_t faceVertexIndex = 0; faceVertexIndex < proto.mFaceIndices.size(); faceVertexIndex += 3 )
		{
			// Construct the face.
			size_t vi1 = baseVertexIndex + proto.mFaceIndices[ static_cast<unsigned int>( faceVertexIndex ) + 0 ];
			size_t vi2 = baseVertexIndex + proto.mFaceIndices[ static_cast<unsigned int>( faceVertexIndex ) + 1 ];
			size_t vi3 = baseVertexIndex + proto.mFaceIndices[ static_cast<unsigned int>( faceVertexIndex ) + 2 ];
			mParticleMesh->setFace( faceIndex, vi1, vi2, vi3 );

			// Move to next face.
			faceIndex++;
		}
	}
}

void ParticleSystem::releaseParticle( int particleIndex )
{
	// Position the particle to very far from current location. It is still bound to the mesh
	// so that particular particle face will be moved to "unallocated particle position".
	setParticlePosition( particleIndex, mUnallocatedParticlePosition );
	mParticles[ particleIndex ].endParticleLife();

	// Add particle index to the collection of free particles.
	mUnallocatedParticleIndices.push_back( particleIndex );

}

int ParticleSystem::allocateParticle()
{
	// Check if we have any free particles.
	if( mUnallocatedParticleIndices.size() == 0 )
		return -1;

	// Allocate the particle.
	int particleIndex = mUnallocatedParticleIndices.back();
	mUnallocatedParticleIndices.pop_back();
	return particleIndex;
}

void ParticleSystem::setParticleVertices(
	unsigned int particleIndex,
	const Vec3& particlePos,
	const Quaternion& quat )
{
	// Position the vertices of particleIndex'th particle around the new particle position.
	Matrix3x3 rotMat = quat.toRotationMatrix3();
	const ParticleMeshData::ParticlePrototype& proto = mMeshData.getParticlePrototype();
	unsigned int vertexCount = static_cast<unsigned int>( proto.mVertices.size() );
	unsigned int baseVertexIndex = particleIndex * vertexCount;
	for( unsigned int i = 0; i < vertexCount; ++i )
	{
		// Position the vertex.
		Vec3 vp = rotMat * proto.mVertices[ i ].mPos;
		vp += particlePos;
		mParticleMesh->setPosition( static_cast<size_t>( baseVertexIndex ) + i, vp );
	}
}
