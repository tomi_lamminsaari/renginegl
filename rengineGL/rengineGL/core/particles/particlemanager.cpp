
#include "../defines.h"
#include "particlemanager.h"
#include "particlesparks.h"
#include "particlesprinkler.h"

ParticleManager::ParticleManager( RenderCore &core ) :
	ManagerBase( core ),
	mCore( core )
{

}

ParticleManager::~ParticleManager()
{
	// Delete all the particle systems.
	for( ParticleSystem* partSys : mParticleSystems )
		delete partSys;
	mParticleSystems.clear();
	mFreeParticleSprinklers.clear();
	mFreeParticleSparks.clear();
	mActiveParticleSystems.clear();
}

void ParticleManager::update( double timeStepSecs )
{
	// Loop all active particle systems through and let them animate themselves.
	auto startTime = std::chrono::high_resolution_clock::now();
	for( size_t index = 0; index < mActiveParticleSystems.size(); index++ )
	{
		// Update the particle system.
		bool isDead = mActiveParticleSystems[ index ]->update( timeStepSecs );

		// If the particle system has reached the end of its life, we remove it from the list of
		// active particle systems, mark it free and append it to the list of free particle systems.
		if( isDead )
		{
			// Remove the current particle system from the list of active particle systems.
			ParticleSystem* partSys = mActiveParticleSystems[ index ];
			mActiveParticleSystems.erase( mActiveParticleSystems.begin() + index );

			// Notify the particle system that it will be removed from the scene.
			partSys->detachFromScene();
			partSys->deactivate();

			// Add the particle system to the list of free particle systems for reuse.
			appendParticleSystemToRightList( partSys );

			// Rewind the current index back because we removed the index'th particle system
			// from the array. Otherwise we would skip one particle system in the array of
			// active particle systems.
			index--;
		}
	}
	auto endTime = std::chrono::high_resolution_clock::now();
	mUpdateDuration = std::chrono::duration_cast< std::chrono::microseconds >( endTime - startTime ).count();
}

ParticleSystem* ParticleManager::getFreeParticleSystem( ParticleSystemType systemType )
{
	// Based on given particle system type, find free particle system.
	ParticleSystem* partSys = nullptr;
	switch( systemType )
	{
	case KParticleSystemTypeSprinklers:
		partSys = removeFirstParticleSystemFromList( mFreeParticleSprinklers );
		break;

	case KParticleSystemTypeSparks:
		partSys = removeFirstParticleSystemFromList( mFreeParticleSparks );
		break;

	default:
		break;
	}

	// Did we find free particle system.
	if( partSys != nullptr )
	{
		// Yes. We have a valid pointer to particle system. Reset the particle system to default state and
		// add it to the list of active particle systems.
		mActiveParticleSystems.push_back( partSys );
		partSys->activate();
	}

	// Returns the particle system.
	return partSys;
}

void ParticleManager::intializeParticleSystemsPool()
{
	// Reserve enough room for all particle systems.
	unsigned int totalParticleSystemCount = KDefaultParticleSparksPoolSize;
	mFreeParticleSprinklers.reserve( KDefaultParticleSprinklerPoolSize );
	mFreeParticleSparks.reserve( KDefaultParticleSparksPoolSize );
	mActiveParticleSystems.reserve( totalParticleSystemCount );
	mParticleSystems.reserve( totalParticleSystemCount );

	// Initialize the pool of sprinkler particle systems.
	for( unsigned int i = 0; i < KDefaultParticleSprinklerPoolSize; ++i )
	{
		// Create the particle system object.
		ParticleSprinkler* particleSystem = dynamic_cast< ParticleSprinkler* >( createParticleSystem( KParticleSystemTypeSprinklers ) );
		
		// Add it to the master list and to the list of free sprinkler particle systems.
		mParticleSystems.push_back( particleSystem );
		mFreeParticleSprinklers.push_back( particleSystem );
		particleSystem->addedToParticleManager();
	}

	// Initialize the pool of spark particle system.
	for( unsigned int i = 0; i < KDefaultParticleSparksPoolSize; ++i )
	{
		// Create the particle system object.
		ParticleSparks* particleSystem = dynamic_cast< ParticleSparks* >( createParticleSystem( KParticleSystemTypeSparks ) );
		
		// Add it ot the master list and to the list of spark particle systems.
		mParticleSystems.push_back( particleSystem );
		mFreeParticleSparks.push_back( particleSystem );
		particleSystem->addedToParticleManager();
	}

	// Create next particle type.
}

ParticleSystem* ParticleManager::removeFirstParticleSystemFromList(
	std::vector< ParticleSystem* >& listOfPartSystems
)
{
	ParticleSystem* partSystem = nullptr;
	if( listOfPartSystems.size() > 0 )
	{
		// There are particle systems in the list. Remove the last particle system one from the list.
		partSystem = listOfPartSystems.back();
		listOfPartSystems.erase( listOfPartSystems.begin() + ( listOfPartSystems.size() - 1 ) );
	}
	return partSystem;
}

void ParticleManager::appendParticleSystemToRightList( ParticleSystem* partSys )
{
	switch( partSys->getConfiguration().mType )
	{
	case KParticleSystemTypeSprinklers:
		mFreeParticleSprinklers.push_back( partSys );
		break;

	case KParticleSystemTypeSparks:
		mFreeParticleSparks.push_back( partSys );
		break;

	default:
		// We did not find correct list. Don't add anywhere that will eventually cause the particle system
		// to be destroyed because no-one is referencing it anymore.
		break;
	}
}

ParticleSystem* ParticleManager::createParticleSystem( ParticleSystemType systemType )
{
	// Create right kind of particle system.
	ParticleSystem* partSys = nullptr;
	switch( systemType )
	{
	case KParticleSystemTypeSprinklers:
		partSys = ParticleSprinkler::createParticleSystem( *this );
		break;
		
	case KParticleSystemTypeSparks:
		partSys = ParticleSparks::createParticleSparks( *this );
		break;
		
	default:
		throw std::runtime_error( "Unknown particle system type." );
	}
	
	return partSys;
}

void ParticleManager::cleanup()
{
	// Destroy all particle system objects.
	mActiveParticleSystems.clear();
	mFreeParticleSprinklers.clear();
	for( ParticleSystem* ps : mParticleSystems )
	{
		ps->removeFromParticleManager();
		delete ps;
	}
		
	mParticleSystems.clear();
}
