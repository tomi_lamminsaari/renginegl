
#pragma once

/// <summary>Datatype for defining particle system types.</summary>
typedef int ParticleSystemType;

/// <summary>Type code for built-in water sprinklers particle system.</summary>
const ParticleSystemType KParticleSystemTypeSprinklers = 1;

/// <summary>Type code for built-in electric sparks particle system.</summary>
const ParticleSystemType KParticleSystemTypeSparks = 2;

/// <summary>
/// First freely usable particle system type code. The second custom type should be this +1 and
/// third custom type code is this +2.
/// </summary>
const ParticleSystemType KParticleSystemTypeCustom = 5000;
