
#pragma once

#include <set>
#include <vector>
#include <memory>
#include <random>
#include "particle.h"
#include "particledefines.h"
#include "particlemeshdata.h"
#include "../../math/quaternion.h"
#include "../vertexlayout.h"

// Forward declarations.
class Mesh;
class Node;
class Particle;
class ParticleManager;
class Renderable;
class Scene;

/// <summary>
/// Base class of all the particle systems.
/// <para>
/// After the particle system has been created, you must call initializeParticleSystem() function
/// to initialize.
/// </para>
/// </summary>
class ParticleSystem
{
public:

	/// <summary>
	/// Configuration class works as a collection of particle system parameters that configure the particle
	/// system behavior. It contains the information about the number of particles and particle lifetimes.
	/// <summary>
	class Configuration
	{
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		/// <param name="particleCount">Number of particles the particle system has.</param>
		/// <param name="gravity">The generic gravity that affects to the movement of the particles.</param>
		/// <param name="systemType">The type code of the particle system.</param>
		/// <param name="lifetime">The minimum and maximum lifetime of the particle system.</param>
		Configuration(
			unsigned int particleCount,
			const Vec3& gravity,
			ParticleSystemType systemType,
			const std::pair<double, double>& lifetime );

		/// <summary>
		/// A copy constructor.
		/// </summary>
		/// <param name="obj">Another configuration object.</param>
		Configuration( const Configuration& obj );

		/// <summary>
		/// An assignment operator.
		/// </summary>
		/// <param name="obj">Another configuration object.</param>
		/// <returns>Reference to this object.</returns>
		Configuration& operator=( const Configuration& obj );

		/// <summary>
		/// Gets the random lifetime value.
		/// </summary>
		/// <returns>Returns a random lifetime value based on lifetime setting.</returns>
		double getRandomLifetime() const;

	public:
		/// <summary>Number of particles the particle system has.</summary>
		unsigned int mParticleCount;

		/// <summary>The gravity of the particle system. Affects to each moving particle.</summary>
		Vec3 mGravity;

		/// <summary>The type of this particle system.</summary>
		ParticleSystemType mType;

		/// <summary>Defines the minimum and maximum lifetime of the particle system.</summary>
		std::pair<double, double> mLifetimeRange;
	};

public:
	/// <summary>
	/// Creates random floating number that is between the given range.
	/// </summary>
	/// <param name="minFloat">The minimum value.</param>
	/// <param name="maxFloat">The maximum value.</param>
	/// <returns>Returns a number value between minFloat and maxFloat.</returns>
	static float generateRandom( float minFloat, float maxFloat );

public:

	/// <summary>
	/// Creates new ParticleSystem object with given number of particles.
	/// </summary>
	/// <param name="core">Reference to particle manager object.</param>
	/// <param name="systemConfiguration">The general parameters of the particle system.</param>
	/// <param name="meshData">The mesh and particle prototype information of the particle system.</param>
	ParticleSystem(
		ParticleManager& manager,
		const Configuration& systemConfiguration,
		const ParticleMeshData& meshData );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~ParticleSystem();

	ParticleSystem( const ParticleSystem &obj ) = delete;  //!< Disabled.
	ParticleSystem &operator = ( const ParticleSystem &obj ) = delete;  //!< Disabled.

public:  // virtual methods.
#pragma region Lifecycle functions

	/// <summary>
	/// Called by ParticleManager when particle system gets activated for use and when it has been
	/// added to the scene. This baseclass implementation will initialize the lifetime properties.
	/// <para>
	/// If you override this method, you must call this baseclass implementation from derived class.
	/// </para>
	/// <para>
	/// If you want, you could spawn all the particles at once in activation and later animate them
	/// in update method.
	/// </para>
	/// </summary>
	virtual void activate();

	/// <summary>
	/// Called by the ParticleManager when particle system has reached the end of its lifecycle and gets
	/// removed from the scene.
	/// </summary>
	virtual void deactivate();

	/// <summary>
	/// Called by the ParticleManager when this particle system gets added there. This function will
	/// create the actual mesh for this particle system.
	/// <para>
	/// You can override this function but you must call first this base class implementation.
	/// </para>
	virtual void addedToParticleManager();

	/// <summary>
	/// Called by the ParticleManager when it wants to discard this particle system. This function
	/// will release the mesh related graphics resources.
	/// <para>
	/// You can override this function but you must call this base class implementation.
	/// </para>
	virtual void removeFromParticleManager();

	/// <summary>
	/// Shows the particle system in scene. This makes the particle system visible.
	/// <para>
	/// If you override this method, you must call this base class implementation as well.
	/// </para>
	/// </summary>
	/// <param name="scene">Pointer to scene where the particle system will be added.</param>
	/// <param name="parentNode">
	/// The scene node where the particle system will be attached to. Pass nullptr to attach
	/// the particle system to root node of the scene.
	/// </param>
	virtual void attachToScene( std::shared_ptr<Scene> scene, std::shared_ptr<Node> parentNode );

	/// <summary>
	/// Removes the particle system from scene. Particle system stops being visible. The particle system
	/// can still be alive but it is no longer rendered when scene is rendered.
	/// <para>
	/// If you override this method, you must call this base class implementation as well.
	/// </para>
	/// </summary>
	virtual void detachFromScene();

	/// <summary>
	/// Called by the update function. This function must be overridden in child classes. The
	/// purpose of this function is to schedule the creation of new particles as the time progresses.
	/// </summary>
	/// <param name="timeStepSecs">The time since previous update.</param>
	virtual void scheduleParticleSpawn( double timeStepSecs ) = 0;

#pragma endregion

public:

	/// <summary>
	/// Returns the particle system configuration data.
	/// </summary>
	/// <returns>Particle system configuration data.</returns>
	const Configuration& getConfiguration() const;

	/// <summary>
	/// Returns the information about the particle system mesh structure. This ParticleMeshData object
	/// will contain also the prototype for particle polygons.
	/// </summary>
	/// <returns>Returns the mesh data object</returns>
	const ParticleMeshData& getParticleMeshData() const;

	/// <summary>
	/// Sets the position of particle system. This does nothing if particle system has not been
	/// attached to the scene. This positions the particle system's scene node in relation to the
	/// location of the parent node.
	/// </summary>
	/// <param name="pos">Position of the particle system.</param>
	void setParticleSystemPosition( const Vec3& pos );

	/// <summary>
	/// Returns the scene node of this particle system. Returns nullptr if this particle system has
	/// not been attached to the scene.
	/// </summary>
	/// <returns>Pointer to scene node. Nullptr if particle system is not attached to scene.</returns>
	std::shared_ptr<Node> getSceneNode() const;

	/// <summary>
	/// Updates and animates this particle system. As long as particle system is alive, the
	/// ParticleManager will call this.
	/// </summary>
	/// <param name="timeSecs">Time from previous update, in seconds.</param>
	/// <returns>Returns true if this particle system has reached the end of its lifecycle.</returns>
	bool update( double timeSecs );

	/// <summary>
	/// Sets the position of a single particle.
	/// </summary>
	/// <param name="particleIndex">The index of the particle to reposition.</param>
	/// <param name="particlePos">The coordinate where the particle will be positioned to.</param>
	void setParticlePosition( unsigned int particleIndex, const Vec3& particlePos );

	/// <summary>
	/// Spawns new particle to this particle system.
	/// </summary>
	/// <param name="particlePos">The intinial position of the particle.</param>
	/// <param name="velocity">Initial movement velocity of the particle.</param>
	/// <param name="timeToLive">The life time of the new particle.</param>
	/// <returns>Returns the index of the particle that was allocated.</returns>
	int spawnParticle( const Vec3& particlePos, const Vec3& velocity, double timeToLive );

private:  // private methods

	/// <summary>
	/// Initializes the mesh for this particle system. Cretes the particle polygons so that they can be
	/// later moved around. This function expects that 'mMeshData' member contains the valid particle
	/// mesh data.
	/// </summary>
	void createParticleMesh();

	/// <summary>
	/// Releases the particle number.
	/// </summary>
	/// <param name="particleIndex">The index number of the particle to release.</param>
	void releaseParticle( int particleIndex );

	/// <summary>
	/// Allocates particle from the pool of free particles. After the particle has been allocated,
	/// we can position the faces in particle mesh to correct position.
	/// </summary>
	/// <returns>
	/// Returns the index of the allocated particle. Negative number if there were no free particles
	/// in the free particles pool.
	/// </returns>
	int allocateParticle();

	/// <summary>
	/// Sets the particle's vertices and rotates the particle faces.
	/// </summary>
	/// <param name="particleIndex">Index number of the particle to set.</param>
	/// <param name="particlePos">The center position of the particle.</param>
	/// <param name="quat">The rotation of the particle.</param>
	void setParticleVertices(
		unsigned int particleIndex,
		const Vec3& particlePos,
		const Quaternion& quat );

protected:

	/// <summary>Reference to owning particle manager.</summary>
	ParticleManager& mManager;

	/// <summary>The general particle system parameters.</summary>
	Configuration mConfig;

	/// <summary>Particle mesh and single particle mesh prototype.</summary>
	ParticleMeshData mMeshData;

	/// <summary>Collection of individual particle objects.</summary>
	std::vector< Particle > mParticles;

	/// <summary>The mesh for this particle system.</summary>
	std::shared_ptr<Mesh> mParticleMesh;

	/// <summary>Indices of unallocated particle objects. These operate as indices to mParticles vector.</summary>
	std::vector< int > mUnallocatedParticleIndices;

	/// <summary>The position where all unallocated particles are located to.</summary>
	Vec3 mUnallocatedParticlePosition;

	/// <summary>Total lifetime this particle system has.</summary>
	double mLifeLeft;

	/// <summary>The scene where this particle system has been made visible.</summary>
	std::shared_ptr<Scene> mScene;

	/// <summary>The renderable we use when rendering this particle system to scene.</summary>
	std::shared_ptr<Renderable> mRenderable;

	/// <summary>ID of the renderable used for rendering this particle system.</summary>
	std::string mRenderableId;

private:
	static std::default_random_engine sGenerator;
	static std::uniform_real_distribution<float> sDistribution;

	// Provide priviledged access for some supporting classes.
	friend class Particle;
};
