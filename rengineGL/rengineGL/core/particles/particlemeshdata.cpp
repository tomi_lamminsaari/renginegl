
#include "particlemeshdata.h"

//
// class Vertex
#pragma region Vertex

ParticleMeshData::Vertex::Vertex() :
	mPos( 0.0f, 0.0f, 0.0f ),
	mColorAttribute( 0.0f, 0.0f, 0.0f, 0.0f ),
	mFloatAttribute( 0.0f )
{

}

ParticleMeshData::Vertex::Vertex(
	const Vec3& pos,
	const Rgba& colorAttribute,
	float floatAttribute ) :
	mPos( pos ),
	mColorAttribute( colorAttribute ),
	mFloatAttribute( floatAttribute )
{
}

ParticleMeshData::Vertex::Vertex( const ParticleMeshData::Vertex& obj ) :
	mPos( obj.mPos ),
	mColorAttribute( obj.mColorAttribute ),
	mFloatAttribute( obj.mFloatAttribute )
{

}

ParticleMeshData::Vertex& ParticleMeshData::Vertex::operator=( const ParticleMeshData::Vertex& obj )
{
	if( this != &obj )
	{
		mPos = obj.mPos;
		mColorAttribute = obj.mColorAttribute;
		mFloatAttribute = obj.mFloatAttribute;
	}
	return *this;
}

void ParticleMeshData::Vertex::setColorAttribute( const Rgba& colorAttribute )
{
	mColorAttribute = colorAttribute;
}

void ParticleMeshData::Vertex::setFloatAttribute( float floatAttribute )
{
	mFloatAttribute = floatAttribute;
}

#pragma endregion

//
// ParticlePrototype
#pragma region ParticlePrototype

ParticleMeshData::ParticlePrototype::ParticlePrototype( unsigned int vertexAttributeBits ) :
	mVertexAttributeBits( vertexAttributeBits )
{

}

ParticleMeshData::ParticlePrototype::ParticlePrototype( const ParticleMeshData::ParticlePrototype& obj ) :
	mVertexAttributeBits( obj.mVertexAttributeBits ),
	mVertices( obj.mVertices ),
	mFaceIndices( obj.mFaceIndices )
{

}

ParticleMeshData::ParticlePrototype& ParticleMeshData::ParticlePrototype::operator=(
	const ParticleMeshData::ParticlePrototype& obj )
{
	if( this != &obj )
	{
		mVertexAttributeBits = obj.mVertexAttributeBits;
		mVertices = obj.mVertices;
		mFaceIndices = obj.mFaceIndices;
	}
	return *this;
}

unsigned int ParticleMeshData::ParticlePrototype::addVertex( const ParticleMeshData::Vertex& v )
{
	unsigned int index = static_cast< unsigned int >( mVertices.size() );
	mVertices.push_back( v );
	return index;
}

void ParticleMeshData::ParticlePrototype::addFace(
	unsigned int v1,
	unsigned int v2,
	unsigned int v3 )
{
	mFaceIndices.push_back( v1 );
	mFaceIndices.push_back( v2 );
	mFaceIndices.push_back( v3 );
}

VertexLayout ParticleMeshData::ParticlePrototype::getVertexLayout() const
{
	std::vector<VertexElement> elements;
	elements.push_back( VertexElement::Position3f );

	// Add additional attributes.
	if( mVertexAttributeBits & KRgbaAttributeBit )
		elements.push_back( VertexElement::Color4f );
	if( mVertexAttributeBits & KFloatAttributeBit )
		throw std::runtime_error( "KFloatAttributeBit has not been implemented." );

	return VertexLayout( elements );
}

#pragma endregion

//
// class ParticleMeshData
#pragma region ParticleMeshData

ParticleMeshData::ParticleMeshData(
	const ParticleMeshData::ParticlePrototype& prototype,
	const std::string& materialId ) :
	mParticlePrototype( prototype ),
	mMaterial( materialId )
{

}

ParticleMeshData::ParticleMeshData( const ParticleMeshData& obj ) :
	mParticlePrototype( obj.mParticlePrototype ),
	mMaterial( obj.mMaterial )
{

}

ParticleMeshData& ParticleMeshData::operator=( const ParticleMeshData& obj )
{
	if( this != &obj )
	{
		mParticlePrototype = obj.mParticlePrototype;
		mMaterial = obj.mMaterial;
	}
	return *this;
}

ParticleMeshData::~ParticleMeshData()
{

}

const ParticleMeshData::ParticlePrototype& ParticleMeshData::getParticlePrototype() const
{
	return mParticlePrototype;
}

const std::string& ParticleMeshData::getMaterialId() const
{
	return mMaterial;
}

#pragma endregion
