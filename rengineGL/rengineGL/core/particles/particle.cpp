
#include "particle.h"

Particle::Particle( ParticleSystem &partSys ) :
    mOwningSystem( partSys ),
    mParticleIndex( -1 ),
    mGenericFriction( 1.0 ),
    mTimeToLive( 0.0 ),
    mRotation( Quaternion::newAxisRotation( 0.0f, Vec3( 0.0, 1.0, 0.0 ) ) )
{

}

Particle::~Particle()
{

}

void Particle::setPosition( const Vec3 &pos )
{
    if( mParticleIndex == -1 )
        throw std::runtime_error( "Cannot position unallocated particle." );

    // Store the current position.
    mPosition = pos;

    // Set the particle vertices to right position.
    mOwningSystem.setParticleVertices( mParticleIndex, pos, mRotation );
}

const Vec3& Particle::getPosition() const
{
    return mPosition;
}

void Particle::setRotation( const Quaternion& rot )
{
    mRotation = rot;
}

void Particle::setVelocity( const Vec3 &velocity )
{
    mVelocity = velocity;
}

bool Particle::update( double timeStepSecs )
{
    // Move the particle.
    const float timeStepSecsFloat = static_cast< float >( timeStepSecs );
    mVelocity += mOwningSystem.mConfig.mGravity * timeStepSecsFloat;
    mVelocity *= mGenericFriction;
    mPosition.mX += mVelocity.mX * timeStepSecsFloat;
    mPosition.mY += mVelocity.mY * timeStepSecsFloat;
    mPosition.mZ += mVelocity.mZ * timeStepSecsFloat;
    mOwningSystem.setParticleVertices( mParticleIndex, mPosition, mRotation );
    

    // Let the time progress.
    bool particleDead = false;
    mTimeToLive -= timeStepSecs;
    if( mTimeToLive < 0.0 )
        particleDead = true;
    return particleDead;
}

void Particle::endParticleLife()
{
    mParticleIndex = -1;
    mTimeToLive = 0.0;
    mVelocity = Vec3( 0.0f, 0.0f, 0.0f );
    mPosition = Vec3( 0.0f, 0.0f, 0.0f );

}

void Particle::startParticleLife(
    int particleIndex,
    double timeToLive,
    const Vec3& initialPosition )
{
    mParticleIndex = particleIndex;
    mTimeToLive = timeToLive;
    setPosition( initialPosition );
}

const Vec3& Particle::getVelocity() const
{
    return mVelocity;
}

int Particle::getParticleIndex() const
{
    return mParticleIndex;
}
