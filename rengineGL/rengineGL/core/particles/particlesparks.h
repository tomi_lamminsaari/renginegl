#pragma once

#include "particlesystem.h"

/// <summary>
/// Particle system that look like metallic impact or electric sparks.
/// </summary>
class ParticleSparks : public ParticleSystem
{
public:
	/// <summary>
	/// Creates new particle sparks system.
	/// </summary>
	/// <param name="manager">Owning particle manager.</param>
	/// <returns>Created particle sparks system.</returns>
	static ParticleSparks* createParticleSparks( ParticleManager& manager );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~ParticleSparks();

	ParticleSparks( const ParticleSparks& obj ) = delete;
	ParticleSparks& operator=( const ParticleSparks& obj ) = delete;

	/// <summary>
	/// Call this to spawn particles sparks to given direction.
	/// </summary>
	/// <param name="particleDirection">The approximated launch direction of the particles.</param>
	void initializeSparks( const Vec3& particleDirection );

public:  // from ParticleSystem

	/// <summary>
	/// Override ParticleSystem::active().
	/// </summary>
	virtual void activate() override;

	/// <summary>
	/// Override deactivate().
	/// </summary>
	virtual void deactivate() override;

	/// <summary>
	/// Called when particle system has been created and added to particle manager. We will
	/// set the initial orientation for the actual particles.
	/// <summary>
	virtual void addedToParticleManager() override;

	/// <summary>
	/// Override scheduleParticleSpawn().
	/// </summary>
	virtual void scheduleParticleSpawn( double timeStepSecs ) override;

private:

	/// <summary>
	/// Private constructor.
	/// </summary>
	/// <param name="manager">The owning particle manager.</param>
	/// <param name="systemConfiguration">The generic particle system properties.</param>
	/// <param name="meshData">Particle system's mesh data.</param>
	ParticleSparks( ParticleManager& manager,
		const Configuration& systemConfiguration,
		const ParticleMeshData& meshData );

private:

	/// <summary>The direction where to spawn the sparks.</summary>
	Vec3 mParticleDir;

	/// <summary>Number of particles we have left for spawning.</summary>
	unsigned int mParticlesLeftToSpawn;

	/// <summary>Current counter value that defines when new particle gets spawned.</summary>
	double mSpawnCounter;
};
