
#pragma once

#include <vector>
#include "../../math/vec3.h"
#include "../../utils/rgba.h"
#include "../vertexlayout.h"

/// <summary>
/// Contains the information related to the mesh and material that will be used
/// when rendering the particle system. This object contains a prototype vertex/face
/// information of a single particle that define the geometry of a the particles.
/// </summary>
class ParticleMeshData
{
public:
	/// <summary>
	/// The Vertex class defines the vertex attributes of a vertex that is in particle system.
	/// </summary>
	class Vertex
	{
	public:
		
		/// <summary>
		/// Constructor. Sets position to origo and disables other attributes.
		/// </summary>
		Vertex();

		/// <summary>
		/// Constructs vertex object and sets the other attributes as well.
		/// </summary>
		/// <param name="pos">The position of the vertex.</param>
		/// <param name="colorAttribute">Possible color of the vertex.</param>
		/// <param name="floatAttribute">Possible float attribute of the vertex.</param>
		Vertex( const Vec3& pos,
			const Rgba& colorAttribute = Rgba(),
			float floatAttribute = 0.0f );

		/// <summary>
		/// A copy constructor.
		/// </summary>
		/// <param name="obj">Another vertex.</param>
		Vertex( const Vertex& obj );

		/// <summary>
		/// An assignment operator.
		/// </summary>
		/// <param name="obj">Another vertex.</param>
		/// <returns>Reference to this.</returns>
		Vertex& operator=( const Vertex& obj );

		/// <summary>
		/// Sets the color and enables the color attribute.
		/// </summary>
		/// <param name="colorAttribute">The color value.</param>
		void setColorAttribute( const Rgba& colorAttribute );

		/// <summary>
		/// Sets the float value and enables the float attribute.
		/// </summary>
		/// <param name="floatAttribute">The float value.</param>
		void setFloatAttribute( float floatAttribute );

	public:
		/// <summary>The position of the vertex. A mandatory attribute.</summary>
		Vec3 mPos;

		/// <summary>Color of the vertex if 'KRgbaAttributeBit' is enabled.</summary>
		Rgba mColorAttribute;

		/// <summary>A float value of the vertex if 'KFloatAttributeBit' is enabled.</summary>
		float mFloatAttribute;
	};

	/// <summary>
	/// ParticlePrototype class defines the mesh prototype of a single particle in the particle system.
	/// When the particle system constructs the actual particle mesh, it uses this object as a prototype
	/// of a particle and creates room for all the particles that have vertices and faces defined by this
	/// prototype.
	/// </summary>
	class ParticlePrototype
	{
	public:
		static const unsigned int KRgbaAttributeBit = 1 << 0;
		static const unsigned int KFloatAttributeBit = 1 << 1;

		/// <summary>
		/// Constructs the mesh prototype of a single particle. By default the constructed particle has
		/// zero vertices.
		/// </summary>
		/// <param name="vertexAttributeBits">Defines the vertex attributes we expect each vertex to have.</param>
		ParticlePrototype( unsigned int vertexAttributeBits );

		/// <summary>
		/// A copy constructor.
		/// </summary>
		/// <param name="obj">Another particle prototype.</param>
		ParticlePrototype( const ParticlePrototype& obj );

		/// <summary>
		/// An assignment operator.
		/// </summary>
		/// <param name="obj">Another particle prototype.</param>
		/// <returns>Reference to this object.</returns>
		ParticlePrototype& operator=( const ParticlePrototype& obj );

		/// <summary>
		/// Adds new vertex to particle prototype.
		/// </summary>
		/// <param name="v">The vertex to be added.</param>
		/// <returns>Returns the index number this added vertex received.</returns>
		unsigned int addVertex( const ParticleMeshData::Vertex& v );

		/// <summary>
		/// Adds new faces to particle prototype. The vertex indices refer to the vertices that
		/// have been added to this prototype with addVertex() method.
		/// </summary>
		/// <param name="vi1">First vertex of the face.</param>
		/// <param name="vi2">Second vertex of the face.</param>
		/// <param name="vi3">Third vertex of the face.</param>
		void addFace( unsigned int vi1,
				unsigned int vi2,
				unsigned int vi3 );

		/// <summary>
		/// Based on current particle prototype data, this function returns vertex layout object
		/// that will create equivalent mesh object with defined vertex attributes.
		/// </summary>
		/// <returns>The vertex layout required for creating equivalent mesh object.</returns>
		VertexLayout getVertexLayout() const;

	public:
		/// <summary>A bitfield that defines the vertex attributes we expect each vertex to have.</summary>
		unsigned int mVertexAttributeBits;

		/// <summary>The vertices of a single particle.</summary>
		std::vector<ParticleMeshData::Vertex> mVertices;

		/// <summary>Vertex indices that make up the faces of a particle. 3 subsequential vertex indices make up a face.</summary>
		std::vector< unsigned int > mFaceIndices;
	};

public:
	/// <summary>
	/// Constructs new particle information object.
	/// </summary>
	/// <param name="prototype">Particle mesh prototype.</param>
	/// <param name="materialId">ID of the material that should be used when rendering the particle system.</summary>
	ParticleMeshData(
		const ParticlePrototype& prototype,
		const std::string& materialId );

	/// <summary>
	/// A copy constructor.
	/// </summary>
	/// <param name="obj">>Another ParticleMeshData object.</param>
	ParticleMeshData( const ParticleMeshData& obj );

	/// <summary>
	/// An assignment operator.
	/// </summary>
	/// <param name="obj">Another ParticleMeshData object.</param>
	/// <returns>Reference to this.</returns>
	ParticleMeshData& operator=( const ParticleMeshData& obj );

	/// <summary>
	/// Destructor.
	/// </summary>
	~ParticleMeshData();

	/// <summary>
	/// Returns the prototype mesh data of a single particle.
	/// </summary>
	/// <returns>Mesh prototype of a single particle.</returns>
	const ParticlePrototype& getParticlePrototype() const;

	/// <summary>
	/// Returns the ID of the material we are supposed to use when rendering the particles.
	/// </summmary>
	/// <returns>Material used for rendering particles.</returns>
	const std::string& getMaterialId() const;

private:

	/// <summary>Defines the prototype of a single particle geometry.</summary>
	ParticlePrototype mParticlePrototype;

	/// <summary>Name of the material we use when rendering the particles.</summary>
	std::string mMaterial;
};
