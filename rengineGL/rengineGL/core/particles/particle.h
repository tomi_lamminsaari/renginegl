#pragma once

#include "../../math/quaternion.h"
#include "../../math/vec3.h"
#include "particlesystem.h"

// Forward declarations.
class ParticleSystem;

/// <summary>
/// Particle class represents a single 3D particle that can be spawned to the
/// scene as part of particle system. In practise a particle is a floating triangle
/// that doesn't seem to be attached to other triangles.
/// <para>
/// The owning particle system has the mesh with polygons. A particle has a member
/// called mParticleIndex that is used for mapping this particle to actual face in
/// the mesh.
/// </para>
/// </summary>
class Particle
{
public:

    /// <summary>
    /// Constructs new particle that belongs to given particle system.
    /// </summary>
    Particle( ParticleSystem& particleSystem );

    /// <summary>
    /// Destructor.
    /// </summary>
    ~Particle();

    /// <summary>
    /// Moves the particle and updates its lifetime.
    /// </summary>
    /// <param name="timeSecs">Number of time has passed since previous update.</param>
    /// <returns>Returns true if particle has died.</returns>
    bool update( double timeSecs );

    /// <summary>
    /// Sets particle to unused state. Ends the lifecycle of a particle.
    /// </summary>
    void endParticleLife();

    /// <summary>
    /// Makes this particle to begin its lifecycle.
    /// </summary>
    /// <param name="particleIndex">
    /// Index of this particle. Used for mapping this particle to a polygon in particle
    /// system's mesh.
    /// </param>
    /// <param name="timeToLive">Initial lifetime of this particle.</param>
    /// <param name="initialPosition">Initial starting position of this particle.</param>
    void startParticleLife(
        int particleIndex,
        double timeToLive,
        const Vec3& initialPosition );

    /// <summary>
    /// Sets the position of this particle.
    /// </summary>
    /// <param name="pos">New position to set for this particle.</param>
    void setPosition( const Vec3& pos );

    /// <summary>
    /// Returns the current particle position.
    /// </summary>
    /// <returns>Current particle position.</returns>
    const Vec3& getPosition() const;

    /// <summary>
    /// Sets the rotation of this particle.
    /// </summary>
    /// <param name="rot">The quaternion that defines the orientation of this particle.</param>
    void setRotation( const Quaternion& rot );

    /// <summary>
    /// Sets the velocity of this particle.
    /// </summary>
    /// <param name="dir">The movement direction of this particle.</param>
    void setVelocity( const Vec3& dir );

    /// <summary>
    /// Returns the movement direction of this particle.
    /// </summary>
    /// <returns>Current movement direction.</returns>
    const Vec3& getVelocity() const;

    /// <summary>
    /// Returns the current particle index.
    /// </summary>
    /// <returns>Current particle index. -1 if this particle is free.</returns>
    int getParticleIndex() const;

private:

    /// <summary>The particle system that owns this particle.</summary>
    ParticleSystem& mOwningSystem;

    /// <summary>Index number of this particle.</summary>
    int mParticleIndex;

    /// <summary>Current position of the particle.</summary>
    Vec3 mPosition;

    /// <summary>Current movement velocity of the particle.</summary>
    Vec3 mVelocity;

    /// <summary>Friction that either slows down or speeds up the movement of the particle.</summary>
    double mGenericFriction;

    /// <summary>Current lifetime left.</summary>
    double mTimeToLive;

    /// <summary>Rotation of this particle.</summary>
    Quaternion mRotation;
};
