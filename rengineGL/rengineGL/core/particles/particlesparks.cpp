
#include "../defines.h"
#include "../../math/vec2.h"
#include "particlesparks.h"
#include "particlemeshdata.h"

namespace
{
	/// <summary>Number of particles this particle system has.</summary>
	const unsigned int KNumberOfParticles = 40;

	/// <summary>Default spawn frequency is 10 particles per second.</summary>
	const double KAverageSpawnFrequency = 1.0 / 80.0;
}

ParticleSparks* ParticleSparks::createParticleSparks( ParticleManager& manager )
{
	ParticleSparks* particlesystem = nullptr;
	try
	{
		// Initialize the generic configuration of sparks particle system.
		auto lifetimePair = std::make_pair( 2.5f, 3.0f );
		ParticleSystem::Configuration configuration(
			KNumberOfParticles,
			Vec3( 0.0f, -100.0f, 0.0f ),
			KParticleSystemTypeSparks, lifetimePair );

		// Create the particle prototype.
		ParticleMeshData::ParticlePrototype proto( ParticleMeshData::ParticlePrototype::KRgbaAttributeBit );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( 0.0f, 0.2f, 0.0f ), Rgba( 130, 160, 32 ) ) );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( -0.2f, -0.2f, 0.0f ), Rgba( 170, 140, 32 ) ) );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( 0.2f, -0.2f, 0.0f ), Rgba( 160, 130, 32 ) ) );
		proto.addFace( 0, 1, 2 );
		proto.addFace( 0, 2, 1 );
		ParticleMeshData pdm( proto, BuiltIn::MATERIAL_PARTICLESYSTEM );

		// Create the particle system.
		particlesystem = new ParticleSparks( manager, configuration, pdm );
	}
	catch( ... )
	{
		delete particlesystem;
		particlesystem = nullptr;
		throw;
	}
	return particlesystem;
}

ParticleSparks::~ParticleSparks()
{

}

void ParticleSparks::initializeSparks( const Vec3& particleDirection )
{
	const unsigned int KInitialSpawnCount = 20;
	mParticleDir = particleDirection;
	mParticlesLeftToSpawn = KNumberOfParticles - KInitialSpawnCount;

	// Spawn initial particles.
	for( int i = 0; i < KInitialSpawnCount; ++i )
	{
		Vec2 dirVec1( mParticleDir.mX, mParticleDir.mY );
		dirVec1.rotate( ParticleSystem::generateRandom( 0.1f, 2.0f ) );
		Vec2 dirVec2( mParticleDir.mX, mParticleDir.mZ );
		dirVec1.rotate( ParticleSystem::generateRandom( 0.01f, 2.0f ) );

		Vec3 particleVelocity( dirVec1.mX, dirVec1.mY, dirVec2.mY );
		particleVelocity *= ParticleSystem::generateRandom( 0.9f, 1.3f );

		spawnParticle( Vec3( 0.0f, 0.0f, 0.0f ), particleVelocity, 2.0f );
	}
}

void ParticleSparks::activate()
{
	// Call base object.
	ParticleSystem::activate();

	// Reset variables.
	mSpawnCounter = 0.0;
}

void ParticleSparks::deactivate()
{
	// Call the base object.
	ParticleSystem::deactivate();
}

void ParticleSparks::addedToParticleManager()
{
	// Call base object.
	ParticleSystem::addedToParticleManager();
}

void ParticleSparks::scheduleParticleSpawn( double timeStepSecs )
{
	mSpawnCounter -= timeStepSecs;
	while( mSpawnCounter < 0.0 && mParticlesLeftToSpawn > 0 )
	{
		Vec2 dirVec1( mParticleDir.mX, mParticleDir.mY );
		dirVec1.rotate( ParticleSystem::generateRandom( 0.1f, 2.0f ) );
		Vec2 dirVec2( mParticleDir.mX, mParticleDir.mZ );
		dirVec1.rotate( ParticleSystem::generateRandom( 0.01f, 2.0f ) );

		Vec3 particleVelocity( dirVec1.mX, dirVec1.mY, dirVec2.mY );
		particleVelocity *= ParticleSystem::generateRandom( 0.9f, 1.3f );

		spawnParticle( Vec3( 0.0f, 0.0f, 0.0f ), particleVelocity, 2.0f );
		
		mSpawnCounter += KAverageSpawnFrequency;
		mParticlesLeftToSpawn--;
	}
}

ParticleSparks::ParticleSparks( ParticleManager& manager,
	const Configuration& systemConfiguration,
	const ParticleMeshData& meshData ) :
	ParticleSystem( manager, systemConfiguration, meshData ),
	mParticlesLeftToSpawn( 0 ),
	mSpawnCounter( 0.0 )
{

}
