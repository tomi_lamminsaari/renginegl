
#include "particlesprinkler.h"
#include "../defines.h"

namespace
{
	/// <summary>Default spawn frequency is 100 particles per second.</summary>
	const double KAverageSpawnFrequency = 1.0 / 100;
}

ParticleSprinkler* ParticleSprinkler::createParticleSystem( ParticleManager& manager )
{
	ParticleSprinkler* particlesystem = nullptr;
	try
	{
		// Initialize the generic configuration of sprinkler particle system.
		auto lifetimePair = std::make_pair( 15.0f, 20.0f );
		ParticleSystem::Configuration configuration(
			200,
			Vec3( 0.0f, -100.0f, 0.0f ),
			KParticleSystemTypeSprinklers, lifetimePair );

		// Initialize the prototype of a particle.
		ParticleMeshData::ParticlePrototype proto( ParticleMeshData::ParticlePrototype::KRgbaAttributeBit );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( 0.0f, 0.2f, 0.0f ), Rgba( 40, 70, 180 ) ) );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( -0.2f, -0.2f, 0.0f ), Rgba( 40, 80, 180 ) ) );
		proto.addVertex( ParticleMeshData::Vertex( Vec3( 0.2f, -0.2f, 0.0f ), Rgba( 40, 80, 180 ) ) );
		proto.addFace( 0, 1, 2 );
		proto.addFace( 0, 2, 1 );
		ParticleMeshData pdm( proto, BuiltIn::MATERIAL_PARTICLESYSTEM );

		// Create and intialize the particle system.
		particlesystem = new ParticleSprinkler( manager, configuration, pdm );
	}
	catch( ... )
	{
		delete particlesystem;
		particlesystem = nullptr;
		throw;
	}

	// Returns the ParticleSparks object.
	return particlesystem;
}

ParticleSprinkler::ParticleSprinkler(
	ParticleManager& manager,
	const Configuration& systemConfiguration,
	const ParticleMeshData& meshData ) :
	ParticleSystem( manager, systemConfiguration, meshData ),
	mSpawnFrequency( 0.05 ),
	mSpawnCounter( 0.0 )
{

}

ParticleSprinkler::~ParticleSprinkler()
{

}

void ParticleSprinkler::activate()
{
	// Call the base implementation.
	ParticleSystem::activate();

	// Reset variables.
	mSpawnCounter = 0.0;
}

void ParticleSprinkler::deactivate()
{
	// Call the base implementation.
	ParticleSystem::deactivate();
}

void ParticleSprinkler::addedToParticleManager()
{
	// Call the base implementation.
	ParticleSystem::addedToParticleManager();

	// Randomize the particle orientations.
	for( unsigned int i = 0; i < mParticles.size(); ++i )
	{
		auto quat = Quaternion::newAxisRotation( ParticleSystem::generateRandom( 0.0f, 359.9f ), Vec3( 0.0f, 1.0f, 0.0f ) );
		mParticles[ i ].setRotation( quat );
	}
}

void ParticleSprinkler::scheduleParticleSpawn( double timeStepSecs )
{
	mSpawnCounter -= timeStepSecs;
	while( mSpawnCounter < 0.0 )
	{
		// Define the position of the particle.
		float posX = ParticleSystem::generateRandom( -1.0f, 1.0f );
		float posZ = ParticleSystem::generateRandom( -1.0f, 1.0f );
		Vec3 particlePos( posX, 0.0f, posZ );

		// Define initial direction.
		float dirX = ParticleSystem::generateRandom( 0.5, 1.0f );
		float dirY = 12.0f;
		float dirZ = ParticleSystem::generateRandom( 0.5, 1.0f );
		Vec3 particleVel( dirX, dirY, dirZ );
//		particleVel.normalize();
//		particleVel *= 0.001f;

		// Spawn particles as much as spawn frequency requires.
		int index = spawnParticle( particlePos, particleVel, 1.5 );

		// Update counter and spawn new particle if schedule requires that.
		mSpawnCounter += KAverageSpawnFrequency;
	}
}
