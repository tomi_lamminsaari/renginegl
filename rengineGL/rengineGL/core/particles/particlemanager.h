
#pragma once

#include <chrono>
#include <vector>
#include <memory>
#include "particledefines.h"
#include "particlesystem.h"
#include "../managerbase.h"

// Forward declarations.
class Node;
class RenderCore;

/// <summary>
/// Takes care of managing all the particle systems that have been created.
/// <para>
/// ParticleManager creates multiple particle systems and places them to the pool of free particle
/// systems. When new particle system is needed and we "create" new particle system, the
/// ParticleManager will look from free particle systems' pool first.
/// </para>
/// </summary>
class ParticleManager : public ManagerBase
{
public:
	/// <summary>
	/// Constructs the particle manager.
	/// </summary>
	/// <param name="core">Render engine's main object.</param>
	ParticleManager( RenderCore& core );

	/// <summary>
	/// Destructor.
	/// </summary>
	~ParticleManager();

	ParticleManager( const ParticleManager& obj ) = delete;
	ParticleManager& operator=( const ParticleManager& obj ) = delete;

public:

	/// <summary>
	/// Initializes all particle systems. Creates the particle systems and store them
	/// to the collection of free particle systems.
	/// </summary>
	void intializeParticleSystemsPool();

	/// <summary>
	/// Updates the particle systems. Moves the individual particles.
	/// </summary>
	/// <param name="timeStepSecs">Time that has passed since previous update.</param>
	void update( double timeStepSecs );

	/// <summary>
	/// Gets next free particle system from the pool of particle systems. It is not
	/// guaranteed that there are always free particle systems so this could return
	/// nullptr.
	/// </summary>
	/// <param name="systemType">Type of particle system we want to create.</param>
	/// <returns>
	/// Pointer to free particle system. Nullptr if there are no more free particle systems available.
	/// </returns>
	ParticleSystem* getFreeParticleSystem( ParticleSystemType systemType );

	/// <summary>
	/// Attempts to remove particle system from given list and return it.
	/// </summary>
	/// <param name="listOfPartSystem">
	/// Reference to particle system list from where we take the next particle system. If there are
	/// particle systems in this list, the length of this list will get shorter as the returned
	/// particle system has been removed from it.
	/// </param>
	/// <returns>
	/// Returns the particle system pointer that was removed from the list. Nullptr if
	/// particle system list was already empty.
	/// </returns>
	static ParticleSystem* removeFirstParticleSystemFromList(
		std::vector< ParticleSystem* >& listOfPartSystems );

	/// <summary>
	/// Checks the type of particle system and appends it to the correct free particle systems list.
	/// </summary>
	/// <param name="partSys">Pointer to the particle system we want to add particle systems list.</param>
	void appendParticleSystemToRightList( ParticleSystem* partSys );

	/// <summary>
	/// Creates new particle system of given type. The ownership of the created particle system belongs
	/// to the caller.
	/// </summary>
	/// <param name="systemType">Type of particle system to create.</param>
	/// <returns>
	/// Pointer to created particle system. Ownership of this object is transferred to the caller.
	/// </returns>
	ParticleSystem* createParticleSystem( ParticleSystemType systemType );

public:  // from ManagerBase

	/// <summary>
	/// From ManagerBase, cleanup()
	/// </summary>
	virtual void cleanup() override;

private:
	/// <summary>The render engine's main object.</summary>
	RenderCore& mCore;

	/// <summary>Master array of all created particle systems. This owns the particle system objects.</summary>
	std::vector< ParticleSystem* > mParticleSystems;

	/// <summary>List of currently free sprinkler particle systems. Does not own the particle systems. Raw pointers for performance reasons.</summary>
	std::vector< ParticleSystem* > mFreeParticleSprinklers;

	/// <summary>List of currently free sparks particle systems. Does not own the particle systems. Raw pointers for performance reasons.</summary>
	std::vector< ParticleSystem* >mFreeParticleSparks;

	/// <summary>List of currently active particle systems. Does not own the particle systems. Raw pointers for performance reasons.</summary>
	std::vector< ParticleSystem* > mActiveParticleSystems;

	/// <summary>Member where we keep track of previous update round duration.</summary>
	int64_t mUpdateDuration;
};
