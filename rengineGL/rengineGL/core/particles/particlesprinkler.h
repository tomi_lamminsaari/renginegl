
#pragma once

#include "particlesystem.h"

/// <summary>
/// Particle system that represents sparks.
/// </summary>
class ParticleSprinkler : public ParticleSystem
{
public:
	/// <summary>
	/// Creates new ParticleSparks object.
	/// </summary>
	/// <param name="manager">The owning particle manager.</param>
	/// <returns>
	/// Pointer to created ParticleSparks particle system. The ownership is transferred to the caller.
	/// </returns>
	static ParticleSprinkler* createParticleSystem( ParticleManager& manager );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~ParticleSprinkler();

	ParticleSprinkler( const ParticleSprinkler& obj ) = delete;
	ParticleSprinkler& operator=( const ParticleSprinkler& obj ) = delete;

private:
	/// <summary>
	/// Constructs new particle system.
	/// </summary>
	/// <param name="manager">The owning particle manager.</param>
	/// <param name="systemConfiguration">The general parameters of the particle system.</param>
	/// <param name="meshData">The mesh and particle prototype information of the particle system.</param>
	ParticleSprinkler(
		ParticleManager& manager,
		const Configuration& systemConfiguration,
		const ParticleMeshData& meshData );

public:  // from ParticleSystem
	/// <summary>
	/// Override ParticleSystem::active().
	/// </summary>
	virtual void activate() override;

	/// <summary>
	/// Override deactivate().
	/// </summary>
	virtual void deactivate() override;

	/// <summary>
	/// Called when particle system has been created and added to particle manager. We will
	/// set the initial orientation for the actual particles.
	/// <summary>
	virtual void addedToParticleManager() override;

	/// <summary>
	/// Override scheduleParticleSpawn().
	/// </summary>
	virtual void scheduleParticleSpawn( double timeStepSecs ) override;

private:
	/// <summary>Frequency how often new particle gets spawned.</param>
	double mSpawnFrequency;

	/// <summary>Current counter value that defines when new particle gets spawned.</summary>
	double mSpawnCounter;
};
