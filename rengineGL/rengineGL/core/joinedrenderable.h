
#pragma once

#include "renderable.h"

/// <summary>
/// JoinedRenderable is a renderable that can merge the meshes and models from other renderables
/// and render them all in one render call. Basically this is a batch renderable of its own.
/// </summary>
class JoinedRenderable : public Renderable
{
public:

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="core">Reference to render engine's core object.</param>
	JoinedRenderable( RenderCore& core );

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~JoinedRenderable();

	JoinedRenderable( const JoinedRenderable& obj ) = delete;
	JoinedRenderable& operator=( const JoinedRenderable& obj ) = delete;

public:
	/// <summary>
	/// Merges the given renderable to this renderable.
	/// </summary>
	/// <param name="renderable">Pointer to renderable to merge. This call will hide this renderable.</param>
	void joinWithRenderable( std::shared_ptr<Renderable> renderable );
};