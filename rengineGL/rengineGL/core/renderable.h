
#pragma once

#include "scenenodeobject.h"

// Forward declarations.
class Camera;
class Mesh;
class Material;
class Model;
class Node;
class RenderCore;
class RenderPassInfo;
class SceneGlobalRenderPassInfo;
class ShaderProgram;

/// <summary>
/// Renderable is an object in scene that can be rendered.
///
/// The Renderable can be added to the scene. It contains the information about the actual mesh and
/// materials that will be used in rendering.
/// </summary>
class Renderable : public SceneNodeObject
	{
	public:
		/// <summary>
		/// Possible types of renderables.
		/// </summary>
		enum class Type
			{
			/// <summary>Normal renderable. Rendered immediately with individial draw calls.</summary>
			NormalRenderable,

			/// <summary>Batch renderable. Included to render batch to minimize the needed draw calls.</summary>
			BatchRendereable
			};

	public:
		Renderable( RenderCore &core );
		virtual ~Renderable();
		Renderable(const Renderable &obj) = delete;
		Renderable& operator = (const Renderable &obj) = delete;

	public:
		/// <summary>
		/// Sets the ID of this renderable.
		/// </summary>
		/// <param name="id">The ID of this renderbale.</param>
		void setId( const std::string& id );

		/// <summary>
		/// Returns the ID of this renderable.
		/// </summary>
		/// <returns>ID of this renderable.</returns>
		const std::string& getId() const;

		/// <summary>
		/// Sets this renderable to use certain mesh and material in rendering.
		/// </summary>
		/// <param name="mesh">Pointer to mesh that will be rendered.</param>
		/// <param name="material">The material that will be used for rendering the mesh.</param>
		void setGeometryObjects( std::shared_ptr<Mesh> mesh, std::shared_ptr<Material> material );

		/// <summary>
		/// Sets the mesh this renderable uses.
		/// </summary>
		/// <param name="meshId">ID of the mesh that will be used for rendering. The MeshManager should contain a mesh with this ID.</param>
		/// <param name="materialId">
		/// ID of the material that will be used in rendering. The MaterialManager should contain a material
		/// with this ID.
		/// </param>
		void setGeometryIds( const std::string &meshId, const std::string &materialId );

		/// <summary>
		/// Sets the model this renderable will use in rendering.
		/// </summary>
		/// <param name="modelId">ID of the model.</param>
		void setModel( const std::string &modelId );

		/// <summary>
		/// Returns the model that describes the geometry and material this renderable is using.
		/// </summary>
		/// <returns>Pointer to Model object.</returns>
		virtual std::shared_ptr<Model> getModel() const;

		/// <summary>
		/// Returns the type of renderable this is.
		/// </summary>
		/// <returns>Returns the type of renderable.</returns>
		virtual Renderable::Type getType() const;

	public:  // Functions for render-time behavior. The Scene-object should be only one using these.
		/// <summary>
		/// Called before this renderable will be rendered. Determines object visibility
		/// in front of camera.
		/// </summary>
		/// <param name="camera">The camera that will be used in rendering.</param>
		virtual void prepareRender( Camera& camera );

		/// <summary>
		/// Calling this will use OpenGL to render the renderable's meshes with textures and shaders defined
		/// by the renderable's material.
		/// </summary>
		/// <param name="camera">The camera used in rendering.</param>
		/// <param name="globalRenderPassInfo">
		/// Contains global information about the rendering environment. There are
		/// ambient and diffuse light positions, etc.
		/// </param>
		virtual void render( Camera& camera, SceneGlobalRenderPassInfo *globalRenderPassInfo );

		/// <summary>
		/// Renders this renderable without applying the material and shader. This function expects that
		/// shader program has been activate already. This function can still update uniforms and bind
		/// new textures to texture units that are required for rendering the renderable.
		/// </summary>
		/// <param name="camera">Current camera.</param>
		/// <param name="currentProgram">Currently activated shader program.</param>
		/// <param name="globalRenderPassInfo">
		/// Contains global information about the rendering environment. There are
		/// ambient and diffuse light positions, etc.
		/// </param>
		virtual void renderSimplifiedMaterial(
			const Camera& camera,
			ShaderProgram& currentProgram,
			SceneGlobalRenderPassInfo* globalRenderPassInfo );

		/// <summary>
		/// Sets the renderable visibility in camera view. You can override the
		/// visibility with this method.
		/// </summary>
		/// <param name="visibility">Defines if this renderable is visible in camera.</param>
		void setVisibilityInCamera( bool visibility );

	protected:
		/// <summary>
		/// Checks if this renderable can handle the given mesh object. Some renderables may have limitations that require
		/// certain properties from the meshes. The setGeometryObjects(), setGeometryIds() and setModel() functions call this
		/// to validate the meshes.
		/// </summary>
		/// <param name="mesh">Pointer to the mesh.</param>
		/// <returns>Returns 'true' if we can handle the given mesh object.</returns>
		virtual bool validateMesh( std::shared_ptr<Mesh>& mesh ) const;

		void batchRender();
		void immediateRender( std::shared_ptr<Camera>& camera, SceneGlobalRenderPassInfo* globalRenderPassInfo );

	protected:
		/// <summary>Reference to rendering engine's core object.</summary>
		RenderCore &mCore;

		/// <summary>ID of this renderable.</summary>
		std::string mId;

		/// <summary>The model that is associated with this renderable.</summary>
		std::shared_ptr<Model> mModel;

		/// <summary>True, is the model managed by this renderable only. False if model is managed by the ModelManager</summary>
		bool mOwnModel;

		/// <summary>Actual type of renderable.</summary>
		Renderable::Type mType;

		/// <summary>Set before actual rendering if this renderable is actually outside of renderable view.</summary>
		bool mIsOutsideCameraView;
	};
