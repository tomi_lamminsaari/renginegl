
#include "../math/matrix4x4.h"
#include "../math/vec3.h"
#include "../utils/rgba.h"
#include "material.h"
#include "rendercore.h"
#include "shadermanager.h"
#include "shaderprogram.h"

// Definitions of common uniforms that are used in shader applications.
const std::string ShaderProgram::KUniformName_M = "uM";
const std::string ShaderProgram::KUniformName_MV = "uMV";
const std::string ShaderProgram::KUniformName_MVP = "uMVP";
const std::string ShaderProgram::KUniformName_LightMVP = "uLightSourceMVP";
const std::string ShaderProgram::KUniformName_Color = "uSolidColor";
const std::string ShaderProgram::KUniformName_EyePos = "uEyePos";
const std::string ShaderProgram::KUniformName_Texture0 = "uTexture0";
const std::string ShaderProgram::KUniformName_Texture1 = "uTexture1";
const std::string ShaderProgram::KUniformName_Texture2 = "uTexture2";
const std::string ShaderProgram::KUniformName_Texture3 = "uTexture3";
const std::string ShaderProgram::KUniformName_Texture4 = "uTexture4";
const std::string ShaderProgram::KUniformName_Texture5 = "uTexture5";
const std::string ShaderProgram::KUniformName_Texture6 = "uTexture6";
const std::string ShaderProgram::KUniformName_Texture7 = "uTexture7";
const std::string ShaderProgram::KUniformName_Texture8 = "uTexture8";
const std::string ShaderProgram::KUniformName_Texture9 = "uTexture9";
const std::string ShaderProgram::KUniformName_Texture10 = "uTexture10";
const std::string ShaderProgram::KUniformName_Texture11 = "uTexture11";
const std::string ShaderProgram::KUniformName_Texture12 = "uTexture12";
const std::string ShaderProgram::KUniformName_Texture13 = "uTexture13";
const std::string ShaderProgram::KUniformName_Texture14 = "uTexture14";
const std::string ShaderProgram::KUniformName_Texture15 = "uTexture15";
const std::string ShaderProgram::KUniformName_Texture0TileCount = "uTexture0TileCount";
const std::string ShaderProgram::KUniformName_Texture1TileCount = "uTexture1TileCount";
const std::string ShaderProgram::KUniformName_Texture2TileCount = "uTexture2TileCount";
const std::string ShaderProgram::KUniformName_Texture3TileCount = "uTexture3TileCount";
const std::string ShaderProgram::KUniformName_TextureSpecular = "uTextureSpecular";
const std::string ShaderProgram::KUniformName_TextureNormals = "uTextureNormals";
const std::string ShaderProgram::KUniformName_TerrainTex0 = "uTerrainTex0";
const std::string ShaderProgram::KUniformName_TerrainTex1 = "uTerrainTex1";
const std::string ShaderProgram::KUniformName_TerrainTex2 = "uTerrainTex2";
const std::string ShaderProgram::KUniformName_TerrainTex3 = "uTerrainTex3";
const std::string ShaderProgram::KUniformName_BlendMap = "uBlendMap";
const std::string ShaderProgram::KUniformName_ShadowMap = "uShadowMap";
const std::string ShaderProgram::KUniformName_DiffuseLightDir = "uDiffuseLightDir";
const std::string ShaderProgram::KUniformName_DiffuseLightPos = "uDiffuseLightPos";
const std::string ShaderProgram::KUniformName_DiffuseLightColor = "uDiffuseLightColor";
const std::string ShaderProgram::KUniformName_DiffuseLightIntensity = "uDiffuseLightIntensity";
const std::string ShaderProgram::KUniformName_DiffuseLightArray = "uDiffuseLightArray";
const std::string ShaderProgram::KUniformName_AmbientLightColor = "uAmbientColor";
const std::string ShaderProgram::KUniformName_AmbientLightIntensity = "uAmbientIntensity";
const std::string ShaderProgram::KUniformName_SpecularColor = "uSpecularColor";
const std::string ShaderProgram::KUniformName_SpecularExponent = "uSpecularExponent";
const std::string ShaderProgram::KUniformName_SpecularEnabled = "uSpecularEnabled";
const std::string ShaderProgram::KUniformName_OffsetPosition = "uOffsetPosition";
const std::string ShaderProgram::KUniformName_LightsourcesArray = "uLightSources";


ShaderProgram::ShaderProgram( ShaderManager &shaderManager ) :
	mVertexShaderProgram( 0 ),
	mFragmentShaderProgram( 0 ),
	mShaderProgram( 0 ),
	mMustDoCleanup( false ),
	mProgramIsReady( false ),
	mProgramActivated( false ),
	mShaderManager( shaderManager )
{

}

ShaderProgram::~ShaderProgram()
{
	// Destroy shader by removing its GPU resources.
	removeFromGPU();
}

void ShaderProgram::setProgramId( const std::string &id )
{
	mId = id;
}

const std::string& ShaderProgram::getProgramId() const
{
	return mId;
}

void ShaderProgram::setShaderFilePath( const std::string& filepath )
{
	mShaderFileName = filepath;
}

const std::string& ShaderProgram::getShaderFilePath() const
{
	return mShaderFileName;
}

void ShaderProgram::setSourceCodes( const std::string &vertexShaderCode, const std::string &fragmentShaderCode )
{
	mVertexShaderCode = vertexShaderCode;
	mFragmentShaderCode = fragmentShaderCode;
	mProgramIsReady = false;
}

void ShaderProgram::setSingleSourceCode( const std::string &shaderSource )
{
	// The VERTEX_SHADER and FRAGMENT_SHADER definitions should be after the #version line.
	size_t versionLocation = shaderSource.find( "#version" );
	size_t insertPosition = 0;
	if( versionLocation >= 0 )
	{
		// The '#version' was found. Find the next new line and add the '#define' parts after the new line.
		insertPosition = versionLocation;
		while( insertPosition < shaderSource.length() )
		{
			if( shaderSource[ insertPosition ] == '\n' && insertPosition < shaderSource.length() )
			{
				// New line symbol found.
				insertPosition += 1;
				break;
			}

			insertPosition++;
		}
	}
	else
	{
		// '#version' was not found. It's ok to add the defines to the beginning.
		insertPosition = 0;
	}
	mVertexShaderCode = shaderSource;
	mVertexShaderCode.insert( insertPosition, "#define VERTEX_SHADER\n" );

	mFragmentShaderCode = shaderSource;
	mFragmentShaderCode.insert( insertPosition, "#define FRAGMENT_SHADER" );

	mProgramIsReady = false;
}

bool ShaderProgram::buildProgram()
{
	// Make sure the previous compiled program gets destroyed.
	removeFromGPU();

	// Compile the vertex and fragment shader and link them to a single program.
	if( mVertexShaderProgram == 0 && mFragmentShaderProgram == 0 && mShaderProgram == 0 )
	{
		// Make sure we have shader codes.
		if( mVertexShaderCode.size() == 0 || mFragmentShaderCode.size() == 0 )
		{
			mErrorMessage = "Vertex or Fragment shader code was not provided.";
			return false;
		}

		mMustDoCleanup = true;

		// Create vertex shader programs.
		GLint shaderStatus = 0;
		mVertexShaderProgram = glCreateShader( GL_VERTEX_SHADER );
		GLASSERT_EMPTY();
		mFragmentShaderProgram = glCreateShader( GL_FRAGMENT_SHADER );
		GLASSERT_EMPTY();
		if( mVertexShaderProgram == 0 )
		{
			mErrorMessage = "Creation of vertex shader failed.";
			return false;
		}
		if( mFragmentShaderProgram == 0 )
		{
			mErrorMessage = "Creation of fragment shader failed.";
			return false;
		}

		// Compile the vertex shader.
		bool success = compileShader( mVertexShaderProgram, mVertexShaderCode, mErrorMessage );
		GLASSERT_EMPTY();
		if( !success )
		{
			return false;
		}

		// Compile the fragment shader.
		success = compileShader( mFragmentShaderProgram, mFragmentShaderCode, mErrorMessage );
		GLASSERT_EMPTY();
		if( !success )
		{
			return false;
		}

		// Link the programs together.
		mShaderProgram = glCreateProgram();
		GLASSERT_EMPTY();
		if( mShaderProgram == 0 )
		{
			mErrorMessage = "Creation of shader program failed.";
			return false;
		}
		GLASSERT( glAttachShader( mShaderProgram, mVertexShaderProgram ) );
		GLASSERT( glAttachShader( mShaderProgram, mFragmentShaderProgram ) );
		GLASSERT( glLinkProgram( mShaderProgram ) );

		// Get link status.
		GLint programStatus = 0;
		glGetProgramiv( mShaderProgram, GL_LINK_STATUS, &programStatus );
		if( programStatus == GL_FALSE )
		{
			// Linking failed. Get the link error.
			char linkErrors[ 2048 ] = { 0 };
			GLsizei errorMessageLength = 0;
			glGetProgramInfoLog( mShaderProgram, sizeof( linkErrors ), &errorMessageLength, linkErrors );
			mErrorMessage = linkErrors;
			return false;
		}

		// Get the uniform parameter locations.
		GLASSERT( glUseProgram( mShaderProgram ) );
		findUniformLocations();
		GLASSERT( glUseProgram( 0 ) );

		// Program is ready to be used.
		mProgramIsReady = true;
		mMustDoCleanup = false;
	}
	return true;
}

const std::string &ShaderProgram::getBuildError() const
{
	return mErrorMessage;
}

bool ShaderProgram::isReady() const
{
	return mProgramIsReady;
}

void ShaderProgram::activateProgram() const
{
	// Activate the shader program unless it's already in use.
	GLint currentProgram = 0;
	GLASSERT( glGetIntegerv( GL_CURRENT_PROGRAM, &currentProgram ) );
	if( currentProgram != mShaderProgram )
	{
		// Activate the program.
		GLASSERT( glUseProgram( mShaderProgram ) );

		// Update the statistics.
		mShaderManager.getCore().getActiveStats().increaseProgramChangeCount();
		mShaderManager.recordShaderUsage( this );
	}
}

void ShaderProgram::deactivateProgram() const
{
	// Optimize rendering by not deactivating the program.
}

void ShaderProgram::uploadToGPU()
{
	// Compile and build program only if there are changes in its source
	// from last time.
	if( !mProgramIsReady )
	{
		bool buildSuccessful = buildProgram();

		// Attempt to show the message.
		if( buildSuccessful == false )
			mShaderManager.getCore().showMessage( mErrorMessage );
		assert( buildSuccessful );
	}
}

void ShaderProgram::removeFromGPU()
{
	if( mShaderProgram != 0 )
	{
		GLASSERT( glDeleteProgram( mShaderProgram ) );
		mShaderProgram = 0;
	}
	if( mFragmentShaderProgram != 0 )
	{
		GLASSERT( glDeleteShader( mFragmentShaderProgram ) );
		mFragmentShaderProgram = 0;
	}
	if( mVertexShaderProgram != 0 )
	{
		GLASSERT( glDeleteShader( mVertexShaderProgram ) );
		mVertexShaderProgram = 0;
	}
	mMustDoCleanup = false;
	mProgramIsReady = false;
}

bool ShaderProgram::hasUniform( const std::string &uniformName )
{
	// If uniform location is other than -1, it means the uniform exists in the shader program.
	return getUniformLocation( uniformName ) != -1;
}

void ShaderProgram::setCameraUniforms(
	const Vec3& eyePos,
	const Matrix4x4* modelViewProjectionMat,
	const Matrix4x4* modelViewMat,
	const Matrix4x4* modelMat )
{
	// Set the model matrix uniform.
	if( modelMat )
	{
		GLint loc = getUniformLocation( KUniformName_M );
		if( loc != -1 )
		{
			GLASSERT( glUniformMatrix4fv( loc, 1, GL_TRUE, modelMat->mData ) );
		}
	}

	// Set model-view matrix uniform.
	if( modelViewMat )
	{
		GLint loc = getUniformLocation( KUniformName_MV );
		if( loc != -1 )
		{
			GLASSERT( glUniformMatrix4fv( loc, 1, GL_TRUE, modelViewMat->mData ) );
		}
	}

	// Set model-view-projection matrix uniform.
	if( modelViewProjectionMat )
	{
		GLint loc = getUniformLocation( KUniformName_MVP );
		if( loc != -1 )
		{
			GLASSERT( glUniformMatrix4fv( loc, 1, GL_TRUE, modelViewProjectionMat->mData ) );
		}
	}

	// Set camera position matrix.
	GLint loc = getUniformLocation( KUniformName_EyePos );
	if( loc != -1 )
	{
		GLASSERT( glUniform3f( loc, eyePos.mX, eyePos.mY, eyePos.mZ ) );
	}
}

void ShaderProgram::setUniformMat4x4( const std::string &uniformName, const Matrix4x4 &mat4 )
{
	GLint loc = getUniformLocation( uniformName );
	if( loc != -1 )
	{
		Matrix4x4 tm = mat4.transposed();
		GLASSERT( glUniformMatrix4fv( loc, 1, GL_FALSE, tm.mData ) );
	}
}

void ShaderProgram::setUniformVec3( const std::string &uniformName, const Vec3 &vec )
{
	GLint loc = getUniformLocation( uniformName );
	if( loc != -1 )
	{
		GLASSERT( glUniform3f( loc, vec.mX, vec.mY, vec.mZ ) );
	}
}

void ShaderProgram::setUniformVec4(const std::string &uniformName, const Vec4 &vec)
	{
	GLint loc = getUniformLocation(uniformName);
	if (loc != -1)
		{
		GLASSERT( glUniform4f(loc, vec.mX, vec.mY, vec.mZ, vec.mW) );
		}
	}

void ShaderProgram::setUniformFloat(
	const std::string &uniformName,
	GLfloat floatValue )
{
	GLint loc = getUniformLocation( uniformName );
	if( loc != -1 )
	{
		GLASSERT( glUniform1f( loc, floatValue ) );
	}
}

void ShaderProgram::setUniformFloatArray(
	const std::string &uniformName,
	const GLfloat *floatArrayPtr,
	GLsizei numOfFloats )
{
	GLint loc = getUniformLocation( uniformName );
	if( loc != -1 )
	{
		GLASSERT( glUniform1fv( loc, numOfFloats, floatArrayPtr ) );
	}
}

void ShaderProgram::setUniformInt(
	const std::string &uniformName,
	GLint intValue )
{
	GLint loc = getUniformLocation( uniformName );
	if( loc != -1 )
	{
		GLASSERT( glUniform1i( loc, intValue ) );
	}
}

void ShaderProgram::setAmbientLight( const Rgba &lightColor, GLfloat lightIntensity )
{
	setUniformVec4( KUniformName_AmbientLightColor, lightColor.asVec4() );
	setUniformFloat( KUniformName_AmbientLightIntensity, lightIntensity );
}

void ShaderProgram::setDiffuseLight(
	const Rgba &lightColor,
	const Vec3 &lightPosition,
	GLfloat lightIntensity )
{
	setUniformVec4( KUniformName_DiffuseLightColor, lightColor.asVec4() );
	setUniformVec3( KUniformName_DiffuseLightPos, lightPosition );
	setUniformFloat( KUniformName_DiffuseLightIntensity, lightIntensity );
}

void ShaderProgram::setDiffuseLightArray(
	const GLStructs::GLPointLightBuffer &diffuseLightBuffer )
{
	setUniformFloatArray( KUniformName_DiffuseLightArray, diffuseLightBuffer.mLightBuffer, diffuseLightBuffer.getFloatCount() );
}

void ShaderProgram::setSpecularColor(
	const Rgba &specColor,
	GLfloat specExponent,
	GLint specEnabled )
{
	setUniformInt( KUniformName_SpecularEnabled, specEnabled );
	setUniformVec4( KUniformName_SpecularColor, specColor.asVec4() );
	setUniformFloat( KUniformName_SpecularExponent, specExponent );
}

void ShaderProgram::setShadowMappingLightMatrix( const Matrix4x4 &lightMVP )
{
	setUniformMat4x4( KUniformName_LightMVP, lightMVP );
}

#if 0
void ShaderProgram::bindShadowMapTexture( Texture *shadowTexture )
{
	// Set the shadow map if this shader has shadow map sampler uniform.
	GLint loc = getStoredUniformLocation( KUniformName_ShadowMap );
	if( loc != -1 )
	{
		// This shader uses shadowmap sampler.
		GLASSERT( glActiveTexture( glTextureUnit ) );
		GLASSERT( glBindTexture( GL_TEXTURE_2D,
			mShadowFBO->getDepthAttachmentTexture()->getOpenGLTextureObject() ) );
		GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE ) );
		GLASSERT( glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE ) );
	}
}
#endif

bool ShaderProgram::compileShader( GLuint shaderObject, const std::string &shaderSource, std::string &compileErrorOut )
{
	bool compileSuccessful = false;

	// Compile the shader code.
	const char *sourcePtr = shaderSource.c_str();
	GLASSERT( glShaderSource( shaderObject, 1, &sourcePtr, nullptr ) );
	GLASSERT( glCompileShader( shaderObject ) );

	// Check for compilation errors.
	GLint shaderStatus = 0;
	glGetShaderiv( shaderObject, GL_COMPILE_STATUS, &shaderStatus );
	if( shaderStatus == GL_FALSE )
	{
		// Get the compilation error.
		char errorMessage[ 2048 ] = { 0 };
		GLsizei returnedErrorLength = 0;
		glGetShaderInfoLog( shaderObject, sizeof( errorMessage ), &returnedErrorLength, errorMessage );
		compileErrorOut = errorMessage;
		compileSuccessful = false;

	}
	else
	{
		compileSuccessful = true;
	}
	return compileSuccessful;
}

void ShaderProgram::findUniformLocations()
{
	// Bind the texture units and uniform locations.
	bindUniformLocationsToTextureUnits();
}

GLint ShaderProgram::getUniformLocation( const std::string& uniformName )
{
	// First look from our location cache if this uniform location has already been resolved.
	auto iter = mUniforms.find( uniformName );
	if( iter != mUniforms.end() )
		return iter->second;

	// Uniform has not been stored to our local location cache yet. Find it from the shader program.
	GLint loc = glGetUniformLocation( mShaderProgram, uniformName.c_str() );
	if( loc != -1 )
	{
		// Uniform was found. Store it to our cache.
		mUniforms[ uniformName ] = loc;
	}
	return loc;
}

void ShaderProgram::bindUniformLocationsToTextureUnits()
{
	GLint loc = -1;

	// Bind normal texture units and texture uniforms.
	loc = getUniformLocation( KUniformName_ShadowMap );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KShadowMapTextureIndex ) );

	loc = getUniformLocation( KUniformName_Texture0 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 0 ) );

	loc = getUniformLocation( KUniformName_Texture1 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 1 ) );

	loc = getUniformLocation( KUniformName_Texture2 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 2 ) );

	loc = getUniformLocation( KUniformName_Texture3 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 3 ) );

	loc = getUniformLocation( KUniformName_Texture4 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 4 ) );

	loc = getUniformLocation( KUniformName_Texture5 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 5 ) );

	loc = getUniformLocation( KUniformName_Texture6 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 6 ) );

	loc = getUniformLocation( KUniformName_Texture7 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 7 ) );

	loc = getUniformLocation( KUniformName_Texture8 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 8 ) );

	loc = getUniformLocation( KUniformName_Texture9 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 9 ) );

	loc = getUniformLocation( KUniformName_Texture10 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 10 ) );

	loc = getUniformLocation( KUniformName_Texture11 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 11 ) );

	loc = getUniformLocation( KUniformName_Texture12 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 12 ) );

	loc = getUniformLocation( KUniformName_Texture13 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 13 ) );

	loc = getUniformLocation( KUniformName_Texture14 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 14 ) );

	loc = getUniformLocation( KUniformName_Texture15 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 15 ) );

	// Bind terrain texture units and texture uniforms.
	loc = getUniformLocation( KUniformName_TerrainTex0 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 0 ) );

	loc = getUniformLocation( KUniformName_TerrainTex1 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 1 ) );

	loc = getUniformLocation( KUniformName_TerrainTex2 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 2 ) );

	loc = getUniformLocation( KUniformName_TerrainTex3 );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KCustomTextureIndex0 + 3 ) );

	loc = getUniformLocation( KUniformName_BlendMap );
	if( loc != -1 )
		GLASSERT( glUniform1i( loc, Material::KBlendMapTextureIndex ) );
}
