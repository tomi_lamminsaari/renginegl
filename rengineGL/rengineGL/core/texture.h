
#pragma once

#include <map>
#include <memory>
#include <string>
#include <GL/glew.h>
#include "textureparameters.h"
#include "../utils/rgba.h"

// Forward declarations.
class TextureManager;
struct SDL_Surface;

/// <summary>
/// Texture-class represents an texture bitmap that can be created, loaded from file
/// and that also manages the OpenGL resources reserved for the texture.
/// </summary>
class Texture : public std::enable_shared_from_this< Texture >
{
public:
	/// <summary>Texture wrapping modes.</summary>
	enum class Wrap
	{
		Repeat,
		MirroredRepeat,
		ClampToEdge,
		ClampToBorder
	};

	/// <summary>
	/// Constructor.
	/// </summary>
	/// <param name="texman">Reference to the texture manager.</param>
	Texture( TextureManager &texman );
	~Texture();
	Texture( const Texture &obj ) = delete;
	Texture& operator = ( const Texture &obj ) = delete;

public:

	/// <summary>
	/// Returns the ID of this texture.
	///  </summary>
	/// <returns>ID of this texture.</returns>
	const std::string& getId() const;

	/// <summary>
	/// Sets the image file path that will be loaded to this texture.
	/// </summary>
	/// <param name="texturePath">File path of the texture file.</param>
	void setTextureFilePath( const std::string &texturePath );

	/// <summary>
	/// Returns the OpenGL texture object ID.
	/// </summary>
	/// <returns>OpenGL texture object identifier.</returns>
	GLuint getOpenGLTextureObject() const;

	/// <summary>
	/// Binds this texture to a OpenGL texture unit.
	/// </summary>
	/// <param name="textureSlotNumber">The OpenGL texture unit where to bind this texture.</param>
	void activateTexture( size_t textureSlotNumber );

	/// <summary>
	/// Sets the texture's wrap mode.
	/// </summary>
	/// <param name="horizontalWrap">Defines how texture is wrapped horizontally.</param>
	/// <param name="verticalWrap">Defines how texture is wrapped vertically.</param>
	void setWrapMode( Wrap horizontalWrap, Wrap verticalWrap );

	/// <summary>
	/// Enables or disables the mip-maps of the texture. Changing this causes the
	/// texture file to be loaded again so it is very heavy operation.
	/// </summary>
	/// <param name="mipmaps">Pass 'true' to enable the mip-maps.</param>
	void setMipMaps( bool mipmaps );

	/// <summary>
	/// Sets the texture's border color. This is used if Wrap::ClampToBorder wrap mode is being used.
	/// </summary>
	/// <param name="borderColor">The border color.</param>
	void setBorderColor( const Rgba& borderColor );

	/// <summary>
	/// Tells if this texture is using mip-maps.
	/// </summary>
	/// <returns>Returns 'true' if mipmaps are enabled.</returns>
	bool isUsingMipMaps() const;

	/// <summary>
	/// Allocates the GPU resources for the texture and initializes it with given set of
	/// parameters. Does allocate the buffer with specific size.
	/// </summary>
	/// <param name="textureParams">
	/// Collection of OpenGL texture parameters that will be applied to the new texture.
	/// </param>
	bool initializeWithParameters( const TextureParameters &textureParams );

	/// <summary>
	/// Allocates the GPU resources for this texture and uploads its data there.
	/// </summary>
	/// <returns>Returns 0 if successful. Otherwise of the rendering engine error codeds.</returns>
	/// <exception cref="RengineError">Thrown with OpenGL error code if texture uploading fails.</exception>
	void uploadToGPU();

	/// <summary>
	/// Releases the GPU resources allocated for this texture.
	/// </summary>
	void removeFromGPU();

	/// <summary>
	/// Returns the SDL_Surface that has been used for loading/creating the actual OpenGL texture.
	/// </summary>
	/// <returns>Pointer to SDL_Surface</returns>
	SDL_Surface* getSDLBitmap() const;

private:
	/// <summary>
	/// Sets the ID of this texture.
	/// </summary>
	/// <param name="id">The ID of this texture.</param>
	void setId( const std::string &id );

	void createOpenGLTexture();

	/// <summary>
	/// Returns the texture's wrap mode as OpenGL constant.
	/// </summary>
	/// <param name="wrapMode">The wrap mode.</param>
	/// <returns>Wrap mode as OpenGL constant value.</returns>
	static GLint getWrapModeOpenGL( Wrap wrapMode );

private:

	/// <summary>Reference to the texture manager.</summary>
	TextureManager &mManager;

	/// <summary>ID of this texture.</summary>
	std::string mId;

	/// <summary>Texture file path.</summary>
	std::string mTexturePath;

	/// <summary>The image as SDL surface.</summary>
	SDL_Surface *mSDLSurface;

	/// <summary>The OpenGL texture object. 0 if there is no GPU resource yet.</summary>
	GLuint mGL_TextureObj;

	/// <summary>Texture's horizontal wrap mode.</summary>
	Wrap mHorizontalWrap;

	/// <summary<Texture's vertical wrap mode.</summary>
	Wrap mVerticalWrap;

	/// <summary>Texture's border color.</summary>
	Rgba mBorderColor;

	/// <summary>Flag that tells if mipmaps are in use.</summary>
	bool mUseMipMaps;

	/// <summary>Texture parameters.</summary>
	TextureParameters mTextureParameters;

	friend class TextureManager;
};
