
#include "camera.h"

Camera::Camera() :
	mParams( 6, 0.0f )
{

}

Camera::~Camera()
{

}

void Camera::setProjectionParameters( float l, float r, float t, float b, float n, float f )
{
	mParams[ KLeftParamIndex ] = l;
	mParams[ KRightParamIndex ] = r;
	mParams[ KTopParamIndex ] = t;
	mParams[ KBottomParamIndex ] = b;
	mParams[ KNearParamIndex ] = n;
	mParams[ KFarParamIndex ] = f;
}

void Camera::setPerspProjection()
{
	mProjectionMat.setFrustum2( mParams[ KLeftParamIndex ], mParams[ KRightParamIndex ],
			mParams[ KTopParamIndex ], mParams[ KBottomParamIndex ], 
			mParams[ KNearParamIndex ], mParams[ KFarParamIndex ] );
}

void Camera::setOrthogonalProjection()
{
	mProjectionMat.setOrthographic( mParams[ KLeftParamIndex ], mParams[ KRightParamIndex ],
			mParams[ KTopParamIndex ], mParams[ KBottomParamIndex ], 
			mParams[ KNearParamIndex ], mParams[ KFarParamIndex ] );
}

void Camera::setPosition(const Vec3 &pos)
{
	mPosition = pos;
}

const Vec3& Camera::getPosition() const
{
	return mPosition;
}

void Camera::setLookAt(const Vec3 &pos)
{
	mLookAt = pos;
}

const Vec3& Camera::getLookAt() const
{
	return mLookAt;
}

void Camera::rotateLookAt(const Quaternion &quat)
{
	Matrix4x4 rotMat = quat.toRotationMatrix4();
	Vec3 frontVec = mLookAt - mPosition;
	float frontVecLen = frontVec.length();
	frontVec.normalize();
	frontVec = rotMat * frontVec;
	mLookAt = mPosition + (frontVec * frontVecLen);
}

Vec3 Camera::getFrontVec() const
	{
	return mFrontVecTrans;
	}

Vec3 Camera::getUpVec() const
	{
	return mUpVecTrans;
	}

Vec3 Camera::getRightVec() const
	{
	return mRightVecTrans;
	}

void Camera::updateMatrices()
{
	mFrontVecTrans = mLookAt - mPosition;
	mRightVecTrans = Vec3::crossProduct(Vec3(0, 1, 0), mFrontVecTrans);
	mUpVecTrans = Vec3::crossProduct(mFrontVecTrans, mRightVecTrans);

	mFrontVecTrans.normalize();
	mRightVecTrans.normalize();
	mUpVecTrans.normalize();

	mViewMatrix.setLookAt(mPosition, mLookAt, Vec3(0, 1, 0));
	mViewProjMatrix = mProjectionMat * mViewMatrix;
}

const Matrix4x4& Camera::getViewProjMatrix() const
{
	return mViewProjMatrix;
}

const Matrix4x4& Camera::getViewMatrix() const
{
	return mViewMatrix;
}

void Camera::setCameraVectors(const Vec3 &pos,
	const Vec3 &lookAt,
	const Vec3 &/*up*/)
{
	mPosition = pos;
	mLookAt = lookAt;
}

void Camera::moveForward(float units)
{
	mPosition += mFrontVecTrans * units;
	mLookAt += mFrontVecTrans * units;
}


void Camera::moveSideways(float units)
{
	mPosition += mRightVecTrans * units;
	mLookAt += mRightVecTrans * units;
}

void Camera::move(const Vec3 &dir, bool moveLookAt)
{
	mPosition += dir;
	if (moveLookAt) {
		mLookAt += dir;
	}
}

std::vector< Vec3 > Camera::getFrustumWorldCoordinates() const
{
	// Calculate the inverse camera MVP matrix.
	Matrix4x4 mvpMat = getViewProjMatrix();


	std::vector< Vec3 > cornerCoords;
	cornerCoords.resize( 8, Vec3() );
	cornerCoords[0] = mPosition + ( mFrontVecTrans * mParams[Camera::KFarParamIndex] );
	cornerCoords[1] = mPosition + ( mFrontVecTrans * mParams[Camera::KFarParamIndex] );
	cornerCoords[2] = mPosition + ( mFrontVecTrans * mParams[Camera::KFarParamIndex] );
	cornerCoords[3] = mPosition + ( mFrontVecTrans * mParams[Camera::KFarParamIndex] );
	cornerCoords[4] = mPosition + ( mFrontVecTrans * mParams[Camera::KNearParamIndex] );
	cornerCoords[5] = mPosition + ( mFrontVecTrans * mParams[Camera::KNearParamIndex] );
	cornerCoords[6] = mPosition + ( mFrontVecTrans * mParams[Camera::KNearParamIndex] );
	cornerCoords[7] = mPosition + ( mFrontVecTrans * mParams[Camera::KNearParamIndex] );
	return cornerCoords;
}

Aabb Camera::getFrustCoordinatesAsAabb() const
{
	// Get the view frustum coordintes.
	std::vector< Vec3 > cornerCoords = getFrustumWorldCoordinates();

	// Construct AABB that contains all the coordinates.
	Aabb aabb;
	aabb.reset();
	for( const Vec3 &pos : cornerCoords )
	{
		aabb.include( pos );
	}
	return aabb;
}
