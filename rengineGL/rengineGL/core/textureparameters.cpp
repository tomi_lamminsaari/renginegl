
#include "defines.h"
#include "textureparameters.h"

TextureParameters::TextureParameters() :
	mWidth( 0 ),
	mHeight( 0 ),
	mFormat( TextureParameters::Format::Rgb )
{

}


TextureParameters::~TextureParameters()
{

}

TextureParameters::TextureParameters( GLsizei width, GLsizei height, Format texFormat ) :
	mWidth( width ),
	mHeight( height ),
	mFormat( texFormat )
{

}

void TextureParameters::setSize( GLsizei width, GLsizei height, Format texFormat )
{
	mWidth = width;
	mHeight = height;
	mFormat = texFormat;
}

GLsizei TextureParameters::getWidth() const
{
	return mWidth;
}

GLsizei TextureParameters::getHeight() const
{
	return mHeight;
}

GLint TextureParameters::getInternalFormat() const
{
	switch( mFormat )
	{
	case Format::Rgb:
		return GL_RGB;

	case Format::Rgba:
		return GL_RGBA;

	case Format::Depth:
		return GL_DEPTH_COMPONENT16;

	default:
		break;
	}
	return GL_RGB;
}

void TextureParameters::getSourceFormat( GLenum* sourceFormat, GLenum* sourceType )
{
	switch( mFormat )
	{
	case Format::Rgb:
		*sourceFormat = GL_RGB;
		*sourceType = GL_UNSIGNED_BYTE;
		break;

	case Format::Rgba:
		*sourceFormat = GL_RGBA;
		*sourceType = GL_UNSIGNED_BYTE;
		break;

	case Format::Depth:
		*sourceFormat = GL_DEPTH_COMPONENT;
		*sourceType = GL_FLOAT;
		break;

	default:
		*sourceFormat = GL_RGB;
		*sourceType = GL_UNSIGNED_BYTE;
		break;
	}
}

void TextureParameters::setGlParameterf( GLenum parameterName, GLfloat parameterValue )
{
	mParameters[ parameterName ] = GLVariant( parameterValue );
}

void TextureParameters::setGlParameteri( GLenum parameterName, GLint parameterValue )
{
	mParameters[ parameterName ] = GLVariant( parameterValue );
}

void TextureParameters::apply()
{
	// We assume that texture object has been bound already. If it has been, then the following
	// calls to glTexParameters() functions will set the parameters to the texture. We will iterate
	// all parameters through and call the glTextParameterf(), glTexParameteri(), etc. function
	// respectively.
	auto iter = mParameters.begin();
	while( iter != mParameters.end() )
	{
		GLVariant variantValue = iter->second;
		if( variantValue.getType() == GLVariant::GLType::Float )
		{
			// This is a GLfloat parameter.
			GLASSERT( glTexParameterf( GL_TEXTURE_2D, iter->first, variantValue.asFloat() ) );
		}
		else if( variantValue.getType() == GLVariant::GLType::Integer )
		{
			// THis is a GLint parameter.
			GLASSERT( glTexParameteri( GL_TEXTURE_2D, iter->first, variantValue.asInt() ) );
		}

		// Move to next parameter.
		iter++;
	}
}
