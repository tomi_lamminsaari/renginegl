
#pragma once

#include <string>

/// <summary>
/// An utility class that writes data to log file.
/// </summary>
class GameLog
{
public:
	enum SpecialCode {
		EndLine
	};

	static void init();
	
	static GameLog& get();

	void setEnabled( bool enabled );
	GameLog& operator<< (const char *str);
	GameLog& operator<< (const std::string &txt);
	GameLog& operator<< (double val);
	GameLog& operator<< (int val);
	GameLog& operator<< ( uint32_t val );
	GameLog& operator<< (bool val);
	GameLog& operator<< (size_t val);
	GameLog& operator<< (SpecialCode sc);

private:
	GameLog(const std::string &path);

private:
	static GameLog *sInstance;
	std::string mFilePath;
	bool mEnabled;
};