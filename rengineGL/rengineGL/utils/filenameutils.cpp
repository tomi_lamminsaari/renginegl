
#include <algorithm>
#include "filenameutils.h"

std::string FileNameUtils::replaceFileName( const std::string &originalPath, const std::string &newName )
{
	std::string fullPath = originalPath;
	for( size_t i = 0; i < fullPath.length(); ++i )
	{
		if( fullPath[ i ] == '\\' )
		{
			fullPath[ i ] = '/';
		}
	}
	size_t slashPos = fullPath.rfind( "/" );
	if( slashPos == std::string::npos )
	{
		// Slash not found.
		return newName;
	}
	std::string pathBeg = fullPath.substr( 0, slashPos + 1 );
	return pathBeg + newName;
}

std::string FileNameUtils::getFileNamePart( const std::string &path )
{
	std::string fullPath = path;
	for( size_t i = 0; i < fullPath.length(); ++i )
	{
		if( fullPath[ i ] == '\\' )
		{
			fullPath[ i ] = '/';
		}
	}
	size_t slashPos = fullPath.rfind( "/" );
	if( slashPos == std::string::npos )
	{
		// No slash. Whole string is the filename.
		return fullPath;
	}
	std::string filenamePart = fullPath.substr( slashPos + 1 );
	return filenamePart;
}

bool FileNameUtils::endsWith( const std::string &path, std::string ext )
{
	std::transform( ext.begin(), ext.end(), ext.begin(), ::tolower );
	std::string tmpExt = path.substr( path.length() - ext.length() );
	std::transform( tmpExt.begin(), tmpExt.end(), tmpExt.begin(), ::tolower );
	if( tmpExt == ext )
	{
		return true;
	}
	return false;
}

std::string FileNameUtils::trimFilename( const std::string &filenameToTrim )
{
	// Take copy of original string.
	std::string filenameCopy = filenameToTrim;

	// Remove spaces from the string copy.
	if( filenameCopy.length() == 0 )
	{
		return filenameCopy;
	}
	// Trim from left.
	while( filenameCopy.length() > 0 && isspace( filenameCopy[ 0 ] ) )
	{
		filenameCopy.erase( filenameCopy.begin() );
	}

	// Trim from right.
	while( filenameCopy.length() > 0 && isspace( filenameCopy[ filenameCopy.length() - 1 ] ) )
	{
		filenameCopy.erase( filenameCopy.length() - 1 );
	}
	return filenameCopy;
}
