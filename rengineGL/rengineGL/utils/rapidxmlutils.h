
#pragma once

#include <string>
#include <stdexcept>
#include <rapidxml/rapidxml.hpp>

namespace rapidxmlutils
{

	/// <summary>
	/// Attempts to parse the attribute from given xml-node. If requested attribute cannot be
	/// found from the node, the default value will be returned.
	/// </summary>
	/// <param name="node">Pointer to xml-node.</param>
	/// <param name="attributeName">Name of the attribute to search for.</param>
	/// <param name="defaultValue">The default value that will be returned if attribute cannot be found.</param>
	/// <returns>The attribute's value or defaultValue.</returns>
	std::string getAttributeValueWitDefault(
		rapidxml::xml_node<>* node,
		const std::string& attributeName,
		const std::string& defaultValue
	);

	/// <summary>
	/// Parses the attribute from given node. Throws an error if attribute does not exist.
	/// </summary>
	/// <param name="node">Pointer to xml-node.</param>
	/// <param name="attributeName">The attribute to look for.</param>
	/// <returns>The attribute value.</returns>
	/// <exception cref="std::invalid_argument">Thrown, if node does not contain the given attribute.</exception>
	std::string getAttributeValue(
		rapidxml::xml_node<>* node,
		const std::string& attributeName
	);
}
