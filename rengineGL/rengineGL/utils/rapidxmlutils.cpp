
#include "rapidxmlutils.h"

std::string rapidxmlutils::getAttributeValueWitDefault(
	rapidxml::xml_node<>* node,
	const std::string& attributeName,
	const std::string& defaultValue
)
{
	rapidxml::xml_attribute<>* attribute = node->first_attribute( attributeName.c_str() );
	if( attribute != nullptr )
	{
		// Attribute found.
		return attribute->value();
	}
	else
	{
		// Attribute does not exists. Return the default value.
		return defaultValue;
	}
}

std::string rapidxmlutils::getAttributeValue(
	rapidxml::xml_node<>* node,
	const std::string& attributeName
)
{
	rapidxml::xml_attribute<>* attribute = node->first_attribute( attributeName.c_str() );
	if( attribute == nullptr )
	{
		// Attribute was not found.
		std::string msg = "Cannot parse xml-node. Cannot find attribute: " + attributeName;
		throw std::invalid_argument( msg );
	}

	// Parse the attribute value.
	return attribute->value();
}
