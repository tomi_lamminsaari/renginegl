
#include "gamelog.h"
#include <filesystem>
#include <fstream>
#include <fstream>
#include <iostream>
#include <stdio.h>



GameLog* GameLog::sInstance = nullptr;

void GameLog::init()
{
	std::remove("startup_log.txt");

	if (sInstance == nullptr) {
		sInstance = new GameLog("startup_log.txt");
		( *sInstance ) << "Logging started" << GameLog::EndLine;
		( *sInstance ) << "---------------" << GameLog::EndLine;
	}
}


GameLog& GameLog::get()
{
	return *sInstance;
}
void GameLog::setEnabled( bool enabled )
{
	mEnabled = enabled;
}

GameLog& GameLog::operator<< (const char *str)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);

	fout << str;
	fout.close();

	return *this;
}

GameLog& GameLog::operator<< (const std::string &text)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	fout << text;
	fout.close();

	return *this;
}

GameLog& GameLog::operator<< (double val)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	fout.precision(4);
	fout << val;
	fout.close();
	return *this;
}

GameLog& GameLog::operator<< (int val)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	fout << val;
	fout.close();
	return *this;
}

GameLog& GameLog::operator<< ( uint32_t val )
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout( mFilePath.c_str(), std::ios_base::app );
	fout << val;
	fout.close();
	return *this;
}

GameLog& GameLog::operator<< (bool val)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	if (val) {
		fout << " TRUE ";
	} else {
		fout << " FALSE ";
	}
	fout.close();
	return *this;
}

GameLog& GameLog::operator<< (SpecialCode sc)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	fout << "\n";
	fout.flush();
	fout.close();
	return *this;
}

GameLog& GameLog::operator<<(size_t val)
{
	if( !mEnabled ) {
		return *this;
	}

	std::ofstream fout(mFilePath.c_str(), std::ios_base::app);
	fout << val;
	fout.close();
	return *this;
}


GameLog::GameLog(const std::string &path) :
	mFilePath(path),
	mEnabled( true )
{

}

