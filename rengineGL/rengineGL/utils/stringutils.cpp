
#include <stdexcept>
#include "stringutils.h"
#include <boost/algorithm/string/trim.hpp>

std::vector<std::string> StringUtils::tokenizeString( const std::string& stringToTokenize, const std::string& delimiter )
{
	std::vector<std::string> tokens;
	size_t startPos = 0;
	size_t endPos = stringToTokenize.find( delimiter );
	while( endPos != std::string::npos )
	{
		// Push the next token to vector.
		std::string token = stringToTokenize.substr( startPos, endPos - startPos );
		boost::algorithm::trim( token );
		tokens.push_back( token );

		// Search for next token.
		startPos = endPos + delimiter.length();
		endPos = stringToTokenize.find( delimiter, startPos );
	}

	// Push the last token to the vector.
	std::string token = stringToTokenize.substr( startPos, endPos );
	boost::algorithm::trim( token );
	tokens.push_back( token );
	return tokens;
}

std::vector<int> StringUtils::convertStringsToInts( const std::vector<std::string>& stringTokens )
{
	std::vector<int> intTokens;
	for( const std::string& token : stringTokens )
	{
		// Convert to integer and push to the vector.
		intTokens.push_back( std::stoi( token ) );
	}
	return intTokens;
}