
#include <iomanip>
#include <sstream>
#include "conversionutils.h"
#include "stringutils.h"

std::string ConversionUtils::convertToString( int intVal )
{
	std::stringstream instream;
	instream << intVal;
	return instream.str();
}

std::string ConversionUtils::convertUIntToString( unsigned int intVal )
{
	std::stringstream instream;
	instream << intVal;
	return instream.str();
}

std::string ConversionUtils::convertToString( float floatVal, unsigned int precision )
{
	// Fail if no precision set.
	if( precision < 1 )
		throw std::runtime_error( "Decimal number precision cannot be 0." );

	// Convert the float to string by using the stringstream.
	std::stringstream outstream;
	outstream << std::setprecision( precision ) << floatVal;
	return outstream.str();
}

Rgba ConversionUtils::convertToRgba( const std::string& rgbaString )
{
	// Convert the strings tokens to integers.
	std::vector<std::string> rgbaStringTokens = StringUtils::tokenizeString( rgbaString, "," );
	std::vector<int> rgbaIntValues = StringUtils::convertStringsToInts( rgbaStringTokens );
	if( rgbaIntValues.size() < 3 || rgbaIntValues.size() > 4 )
		throw std::invalid_argument( "Cannot convert string to Rgba-color. Wrong amount of color components. Input string was: " + rgbaString );
	
	// Check that all Rgba values are between valid range.
	for( int val : rgbaIntValues )
	{
		if( val < 0 || val >= 256 )
			throw std::invalid_argument( "Cannot convert string to Rgba-color. Invalid color value. Input string was: " + rgbaString );
	}

	// Create the Rgba color value.
	if( rgbaIntValues.size() == 3 )
		return Rgba( rgbaIntValues[ 0 ], rgbaIntValues[ 1 ], rgbaIntValues[ 2 ] );
	return Rgba( rgbaIntValues[ 0 ], rgbaIntValues[ 1 ], rgbaIntValues[ 2 ], rgbaIntValues[ 3 ] );
}