
#pragma once

#include <string>
#include <vector>

namespace StringUtils
{
	/// <summary>
	/// Tokenizes the given string by splitting it by given delimiter string.
	/// </summary>
	/// <param name="stringToTokenize">The input string.</param>
	/// <param name="delimiter">The delimiter string.</param>
	/// <returns>A vector with substrings from 'stringToTokenize' when it was split with given delimiter.</returns>
	std::vector<std::string> tokenizeString( const std::string& stringToTokenize, const std::string& delimiter );

	/// <summary>
	/// Converts the array of strings to an array of integers. Throws an error if conversion fails.
	/// </summary>
	/// <param name="stringTokens">The string segments to convert to integers.</param>
	/// <returns>A vector that contains the integer versions of input strings.</returns>
	/// <exception cref="std::invalid_argument">Thrown if conversion fails.</exception>
	/// <exception cref="std::out_of_range">Thrown if value is out of valid value range.</exception>
	std::vector<int> convertStringsToInts( const std::vector<std::string>& stringTokens );
}
