#pragma once

// Forward declaration.
struct SDL_Surface;

/// <summary>
/// ScopedSdlSurface is an utility class that manages the lifecycle of an SDL
/// surface.
/// </summary>
class ScopedSdlSurface
{
public:
    /// <summary>
    /// Constructs new scoped surface. The surface gets destroyed when this
    /// ScopedSdlSurface goes out of scope.
    /// </summary>
    /// <param name="surface">Pointer to SDL surface. Ownership is transferred.</param>
    ScopedSdlSurface( SDL_Surface* surface );

    ~ScopedSdlSurface();

public:
    /// <summary>
    /// Returns the pointer to owned SDL_Surface data.
    /// </summary>
    /// <returns>Pointer to owned SDL_Surface object.</returns>
    SDL_Surface* get();

    /// <summary>
    /// Sets new SDL_Surface to this object. Possible previous surface gets
    /// destroyed.
    /// </summary>
    /// <param name="surface"></param>
    void reset( SDL_Surface* surface );

private:
    SDL_Surface* mSurface;
};