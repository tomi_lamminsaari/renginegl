
#include <SDL.h>
#include <SDL_surface.h>
#include "scopedsdlsurface.h"

ScopedSdlSurface::ScopedSdlSurface( SDL_Surface* surface ) :
    mSurface( surface )
{

}

ScopedSdlSurface::~ScopedSdlSurface()
{
    if( mSurface != nullptr ) {
        SDL_FreeSurface( mSurface );
        mSurface = nullptr;
    }
}

SDL_Surface* ScopedSdlSurface::get()
{
    return mSurface;
}

void ScopedSdlSurface::reset( SDL_Surface* surface )
{
    if( mSurface != nullptr ) {
        SDL_FreeSurface( mSurface );
        mSurface = nullptr;
    }

    mSurface = surface;
}