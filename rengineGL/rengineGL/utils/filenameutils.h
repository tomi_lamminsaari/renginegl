
#pragma once

#include <string>

/**
* @class FileNameUtils
* @brief This class contains collection of utility function for manipulating filenames.
*/
class FileNameUtils
	{
	public:
		/**
		* Replaces the filename part of original path with given new filename. The path section
		* remains the same.
		* @param originalPath The original file path with old filename.
		* @param newName The new filename that will replace the filename part of 'originalPath'.
		* @return New filename with old path.
		*/
		static std::string replaceFileName( const std::string &originalPath, const std::string &newName );

		/**
		* Returns the filename part of the given full path.
		* @param path The full path.
		* @return The filename part of the given path.
		*/
		static std::string getFileNamePart( const std::string &path );

		/**
		* Checks if given path ends with given file extension.
		* @param path The full path to process.
		* @param ext The extension to check for.
		* @return Returns 'true' if file path ends with given extension.
		*/
		static bool endsWith( const std::string &path, std::string ext );

		/**
		* Removes extra spaces from begining and from end of given string.
		* @param filenameToTrim The filename that gets trimmed.
		* @return Trimmed filename.
		*/
		static std::string trimFilename( const std::string &filenameToTrim );
	};