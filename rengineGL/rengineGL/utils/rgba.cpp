
#include <exception>
#include "rgba.h"

namespace
{
	const unsigned int RedVal = 0;
	const unsigned int GreenVal = 1;
	const unsigned int BlueVal = 2;
	const unsigned int AlphaVal = 3;
}

Rgba Rgba::Red( 1.0f, 0.0f, 0.0f );
Rgba Rgba::Green( 0.0f, 1.0f, 0.0f );
Rgba Rgba::Blue( 0.0f, 0.0f, 1.0f );
Rgba Rgba::White( 1.0f, 1.0f, 1.0f );
Rgba Rgba::Black( 0.0f, 0.0f, 0.0f );

Rgba::Rgba()
{
	mData[ RedVal ] = 0.0f;
	mData[ GreenVal ] = 0.0f;
	mData[ BlueVal ] = 0.0f;
	mData[ AlphaVal ] = 1.0f;
}

Rgba::Rgba( float r, float g, float b, float a )
{
	if( r < 0.0f || r > 1.0f )
		throw std::exception( "Red-component out of valid range." );
	if( g < 0.0f || g > 1.0f )
		throw std::exception( "Green-component out of valid range." );
	if( b < 0.0f || b > 1.0f )
		throw std::exception( "Blue-component out of valid range." );
	if( a < 0.0f || a > 1.0f )
		throw std::exception( "Alpha-component out of valid range." );
	mData[ RedVal ] = r;
	mData[ GreenVal ] = g;
	mData[ BlueVal ] = b;
	mData[ AlphaVal ] = a;
}

Rgba::Rgba( const Vec4 rgbaAsVec )
{
	if( rgbaAsVec.mX < 0.0f || rgbaAsVec.mX > 1.0f )
		throw std::exception( "Red-component out of valid range." );
	if( rgbaAsVec.mY < 0.0f || rgbaAsVec.mY > 1.0f )
		throw std::exception( "Green-component out of valid range." );
	if( rgbaAsVec.mZ < 0.0f || rgbaAsVec.mZ > 1.0f )
		throw std::exception( "Blue-component out of valid range." );
	if( rgbaAsVec.mW < 0.0f || rgbaAsVec.mW > 1.0f )
		throw std::exception( "Alpha-component out of valid range." );

	mData[ RedVal ] = rgbaAsVec.mX;
	mData[ GreenVal ] = rgbaAsVec.mY;
	mData[ BlueVal ] = rgbaAsVec.mZ;
	mData[ AlphaVal ] = rgbaAsVec.mW;
}

Rgba::Rgba( int r, int g, int b, int a )
{
	GLfloat rr = static_cast< GLfloat >( r ) / 255.0f;
	GLfloat gg = static_cast< GLfloat >( g ) / 255.0f;
	GLfloat bb = static_cast< GLfloat >( b ) / 255.0f;
	GLfloat aa = static_cast< GLfloat >( a ) / 255.0f;
	if( rr < 0.0f || rr > 1.0f )
		throw std::exception( "Red-component out of valid range." );
	if( gg < 0.0f || gg > 1.0f )
		throw std::exception( "Green-component out of valid range." );
	if( bb < 0.0f || bb > 1.0f )
		throw std::exception( "Blue-component out of valid range." );
	if( aa < 0.0f || aa > 1.0f )
		throw std::exception( "Alpha-component out of valid range." );
	mData[ RedVal ] = rr;
	mData[ GreenVal ] = gg;
	mData[ BlueVal ] = bb;
	mData[ AlphaVal ] = aa;
}

Rgba::Rgba( const Rgba &obj )
{
	mData[ RedVal ] = obj.mData[ RedVal ];
	mData[ GreenVal ] = obj.mData[ GreenVal ];
	mData[ BlueVal ] = obj.mData[ BlueVal ];
	mData[ AlphaVal ] = obj.mData[ AlphaVal ];
}

Rgba &Rgba::operator = ( const Rgba &obj )
{
	if( &obj != this )
	{
		mData[ RedVal ] = obj.mData[ RedVal ];
		mData[ GreenVal ] = obj.mData[ GreenVal ];
		mData[ BlueVal ] = obj.mData[ BlueVal ];
		mData[ AlphaVal ] = obj.mData[ AlphaVal ];
	}
	return *this;
}

GLfloat Rgba::red() const
{
	return mData[ RedVal ];
}

GLfloat Rgba::green() const
{
	return mData[ GreenVal ];
}

GLfloat Rgba::blue() const
{
	return mData[ BlueVal ];
}

GLfloat Rgba::alpha() const
{
	return mData[ AlphaVal ];
}

void Rgba::setRed( float r )
{
	if( r < 0.0f || r > 1.0f )
		throw std::exception( "Rgba::setRed(): Red-component is out of valid range." );
	mData[ RedVal ] = r;
}

void Rgba::setGreen( float g )
{
	if( g < 0.0f || g > 1.0f )
		throw std::exception( "Rgba::setGreen(): Green-component is out of valid range." );
	mData[ GreenVal ] = g;
}

void Rgba::setBlue( float b )
{
	if( b < 0.0f || b > 1.0f )
		throw std::exception( "Rgba::setBlue(): Blue-component is out of valid range." );
	mData[ BlueVal ] = b;
}

void Rgba::setAlpha( float a )
{
	if( a < 0.0f || a > 1.0f )
		throw std::exception( "Rgba::setAlpha(): Alpha-component is out of valid range." );
	mData[ AlphaVal ] = a;
}

Vec3 Rgba::asVec3() const
{
	Vec3 retVec( mData[ RedVal ], mData[ GreenVal ], mData[ BlueVal ] );
	return retVec;
}

Vec4 Rgba::asVec4() const
{
	Vec4 retVec( mData[ RedVal ], mData[ GreenVal ], mData[ BlueVal ], mData[ AlphaVal ] );
	return retVec;
}

void Rgba::forceSetValues( float r, float g, float b, float a )
{
	mData[ RedVal ] = r;
	mData[ GreenVal ] = g;
	mData[ BlueVal ] = b;
	mData[ AlphaVal ] = a;
}

const GLfloat *Rgba::dataPtr() const
{
	return mData;
}
