
#pragma once

#include <GL/glew.h>
#include "../math/vec3.h"
#include "../math/vec4.h"

class Rgba
	{
	public:
		static Rgba Red;
		static Rgba Green;
		static Rgba Blue;
		static Rgba White;
		static Rgba Black;

	public:
		Rgba();
		Rgba( float r, float g, float b, float a = 1.0f );
		Rgba( int r, int g, int b, int a = 255 );
		Rgba( const Vec4 rgbaAsVec );
		Rgba( const Rgba &obj );
		Rgba& operator = ( const Rgba &obj );

	public:
		GLfloat red() const;
		GLfloat green() const;
		GLfloat blue() const;
		GLfloat alpha() const;

		void setRed( GLfloat r );
		void setGreen( GLfloat g );
		void setBlue( GLfloat b );
		void setAlpha( GLfloat a );

		Vec3 asVec3() const;
		Vec4 asVec4() const;

		/// <summary>
		/// Sets values without validating the color values.
		/// </summary>
		/// <param name="r">Red value.</param>
		/// <param name="g">Green value.</param>
		/// <param name="b">Blue value.</param>
		/// <param name="a">Alpha value.</param>
		void forceSetValues( float r, float g, float b, float a );

		const GLfloat* dataPtr() const;

	private:
		
		GLfloat mData[ 4 ];
	};