
#pragma once

#include <string>
#include "rgba.h"

/// <summary>
/// ConversionUtils is an utility class for value conversions.
/// </summary>
class ConversionUtils
{
public:

	/// <summary>
	/// Converts the given integer value to string.
	/// </summary>
	/// <param name="intVal">The integer value to convert.</param>
	/// <returns>A string that contains the integer.</returns>
	static std::string convertToString( int intVal );

	/// <summary>
	/// Converts the given unsigned integer value to string.
	/// </summary>
	/// <param name="intVal">The unsigned integer value to convert.</param>
	/// <returns>A string that contains the integer.</returns>
	static std::string convertUIntToString( unsigned int intVal );

	/// <summary>
	/// Converts the given floating point value to string.
	/// </summary>
	/// <param name="floatVal">The float value to convert.</param>
	/// <param name="precision">The number of digits after decimal point.</param>
	/// <returns>A string that contains the float.</returns>
	static std::string convertToString( float floatVal, unsigned int precision );

	/// <summary>
	/// Parses an Rgba color value from given string. This function expects
	/// the color string to be one of the following format:
	/// - 255,255,0,255 the color components are R, G, B, A within range 0-255 of each.
	/// </summary>
	/// <param name="rgbaString">The rgba string value.</param>
	/// <returns>The parsed Rgba color value</returns>
	static Rgba convertToRgba( const std::string& rgbaString );
};
