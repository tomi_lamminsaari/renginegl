
#define SDL_MAIN_HANDLED
#include <iostream>
#include "../testapp/testapp.h"
#include "ext/imgui/imgui.h"
#include "ext/imgui/imgui_impl_sdl.h"
#include "ext/imgui/imgui_impl_opengl3.h"
#include "utils/gamelog.h"
#include <SDL.h>
#include <SDL_opengl.h>


int main()
{
	try
	{
		GameLog::init();
		TestApp app;

		// Initialize the application.
		BaseApplication::InitializationParams initParams;
		initParams.displayWidth = 1200;
		initParams.displayHeight = 800;
		initParams.initialCameraPosition = Vec3( -50.0f, 50.0f, 0.0f );
		initParams.initialCameraLookAt = Vec3( 0.0f, 0.0f, 0.0f );
		app.initialize( initParams );

		// Execute the application. Execution returns when application
		// closes.
		app.runApplication();

		// Unintialize the application.
		app.uninitialize();
	}
	catch( const std::exception& ex )
	{
		// On exception case we show the error message in Allegro alert dialog.
        
	}
	return 0;
}
