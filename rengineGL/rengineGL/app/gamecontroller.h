#pragma once

#include <SDL.h>
#include "../math/vec2.h"

/// <summary>
/// GameController is a help wrapper class for accessing joystick.
/// </summary>
class GameController
{
public:
    /// <summary>
    /// Constructs new GameController object and binds it with given SDL
    /// joystick.
    /// </summary>
    /// <param name="joystick">The SDL joystick to bind with. Can be null.</param>
    GameController( SDL_Joystick* joystick );

    /// <summary>
    /// Gets the left stick movement amount. Returned values are between the range
    /// -1.0 to 1.0. Returns (0.0, 0.0) if SDL joystick was null.
    /// </summary>
    /// <returns>Both X and Y directions of the joystick's left stick.</returns>
    Vec2 getLeftStick();

    /// <summary>
    /// Gets the right stick movement amount. Returned values are between the range
    /// -1.0 to 1.0. Returns (0.0, 0.0) if SDL joystick was null.
    /// </summary>
    /// <returns>Both X and Y directions of the joystick's right stick.</returns>
    Vec2 getRightStick();

private:
    SDL_Joystick* mJoystick;
};