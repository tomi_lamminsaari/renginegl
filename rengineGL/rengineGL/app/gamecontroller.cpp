
#include "gamecontroller.h"

namespace
{
    const float KLeftStickDeadZone = 5500.0f;
    const float KRightStickDeadZone = 5500.0f;
}

GameController::GameController( SDL_Joystick* joystick ) :
    mJoystick( joystick )
{

}

Vec2 GameController::getLeftStick()
{
    if( mJoystick == nullptr )
    {
        return Vec2( 0.0f, 0.0f );
    }
    Sint16 xaxis = SDL_JoystickGetAxis( mJoystick, 0 );
    Sint16 yaxis = SDL_JoystickGetAxis( mJoystick, 1 );
    if( xaxis > -KLeftStickDeadZone && xaxis < KLeftStickDeadZone )
        xaxis = 0;
    if( yaxis > -KLeftStickDeadZone && yaxis < KLeftStickDeadZone )
        yaxis = 0;

    // Scale the value to range -1.0 to 1.0
    float xval = static_cast< float >( xaxis ) / 32768.0f;
    float yval = static_cast< float >( yaxis ) / 32768.0f;
    return Vec2( xval, yval );
}

Vec2 GameController::getRightStick()
{
    if( mJoystick == nullptr )
    {
        return Vec2( 0.0f, 0.0f );
    }
    Sint16 xaxis = SDL_JoystickGetAxis( mJoystick, 2 );
    Sint16 yaxis = SDL_JoystickGetAxis( mJoystick, 3 );
    if( xaxis > -KRightStickDeadZone && xaxis < KRightStickDeadZone )
        xaxis = 0;
    if( yaxis > -KRightStickDeadZone && yaxis < KRightStickDeadZone )
        yaxis = 0;

    // Scale to range -1.0 to 1.0
    float xval = static_cast< float >( xaxis ) / 32768.0f;
    float yval = static_cast< float >( yaxis ) / 32768.0f;
    return Vec2( xval, yval );
}
