
#include <iostream>
#include "../ext/imgui/imgui.h"
#include "../ext/imgui/imgui_impl_sdl.h"
#include "../ext/imgui/imgui_impl_opengl3.h"

#include <boost/format.hpp>
#include <GL/glew.h>
#include "baseapplication.h"
#include "../math/vec4.h"
#include "../core/animators/noderotator.h"
#include "../core/animators/backforthanimator.h"
#include "../core/camera.h"
#include "../core/mesh.h"
#include "../core/materialmanager.h"
#include "../core/material.h"
#include "../core/mesh.h"
#include "../core/meshmanager.h"
#include "../core/model.h"
#include "../core/modelmanager.h"
#include "../core/node.h"
#include "../core/lightsources.h"
#include "../core/renderable.h"
#include "../core/rendercore.h"
#include "../core/scene.h"
#include "../core/shaderprogram.h"
#include "../core/texture.h"
#include "../core/texturemanager.h"
#include "../core/renderutils/skybox.h"
#include "../importers/wavefrontimporter.h"
#include "../importers/terrainimporter.h"

namespace
{
	const double KUpdateTimerTickDuration = 1.0 / 60.0;
}


// BaseApplication::InitializationParams class.

BaseApplication::InitializationParams::InitializationParams() :
	initialCameraPosition( -50.0f, 50.0f, 0.0f ),
	initialCameraLookAt( 0.0f, 0.0f, 0.0f ),
	displayWidth( 1028 ),
	displayHeight( 768 )
{

}

BaseApplication::InitializationParams::InitializationParams(
	const BaseApplication::InitializationParams& obj ) :
	initialCameraPosition( obj.initialCameraPosition ),
	initialCameraLookAt( obj.initialCameraLookAt ),
	displayWidth( obj.displayWidth ),
	displayHeight( obj.displayHeight )
{

}

BaseApplication::InitializationParams& BaseApplication::InitializationParams::operator=(
	const BaseApplication::InitializationParams& obj )
{
	if( this != &obj )
	{
		initialCameraPosition = obj.initialCameraPosition;
		initialCameraLookAt = obj.initialCameraLookAt;
		displayWidth = obj.displayWidth;
		displayHeight = obj.displayHeight;
	}
	return *this;
}



// BaseApplication class.

BaseApplication::BaseApplication() :
	mWindow( nullptr ),
	mContext( NULL ),
	mJoystick( nullptr ),
	mUpdateCounter( 0 ),
	mExitNow( false ),
	mNeedsRendering( false ),
	mCumulativeTime( 0.0 ),
	mNextTerminalPrint( 3.0 ),
	mImGuiContext( nullptr )
{
}

BaseApplication::~BaseApplication()
{

}

void BaseApplication::initialize( const InitializationParams& initParams )
{
	// Initialize the Allegro library.
	if( SDL_Init( SDL_INIT_TIMER | SDL_INIT_EVENTS | SDL_INIT_VIDEO | SDL_INIT_JOYSTICK ) != 0 ) {
		std::string sdlErrorMessage = SDL_GetError();
		throw SDLError( "Initializing SDL2 library failed. " + sdlErrorMessage );
	}
	
	// Create the primary display with OpenGL binding.
	SDL_GL_SetAttribute( SDL_GL_ACCELERATED_VISUAL, 1 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MAJOR_VERSION, 4 );
	SDL_GL_SetAttribute( SDL_GL_CONTEXT_MINOR_VERSION, 5 );
	SDL_GL_SetAttribute( SDL_GL_DOUBLEBUFFER, 1 );
	SDL_GL_SetAttribute( SDL_GL_DEPTH_SIZE, 24 );
	SDL_GL_SetAttribute( SDL_GL_MULTISAMPLESAMPLES, 4 );
	mWindow = SDL_CreateWindow( "rengineGL", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
			initParams.displayWidth, initParams.displayHeight,
			SDL_WINDOW_OPENGL );
	if( !mWindow ) {
		std::string errMessage( SDL_GetError() );
		throw SDLError( "Failed to create OpenGL window. " + errMessage );
	}
	
	mContext = SDL_GL_CreateContext( mWindow );
	if( mContext == NULL ) {
		std::string errMessage( SDL_GetError() );
		throw SDLError( "Failed to create OpenGL context. " + errMessage );
	}

	// Initialize the glew OpenGL utility library.
	GLenum glewInitStatus = glewInit();
	if( GLEW_OK != glewInitStatus ) {
		throw RengineError( ( boost::format( "glewInit() failure: %1%" ) % glewInitStatus ).str() );
	}

	// Create the rendering engine instance.
	mRenderCore.reset( new RenderCore );
	mRenderCore->initialize( this );

	// Set the OpenGL viewport to match the screen size.
	mRenderCore->setViewport( initParams.displayWidth, initParams.displayHeight );

	// Set camera.
	mRenderCore->getCamera()->setPosition( initParams.initialCameraPosition );
	mRenderCore->getCamera()->setLookAt( initParams.initialCameraLookAt );

	// Initialize the ImGui UI library.
	initImGui();

	// Initialize joystick.
	int numOfJoysticks = SDL_NumJoysticks();
	if( numOfJoysticks > 0 )
	{
		mJoystick = SDL_JoystickOpen( 0 );
	}

	// The engine initialization is done. Now it's time for initializing the actual application
	// specific resources. We call initApplication() virtual class that can be overridden
	// by the sub-class.
	initApplication();
}

void BaseApplication::uninitialize()
{
	// Close ImGui UI library.
	uninitImGui();

	// Release the rendering engine.
	if( mRenderCore != nullptr )
	{
		mRenderCore->uninitialize();
		mRenderCore.reset( nullptr );
	}

	if( mJoystick != nullptr )
	{
		SDL_JoystickClose( mJoystick );
		mJoystick = nullptr;
	}
	if( mContext != NULL )
	{
		SDL_GL_DeleteContext( mContext );
		mContext = NULL;
	}
	if( mWindow )
	{
		SDL_DestroyWindow( mWindow );
		mWindow = nullptr;
	}

	SDL_Quit();
}

void BaseApplication::runApplication()
{
	// When application starts, the first thing that we do is to prepare the scene with all
	// the contents that should be rendered by the rendering engine. We do this by calling
	// the prepareScene() method that sub-class can override.
	prepareScene();

	// Now the scene is prepared. We can start the main timer and start processing the events
	// in the main event loop.

	// Start the timer.
	

	// Start the main loop.
	uint64_t lastUpdateTicks = SDL_GetTicks64();
	while( mExitNow == false )
	{
		// Process events.
		SDL_Event sdlEvent;
		while( SDL_PollEvent( &sdlEvent ) ) {
			if( sdlEvent.type == SDL_QUIT ) {
				requestApplicationExit();
			}
		}

		// Wait until timer has ticked.
		uint64_t currentTicks = SDL_GetTicks64();
		uint64_t ticksSinceLastUpdate = currentTicks - lastUpdateTicks;
		double secSinceLastUpdate = static_cast< double >( ticksSinceLastUpdate ) / 1000.0;
		int maxUpdatesToGoThisRound = 10;
		while( secSinceLastUpdate > KUpdateTimerTickDuration && maxUpdatesToGoThisRound > 0 ) {
			// At least one time step has passed. Update the world.
			update( KUpdateTimerTickDuration );
			mNeedsRendering = true;
			secSinceLastUpdate -= KUpdateTimerTickDuration;
			maxUpdatesToGoThisRound--;
		}

		// Render the world.
		if( mNeedsRendering ) {
			lastUpdateTicks = currentTicks;
			render();
			mNeedsRendering = false;
		} else {
			std::this_thread::sleep_for( std::chrono::milliseconds( 1 ) );
		}
	}
}

RenderCore& BaseApplication::getRenderCore()
{
	return *( mRenderCore.get() );
}

void BaseApplication::requestApplicationExit()
{
	mExitNow = true;
}

GameController BaseApplication::getGameController()
{
	return GameController( mJoystick );
}

void BaseApplication::showErrorMessage( const std::string& messageText )
{
	
}

void BaseApplication::printToTerminalWindow( int lineId, const std::string& message )
{
	mTerminalPrintLines[ lineId ] = message;
}


void BaseApplication::update( double timeSincePrevious )
{
	// Update the total time.
	mCumulativeTime += timeSincePrevious;
	mNextTerminalPrint -= timeSincePrevious;

	// Update the render engine.
	mRenderCore->update( timeSincePrevious );
}

void BaseApplication::render()
{
	// First render the 3D engine stuff.
	mRenderCore->render();

	// Then, overlay the ImGui user interface.
	renderGUI();
	
	// Finally swap rendered framebuffer to visible.
	SDL_GL_SwapWindow( mWindow );

	// Handle debug printing.
	if( mNextTerminalPrint < 0.0 )
	{
		printLinesToTerminal();
		mNextTerminalPrint = 3.0;
	}
}

void BaseApplication::initApplication()
{
}

void BaseApplication::uninitApplication()
{
}

void BaseApplication::prepareScene()
{
}

void BaseApplication::renderGUI()
{

}

void BaseApplication::printLinesToTerminal()
{
	// Clear the terminal window.
	system( "cls" );

	// Print the lines to terminal.
	auto iter = mTerminalPrintLines.begin();
	while( iter != mTerminalPrintLines.end() )
	{
		std::cout << iter->second << std::endl;
		iter++;
	}

	mTerminalPrintLines.clear();
}

void BaseApplication::initImGui()
{
	IMGUI_CHECKVERSION();
	mImGuiContext = ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); ( void )io;
	ImGui::StyleColorsDark();
	// Setup Platform/Renderer backends
	ImGui_ImplSDL2_InitForOpenGL( mWindow, mContext );

	const char* glsl_version = "#version 130";
	ImGui_ImplOpenGL3_Init( glsl_version );
}

void BaseApplication::uninitImGui()
{
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplSDL2_Shutdown();
	ImGui::DestroyContext();
}
