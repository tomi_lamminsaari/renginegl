
#pragma once

#include <atomic>
#include <vector>
#include <deque>
#include <map>
#include <memory>
#include <GL/glew.h>
#include <SDL.h>
#include <SDL_opengl.h>
#include "gamecontroller.h"
#include "../utils/rgba.h"
#include "../core/rendercore.h"

// Forward declarations.
struct SDL_Window;
class ImGuiContext;

/// <summary>
/// BaseApplication is the base class of the application that uses rengineGL rendering engine.
/// The rengineGL does not actually require this base class but it makes it a bit easier to
/// setup an application that uses rengineGL.
/// </summary>
class BaseApplication : public IRenderCoreHost
{
public:

	/// <summary>
	/// InitializationParams is an utility class for providing the initial
	/// parameter values to applications.
	/// </summary>
	class InitializationParams
	{
	public:
		/// <summary>
		/// Constructor.
		/// </summary>
		InitializationParams();

		/// <summary>
		/// Copy constructor.
		/// </summary>
		InitializationParams( const InitializationParams &obj );

		/// <summary>
		/// An assignment operator.
		/// </summary>
		InitializationParams &operator=( const InitializationParams &obj );

	public:

		/// <summary>World coordinate position where the main camera is at the beginning.</summary>
		Vec3 initialCameraPosition;

		/// <summary>World coordinate position where the camera looks at.</summary>
		Vec3 initialCameraLookAt;

		/// <summary>Width of the display window to open.</summary>
		int displayWidth;

		/// <summary>Height of the display window.</summary>
		int displayHeight;
	};

public:
	/// <summary>
	/// Constructs an rengineGL application object. You need to still call initialize() before you can
	/// start using the application object.
	/// </summary>
	BaseApplication();

	/// <summary>
	/// Destructor.
	/// </summary>
	virtual ~BaseApplication();

	/// <summary>
	/// Intializes the application object. This will set up the display and create OpenGL context as well.
	/// This also initializes the rendering engine and loads the default resources the rengineGL needs.
	/// <para>
	/// The rengineGL as well as this simple application framework use Allegro for some operations This
	/// function will also call necessary Allegro initialization functions so Allegro will be functional
	/// after calling this function.
	/// </summary>
	/// <param name="initParams">Application initialization paramters.</param>
	/// <exception cref="RengineError">Thrown if initialization fails because of rengineGL engine.</exception>
	void initialize( const InitializationParams &initParams );

	/// <summary>
	/// Releases the resource reserved for the rendering engine, display, keyboard, etc.
	/// </summary>
	void uninitialize();

	/// <summary>
	/// Runs the application. This function starts executing the main eventloop 
	void runApplication();

	/// <summary>
	/// Returns access to render engine instance.
	/// </summary>
	/// <returns>Returns pointer to the render engine's main object.</returns>
	RenderCore& getRenderCore();

	/// <summary>
	/// Requests the application to exit as soon as possible. This exiting will
	/// happen right after the next update round.
	/// </summary>
	void requestApplicationExit();

	/// <summary>
	/// Gets the game controller object that can be used for reading gamepad
	/// movement information.
	/// </summary>
	/// <returns></returns>
	GameController getGameController();

public:  // from IRenderCoreHost

	/// <summary>
	/// From IRenderCoreHost
	/// </summary>
	virtual void showErrorMessage( const std::string &messageText ) override;

	/// <summary>
	/// From IRenderCoreHost
	/// </summary>
	virtual void printToTerminalWindow( int lineId, const std::string &message ) override;


// Protected functions for sub classes.
protected:
	/// <summary>
	/// Called by BaseApplication when parent call has already initialized itself.
	/// This allows the custom application object to perform some of its own
	/// extra initializations.
	/// </summary>
	virtual void initApplication();

	/// <summary>
	/// Called by the BaseApplication parent class when application is shutting down.
	/// Your custom application can do some shut down related actions at this point.
	/// The rendering engine instance itself has not been released yet so it can be
	/// accessed but you can't render the screen anymore.
	/// </summary>
	virtual void uninitApplication();

	/// <summary>
	/// Called after the application initialization has completed and actual scene can be
	/// constructed. The rendering engine itself has already been initialized so you can
	/// start using built-in resources and create your custom meshes, materials and load
	/// custom textures. The default implementation of this function does not do anything
	/// and does not create anything visual elements to the scene at all. Use the
	/// getRenderCore() to get access to the rendering engine's main object.
	/// </summary>
	virtual void prepareScene();

	/// <summary>
	/// Called by the BaseApplication when next animation frame should be prepared.
	/// By default this will be called  60 times per second.
	/// <para>
	/// Default implementation updates the rendering engine's core object so usually
	/// there is no need to override this method in child class. But if you need to
	/// override this, you should still call this base class implementation since or
	/// you can call "getRenderCore()->update( timeSincePrevious )" in your
	/// overridden function yourself.
	/// </para>
	/// </summary>
	/// <param name="timeSincePrevious">
	/// Time in seconds since previous call to update. For the very first frame the value
	/// will be 1/60'th of a second.
	/// </param>
	virtual void update( double timeSincePrevious );

	/// <summary>
	/// Called by the BaseApplication when application should render itself to the frame
	/// buffer. Default implementation call RenderCore::render() to render the current
	/// scene contents and to flip it to visible screen.
	/// </summary>
	virtual void render();

	/// <summary>
	/// Called by the BaseApplication when RenderCore has been rendered. This callback is
	/// supposed to render the UI on top of 3D scene rendered by the RenderCore. Default
	/// implementation doesn't draw anything.
	/// </summary>
	virtual void renderGUI();

private:
	void printLinesToTerminal();
	void initImGui();
	void uninitImGui();

protected:

	/// <summary>The SDL2 window object.</summary>
	SDL_Window* mWindow;

	/// <summary>The SDL2 OpenGL context object.</summary>
	SDL_GLContext mContext;
	std::atomic< int > mUpdateCounter;

	/// <summary>A joystick we can use for movement.</summary>
	SDL_Joystick* mJoystick;

	/// <summary>The ImGui context object.</summary>
	ImGuiContext* mImGuiContext;
	
	/// <summary>A flag that indicates when application's main loop should exit.</summary>
	bool mExitNow;

	/// <summary>Set by the main loop when it wants next frame to be rendered.</summary>
	bool mNeedsRendering;

	/// <summary>A total logical time the application has been running.</summary>
	double mCumulativeTime;

	/// <summary>Time until we next time print the debug texts to the terminal window.</summary>
	double mNextTerminalPrint;

	/// <summary>The text lines that will be printed to the terminal window.</summary>
	std::map< int, std::string > mTerminalPrintLines;

	/// <summary>The rendering engine instance owned by us.</summary>
	std::unique_ptr<RenderCore> mRenderCore;
};
