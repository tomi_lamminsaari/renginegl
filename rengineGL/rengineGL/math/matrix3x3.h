
/**
 * \file    matrix3x3.h
 * Class Matrix3x3 represents 3x3 matrix.
 */
#ifndef CR_MATRIX3X3_H_
#define CR_MATRIX3X3_H_

#include "vec3.h"

class Matrix3x3
{
public:
	float mData[9];

public:
    Matrix3x3();
    Matrix3x3(const Matrix3x3 &obj);
    ~Matrix3x3();
    Matrix3x3& operator=(const Matrix3x3 &obj);

    Matrix3x3 operator * (const Matrix3x3 &mat) const;
    Vec3 operator * (const Vec3 &vec) const;
	Matrix3x3 operator * (const float scalar) const;
	Matrix3x3& operator *= (const float scalar);
	float operator() (int row, int col) const;
	float& operator() (int row, int col);

public:
    void setIdentity();
	void set(int row, int col, float value);
	float get(int row, int col) const;
    Vec3 getRow(int row) const;
    Vec3 getColumn(int col) const;
    void setRow(int row, const Vec3 &vec);
    void setColumn(int col, const Vec3 &vec);
	float det() const;
    void transpose();
    Matrix3x3 transposed() const;
    Matrix3x3 inversed() const;
    void inverse();
    std::string toString() const;
};

Vec3 operator* (const Vec3 &vec, Matrix3x3 &mat);

#endif
