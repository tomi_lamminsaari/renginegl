/**
 * \file    matrix4x4.h
 * Header of Matrix4x4 class
 */

#ifndef CR_MATRIX4X4_H_
#define CR_MATRIX4X4_H_

#include <string>
#include "vec3.h"
#include "vec4.h"
#include "matrix3x3.h"


/// <summary>
/// Matrix4x4 is a datatype that operates as 4x4 square matrix.
/// </summary>
class Matrix4x4
{
public:
    /// <summary>
    /// Public array of matrix data. Indexing order is by rows so indices 0-3 contain the first row,
    /// 4-7 have the 2nd row and so forth.
    /// </summary>
	float mData[ 16 ];

public:
    /// <summary>
    /// Default constructor. Sets all matrix values to 0.0.
    /// </summary>
    Matrix4x4();

    /// <summary>
    /// Multiplies two 4x4 matrices together.
    /// </summary>
    /// <param name="mat">The other matrix to multiply with.</param>
    /// <returns>Returns the multiplication of this x mat.</returns>
    Matrix4x4 operator* ( const Matrix4x4& mat ) const;

    /// <summary>
    /// Multiplies the the given vector with this matrix.
    /// </summary>
    /// <param name="vec">The vector that will be multiplied.</param>
    /// <returns>Result of the multiplication.</returns>
    Vec4 operator*( const Vec4 &vec ) const;

    /// <summary>
    /// Multiplies the given Vec3 with this matrix. Multiplication is done by using homogenous coordinates so
    /// the 4th component of the vector will be 1.0.
    /// </summary>
    /// <param name="vec">The vector multiply. The 4th component will be 1.0.</param>
    /// <returns>Result of the multiplication.</returns>
    Vec3 operator*( const Vec3& vec ) const;

    /// <summary>
    /// Returns new matrix with values from this matrix that are multiplied with given scalar.
    /// </summary>
    /// <param name="scalar">The scalar to multiply.</param>
    /// <returns>New matrix that has values multiplied with given scalar.</returns>
    Matrix4x4 operator*( float scalar ) const;

    /// <summary>
    /// Changes this matrix by multiplying this with given scalar.
    /// </summary>
    /// <param name="scalar">The multiplier scalar.</param>
    /// <returns>Reference to this.</returns>
    Matrix4x4& operator*=( float scalar );

    /// <summary>
    /// Compares two matrices. Float comparison checks if values are close enough to each other and
    /// matrices are equal if all values are close enough to each other.
    /// <param name="mat">The matrix to compare with.</param>
    /// <returns>Returns 'true' if matrices are equal.</returns>
    bool operator==( const Matrix4x4& mat ) const;

    /// <summary>
    /// Operator for accessing an element of the matrix. This function does not validate the element coordinates
    /// so it is possible to cause application to crash if using invalid row and column parameters.
    /// </summary>
    /// <param name="row">The row of the matrix to access.</param>
    /// <param name="col">The column of the matrix to access.</param>
    /// <returns>The value from requested matrix location.</returns>
	inline float operator()( int row, int col ) const {
        return mData[ ( row << 2 ) + col ];
    }

    /// <summary>
    /// Operator for accessing an element of the matrix for modifications. This function does not validate the 
    /// element coordinates so it is possible to cause application to crash if using invalid row and column parameters.
    /// </summary>
    /// <param name="row">The row of the matrix to access.</param>
    /// <param name="col">The column of the matrix to access.</param>
    /// <returns>Reference to the value from requested matrix location so that it can be modified by using an assignment operator.</returns>
	inline float& operator()( int row, int col ) {
        return mData[ ( row << 2 ) + col ];
    }

    /// <summary>
    /// Makes this matrix to be an identity matrix.
    /// </summary>
    void setIdentity();

    /// <summary>
    /// Sets all values of this matrix to 0.0.
    /// </summary>
    void setNull();

    /// <summary>
    /// Sets an element if particular cell in the matrix. This function does not validate the given row and column
    /// parameters so using invalid values can crash your application.
    /// </summary>
    /// <param name="row">The row of the matrix access.</param>
    /// <param name="col">The column of the matrix to access.</param>
    /// <param name="value">The value to set to the matrix cell.</param>
	inline void set( int row, int col, float value )
    {
        mData[ ( row << 2 ) + col ] = value;
    }

    /// <summary>
    /// Gets the value from particular cell in the matrix. This function does not validate the given row and column
    /// parameters so using invalid values can crash your application.
    /// </summary>
    /// <param name="row">Row of the matrix to access.</param>
    /// <param name="col">Column of the matrix to access.</param>
    /// <returns>The value from the matrix.</returns>
	inline float get( int row, int col ) const
    {
        return mData[ ( row << 2 ) + col ];
    }

    /// <summary>
    /// Gets the values from particular matrix row as Vec4.
    /// </summary>
    /// <param name="lineNum">The row number to access.</param>
    /// <returns>A Vec4 that contains the matrix values from requested row.</returns>
    Vec4 getRow( int lineNum ) const;

    /// <summary>
    /// Gets the vales from particular matrix column as Vec4.
    /// </summary>
    /// <param name="colNum">The column number to access.</param>
    /// <returns>A Vec4 that contains the matrix values from requested column</returns>
    Vec4 getColumn( int colNum ) const;

    /// <summary>
    /// Sets the values to a certain row.
    /// </summary>
    /// <param name="rowNum">The row number to access.</param>
    /// <param name="vec">The Vec4 that contains the values that will be set to the certain row in the matrix.</param>
    void setRow( int rowNum, const Vec4& vec );

    /// <summary>
    /// Sets the values to a certain column.
    /// </summary>
    /// <param name="colNum">The column number to access.</param>
    /// <param name="vec">The Vec4 that contains the values that will be set to the certain column in the matrix.</param>
    void setColumn( int colNum, const Vec4& vec );

    /// <summary>
    /// Fills the entire matrix with certain value.
    /// </summary>
    /// <param name="value">The value to be set to all cells in the matrix.</param>
    void fill( float value );

    /// <summary>
    /// Sets the translation part of the matrix when this matrix contains a homogenous transformation matrix.
    /// </summary>
    /// <param name="vec">The translation values.</param>
    void setTranslation( const Vec3& vec );

    /// <summary>
    /// Gets the translation part of the matrix when this matrix contains a homogenous transformation matrix.
    /// </summary>
    /// <returns>The translation values.</returns>
    Vec3 getTranslation() const;

    /// <summary>
    /// Gets the rotation part of the matrix when this matrix contains a homogenous transformation matrix.
    /// </summary>
    /// <param name="mat">Receives the rotation matrix values.</param>
    void getRotationMatrix( Matrix3x3& mat ) const;

    /// <summary>
    /// Sets the rotation part of the matrix when this matrix contains a homogenous transformation matrix.
    /// </summary>
    /// <param name="mat">The rotation matrix values to set.</param>
    void setRotationMatrix( const Matrix3x3& mat );

    /// <summary>
    /// Calculates the determinant of this matrix.
    /// </summary>
    /// <returns>Determinant of this matrix.</returns>
	float det() const;

    /// <summary>
    /// Gets a 3x3 matrix so that one row and line are left out.
    /// </summary>
    /// <param name="skipRow">The row number that we skip.</param>
    /// <param name="skipColumn">The column number that we skip.</param>
    /// <returns>A 3x3 matrix that contains values from this matrix but requested row and column were left out.</returns>
    Matrix3x3 subMatrix3x3( int skipRow, int skipColumn ) const;

    /// <summary>
    /// Transposes this matrix.
    /// </summary>
    void transpose();

    /// <summary>
    /// Returns the transposed version of this matrix.
    /// </summary>
    /// <returns>Transposed matrix.</returns>
    Matrix4x4 transposed() const;

    /// <summary>
    /// Calculates an inversion of this matrix.
    /// </summary>
    /// <returns>Inversed version of this matrix.</returns>
    Matrix4x4 inversed() const;

    /// <summary>
    /// Inverses this matrix.
    /// </summary>
    void inverse();

    /// <summary>
    /// Calculates the view matrix. This can be used when your matrix actually represents a camera.
    /// </summary>
    /// <param name="eyePos">The position of the eye.</param>
    /// <param name="lookAt">The position the camera is looking at.</param>
    /// <param name="up">A vector that indicates which way is up.</param>
    void setLookAt(const Vec3 &eyePos, const Vec3 &lookAt, const Vec3 &up);
	//void setFrustum(float left, float right, float bottom, float top, float near, float far);

	void setFrustum2( float leftV, float rightV, float topV, float bottomV, float nearV, float farV );
	void setOrthographic( float leftV, float rightV, float topV, float bottomV, float nearV, float farV );
    void translate(const Vec3 &pos);

    /// <summary>
    /// Gets a string representation of this matrix.
    /// </summary>
    /// <returns>A string representation.</returns>
    std::string toString() const;
};

Vec4 operator* (const Vec4 &vec, Matrix4x4 &mat);


#endif
