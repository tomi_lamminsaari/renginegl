/**
 * \file    vec3.h
 * A  header that contains the Vec3 class.
 */
#ifndef CR_VEC3_H_
#define CR_VEC3_H_

#include <string>
#include <cmath>
#include <sstream>
#include <cstring>


/// <summary>
/// Vec3 represents 3 dimensional vector.
/// </summary>
class Vec3
{
public:
    /// <summary>X component.</summary>
    float mX;

    /// <summary>Y component.</summary>
    float mY;

    /// <summary>Z component.</summary>
    float mZ;

public:
    /// <summary>
    /// Constructs new null-vector.
    /// </summary>
    inline Vec3() : mX(0.0f), mY(0.0f), mZ(0.0f)
    {
    }

    /**
     * Constructs new vector with given component values.
     * \param   x               The X component.
     * \param   y               The Y component.
     * \param   z               The Z component.
     */
	inline Vec3(float x, float y, float z) : mX(x), mY(y), mZ(z)
    {
    }

    /**
     * Copy constructor.
     * \param   vec             Another vector.
     */
    inline Vec3(const Vec3 &vec) : mX(vec.mX), mY(vec.mY), mZ(vec.mZ)
    {
    }

    /**
     * Destructor.
     */
    inline ~Vec3()
    {
    }

public:
    /**
     * Sums this and another vector.
     * \param   vec             Another vector.
     * \return  Sum-vector.
     */
    inline Vec3 operator+ (const Vec3& vec) const
    {
        return Vec3(vec.mX + mX, vec.mY + mY, vec.mZ + mZ);
    }

    inline Vec3 operator- (const Vec3& vec) const
    {
        return Vec3(mX - vec.mX, mY - vec.mY, mZ - vec.mZ);
    }

    inline Vec3& operator = (const Vec3& vec)
    {
        if (&vec != this) {
            mX = vec.mX;
            mY = vec.mY;
            mZ = vec.mZ;
        }
        return *this;
    }

	inline Vec3 operator* (float scalar) const
    {
        return Vec3(mX * scalar, mY * scalar, mZ * scalar);
    }

    inline Vec3 operator*( double scalar ) const
    {
        const float constScalar = static_cast< float >( scalar );
        return Vec3( mX * constScalar, mY * constScalar, mZ * constScalar );
    }

    inline Vec3& operator+= (const Vec3& vec)
    {
        mX += vec.mX;
        mY += vec.mY;
        mZ += vec.mZ;
        return *this;
    }

    inline Vec3& operator-= (const Vec3& vec)
    {
        mX -= vec.mX;
        mY -= vec.mY;
        mZ -= vec.mZ;
        return *this;
    }

	inline Vec3& operator*= (float scaleFactor)
    {
        mX *= scaleFactor;
        mY *= scaleFactor;
        mZ *= scaleFactor;
        return *this;
    }

    inline Vec3& operator*=( double scaleFactor )
    {
        const float constScale = static_cast< float >( scaleFactor );
        mX *= constScale;
        mY *= constScale;
        mZ *= constScale;
        return *this;
    }

	inline Vec3& operator/=( float divider )
		{
		mX /= divider;
		mY /= divider;
		mZ /= divider;
		return *this;
		}

    inline bool operator == (const Vec3 &vec) const
    {
		float diff = vec.mX - mX;
        if (diff < -0.00001f || diff > 0.00001f) {
            return false;
        }
        diff = vec.mY - mY;
        if (diff < -0.00001f || diff > 0.00001f) {
            return false;
        }
        diff = vec.mZ - mZ;
        if (diff < -0.00001f || diff > 0.00001f) {
            return false;
        }
        return true;
    }

    inline bool operator != (const Vec3 &vec) const
    {
        return !( *this == vec);
    }

	inline float length() const
    {
        return std::sqrt(mX*mX + mY*mY + mZ*mZ);
    }

	inline float length2() const
    {
        return mX*mX + mY*mY + mZ*mZ;
    }

    inline Vec3 normalized() const
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY + mZ*mZ);
        return Vec3(mX * invLen, mY * invLen, mZ * invLen);
    }

    inline void normalize()
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY + mZ*mZ);
        mX *= invLen;
        mY *= invLen;
        mZ *= invLen;
    }

    inline void opposite() {
        mX = -mX;
        mY = -mY;
        mZ = -mZ;
    }


	inline static float dotProduct(const Vec3& v1, const Vec3& v2)
    {
        return v1.mX * v2.mX + v1.mY * v2.mY + v1.mZ * v2.mZ;
    }

    inline static Vec3 crossProduct(const Vec3& v1, const Vec3& v2)
    {
        return Vec3(v1.mY*v2.mZ - v1.mZ*v2.mY,
                    v1.mZ*v2.mX - v1.mX*v2.mZ,
                    v1.mX*v2.mY - v1.mY*v2.mX);
    }

    inline std::string toString() const
    {
		std::ostringstream oss;
		oss.precision(4);
		oss << "{\"mX\": " << mX << ", \"mY\": " << mY << ", \"mZ\": " << mZ << "}";
		return oss.str();
    }

    inline static Vec3 xaxis() { return Vec3(1.0f, 0.0f, 0.0f); }
    inline static Vec3 yaxis() { return Vec3(0.0f, 1.0f, 0.0f); }
    inline static Vec3 zaxis() { return Vec3(0.0f, 0.0f, 1.0f); }
};

#endif
