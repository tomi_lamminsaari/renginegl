
#include "mathutils.h"


char MathUtils::convBuff[MATHUTILS_CONVBUFF_SIZE];

std::string MathUtils::toString(int v)
{
	std::ostringstream out;
	out << v;
	out.flush();
	return out.str();
}

std::string MathUtils::toString(float v)
{
	std::ostringstream out;
	out << std::setprecision(6) << v;
	out.flush();
	return out.str();
}

/*
char* MathUtils::toCString(int v)
{
	std::ostringstream oss;
	oss << v;
	std::string valStr = oss.str();

	memset(convBuff, 0, MATHUTILS_CONVBUFF_SIZE);
	strcpy(convBuff, valStr.c_str());
	return convBuff;
}

char* MathUtils::toCString(float v)
{
	std::ostringstream oss;
	oss << v;
	std::string valStr = oss.str();
	memset(convBuff, 0, MATHUTILS_CONVBUFF_SIZE);
	strcpy(convBuff, valStr.c_str());
	return convBuff;
}
*/

bool MathUtils::isZero(double val)
	{
	if (val < -EQUAL_THRESHOLD || val > EQUAL_THRESHOLD)
		return false;
	return true;
	}

bool MathUtils::areEqual( float v1, float v2 )
	{
	if( std::abs( v1 - v2 ) < std::numeric_limits< float >::epsilon() )
		return true;
	return false;
	}

bool MathUtils::isPowerOf2( unsigned int val )
{
	// Test all power of 2 values and tell if given value is equal with any of them.
	for( unsigned int shift = 0; shift < 32; shift++ )
	{
		unsigned int pow2 = ( 1 << shift );
		if( val == pow2 )
			return true;
	}
	return false;
}