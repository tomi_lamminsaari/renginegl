
#pragma once

#include "vec3.h"

/// <summary>
/// Size3 class represents a 3 dimensional size information object. There are 3 properties -
/// width, height and breadh.
/// </summary>
class Size3
{
public:
	/// <summary>The size along the X-axis.</summary>
	float mSizeX;

	/// <summary>The size along the Y-axis.</summary>
	float mSizeY;

	/// <summary>The size along the Z-axis.</summary>
	float mSizeZ;

public:

	/// <summary>
	/// Constructs size object with 0 size in every direction.
	/// </summary>
	inline Size3() :
		mSizeX( 0.0f ),
		mSizeY( 0.0f ),
		mSizeZ( 0.0f )
	{

	}

	/// <summary>
	/// Constructs a size object where each dimension is the same length. Basically this
	/// size represents a cube.
	/// </summary>
	/// <param name="sizeValue">The cube side length.</param>
	inline Size3( float sizeValue ) :
		mSizeX( sizeValue ),
		mSizeY( sizeValue ),
		mSizeZ( sizeValue )
	{

	}

	/// <summary>
	/// Constructs new size object and takes its values from given Vec3 object.
	/// </summary>
	/// <param name="v">The Vec3 object that provides size data.</param>
	inline Size3( const Vec3& v ) :
		mSizeX( v.mX ),
		mSizeY( v.mY ),
		mSizeZ( v.mZ )
	{

	}

	/// <summary>
	/// Constructs a size object with given size properties.
	/// </summary>
	/// <param name="sizeX">The width. Size along the X-axis.</param>
	/// <param name="sizeY">The height. Size along the Y-axis.</param>
	/// <param name="sizeZ">The breadth. Size along the Z-axis.</param>
	inline Size3(
		const float sizeX,
		const float sizeY,
		const float sizeZ
	) :
		mSizeX( sizeX ),
		mSizeY( sizeY ),
		mSizeZ( sizeZ )
	{
	}

	/// <summary>
	/// A copy constructor.
	/// </summary>
	inline Size3( const Size3& obj ) :
		mSizeX( obj.mSizeX ),
		mSizeY( obj.mSizeY ),
		mSizeZ( obj.mSizeZ )
	{
	}

	/// <summary>
	/// Destructor.
	/// </summary>
	inline ~Size3()
	{
	}

	/// <summary>
	/// An assingment operator.
	/// </summary>
	inline Size3& operator=( const Size3& obj )
	{
		if( this != &obj )
		{
			mSizeX = obj.mSizeX;
			mSizeY = obj.mSizeY;
			mSizeZ = obj.mSizeZ;
		}
		return *this;
	}

	/// <summary>
	/// Add and assign operator that increases the size values of this object by the size of given object.
	/// </summary>
	/// <param name="s">The other size information.</param>
	/// <returns>Reference to this object.</returns>
	inline Size3 operator+=( const Size3& s )
	{
		mSizeX += s.mSizeX;
		mSizeY += s.mSizeY;
		mSizeZ += s.mSizeZ;
		return *this;
	}

	/// <summary>
	/// An addition operator. Adds two Size3 objects together and returns the new combined size.
	/// </summary>
	/// <param name="s">The other size object to be added.</param>
	inline Size3 operator+( const Size3& s ) const
	{
		return Size3( mSizeX + s.mSizeX, mSizeY + s.mSizeY, mSizeZ + s.mSizeZ );
	}

public:

	/// <summary>
	/// Converts this size object to Vec3.
	/// </summary>
	/// <returns>Size information as Vec3.</returns>
	inline Vec3 toVec3() const
	{
		return Vec3( mSizeX, mSizeY, mSizeZ );
	}
};
