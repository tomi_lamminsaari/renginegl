/**
 * \file    plane.h
 * Declaration of Plane class.
 */

#ifndef PLANE_H_
#define PLANE_H_

#include "vec3.h"

class Ray;

/// <summary>
/// Plane specifies a 2D plane in 3D space.
/// </summary>
class Plane
{
public:
    /// <summary>Point on plane.</summary>
    Vec3 mPosition;

    /// <summary>Normal vector of the plane.</summary>
    Vec3 mNormal;

public:
    /// <summary>
    /// Default constructor constructs a plane that has point in origo and is
    /// placed on XZ-plane. Normal vector of the plane is up along Y-axis.
    /// </summary>
    Plane();

    /// <summary>
    /// Creates a plane based on given refence point and normal vector.
    /// </summary>
    /// <param name="pos">Point on plane.</param>
    /// <param name="normal">Normal vector of the plane.</param>
    Plane(const Vec3 &pos, const Vec3 &normal);

    Plane(const Plane &obj);
    ~Plane();
    Plane& operator=(const Plane &obj);

public:
    /// <summary>
    /// Sets the reference point of the plane.
    /// </summary>
    /// <param name="pos">Point on the plane.</param>
    void setPosition(const Vec3 &pos);

    /// <summary>
    /// Returns the reference point of the plane.
    /// </summary>
    /// <returns>Reference point of the plane.</returns>
    const Vec3& getPosition() const;

    /// <summary>
    /// Sets the normal vector of the plane.
    /// </summary>
    /// <param name="normal">Normal vector of the plane.</param>
    void setNormal(const Vec3 &normal);

    /// <summary>
    /// Returns the normal vector of the plane.
    /// </summary>
    /// <returns>Normal vector of the plane.</returns>
    const Vec3& getNormal() const;

	/// <summary>
	/// Calculates the point on the plane where the given ray intersects it.
	/// </summary>
	/// <param name="ray">Ray we want to check.</param>
	/// <param name="maxDistance">Maximum distance from ray's starting position.</param>
	/// <param name="outPos">Receives the intersection coordinate. Remains
    /// unchanged if intersection point is further than maxDistance from ray's
    /// starting position.</param>
	/// <returns>Returns true if intersection is found. False if there is no
    /// intersection or intersection is further than maxDistance from ray's
    /// starting position.</returns>
	bool intersects(const Ray &ray, double maxDistance, Vec3 *outPos) const;

	/// <summary>
	/// Calculates the distance of a point from this plane.
	/// </summary>
	/// <param name="point">The point to check.</param>
	/// <returns>Point's distance from this plane.</returns>
	float distanceFromPlane(const Vec3 &point) const;

    /// <summary>
    /// Projects the given point to the plane.
    /// </summary>
    /// <param name="point">An in-out parameter. Receives the point
    /// projected on this plane.</param>
    void projectPointToPlane(Vec3 &point) const;
};


#endif /* PLANE_H_ */
