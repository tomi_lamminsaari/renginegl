/**
 * \file    matrix4x4.cpp
 * Implementation of Matrix4x4 class
 */

#include <string>
#include "matrix4x4.h"
#include "mathutils.h"

// Macro to help accessing the matrix data.
// x is row and y is column.
#define DAT(x,y) mData[x*4 + y]


Matrix4x4::Matrix4x4()
{
    for (int i=0; i < 16; ++i) {
        mData[i] = 0.0f;
    }
}

Matrix4x4 Matrix4x4::operator *(const Matrix4x4 &mat) const
{
    Matrix4x4 res;
    res(0,0) = get(0,0)*mat(0,0) + get(0,1)*mat(1,0) + get(0,2)*mat(2,0) + get(0,3)*mat(3,0);
    res(0,1) = get(0,0)*mat(0,1) + get(0,1)*mat(1,1) + get(0,2)*mat(2,1) + get(0,3)*mat(3,1);
    res(0,2) = get(0,0)*mat(0,2) + get(0,1)*mat(1,2) + get(0,2)*mat(2,2) + get(0,3)*mat(3,2);
    res(0,3) = get(0,0)*mat(0,3) + get(0,1)*mat(1,3) + get(0,2)*mat(2,3) + get(0,3)*mat(3,3);

    res(1,0) = get(1,0)*mat(0,0) + get(1,1)*mat(1,0) + get(1,2)*mat(2,0) + get(1,3)*mat(3,0);
    res(1,1) = get(1,0)*mat(0,1) + get(1,1)*mat(1,1) + get(1,2)*mat(2,1) + get(1,3)*mat(3,1);
    res(1,2) = get(1,0)*mat(0,2) + get(1,1)*mat(1,2) + get(1,2)*mat(2,2) + get(1,3)*mat(3,2);
    res(1,3) = get(1,0)*mat(0,3) + get(1,1)*mat(1,3) + get(1,2)*mat(2,3) + get(1,3)*mat(3,3);

    res(2,0) = get(2,0)*mat(0,0) + get(2,1)*mat(1,0) + get(2,2)*mat(2,0) + get(2,3)*mat(3,0);
    res(2,1) = get(2,0)*mat(0,1) + get(2,1)*mat(1,1) + get(2,2)*mat(2,1) + get(2,3)*mat(3,1);
    res(2,2) = get(2,0)*mat(0,2) + get(2,1)*mat(1,2) + get(2,2)*mat(2,2) + get(2,3)*mat(3,2);
    res(2,3) = get(2,0)*mat(0,3) + get(2,1)*mat(1,3) + get(2,2)*mat(2,3) + get(2,3)*mat(3,3);

    res(3,0) = get(3,0)*mat(0,0) + get(3,1)*mat(1,0) + get(3,2)*mat(2,0) + get(3,3)*mat(3,0);
    res(3,1) = get(3,0)*mat(0,1) + get(3,1)*mat(1,1) + get(3,2)*mat(2,1) + get(3,3)*mat(3,1);
    res(3,2) = get(3,0)*mat(0,2) + get(3,1)*mat(1,2) + get(3,2)*mat(2,2) + get(3,3)*mat(3,2);
    res(3,3) = get(3,0)*mat(0,3) + get(3,1)*mat(1,3) + get(3,2)*mat(2,3) + get(3,3)*mat(3,3);

    return res;
}

Vec4 Matrix4x4::operator *(const Vec4 &vec) const
{
    Vec4 res;
    res.mX = get(0,0)*vec.mX + get(0,1)*vec.mY + get(0,2)*vec.mZ + get(0,3)*vec.mW;
    res.mY = get(1,0)*vec.mX + get(1,1)*vec.mY + get(1,2)*vec.mZ + get(1,3)*vec.mW;
    res.mZ = get(2,0)*vec.mX + get(2,1)*vec.mY + get(2,2)*vec.mZ + get(2,3)*vec.mW;
    res.mW = get(3,0)*vec.mX + get(3,1)*vec.mY + get(3,2)*vec.mZ + get(3,3)*vec.mW;
    return res;
}

Vec3 Matrix4x4::operator * (const Vec3 &vec) const
{
    Vec3 res;
    res.mX = get(0,0) * vec.mX + get(0,1) * vec.mY + get(0,2) * vec.mZ + get(0,3);
    res.mY = get(1,0) * vec.mX + get(1,1) * vec.mY + get(1,2) * vec.mZ + get(1,3);
    res.mZ = get(2,0) * vec.mX + get(2,1) * vec.mY + get(2,2) * vec.mZ + get(2,3);
	float w = 1.0f / (get(3, 0) * vec.mX + get(3, 1) * vec.mY + get(3, 2) * vec.mZ + get(3, 3));
    res.mX *= w;
    res.mY *= w;
    res.mZ *= w;
    return res;
}

Matrix4x4 Matrix4x4::operator *(float scalar) const
{
    Matrix4x4 tmpMat;
    for (int i=0; i < 16; ++i) {
        tmpMat.mData[i] = mData[i] * scalar;
    }
    return tmpMat;
}

Matrix4x4& Matrix4x4::operator *=(float scalar)
{
    for (int i=0; i < 16; ++i) {
        mData[i] *= scalar;
    }
    return *this;
}

bool Matrix4x4::operator ==(const Matrix4x4 &mat) const
{
	float diff;
    for (int i=0; i < 16; ++i) {
        diff = mData[i] - mat.mData[i];
        if (diff < -EQUAL_THRESHOLD || diff > EQUAL_THRESHOLD) {
            return false;
        }
    }
    return true;
}

void Matrix4x4::setIdentity()
{
    for (int i=0; i < 16; ++i) {
        mData[i] = (i%5 == 0) ? 1.0f : 0.0f;
    }
}

void Matrix4x4::setNull()
{
    for (int i=0; i < 16; ++i) {
        mData[i] = 0.0f;
    }
}

Vec4 Matrix4x4::getRow(int lineNum) const
{
    int index = lineNum * 4;
    return Vec4(mData[index], mData[index+1], mData[index+2], mData[index+3]);
}

Vec4 Matrix4x4::getColumn(int colNum) const
{
    return Vec4(mData[colNum], mData[colNum + 4], mData[colNum + 8], mData[colNum+12]);
}

void Matrix4x4::setRow(int rowNum, const Vec4 &vec)
{
    int index = rowNum * 4;
    mData[index++] = vec.mX;
    mData[index++] = vec.mY;
    mData[index++] = vec.mZ;
    mData[index] = vec.mW;
}

void Matrix4x4::setColumn(int colNum, const Vec4 &vec)
{
    mData[colNum] = vec.mX;
    mData[colNum + 4] = vec.mY;
    mData[colNum + 8] = vec.mZ;
    mData[colNum + 12] = vec.mW;
}

void Matrix4x4::fill(float value)
{
    for (int i=0; i < 16; ++i) {
        mData[i] = value;
    }
}

void Matrix4x4::setTranslation(const Vec3 &vec)
{
    (*this)(0, 3) = vec.mX;
    (*this)(1, 3) = vec.mY;
    (*this)(2, 3) = vec.mZ;
}

Vec3 Matrix4x4::getTranslation() const
{
    return Vec3((*this)(0, 3),
                (*this)(1, 3),
                (*this)(2, 3));
}

void Matrix4x4::getRotationMatrix(Matrix3x3 &mat) const
{
    mat = subMatrix3x3(3,3);
}

void Matrix4x4::setRotationMatrix(const Matrix3x3 &mat)
{
    for (int i=0; i < 3; ++i) {
        for (int j=0; j < 3; ++j) {
            mData[i*4 + j] = mat(i,j);
        }
    }
}

float Matrix4x4::det() const
{
	float value =
             DAT(0,3)*DAT(1,2)*DAT(2,1)*DAT(3,0) - DAT(0,2)*DAT(1,3)*DAT(2,1)*DAT(3,0) - DAT(0,3)*DAT(1,1)*DAT(2,2)*DAT(3,0) + DAT(0,1)*DAT(1,3)*DAT(2,2)*DAT(3,0);
    value += DAT(0,2)*DAT(1,1)*DAT(2,3)*DAT(3,0) - DAT(0,1)*DAT(1,2)*DAT(2,3)*DAT(3,0) - DAT(0,3)*DAT(1,2)*DAT(2,0)*DAT(3,1) + DAT(0,2)*DAT(1,3)*DAT(2,0)*DAT(3,1);
    value += DAT(0,3)*DAT(1,0)*DAT(2,2)*DAT(3,1) - DAT(0,0)*DAT(1,3)*DAT(2,2)*DAT(3,1) - DAT(0,2)*DAT(1,0)*DAT(2,3)*DAT(3,1) + DAT(0,0)*DAT(1,2)*DAT(2,3)*DAT(3,1);
    value += DAT(0,3)*DAT(1,1)*DAT(2,0)*DAT(3,2) - DAT(0,1)*DAT(1,3)*DAT(2,0)*DAT(3,2) - DAT(0,3)*DAT(1,0)*DAT(2,1)*DAT(3,2) + DAT(0,0)*DAT(1,3)*DAT(2,1)*DAT(3,2);
    value += DAT(0,1)*DAT(1,0)*DAT(2,3)*DAT(3,2) - DAT(0,0)*DAT(1,1)*DAT(2,3)*DAT(3,2) - DAT(0,2)*DAT(1,1)*DAT(2,0)*DAT(3,3) + DAT(0,1)*DAT(1,2)*DAT(2,0)*DAT(3,3);
    value += DAT(0,2)*DAT(1,0)*DAT(2,1)*DAT(3,3) - DAT(0,0)*DAT(1,2)*DAT(2,1)*DAT(3,3) - DAT(0,1)*DAT(1,0)*DAT(2,2)*DAT(3,3) + DAT(0,0)*DAT(1,1)*DAT(2,2)*DAT(3,3);
    return value;
}

Matrix3x3 Matrix4x4::subMatrix3x3(int skipRow, int skipColumn) const
{
    Matrix3x3 mat;
    int row = 0;
    for (int j=0; j < 4; ++j) {
        if (skipRow == j) {
            continue;
        }
        Vec3 rowContent;
        if (skipColumn == 0) {
            rowContent = Vec3(mData[j*4 + 1],
                              mData[j*4 + 2],
                              mData[j*4 + 3]);
        } else if (skipColumn == 1) {
            rowContent = Vec3(mData[j*4],
                              mData[j*4 + 2],
                              mData[j*4 + 3]);
        } else if (skipColumn == 2) {
            rowContent = Vec3(mData[j*4],
                              mData[j*4 + 1],
                              mData[j*4 + 3]);
        } else if (skipColumn == 3) {
            rowContent = Vec3(mData[j*4],
                              mData[j*4 + 1],
                              mData[j*4 + 2]);
        }
        mat.setRow(row, rowContent);
        row++;
    }
    return mat;
}

void Matrix4x4::transpose()
{
    Matrix4x4 mat = transposed();
    memcpy(mData, mat.mData, sizeof(mData));
}

Matrix4x4 Matrix4x4::transposed() const
{
    Matrix4x4 mat;
    for (int i=0; i < 4; ++i) {
        for (int j=0; j < 4; ++j) {
            mat(i,j) = get(j,i);
        }
    }
    return mat;
}

Matrix4x4 Matrix4x4::inversed() const
{
    Matrix4x4 m;
    m(0,0) = DAT(1,2)*DAT(2,3)*DAT(3,1) - DAT(1,3)*DAT(2,2)*DAT(3,1) + DAT(1,3)*DAT(2,1)*DAT(3,2) - DAT(1,1)*DAT(2,3)*DAT(3,2) - DAT(1,2)*DAT(2,1)*DAT(3,3) + DAT(1,1)*DAT(2,2)*DAT(3,3);
    m(0,1) = DAT(0,3)*DAT(2,2)*DAT(3,1) - DAT(0,2)*DAT(2,3)*DAT(3,1) - DAT(0,3)*DAT(2,1)*DAT(3,2) + DAT(0,1)*DAT(2,3)*DAT(3,2) + DAT(0,2)*DAT(2,1)*DAT(3,3) - DAT(0,1)*DAT(2,2)*DAT(3,3);
    m(0,2) = DAT(0,2)*DAT(1,3)*DAT(3,1) - DAT(0,3)*DAT(1,2)*DAT(3,1) + DAT(0,3)*DAT(1,1)*DAT(3,2) - DAT(0,1)*DAT(1,3)*DAT(3,2) - DAT(0,2)*DAT(1,1)*DAT(3,3) + DAT(0,1)*DAT(1,2)*DAT(3,3);
    m(0,3) = DAT(0,3)*DAT(1,2)*DAT(2,1) - DAT(0,2)*DAT(1,3)*DAT(2,1) - DAT(0,3)*DAT(1,1)*DAT(2,2) + DAT(0,1)*DAT(1,3)*DAT(2,2) + DAT(0,2)*DAT(1,1)*DAT(2,3) - DAT(0,1)*DAT(1,2)*DAT(2,3);
    m(1,0) = DAT(1,3)*DAT(2,2)*DAT(3,0) - DAT(1,2)*DAT(2,3)*DAT(3,0) - DAT(1,3)*DAT(2,0)*DAT(3,2) + DAT(1,0)*DAT(2,3)*DAT(3,2) + DAT(1,2)*DAT(2,0)*DAT(3,3) - DAT(1,0)*DAT(2,2)*DAT(3,3);
    m(1,1) = DAT(0,2)*DAT(2,3)*DAT(3,0) - DAT(0,3)*DAT(2,2)*DAT(3,0) + DAT(0,3)*DAT(2,0)*DAT(3,2) - DAT(0,0)*DAT(2,3)*DAT(3,2) - DAT(0,2)*DAT(2,0)*DAT(3,3) + DAT(0,0)*DAT(2,2)*DAT(3,3);
    m(1,2) = DAT(0,3)*DAT(1,2)*DAT(3,0) - DAT(0,2)*DAT(1,3)*DAT(3,0) - DAT(0,3)*DAT(1,0)*DAT(3,2) + DAT(0,0)*DAT(1,3)*DAT(3,2) + DAT(0,2)*DAT(1,0)*DAT(3,3) - DAT(0,0)*DAT(1,2)*DAT(3,3);
    m(1,3) = DAT(0,2)*DAT(1,3)*DAT(2,0) - DAT(0,3)*DAT(1,2)*DAT(2,0) + DAT(0,3)*DAT(1,0)*DAT(2,2) - DAT(0,0)*DAT(1,3)*DAT(2,2) - DAT(0,2)*DAT(1,0)*DAT(2,3) + DAT(0,0)*DAT(1,2)*DAT(2,3);
    m(2,0) = DAT(1,1)*DAT(2,3)*DAT(3,0) - DAT(1,3)*DAT(2,1)*DAT(3,0) + DAT(1,3)*DAT(2,0)*DAT(3,1) - DAT(1,0)*DAT(2,3)*DAT(3,1) - DAT(1,1)*DAT(2,0)*DAT(3,3) + DAT(1,0)*DAT(2,1)*DAT(3,3);
    m(2,1) = DAT(0,3)*DAT(2,1)*DAT(3,0) - DAT(0,1)*DAT(2,3)*DAT(3,0) - DAT(0,3)*DAT(2,0)*DAT(3,1) + DAT(0,0)*DAT(2,3)*DAT(3,1) + DAT(0,1)*DAT(2,0)*DAT(3,3) - DAT(0,0)*DAT(2,1)*DAT(3,3);
    m(2,2) = DAT(0,1)*DAT(1,3)*DAT(3,0) - DAT(0,3)*DAT(1,1)*DAT(3,0) + DAT(0,3)*DAT(1,0)*DAT(3,1) - DAT(0,0)*DAT(1,3)*DAT(3,1) - DAT(0,1)*DAT(1,0)*DAT(3,3) + DAT(0,0)*DAT(1,1)*DAT(3,3);
    m(2,3) = DAT(0,3)*DAT(1,1)*DAT(2,0) - DAT(0,1)*DAT(1,3)*DAT(2,0) - DAT(0,3)*DAT(1,0)*DAT(2,1) + DAT(0,0)*DAT(1,3)*DAT(2,1) + DAT(0,1)*DAT(1,0)*DAT(2,3) - DAT(0,0)*DAT(1,1)*DAT(2,3);
    m(3,0) = DAT(1,2)*DAT(2,1)*DAT(3,0) - DAT(1,1)*DAT(2,2)*DAT(3,0) - DAT(1,2)*DAT(2,0)*DAT(3,1) + DAT(1,0)*DAT(2,2)*DAT(3,1) + DAT(1,1)*DAT(2,0)*DAT(3,2) - DAT(1,0)*DAT(2,1)*DAT(3,2);
    m(3,1) = DAT(0,1)*DAT(2,2)*DAT(3,0) - DAT(0,2)*DAT(2,1)*DAT(3,0) + DAT(0,2)*DAT(2,0)*DAT(3,1) - DAT(0,0)*DAT(2,2)*DAT(3,1) - DAT(0,1)*DAT(2,0)*DAT(3,2) + DAT(0,0)*DAT(2,1)*DAT(3,2);
    m(3,2) = DAT(0,2)*DAT(1,1)*DAT(3,0) - DAT(0,1)*DAT(1,2)*DAT(3,0) - DAT(0,2)*DAT(1,0)*DAT(3,1) + DAT(0,0)*DAT(1,2)*DAT(3,1) + DAT(0,1)*DAT(1,0)*DAT(3,2) - DAT(0,0)*DAT(1,1)*DAT(3,2);
    m(3,3) = DAT(0,1)*DAT(1,2)*DAT(2,0) - DAT(0,2)*DAT(1,1)*DAT(2,0) + DAT(0,2)*DAT(1,0)*DAT(2,1) - DAT(0,0)*DAT(1,2)*DAT(2,1) - DAT(0,1)*DAT(1,0)*DAT(2,2) + DAT(0,0)*DAT(1,1)*DAT(2,2);
	float d = 1.0f / det();
    m *= d;
    return m;
}

void Matrix4x4::inverse()
{
    Matrix4x4 tmpMat = inversed();
    memcpy(mData, tmpMat.mData, sizeof(mData));
}

Vec4 operator *(const Vec4 &vec, Matrix4x4 &mat)
{
    Vec4 res;
    res.mX = mat.get(0,0)*vec.mX + mat.get(0,1)*vec.mY + mat.get(0,2)*vec.mZ + mat.get(0,3)*vec.mW;
    res.mY = mat.get(1,0)*vec.mX + mat.get(1,1)*vec.mY + mat.get(1,2)*vec.mZ + mat.get(1,3)*vec.mW;
    res.mZ = mat.get(2,0)*vec.mX + mat.get(2,1)*vec.mY + mat.get(2,2)*vec.mZ + mat.get(2,3)*vec.mW;
    res.mW = mat.get(3,0)*vec.mX + mat.get(3,1)*vec.mY + mat.get(3,2)*vec.mZ + mat.get(3,3)*vec.mW;
    return res;
}

void Matrix4x4::setLookAt(const Vec3 &eyePos, const Vec3 &lookAt, const Vec3 &up)
{
	float fx = lookAt.mX - eyePos.mX;
	float fy = lookAt.mY - eyePos.mY;
	float fz = lookAt.mZ - eyePos.mZ;

	float rlf = 1.0f / Vec3(fx, fy, fz).length();
    fx *= rlf;
    fy *= rlf;
    fz *= rlf;

	float sx = fy * up.mZ - fz * up.mY;
	float sy = fz * up.mX - fx * up.mZ;
	float sz = fx * up.mY - fy * up.mX;

	float rls = 1.0f / Vec3(sx, sy, sz).length();
    sx *= rls;
    sy *= rls;
    sz *= rls;

	float ux = sy * fz - sz * fy;
	float uy = sz * fx - sx * fz;
	float uz = sx * fy - sy * fx;

    DAT(0,0) = sx;
    DAT(1,0) = ux;
    DAT(2,0) = -fx;
    DAT(3,0) = 0.0f;

    DAT(0,1) = sy;
    DAT(1,1) = uy;
    DAT(2,1) = -fy;
    DAT(3,1) = 0.0f;

    DAT(0,2) = sz;
    DAT(1,2) = uz;
    DAT(2,2) = -fz;
    DAT(3,2) = 0.0f;

    DAT(0,3) = 0;
    DAT(1,3) = 0;
    DAT(2,3) = 0;
    DAT(3,3) = 1.0f;
    translate(Vec3(-eyePos.mX, -eyePos.mY, -eyePos.mZ));
}

/*
void Matrix4x4::setFrustum(float left, float right, float bottom, float top, float near, float far)
{
    this->fill(0.0f);
	const float r_width = 1.0f / (right - left);
	const float r_height = 1.0f / (top - bottom);
	const float r_depth = 1.0f / (near - far);
	const float x = 2.0f * (near * r_width);
	const float y = 2.0f * (near * r_height);
	const float A = 2.0f * ((right + left) * r_width);
	const float B = (top + bottom) * r_height;
	const float C = (far + near) * r_depth;
	const float D = 2.0f * (far * near * r_depth);
    DAT(0,0) = x;
    DAT(1,1) = y;
    DAT(0,2) = A;
    DAT(1,2) = B;
    DAT(2,2) = C;
    DAT(2,3) = D;
    DAT(3,2) = -1.0f;
}
*/

void Matrix4x4::setFrustum2( float leftV, float rightV, float topV, float bottomV, float nearV, float farV )
	{
	fill( 0.0f );
	DAT( 0, 0 ) = ( 2.0f * nearV ) / ( rightV - leftV );
	DAT( 1, 1 ) = ( 2.0f * nearV ) / ( topV - bottomV );
	DAT( 2, 2 ) = -( farV + nearV ) / ( farV - nearV );
	DAT( 0, 3 ) = -nearV * ( rightV + leftV ) / ( rightV - leftV );
	DAT( 1, 3 ) = -nearV * ( topV + bottomV ) / ( topV - bottomV );
	DAT( 2, 3 ) = ( 2.0f * farV * nearV ) / ( nearV - farV );
	DAT( 3, 2 ) = -1.0f;
	}

void Matrix4x4::setOrthographic( float leftV, float rightV, float topV, float bottomV, float nearV, float farV )
{
    fill( 0.0f );
    DAT( 0, 0 ) = 2.0f / ( rightV - leftV );
    DAT( 1, 1 ) = 2.0f / ( topV - bottomV );
    DAT( 2, 2 ) = -2.0f / ( farV - nearV );
    DAT( 0, 3 ) = -( rightV + leftV ) / ( rightV - leftV );
    DAT( 1, 3 ) = -( topV + bottomV ) / ( topV - bottomV );
    DAT( 2, 3 ) = -( farV + nearV ) / ( farV - nearV );
    DAT( 3, 3 ) = 1.0f;
}

void Matrix4x4::translate(const Vec3 &pos)
{
    DAT(0,3) += DAT(0,0) * pos.mX + DAT(0,1) * pos.mY + DAT(0,2) * pos.mZ;
    DAT(1,3) += DAT(1,0) * pos.mX + DAT(1,1) * pos.mY + DAT(1,2) * pos.mZ;
    DAT(2,3) += DAT(2,0) * pos.mX + DAT(2,1) * pos.mY + DAT(2,2) * pos.mZ;
    DAT(3,3) += DAT(3,0) * pos.mX + DAT(3,1) * pos.mY + DAT(3,2) * pos.mZ;
}

std::string Matrix4x4::toString() const
{
	std::ostringstream oss;
	oss.precision(4);
	oss << "Matrix4x4[ ";
	oss << DAT(0, 0) << ", " << DAT(0, 1) << ", " << DAT(0, 2) << ", " << DAT(0, 3) << "; ";
	oss << DAT(1, 0) << ", " << DAT(1, 1) << ", " << DAT(1, 2) << ", " << DAT(1, 3) << "; ";
	oss << DAT(2, 0) << ", " << DAT(2, 1) << ", " << DAT(2, 2) << ", " << DAT(2, 3) << "; ";
	oss << DAT(3, 0) << ", " << DAT(3, 1) << ", " << DAT(3, 2) << ", " << DAT(3, 3) << " ]";
	return oss.str();
}
