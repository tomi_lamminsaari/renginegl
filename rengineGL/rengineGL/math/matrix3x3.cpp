/**
 * \file    matrix3x3.cpp
 * Implementation of Matrix3x3 class
 */

#include "matrix3x3.h"


#define DAT(x,y) mData[x*3 + y]

Matrix3x3::Matrix3x3()
{
	memset(mData, 0, 9 * sizeof(float));
}

Matrix3x3::Matrix3x3(const Matrix3x3 &obj)
{
    memcpy(mData, obj.mData, sizeof(mData));
}

Matrix3x3::~Matrix3x3()
{

}

Matrix3x3& Matrix3x3::operator =(const Matrix3x3 &obj)
{
    if (this != &obj) {
        memcpy(mData, obj.mData, sizeof(mData));
    }
    return *this;
}

Matrix3x3 Matrix3x3::operator *(const Matrix3x3 &mat) const
{
    Matrix3x3 res;
    res(0,0) = get(0,0)*mat(0,0) + get(0,1)*mat(1,0) + get(0,2)*mat(2,0);
    res(0,1) = get(0,0)*mat(0,1) + get(0,1)*mat(1,1) + get(0,2)*mat(2,1);
    res(0,2) = get(0,0)*mat(0,2) + get(0,1)*mat(1,2) + get(0,2)*mat(2,2);

    res(1,0) = get(1,0)*mat(0,0) + get(1,1)*mat(1,0) + get(1,2)*mat(2,0);
    res(1,1) = get(1,0)*mat(0,1) + get(1,1)*mat(1,1) + get(1,2)*mat(2,1);
    res(1,2) = get(1,0)*mat(0,2) + get(1,1)*mat(1,2) + get(1,2)*mat(2,2);

    res(2,0) = get(2,0)*mat(0,0) + get(2,1)*mat(1,0) + get(2,2)*mat(2,0);
    res(2,1) = get(2,0)*mat(0,1) + get(2,1)*mat(1,1) + get(2,2)*mat(2,1);
    res(2,2) = get(2,0)*mat(0,2) + get(2,1)*mat(1,2) + get(2,2)*mat(2,2);

    return res;
}

Vec3 Matrix3x3::operator *(const Vec3 &vec) const
{
    Vec3 res;
    res.mX = get(0,0)*vec.mX + get(0,1)*vec.mY + get(0,2)*vec.mZ;
    res.mY = get(1,0)*vec.mX + get(1,1)*vec.mY + get(1,2)*vec.mZ;
    res.mZ = get(2,0)*vec.mX + get(2,1)*vec.mY + get(2,2)*vec.mZ;
    return res;
}

Matrix3x3 Matrix3x3::operator *(const float scalar) const
{
    Matrix3x3 mat;
    for (int i=0; i < 9; ++i) {
        mat.mData[i] = mData[i] * scalar;
    }
    return mat;
}

Matrix3x3& Matrix3x3::operator *=(const float scalar)
{
    for (int i=0; i < 9; ++i) {
        mData[i] *= scalar;
    }
    return *this;
}

float Matrix3x3::operator() (int row, int col) const
{
    return mData[(row * 3) + col];
}

float& Matrix3x3::operator() (int row, int col)
{
    return mData[(row * 3) + col];
}

void Matrix3x3::setIdentity()
{
    mData[0] = 1.0f;
    mData[1] = 0.0f;
    mData[2] = 0.0f;

    mData[3] = 0.0f;
    mData[4] = 1.0f;
    mData[5] = 0.0f;

    mData[6] = 0.0f;
    mData[7] = 0.0f;
    mData[8] = 1.0f;
}

void Matrix3x3::set(int row, int col, float value)
{
    mData[row*3 + col] = value;
}

float Matrix3x3::get(int row, int col) const
{
    return mData[row*3 + col];
}

Vec3 Matrix3x3::getRow(int row) const
{
    int index = 0;
    index = row * 3;
    return Vec3(mData[index++], mData[index++], mData[index]);
}

Vec3 Matrix3x3::getColumn(int col) const
{
    return Vec3(mData[col], mData[col + 3], mData[col + 6]);
}

void Matrix3x3::setRow(int row, const Vec3 &vec)
{
    int index = row * 3;
    mData[index++] = vec.mX;
    mData[index++] = vec.mY;
    mData[index] = vec.mZ;
}

void Matrix3x3::setColumn(int col, const Vec3 &vec)
{
    mData[col] = vec.mX;
    mData[col + 3] = vec.mY;
    mData[col + 6] = vec.mZ;
}

float Matrix3x3::det() const
{
    return get(0,0) * get(1,1) * get(2,2) +
            get(0,1) * get(1,2) * get(2,0) +
            get(0,2) * get(1,0) * get(2,1) -
            get(0,0) * get(1,2) * get(2,1) -
            get(0,1) * get(1,0) * get(2,2) -
            get(0,2) * get(1,1) * get(2,0);
}

void Matrix3x3::transpose()
{
    Matrix3x3 mat;
    memcpy(mData, mat.mData, sizeof(mData));
}

Matrix3x3 Matrix3x3::transposed() const
{
    Matrix3x3 mat;
    for (int i=0; i < 3; ++i) {
        for (int j=0; j < 3; ++j) {
            mat(i,j) = get(j,i);
        }
    }
    return mat;
}

Matrix3x3 Matrix3x3::inversed() const
{
    float d = 1.0f / det();
    Matrix3x3 m;
    m(0,0) = (DAT(1,1)*DAT(2,2) - DAT(1,2)*DAT(2,1)) * d;
    m(0,1) = (DAT(0,2)*DAT(2,1) - DAT(0,1)*DAT(2,2)) * d;
    m(0,2) = (DAT(0,1)*DAT(1,2) - DAT(0,2)*DAT(1,1)) * d;
    m(1,0) = (DAT(1,2)*DAT(2,0) - DAT(1,0)*DAT(2,2)) * d;
    m(1,1) = (DAT(0,0)*DAT(2,2) - DAT(0,2)*DAT(2,0)) * d;
    m(1,2) = (DAT(0,2)*DAT(1,0) - DAT(0,0)*DAT(1,2)) * d;
    m(2,0) = (DAT(1,0)*DAT(2,1) - DAT(1,1)*DAT(2,0)) * d;
    m(2,1) = (DAT(0,1)*DAT(2,0) - DAT(0,0)*DAT(2,1)) * d;
    m(2,2) = (DAT(0,0)*DAT(1,1) - DAT(0,1)*DAT(1,0)) * d;
    return m;
}

void Matrix3x3::inverse()
{
    Matrix3x3 m = inversed();
    memcpy(mData, m.mData, sizeof(mData));
}


std::string Matrix3x3::toString() const
{
	std::ostringstream oss;
	oss.precision(4);
	oss << "Matrix3x3[ ";
	oss << DAT(0, 0) << ", " << DAT(0, 1) << ", " << DAT(0, 2) << "; ";
	oss << DAT(1, 0) << ", " << DAT(1, 1) << ", " << DAT(1, 2) << "; ";
	oss << DAT(2, 0) << ", " << DAT(2, 1) << ", " << DAT(2, 2) << " ]";
	return oss.str();
}

Vec3 operator * (const Vec3 &vec, const Matrix3x3 &mat)
{
    Vec3 res;
    res.mX = mat.get(0,0)*vec.mX + mat.get(0,1)*vec.mY + mat.get(0,2)*vec.mZ;
    res.mY = mat.get(1,0)*vec.mX + mat.get(1,1)*vec.mY + mat.get(1,2)*vec.mZ;
    res.mZ = mat.get(2,0)*vec.mX + mat.get(2,1)*vec.mY + mat.get(2,2)*vec.mZ;
    return res;
}

