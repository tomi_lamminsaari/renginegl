/**
 * \file    rect2d.h
 *
 */

#ifndef RECT2D_H_
#define RECT2D_H_

#include "vec2.h"


class Rect2D
{
    Vec2 mTl;
    Vec2 mBr;

public:
    inline Rect2D() {

    }

    inline Rect2D(Vec2 tl, Vec2 br) {
        mTl = tl;
        mBr = br;
    }

    inline Rect2D(Vec2 tl, float w, float h) {
        mTl = tl;
        mBr = Vec2(tl.mX + w, tl.mY + h);
    }

	inline Rect2D(double tlx, double tly, double w, double h) {
        mTl.mX = static_cast<float>(tlx);
        mTl.mY = static_cast<float>(tly);
        mBr.mX = static_cast<float>(tlx + w);
        mBr.mY = static_cast<float>(tly + h);
    }

	inline float getWidth() const {
        return mBr.mX - mTl.mX;
    }

	inline float getHeight() const {
        return mBr.mY - mTl.mY;
    }

    inline void setTl(Vec2 tl) {
        mTl = tl;
    }

    inline void setBr(Vec2 br) {
        mBr = br;
    }

    inline bool inside(const Vec2 &p) const {
        if (p.mX >= mTl.mX && p.mX <= mBr.mX) {
            if (p.mY >= mTl.mY && p.mY <= mBr.mY) {
                return true;
            }
        }
        return false;
    }
};

#endif /* RECT2D_H_ */
