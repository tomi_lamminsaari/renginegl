/**
 * \file    ray.cpp
 * Implementation of Ray class.
 */

#include "ray.h"

Ray::Ray() :
    mStart(0, 0, 0),
    mDir(0, 0, 1)
{

}

Ray::Ray(const Vec3 &start, const Vec3 &dir, float len) :
    mStart(start),
    mDir(dir)
{
    mEnd = start + (dir.normalized() * len);
}

Ray::Ray(const Ray &obj) :
    mStart(obj.mStart),
    mDir(obj.mDir),
    mEnd(obj.mEnd)
{

}

Ray::~Ray()
{

}

Ray& Ray::operator =(const Ray &obj)
{
    if (this != &obj) {
        mStart = obj.mStart;
        mDir = obj.mDir;
        mEnd = obj.mEnd;
    }
    return *this;
}

void Ray::setStart(const Vec3 &start)
{
    mStart = start;
}

void Ray::setDir(const Vec3 &dir, float len)
{
   mDir = dir;
   mEnd = mStart + (dir.normalized() * len);
}

void Ray::setStartEnd(const Vec3 &start, const Vec3 &end)
{
    mStart = start;
    mDir = end - start;
    mEnd = end;
}

const Vec3& Ray::getStart() const
{
    return mStart;
}

const Vec3& Ray::getDir() const
{
    return mDir;
}

float Ray::getLength() const
{
    return (mEnd - mStart).length();
}

const Vec3& Ray::getEnd() const
{
    return mEnd;
}

std::string Ray::toString() const
{
    std::string startStr = mStart.toString();
    std::string dirStr = mDir.toString();
    return std::string("Ray: start: ") + startStr + ", dir: " + dirStr;
}




