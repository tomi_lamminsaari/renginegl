
#pragma once

#include <vector>
#include "vec3.h"
#include "size3.h"

/// <summary>
/// Aabb represents an axis-aligned bounding box that defines the outmost borders of object. It can be used
/// to represent a 3d cuboid that is aligned across the xyz-axises.
/// </summary>
class Aabb
	{
	public:
		typedef unsigned int CornerIndex;
		static const CornerIndex KCornerIndex_FarLeftTop = 0;
		static const CornerIndex KCornerIndex_FarRightTop = 1;
		static const CornerIndex KCornerIndex_FarRightBottom = 2;
		static const CornerIndex KCornerIndex_FarLeftBottom = 3;
		static const CornerIndex KCornerIndex_NearLeftTop = 4;
		static const CornerIndex KCornerIndex_NearRightTop = 5;
		static const CornerIndex KCornerIndex_NearRightBottom = 6;
		static const CornerIndex KCornerIndex_NearLeftBottom = 7;

	public:
		/// <summary>
		/// Default constructor. A zero sized cuboid.
		/// </summary>
		Aabb();

		/// <summary>
		/// Constructs an Axis Aligned Bounding Box symmetrically around the origo
		/// with given size.
		/// </summary>
		/// <param name="boxSize">Size of the bounding box.</param>
		Aabb( const Size3& boxSize );

	public:

		/// <summary>
		/// Expands the bounding box so that given point is included to the box.
		/// </summary>
		/// <param name="point">The 3D point to be included to this bounding box.</param>
		void include( const Vec3 &point );

		/// <summary>
		/// Checks if given point is inside this bounding box.
		/// </summary>
		/// <param name="point">The point to check.</param>
		/// <returns>Returns 'true' if point is inside this bounding box.</returns>
		bool isInside( const Vec3 &point ) const;

		/// <summary>
		/// Returns the corner coordinates. Use CornerIndex enums to index the vector.
		/// </summary>
		/// <returns>
		/// Vector of corner coordinates. The order of coordinates is the same as they are
		/// listed in CornerIndex enum.
		/// </returns>
		std::vector<Vec3> getCorners() const;

		/// <summary>
		/// Calculates the center coordinate of the bounding box.
		/// </summary>
		/// <returns>Center coordinate of this bounding box.</returns>
		Vec3 getCenter() const;

		/// <summary>
		/// Clears the AABB so that min values become the largest possible and max values.
		/// to smallest possible.
		/// </summary>
		void reset();

	private:
		/// <summary>The left, bottom and near edge coordinates of the cuboid.</summary>
		Vec3 mMinValues;

		/// <summary>The right, up and far edge coordiantes of the cuboid.</summary>
		Vec3 mMaxValues;
	};
