/**
 * \file    quaternion.h
 * Declaration of Quaternion class.
 */

#ifndef QUATERNION_H_
#define QUATERNION_H_

#include "vec4.h"
#include "matrix4x4.h"


/// <summary>
/// Quaternion's component indices. Can be used with Quaternion's
/// indexing operator.
/// </summary>
enum QuatComponent {
	QuatW,
	QuatX,
	QuatY,
	QuatZ
};

/// <summary>
/// Quaternions represent object rotations.
/// </summary>
class Quaternion
{
public:
	/// <summary>
	/// Constructs quaternion for rotation around given axis.
	/// </summary>
	/// <param name="angleDegs">Number of degrees to rotate around the axis.</param>
	/// <param name="axis">The rotation axis.</param>
	/// <returns>New quaternion for the rotation.</returns>
	static Quaternion newAxisRotation(float angleDegs, const Vec3 &axis);

	/// <summary>
	/// Constructs a quaterion that represents no rotation at all.
	/// </summary>
	/// <returns>New quaterion for no rotation.</returns>
	static Quaternion noRotation();

	/// <summary>
	/// Default constructor constructs (0, 0, 0, 1) quaternion.
	/// </summary>
	Quaternion();

	/// <summary>
	/// Constructs quaternion with given parameters.
	/// </summary>
	/// <param name="w">w-component.</param>
	/// <param name="x">x-component.</param>
	/// <param name="y">y-component.</param>
	/// <param name="z">z-component.</param>
	Quaternion(const float &w, const float &x, const float &y, const float &z);

	/// <summary>
	/// Constructs quaterion for specifying certain rotation around an axis.
	/// </summary>
	/// <param name="rotation">Rotation around axis. Number of degrees.</param>
	/// <param name="axis">The axis to rotate around.</param>
	Quaternion(float rotation, const Vec3 &axis);

	/// <summary>
	/// Constructs quaterion from given 4D vector.
	/// </summary>
	/// <param name="vec">A vector to take quaternion parameters from.</param>
	Quaternion(const Vec4 &vec);

	Quaternion(const Quaternion &obj);
	~Quaternion();
	Quaternion& operator=(const Quaternion &obj);

public:
	float operator[](int component) const;
	float& operator[](int component);
	Quaternion operator * (const Quaternion &quat) const;
	Quaternion operator * (float scalar) const;
	Quaternion operator + (const Quaternion &quat) const;

	/// <summary>
	/// Sets the rotation around given axis.
	/// </summary>
	/// <param name="angleDegs">Number of degrees to rotate.</param>
	/// <param name="axis">The axis to rotate around.</param>
	void setAxis(float angleDegs, const Vec3 &axis);

	/// <summary>
	/// Sets the rotation around given axis.
	/// </summary>
	/// <param name="angleDegs">Number of degrees to rotate.</param>
	/// <param name="x">X-component of the axis.</param>
	/// <param name="y">Y-component of the axis.</param>
	/// <param name="z">Z-component of the axis.</param>
	void setAxis(float angleDegs, float x, float y, float z);

	/// <summary>
	/// Inverts the quaternion (rotation)
	/// </summary>
	void invert();

	/// <summary>
	/// Returns an inverter quaternion (rotation).
	/// </summary>
	/// <returns>Inverted quaternion.</returns>
	Quaternion inverted() const;
	void fromEuler(float euX, float euY, float euZ);
	void slerp(const Quaternion &quat1, const Quaternion &quat2, float t);
	void nlerp(const Quaternion &quat1, const Quaternion &quat2, float w);

	void normalize();
	Vec4 toVec4() const;
	Matrix4x4 toRotationMatrix4() const;
	Matrix3x3 toRotationMatrix3() const;

	static float dotProduct(const Quaternion &a, const Quaternion &b);

private:
	float mData[4];
};


#endif /* QUATERNION_H_ */
