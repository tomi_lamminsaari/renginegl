
#pragma once

#include <vector>
#include "quaternion.h"
#include "vec3.h"

class Path3DNode
	{
	public:
		Path3DNode();
		Path3DNode( const Vec3 &nodePos, const Quaternion &nodeRotation, double durationSeconds );
		Path3DNode( const Path3DNode &other );
		~Path3DNode();
		Path3DNode& operator=( const Path3DNode &other );

	public:

		/**
		* Returns the position where this node is located at.
		* @return The node's position.
		*/
		const Vec3& position() const;

		/**
		* Returns the rotation of the object in this node.
		* @return The rotation of the object at this node's position.
		*/
		const Quaternion& rotation() const;

		/**
		* Returns the duration it takes from previous node to this node.
		* @return Transition's duration from previous node to this node.
		*/
		double duration() const;

	private:
		/** Coordinate where this path node is located at. */
		Vec3 mPos;

		/** The rotation amount how the object is rotated at this location. */
		Quaternion mRotation;

		/** The duration in seconds it takes to move from previous node to this new node. */
		double mDuration;
	};

class Path3D
	{
	public:

		/**
		* Creates a circular path that can be used for rotating something around the scene.
		* @param centerPos Center position of the rotation.
		* @param radius Radius of the rototation circle.
		* @param duration How long one revolution takes.
		* @param pathNodesCount Number of node we create for the circular path.
		* @return The constructed path object.
		*/
		static Path3D createCircular(
			const Vec3 &centerPos,
			const Vec3 &rotationAxis,
			float radius,
			double duration,
			size_t pathNodesCount );

	public:
		Path3D();
		~Path3D();
		Path3D( const Path3D &other );
		Path3D& operator=( const Path3D &other );

	public:
		/**
		* Gets reference to the path node in given index.
		* @param index Index of the node to return.
		* @return The node in given index.
		*/
		Path3DNode getPathNode( size_t index ) const;

		/**
		* Sets the path node location.
		* @param index Index of the node to return.
		* @return The node in given index.
		*/
		void setPathNode( size_t index, const Path3DNode &node );

		/**
		* Adds new node to the end of path. The time defines how long it would take
		* to move from previous node location to this node location.
		* @param node The position and rotation of the object in this path node.
		*/
		void pushNode( const Path3DNode &node );

	private:

		/**
		* Recalculates the total duration of this path when travelled at 1.0 speed factor.
		*/
		void recalculateDuration();
		
	private:

		/** The array of path location nodes. */
		std::vector< Path3DNode > mNodes;

		/** Full duration of path. */
		double mFullDuration;
	};