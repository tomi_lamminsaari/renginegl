
#include "path3.h"

Path3DNode::Path3DNode() :
	mPos( 0.0f, 0.0f, 0.0f ),
	mRotation( Quaternion::newAxisRotation( 0.0f, Vec3( 0.0f, 1.0f, 0.0f ) ) ),
	mDuration( 1.0f )
	{

	}

Path3DNode::Path3DNode( const Vec3 &nodePos, const Quaternion &nodeRotation, double durationSeconds ) :
	mPos( nodePos ),
	mRotation( nodeRotation ),
	mDuration( durationSeconds )
	{

	}

Path3DNode::Path3DNode( const Path3DNode &other ) :
	mPos( other.mPos ),
	mRotation( other.mRotation ),
	mDuration( other.mDuration )
	{

	}

Path3DNode::~Path3DNode()
	{

	}

Path3DNode& Path3DNode::operator=( const Path3DNode &other )
	{
	if( this != &other )
		{
		mPos = other.mPos;
		mRotation = other.mRotation;
		mDuration = other.mDuration;
		}
	return *this;
	}

const Vec3& Path3DNode::position() const
	{
	return mPos;
	}

const Quaternion& Path3DNode::rotation() const
	{
	return mRotation;
	}

double Path3DNode::duration() const
	{
	return mDuration;
	}

//
// Class Path3D

Path3D Path3D::createCircular(
	const Vec3 &centerPos,
	const Vec3 &rotationAxis,
	float radius,
	double duration,
	size_t pathNodesCount )
	{
	Path3D path;
	for( size_t i = 0; i < pathNodesCount; ++i )
		path.pushNode( Path3DNode() );
	return path;
	}

Path3D::Path3D()
	{

	}

Path3D::~Path3D()
	{

	}

Path3D::Path3D( const Path3D &other ) :
	mNodes( other.mNodes ),
	mFullDuration( other.mFullDuration )
	{

	}

Path3D& Path3D::operator=( const Path3D &other )
	{
	if( this != &other )
		{
		mNodes = other.mNodes;
		mFullDuration = other.mFullDuration;
		}
	return *this;
	}

Path3DNode Path3D::getPathNode( size_t index ) const
	{
	return mNodes[ index ];
	}

void Path3D::setPathNode( size_t index, const Path3DNode &node )
	{
	mNodes[ index ] = node;
	recalculateDuration();
	}

void Path3D::pushNode( const Path3DNode &node )
	{
	mNodes.push_back( node );
	recalculateDuration();
	}

void Path3D::recalculateDuration()
	{
	// Calculate the full duration.
	double dur = 0.0;
	for( const Path3DNode &n : mNodes )
		dur += n.duration();
	mFullDuration = dur;
	}