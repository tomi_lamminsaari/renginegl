
#include <limits>
#include "aabb.h"

Aabb::Aabb() :
	mMinValues( 0.0f, 0.0f, 0.0f ),
	mMaxValues( 0.0f, 0.0f, 0.0f )
{

}

Aabb::Aabb( const Size3& boxSize ) :
	mMinValues( -( boxSize.mSizeX / 2.0f ), -( boxSize.mSizeY / 2.0f ), -( boxSize.mSizeZ / 2.0f ) ),
	mMaxValues( boxSize.mSizeX / 2.0f, boxSize.mSizeY / 2.0f, boxSize.mSizeZ / 2.0f )
{

}

void Aabb::include( const Vec3 &point )
{
	// Update minimum values.
	if( point.mX < mMinValues.mX )
		mMinValues.mX = point.mX;
	if( point.mY < mMinValues.mY )
		mMinValues.mY = point.mY;
	if( point.mZ < mMinValues.mZ )
		mMinValues.mZ = point.mZ;

	// Update maximum values.
	if( point.mX > mMaxValues.mX )
		mMaxValues.mX = point.mX;
	if( point.mY > mMaxValues.mY )
		mMaxValues.mY = point.mY;
	if( point.mZ > mMaxValues.mZ )
		mMaxValues.mZ = point.mZ;
}

bool Aabb::isInside( const Vec3 &point ) const
{
	if( point.mX < mMaxValues.mX && point.mX > mMinValues.mX &&
		point.mY < mMaxValues.mY && point.mY > mMinValues.mY &&
		point.mZ < mMaxValues.mZ && point.mZ > mMinValues.mZ )
	{
		return true;
	}
	return false;
}

std::vector<Vec3> Aabb::getCorners() const
{
	std::vector<Vec3> corners{
		Vec3( mMinValues.mX, mMaxValues.mY, mMaxValues.mZ ),  // FarLeftTop
		Vec3( mMaxValues.mX, mMaxValues.mY, mMaxValues.mZ ),  // FarRightTop
		Vec3( mMaxValues.mX, mMinValues.mY, mMaxValues.mZ ),  // FarRightBottom
		Vec3( mMinValues.mX, mMinValues.mY, mMaxValues.mZ ),  // FarLeftBottom
		Vec3( mMinValues.mX, mMaxValues.mY, mMinValues.mZ ),  // NearLeftTop
		Vec3( mMaxValues.mX, mMaxValues.mY, mMinValues.mZ ),  // NearRightTop
		Vec3( mMaxValues.mX, mMinValues.mY, mMinValues.mZ ),  // NearRightBottom
		Vec3( mMinValues.mX, mMinValues.mY, mMinValues.mZ )  // NearLeftBottom
	};

	return corners;
}

Vec3 Aabb::getCenter() const
{
	// Sum all corner coordinates together and divider by number of corner coordinates.
	// This produces the center coordinate of this AABB.
	std::vector< Vec3 > cornerCoords = getCorners();
	Vec3 centerPos;
	for( const Vec3 &pos : cornerCoords )
		centerPos += pos;
	centerPos /= static_cast< float >( cornerCoords.size() );
	return centerPos;
}

void Aabb::reset()
{
	mMinValues.mX = std::numeric_limits<float>::max();
	mMinValues.mY = std::numeric_limits<float>::max();
	mMinValues.mZ = std::numeric_limits<float>::max();
	mMaxValues.mX = std::numeric_limits<float>::min();
	mMaxValues.mY = std::numeric_limits<float>::min();
	mMaxValues.mZ = std::numeric_limits<float>::min();
}
