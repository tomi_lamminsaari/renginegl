/**
 * \file    vec4.h
 * Declaration of Vec4 class.
 */

#ifndef CR_VEC4_H_
#define CR_VEC4_H_

#include <limits>
#include "vec3.h"

/**
 * \class   Vec4
 * \brief   Vec4 class operates as 4 dimensional vector.
 */
class Vec4
{
public:
	float mX;
	float mY;
	float mZ;
	float mW;

public:
    inline Vec4() : mX(0.0f), mY(0.0f), mZ(0.0f), mW(0.0f)
    {
    }

	inline Vec4(float x, float y, float z, float w) : mX(x), mY(y), mZ(z), mW(w)
    {
    }

    inline Vec4(const Vec4 &vec) : mX(vec.mX), mY(vec.mY), mZ(vec.mZ), mW(vec.mW)
    {
    }

    inline Vec4(const Vec3& vec, float w=1.0f) : mX(vec.mX), mY(vec.mY), mZ(vec.mZ), mW(w)
    {

    }

    inline ~Vec4()
    {
    }

public:
    inline Vec4 operator+ (const Vec4& vec) const
    {
        return Vec4(vec.mX + mX, vec.mY + mY, vec.mZ + mZ, vec.mW + mW);
    }

    inline Vec4 operator- (const Vec4& vec) const
    {
        return Vec4(mX - vec.mX, mY - vec.mY, mZ - vec.mZ, mW - vec.mW);
    }

    inline Vec4& operator = (const Vec4& vec)
    {
        if (&vec != this) {
            mX = vec.mX;
            mY = vec.mY;
            mZ = vec.mZ;
            mW = vec.mW;
        }
        return *this;
    }

    inline Vec4& operator = (const Vec3 &vec)
    {
        mX = vec.mX;
        mY = vec.mY;
        mZ = vec.mZ;
        mW = 1.0;
        return *this;
    }

	inline Vec4 operator* (float scalar) const
    {
        return Vec4(mX * scalar,
                    mY * scalar,
                    mZ * scalar,
                    mW * scalar);
    }

    inline Vec4& operator+= (const Vec4& vec)
    {
        mX += vec.mX;
        mY += vec.mY;
        mZ += vec.mZ;
        mW += vec.mW;
        return *this;
    }

    inline Vec4& operator-= (const Vec4& vec)
    {
        mX -= vec.mX;
        mY -= vec.mY;
        mZ -= vec.mZ;
        mW -= vec.mW;
        return *this;
    }

	inline Vec4& operator*= (float scalar)
    {
        mX *= scalar;
        mY *= scalar;
        mZ *= scalar;
        mW *= scalar;
        return *this;
    }

	inline bool operator==( const Vec4 &x ) const
		{
		if( std::abs( mX - x.mX ) < std::numeric_limits< float >::epsilon() &&
			std::abs( mY - x.mY ) < std::numeric_limits< float >::epsilon() &&
			std::abs( mZ - x.mZ ) < std::numeric_limits< float >::epsilon() )
			{
			return true;
			}
		return false;
		}

	inline float length() const
    {
        return std::sqrt(mX*mX + mY*mY + mZ*mZ + mW*mW);
    }

	inline float length2() const
    {
        return mX*mX + mY*mY + mZ*mZ + mW*mW;
    }

    inline Vec4 normalized() const
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY + mZ*mZ + mW*mW);
        return Vec4(mX * invLen,
                    mY * invLen,
                    mZ * invLen,
                    mW * invLen);
    }

    inline void normalize()
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY + mZ*mZ + mW*mW);
        mX *= invLen;
        mY *= invLen;
        mZ *= invLen;
    }

    inline void opposite()
    {
        mX = -mX;
        mY = -mY;
        mZ = -mZ;
    }

    inline Vec3 toVec3() const
    {
        return Vec3(mX, mY, mZ);
    }

	inline static float dotProduct(const Vec4& v1, const Vec4& v2)
    {
        return v1.mX * v2.mX + v1.mY * v2.mY + v1.mZ * v2.mZ + v1.mW * v2.mW;
    }

    inline std::string toString() const
    {
		std::ostringstream oss;
		oss.precision(4);
		oss << "{\"mX\": " << mX << ", \"mY\": " << mY << ", \"mZ\": " << mZ << ",\"mW\": " << mW;
		return oss.str();
    }
};


#endif
