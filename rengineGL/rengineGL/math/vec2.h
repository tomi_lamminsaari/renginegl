
#ifndef CR_VEC2_H_
#define CR_VEC2_H_

#include <cmath>

/**
 * \class   Vec2
 * \brief   Works as 2 dimensional mathematical vector.
 */
class Vec2
{
public:
    /** X component. */
	float mX;

    /** Y component. */
	float mY;

public:
    /**
     * Default constructor. Creates null vector.
     */
    inline Vec2() : mX(0.0f), mY(0.0f)
    {
    }

    /**
     * Constructor.
     * \param   x               The X component.
     * \param   y               The Y component.
     */
	inline Vec2(float x, float y) : mX(x), mY(y)
    {
    }

    /**
     * Copy constructor.
     * \param   vec             Reference to another vector.
     */
    inline Vec2(const Vec2 &vec) : mX(vec.mX), mY(vec.mY)
    {
    }

    /**
     * Destructor.
     */
    inline ~Vec2()
    {
    }

public:
    /**
     * Sums two vectors together.
     */
    inline Vec2 operator+ (const Vec2& vec) const
    {
        return Vec2(vec.mX + mX, vec.mY + mY);
    }

    /**
     * Substracts two vectors from each other.
     */
    inline Vec2 operator- (const Vec2& vec) const
    {
        return Vec2(mX - vec.mX, mY - vec.mY);
    }

    /**
     * An assignment operator.
     */
    inline Vec2& operator = (const Vec2& vec)
    {
        if (&vec != this) {
            mX = vec.mX;
            mY = vec.mY;
        }
        return *this;
    }

    /**
     * Multiplies this vector by scalar.
     */
	inline Vec2 operator* (float scalar) const
    {
        return Vec2(mX * scalar, mY * scalar);
    }

    inline Vec2& operator+= (const Vec2& vec)
    {
        mX += vec.mX;
        mY += vec.mY;
        return *this;
    }

    inline Vec2& operator-= (const Vec2& vec)
    {
        mX -= vec.mX;
        mY -= vec.mY;
        return *this;
    }

    /**
     * Returns the length of this vector.
     * \return  Length of this vector.
     */
	inline float length() const
    {
        return std::sqrt(mX*mX + mY*mY);
    }

    /**
     * Returns the length without square root.
     * \return  Length without square root.
     */
	inline float length2() const
    {
        return mX*mX + mY*mY;
    }

    /**
     * Returns normalized copy of this vector.
     * \return  Normalized copy.
     */
    inline Vec2 normalized() const
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY);
        return Vec2(mX * invLen, mY * invLen);
    }

    /**
     * Normalizes this vector.
     */
    inline void normalize()
    {
		float invLen = 1.0f / std::sqrt(mX*mX + mY*mY);
        mX *= invLen;
        mY *= invLen;
    }

    /**
     * Rotates this vector around origo by given angle.
     * \param   angle           Rotation angle in radians.
     */
	inline void rotate(float angle)
    {
		float cosAngle = std::cos(angle);
		float sinAngle = std::sin(angle);
		float x = mX;
		float y = mY;
        mX = x * cosAngle - y * sinAngle;
        mY = y * cosAngle + x * sinAngle;
    }

	inline void rotateDegs(float angle)
    {
        angle = angle * (3.14159265358979323846f / 180.0f);
		float cosAngle = std::cos(angle);
		float sinAngle = std::sin(angle);
		float x = mX;
		float y = mY;
        mX = x * cosAngle - y * sinAngle;
        mY = y * cosAngle + x * sinAngle;
    }

    /**
     * Changes the direction of this vector to opposite direction.
     */
    inline void opposite() {
        mX = -mX;
        mY = -mY;
    }



	inline static float dotProduct(const Vec2& v1, const Vec2& v2)
    {
        return v1.mX * v2.mX + v1.mY * v2.mY;
    }
};

#endif
