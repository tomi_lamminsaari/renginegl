/**
 * \file    ray.h
 * Declaration of Ray class.
 */

#ifndef RAY_H_
#define RAY_H_

#include "vec3.h"

/**
 * \class   Ray
 * \brief   Represents a ray that can be casted through scenes to check object intersections.
 */
class Ray
{
public:
    Ray();
    Ray(const Vec3 &start, const Vec3 &dir, float len);
    Ray(const Ray &obj);
    ~Ray();
    Ray& operator = (const Ray &obj);

public:
    void setStart(const Vec3 &start);
    void setDir(const Vec3 &dir, float len);
    void setStartEnd(const Vec3 &start, const Vec3 &end);
    const Vec3& getStart() const;
    const Vec3& getDir() const;
    float getLength() const;
    const Vec3& getEnd() const;

    std::string toString() const;

private:
    Vec3 mStart;
    Vec3 mDir;
    Vec3 mEnd;
};

#endif /* RAY_H_ */
