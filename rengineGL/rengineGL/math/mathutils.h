/**
 * \file    mathutils.h
 * Collection of functions that could be useful.
 */

#ifndef MATHUTILS_H_
#define MATHUTILS_H_

#include <string>
#include <sstream>
#include <iomanip>

#define MATHUTILS_CONVBUFF_SIZE 24
#define EQUAL_THRESHOLD 0.0001f

/**
 * Collection of all kind of utility functions.
 */
class MathUtils
{
public:
	static char convBuff[MATHUTILS_CONVBUFF_SIZE];

	static std::string toString(int v);
	static std::string toString(float v);
//	static char* toCString(int v);
//	static char* toCString(float v);
	static bool isZero( double val );
	static bool areEqual( float v1, float v2 );

	/// <summary>
	/// Tells if given value is equal to any power of 2.
	/// </summary>
	/// </param name="val">The value to check.</param>
	/// <returns>Returns true if value is power of 2</returns>
	static bool isPowerOf2( unsigned int val );
};

#endif /* MATHUTILS_H_ */
