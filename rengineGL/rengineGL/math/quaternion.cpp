/**
 * \file    quaternion.cpp
 * Implementation of Quaternion class.
 */

#include "quaternion.h"

/**
 * Author: Hints to implementation has been taken from
 * http://www.3dkingdoms.com/weekly/quat.h
 */

const float TO_HALF_RAD = 3.14159265f / 360.0f;

Quaternion Quaternion::newAxisRotation(float angleDegs, const Vec3 &axis)
{
    Quaternion q;
    q.setAxis( angleDegs, axis);
    return q;
}

Quaternion Quaternion::noRotation()
{
    Quaternion q;
    q.setAxis( 0.0f, Vec3( 0.0f, 1.0f, 0.0f ) );
    return q;
}

Quaternion::Quaternion()
{
    setAxis(0, 0, 0, 1);
}

Quaternion::Quaternion(const float &w, const float &x, const float &y, const float &z)
{
    mData[0] = w;
    mData[1] = x;
    mData[2] = y;
    mData[3] = z;
}

Quaternion::Quaternion(float rotation, const Vec3 &axis)
{
    setAxis(rotation, axis.mX, axis.mY, axis.mZ);
}

Quaternion::Quaternion(const Vec4 &vec)
{
    mData[0] = vec.mW;
    mData[1] = vec.mX;
    mData[2] = vec.mY;
    mData[3] = vec.mZ;
}

Quaternion::Quaternion(const Quaternion &obj)
{
    mData[0] = obj.mData[0];
    mData[1] = obj.mData[1];
    mData[2] = obj.mData[2];
    mData[3] = obj.mData[3];
}

Quaternion::~Quaternion()
{

}

Quaternion& Quaternion::operator =(const Quaternion &obj)
{
    if (this != &obj) {
        mData[0] = obj.mData[0];
        mData[1] = obj.mData[1];
        mData[2] = obj.mData[2];
        mData[3] = obj.mData[3];
    }
    return *this;
}

void Quaternion::setAxis(float angleDegs, const Vec3 &axis)
{
    setAxis(angleDegs, axis.mX, axis.mY, axis.mZ);
}

void Quaternion::setAxis(float angleDegs, float x, float y, float z)
{
	float half = angleDegs * TO_HALF_RAD;
	float sinA = std::sin(half);
    mData[QuatW] = cos(half);
    mData[QuatX] = x * sinA;
    mData[QuatY] = y * sinA;
    mData[QuatZ] = z * sinA;
}

void Quaternion::invert()
{
    mData[QuatX] = -mData[QuatX];
    mData[QuatY] = -mData[QuatY];
    mData[QuatZ] = -mData[QuatZ];
}

Quaternion Quaternion::inverted() const
{
    Quaternion quat;
    quat.mData[QuatX] = -mData[QuatX];
    quat.mData[QuatY] = -mData[QuatY];
    quat.mData[QuatZ] = -mData[QuatZ];
    quat.mData[QuatW] = mData[QuatW];
    return quat;
}

void Quaternion::fromEuler(float euX, float euY, float euZ)
{
    Quaternion quat1(-euX, Vec3(1, 0, 0));
    Quaternion quat2(-euY, Vec3(0, 1, 0));
    Quaternion quat3(-euZ, Vec3(0, 0, 1));
    quat3 = quat2 * quat3;
    *this = quat1 * quat3;
}

void Quaternion::slerp(const Quaternion &quat1, const Quaternion &quat2, float t)
{
	float w1, w2;
	float cosTheta = dotProduct(quat1, quat2);
	float theta = std::acos(cosTheta);
	float sinTheta = std::sin(theta);

    if (sinTheta > 0.001f) {
        w1 = std::sin((1.0f - t)*theta) / sinTheta;
        w2 = sin(t * theta) / sinTheta;
    } else {
        // quat1 is almost equal to quat2.
        w1 = 1.0f - t;
        w2 = t;
    }
    *this = quat1*w1 + quat2*w2;
}

void Quaternion::nlerp(const Quaternion &quat1, const Quaternion &quat2, float w)
{
	float w1 = 1.0f - w;
    *this = quat1 * w1 + quat2 * w;
    normalize();
}

void Quaternion::normalize()
{
	float invLen = 1.0f / std::sqrt(mData[QuatX] * mData[QuatX] + mData[QuatY] * mData[QuatY] + mData[QuatZ] * mData[QuatZ] + mData[QuatW] * mData[QuatW]);
    mData[0] *= invLen;
    mData[1] *= invLen;
    mData[2] *= invLen;
    mData[3] *= invLen;
}

Vec4 Quaternion::toVec4() const
{
    return Vec4(mData[1], mData[2], mData[3], mData[0]);
}

Matrix4x4 Quaternion::toRotationMatrix4() const
{
	float qw = mData[0];
	float qx = mData[1];
	float qy = mData[2];
	float qz = mData[3];
	const float n = 1.0f / sqrt(qx*qx + qy*qy + qz*qz + qw*qw);
    qw *= n;
    qx *= n;
    qy *= n;
    qz *= n;

    Matrix4x4 mat;
    mat.setRow(0, Vec4(1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy - 2.0f*qz*qw, 2.0f*qx*qz + 2.0f*qy*qw, 0.0f));
    mat.setRow(1, Vec4(2.0f*qx*qy + 2.0f*qz*qw, 1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz - 2.0f*qx*qw, 0.0f));
    mat.setRow(2, Vec4(2.0f*qx*qz - 2.0f*qy*qw, 2.0f*qy*qz + 2.0f*qx*qw, 1.0f - 2.0f*qx*qx - 2.0f*qy*qy, 0.0f));
    mat.setRow(3, Vec4(0.0f, 0.0f, 0.0f, 1.0f));
    return mat;
}

Matrix3x3 Quaternion::toRotationMatrix3() const
{
	float qw = mData[0];
	float qx = mData[1];
	float qy = mData[2];
	float qz = mData[3];
	const float n = 1.0f / sqrt(qx*qx + qy*qy + qz*qz + qw*qw);
    qw *= n;
    qx *= n;
    qy *= n;
    qz *= n;

    Matrix3x3 mat;
    mat.setRow(0, Vec3(1.0f - 2.0f*qy*qy - 2.0f*qz*qz, 2.0f*qx*qy - 2.0f*qz*qw, 2.0f*qx*qz + 2.0f*qy*qw));
    mat.setRow(1, Vec3(2.0f*qx*qy + 2.0f*qz*qw, 1.0f - 2.0f*qx*qx - 2.0f*qz*qz, 2.0f*qy*qz - 2.0f*qx*qw));
    mat.setRow(2, Vec3(2.0f*qx*qz - 2.0f*qy*qw, 2.0f*qy*qz + 2.0f*qx*qw, 1.0f - 2.0f*qx*qx - 2.0f*qy*qy));
    return mat;
}

float Quaternion::operator [](int component) const
{
    return mData[component];
}

float& Quaternion::operator [](int component)
{
    return mData[component];
}

Quaternion Quaternion::operator *(const Quaternion &quat) const
{
    Quaternion ret;
    ret[QuatW] = mData[QuatW]*quat[QuatW] - mData[QuatX]*quat[QuatX] - mData[QuatY]*quat[QuatY] - mData[QuatZ]*quat[QuatZ];
    ret[QuatX] = mData[QuatW]*quat[QuatX] + mData[QuatX]*quat[QuatW] + mData[QuatY]*quat[QuatZ] - mData[QuatZ]*quat[QuatY];
    ret[QuatY] = mData[QuatW]*quat[QuatY] + mData[QuatY]*quat[QuatW] + mData[QuatW]*quat[QuatX] - mData[QuatX]*quat[QuatZ];
    ret[QuatZ] = mData[QuatW]*quat[QuatZ] + mData[QuatZ]*quat[QuatY] + mData[QuatX]*quat[QuatY] - mData[QuatY]*quat[QuatX];
    return ret;
}

Quaternion Quaternion::operator * (float scalar) const
{
    return Quaternion(mData[QuatW]*scalar, mData[QuatX]*scalar, mData[QuatY]*scalar, mData[QuatZ]*scalar);
}

Quaternion Quaternion::operator + (const Quaternion &quat) const
{
    return Quaternion(mData[QuatW] + quat[QuatW],
                      mData[QuatX] + quat[QuatX],
                      mData[QuatY] + quat[QuatY],
                      mData[QuatZ] + quat[QuatZ]);
}

float Quaternion::dotProduct(const Quaternion &a, const Quaternion &b)
{
    return a.mData[0]*b.mData[0] + a.mData[1]*b.mData[1] + a.mData[2]*b.mData[2] + a.mData[3]*b.mData[3];
}

