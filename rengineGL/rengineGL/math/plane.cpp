/**
 * \file    plane.cpp
 * Implementation of Plane class.
 */

#include <cmath>
#include "plane.h"
#include "ray.h"
#include "mathutils.h"

Plane::Plane() :
    mPosition(0.0, 0.0, 0.0),
    mNormal(0.0, 1.0, 0.0)
{

}

Plane::Plane(const Vec3 &pos, const Vec3 &normal) :
    mPosition(pos),
    mNormal(normal.normalized())
{

}

Plane::Plane(const Plane &obj) :
    mPosition(obj.mPosition),
    mNormal(obj.mNormal)
{

}

Plane::~Plane()
{

}

Plane& Plane::operator =(const Plane &obj)
{
    if (this != &obj) {
        mPosition = obj.mPosition;
        mNormal = obj.mNormal;
    }
    return *this;
}

void Plane::setPosition(const Vec3 &pos)
{
    mPosition = pos;
}

const Vec3& Plane::getPosition() const
{
    return mPosition;
}

void Plane::setNormal(const Vec3 &normal)
{
    mNormal = normal.normalized();
}

const Vec3& Plane::getNormal() const
{
    return mNormal;
}

bool Plane::intersects(const Ray &ray, double maxDistance, Vec3 *outPos) const
{
    bool ret = false;
	float denom = Vec3::dotProduct(mNormal, ray.getDir());
    if (MathUtils::isZero(denom)) {
        ret = false;
    } else {
		float d = Vec3::dotProduct(mNormal, mPosition - ray.getStart()) / denom;
        if (d > 0 && d < maxDistance) {
            ret = true;
        }

        if (ret == true && outPos) {
            *outPos = ray.getStart() + (ray.getDir() * d);
        }
    }
    return ret;
}

float Plane::distanceFromPlane(const Vec3 &point) const
{
    Vec3 deltaVec(point - mPosition);
    return Vec3::dotProduct(deltaVec, mNormal);
}

void Plane::projectPointToPlane(Vec3 &point) const
{
	float dist = distanceFromPlane(point);
    point -= mNormal * dist;
}
