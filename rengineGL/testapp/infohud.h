#pragma once

#include <vector>
#include "../core/utilities/frametimemeasurement.h"

class BaseApplication;

class FrameTimeAverage
{
public:
    float frameDurMs = 0.0f;
    float shadowmapDurMs = 0.0f;
    float geometryPassMs = 0.0f;
    float shadingPassMs = 0.0f;

    void sum( const FrameTimeMeasurement& ftm );
    void average( size_t numOfEntries );
};

class InfoHUD
{
public:
    InfoHUD( BaseApplication& app );
    ~InfoHUD();
    InfoHUD( const InfoHUD& ) = delete;
    InfoHUD& operator=( const InfoHUD& ) = delete;

public:
    void render();

private:
    void renderHUD();
    void renderCameraInfo();
    FrameTimeAverage getUpdatedFrameTimes();

private:
    BaseApplication& mApplication;
    std::vector< FrameTimeMeasurement > mFrameTimes;
    size_t mNextFrameTimeSlot;
};