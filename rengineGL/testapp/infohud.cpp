
#include "infohud.h"
#include "../ext/imgui/imgui.h"
#include "../ext/imgui/imgui_impl_sdl.h"
#include "../ext/imgui/imgui_impl_opengl3.h"
#include "../app/baseapplication.h"
#include "../core/camera.h"
#include "../core/rendercore.h"
#include "../core/scene.h"

void FrameTimeAverage::sum( const FrameTimeMeasurement& ftm )
{
	frameDurMs += static_cast< float >( ftm.getDurationMs( FrameTimeMeasurement::TimeSubEvent::FullFrame) );
	shadowmapDurMs += static_cast< float >( ftm.getDurationMs( FrameTimeMeasurement::TimeSubEvent::ShadowmapPass ) );
	geometryPassMs += static_cast< float >( ftm.getDurationMs( FrameTimeMeasurement::TimeSubEvent::GeometryPass ) );
	shadingPassMs += static_cast< float >( ftm.getDurationMs( FrameTimeMeasurement::TimeSubEvent::ShadingPass ) );
}

void FrameTimeAverage::average( size_t numOfEntries )
{
	frameDurMs /= static_cast< float >( numOfEntries );
	shadowmapDurMs /= static_cast< float >( numOfEntries );
	geometryPassMs /= static_cast< float >( numOfEntries );
	shadingPassMs /= static_cast< float >( numOfEntries );
}

InfoHUD::InfoHUD( BaseApplication& app ) :
    mApplication( app ),
	mFrameTimes( 10 ),
	mNextFrameTimeSlot( 0 )
{

}

InfoHUD::~InfoHUD()
{

}

void InfoHUD::render()
{
	// Initialize next ImGui frame.
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplSDL2_NewFrame();
	ImGui::NewFrame();

	// Draw the HUD itself.
	renderHUD();

	// Make UI elements visible.
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );
}

void InfoHUD::renderHUD()
{
	renderCameraInfo();
}

void InfoHUD::renderCameraInfo()
{
	const Vec3& camPos = mApplication.getRenderCore().getCamera()->getPosition();
	int renderPassCount = static_cast<int>(mApplication.getRenderCore().getActiveStats().getRenderPassCount());
	auto frameTimes = getUpdatedFrameTimes();
	int durFullFrame = static_cast<int>(frameTimes.frameDurMs );
	int durShadowPass = static_cast< int >( frameTimes.shadowmapDurMs );
	int durGeometryPass = static_cast< int >( frameTimes.geometryPassMs );
	int durShadingPass = static_cast< int >( frameTimes.shadingPassMs );

	ImGuiWindowFlags cameraInfoFlags =
			ImGuiWindowFlags_NoTitleBar |
			ImGuiWindowFlags_NoResize |
			ImGuiWindowFlags_NoBackground |
			ImGuiWindowFlags_NoMove;
	ImGui::SetNextWindowPos( ImVec2( 20.0f, 770.0f ), ImGuiCond_Once, ImVec2( 0.0f, 0.0f ) );
	ImGui::SetNextWindowSize( ImVec2( 800.0f, 30.0f ) );
	ImGui::Begin( "Info-line", nullptr, cameraInfoFlags );
	ImGui::Text( "Cam: %.2f, %.2f, %.2f  Render calls: %d,  Render times: %d/%d/%d/%d",
				 camPos.mX, camPos.mY, camPos.mZ, renderPassCount,
				 durFullFrame, durShadowPass, durGeometryPass, durShadingPass );
	ImGui::End();
}

FrameTimeAverage InfoHUD::getUpdatedFrameTimes()
{
	mFrameTimes[mNextFrameTimeSlot++] = mApplication.getRenderCore().getScene()->getFrameTimeMeasurements();
	if( mNextFrameTimeSlot >= mFrameTimes.size() )
	{
		mNextFrameTimeSlot = 0;
	}

	FrameTimeAverage fta;
	for( const FrameTimeMeasurement& ftm : mFrameTimes )
	{
		fta.sum( ftm );
	}
	fta.average( mFrameTimes.size() );
	return fta;
}
