
#include <SDL_keyboard.h>
#include <SDL_keycode.h>
#include "testapp.h"
#include "infohud.h"
#include "../rengineGL/math/size3.h"
#include "../rengineGL/core/meshcreation/meshcreation.h"
#include "../rengineGL/core/camera.h"
#include "../rengineGL/core/lightsources.h"
#include "../rengineGL/core/material.h"
#include "../rengineGL/core/materialmanager.h"
#include "../rengineGL/core/mesh.h"
#include "../rengineGL/core/meshmanager.h"
#include "../rengineGL/core/model.h"
#include "../rengineGL/core/modelmanager.h"
#include "../rengineGL/core/node.h"
#include "../rengineGL/core/renderable.h"
#include "../rengineGL/core/rengineexception.h"
#include "../rengineGL/core/scene.h"
#include "../rengineGL/core/texture.h"
#include "../rengineGL/core/texturemanager.h"
#include "../rengineGL/importers/terrainimporter.h"
#include "../rengineGL/importers/wavefrontimporter.h"
#include "../rengineGL/core/animators/backforthanimator.h"
#include "../rengineGL/core/animators/lightintensityanimator.h"
#include "../rengineGL/core/animators/noderotator.h"
#include "../rengineGL/core/particles/particlesparks.h"
#include "../rengineGL/core/particles/particlesystem.h"
#include "../rengineGL/utils/conversionutils.h"

TestApp::TestApp() :
	mRotation( 0.0 ),
	mSpaceBarDown( false )
{

}

TestApp::~TestApp()
{

}

void TestApp::initApplication()
{
	// Initialize custom application part by loading and preparing
	// materials, textures and models.
	try
	{
		createTextures();
		createMaterials();
		loadModels();
	}
	catch( RengineError& err )
	{
		std::string msg{ err.what() };
		std::cout << "Error: " << msg << std::endl;
		assert( false );
	}
}

void TestApp::uninitApplication()
{

}

void TestApp::prepareScene()
{
	RenderCore& core = getRenderCore();

	// Create slab around origo.
	std::shared_ptr<Renderable> slab = getRenderCore().getScene()->createRenderable( "slab1" );
	//slab->setModel( "slab" );
	slab->setModel( "main-terrain" );
	slab->getParentNode()->setPosition( Vec3( 0.0f, -0.0f, 0.0f ) );

	// Create trees
	std::shared_ptr<Renderable> tree1 = getRenderCore().getScene()->createRenderable( "tree-1" );
	tree1->setModel( "tree-model-1" );
	tree1->getParentNode()->setPosition( Vec3( 30.0f, 0.0f, 0.0f ) );
	std::shared_ptr<Renderable> tree2 = getRenderCore().getScene()->createRenderable( "tree-2" );
	tree2->setModel( "tree-model-1" );
	tree2->getParentNode()->setPosition( Vec3( -10.0f, 0.0f, 0.0f ) );
	std::shared_ptr<Renderable> tree3 = getRenderCore().getScene()->createRenderable( "tree-3" );
	tree3->setModel( "tree-model-1" );
	tree3->getParentNode()->setPosition( Vec3( 0.0f, 0.0f, 30.0f ) );
	std::shared_ptr<Renderable> tree4 = getRenderCore().getScene()->createRenderable( "tree-4" );
	tree4->setModel( "tree-model-1" );
	tree4->getParentNode()->setPosition( Vec3( 0.0f, 0.0f, -30.0f ) );
	std::shared_ptr<Renderable> tree6 = getRenderCore().getScene()->createRenderable( "tree-6" );
	tree6->setModel( "tree-model-1" );
	tree6->getParentNode()->setPosition( Vec3( 200.0f, 0.0f, -30.0f ) );

	for( int i = 0; i < 9; ++i )
	{
		float x = -100.0f + static_cast< float >( i * 20 );
		float z = 75.0f + static_cast< float >( i * 2 );
		std::string renderableId = getRenderCore().generateId( RenderCore::IDGenerator::Renderable );
		std::shared_ptr<Renderable> rend = getRenderCore().getScene()->createRenderable( renderableId );
		rend->setModel( "tree-model-1" );
		rend->getParentNode()->setPosition( Vec3( x, -1.2f, z ) );
	}

	newRenderable( "light-post-1-01", "light-post-1", Vec3( 60.0f, 0.0f, 0.0f ), Quaternion::noRotation() );
	newPointLight( "ls-01", Vec3( 60.0f, 40.0f, 0.0f ), Rgba( 1.0f, 0.7f, 0.6f ), 2000.0f );
	newRenderable( "light-post-1-02", "light-post-1", Vec3( -80.0f, 0.0f, 20.0f ), Quaternion::noRotation() );
	auto ls2 = newPointLight( "ls-02", Vec3( -80.0f, 40.0f, 20.0f ), Rgba( 0.2f, 0.3f, 0.8f ), 3000.0f );
	newRenderable( "light-post-1-03", "light-post-1", Vec3( -40.0f, 0.0f, -135.0f ), Quaternion::noRotation() );
	newPointLight( "ls-03", Vec3( -40.0f, 40.0f, -135.0f ), Rgba::White, 1500.0f );

	newRenderable( "pavement-01", "pavement-block", Vec3( -80.0f, 0.0f, -50.0f ), Quaternion::noRotation() );
	newRenderable( "rock-01-01", "rock-02", Vec3( 0.0f, 0.0f, 0.0f ), Quaternion::newAxisRotation( 0.0f, Vec3::yaxis() ) );
	newRenderable( "rock-01-02", "rock-02", Vec3( -40.0f, -0.5f, 35.0f ), Quaternion::newAxisRotation( 45.0f, Vec3::yaxis() ) );
	newRenderable( "rock-01-03", "rock-02", Vec3( -30.0f, 0.0f, 35.0f ), Quaternion::newAxisRotation( 80.0f, Vec3::yaxis() ) );
	newRenderable( "rock-01-04", "rock-02", Vec3( 0.0f, 0.0f, -100.0f ), Quaternion::newAxisRotation( 0.0f, Vec3::yaxis() ) );
	newRenderable( "rock-01-05", "rock-02", Vec3( 0.0f, 0.0f, -120.0f ), Quaternion::newAxisRotation( 0.0f, Vec3::yaxis() ) );
	newRenderable( "tree-01-01", "tree-01", Vec3( -42.0f, -2.0f, 46.0f ), Quaternion::newAxisRotation( 0.0f, Vec3::yaxis() ) );
	newRenderable( "tree-03-01", "tree-03", Vec3( -200.0f, -8.0f, -30.0f ), Quaternion::newAxisRotation( 120.0f, Vec3::yaxis() ) );
	newRenderable( "tree-03-02", "tree-03", Vec3( 10.f, -1.0f, 105.0f ), Quaternion::newAxisRotation( 5.0f, Vec3::xaxis() ) );
	newRenderable( "tree-04-01", "tree-04", Vec3( -12.0f, 0.0f, -46.0f ), Quaternion::noRotation() );
	newRenderable( "fence-01", "fence-01", Vec3( -128.0f, 3.0f, 16.0f ), Quaternion::noRotation() );
	newRenderable( "fence-cube-01", "fence-01", Vec3( 8.0f, 3.0f, 28.0f ), Quaternion::newAxisRotation( 280.0f, Vec3::yaxis() ) );
	newRenderable( "fence-cube-02", "fence-01", Vec3( 15.0f, 3.0f, 13.0f ), Quaternion::newAxisRotation( 180.0f, Vec3::yaxis() ) );
	newRenderable( "fence-cube-03", "fence-01", Vec3( 38.0f, 3.0f, 32.0f ), Quaternion::newAxisRotation( 85.0f, Vec3::yaxis() ) );
	newRenderable( "fence-cube-04", "fence-01", Vec3( 14.0f, 3.0f, 42.0f ), Quaternion::noRotation() );
	newRenderable( "building-01", "building", Vec3( -50.0f, 10.0f, 110.0f ), Quaternion::newAxisRotation(10.0f, Vec3::yaxis() ) );
	newRenderable( "flowerpot-01", "flowerpot-01", Vec3( -70.0f, 0.0f, -60.0f ), Quaternion::noRotation() );

	auto dice = newRenderable( "dice-01", "dice-model-1", Vec3( 20.0f, 13.0f, 30.0f ), Quaternion::noRotation() );
	auto diceRotator = std::make_shared<NodeRotator>( getRenderCore() );
	diceRotator->setRotation( Vec3( 1.0f, 1.0f, 0.0f ), 360.0f, 0.1f );
	diceRotator->setNode( dice->getParentNode() );
	getRenderCore().addAnimator( "dice-rotator", diceRotator );

	// Create lightsources.
#if 1
	auto ls03 = getRenderCore().getScene()->createLightSource( "ls-04", nullptr );
	ls03->makePointLightSource(
			Vec3( -50.0f, 15.0f, -100.0f ),
			Rgba( 0.6f, 0.5f, 0.0f ),
			3000.0f );
	std::shared_ptr<BackAndForthAnimator> anim3 = std::make_shared<BackAndForthAnimator>( getRenderCore() );
	anim3->setMovementRange( Vec3( 150.0f, 15.0f, -100.0f ), Vec3( 150.0f, 15.0f, 100.0f ), 25.0f );
	anim3->setNode( ls03->getParentNode() );
	getRenderCore().addAnimator( "anim3", anim3 );
	ls03->showInScene( getRenderCore().getScene() );
#endif

	mRenderCore->getScene()->getSkyBox().setEnabled( true );

	// Create the HUD objects.
	mInfoHUD.reset( new InfoHUD( *this ) );
}

void TestApp::update( double timeSincePrevious )
{
	// Move the "cursor" object to look at position.
	Vec3 lookat = getRenderCore().getCamera()->getLookAt();

	// Check the keyboard commands.
	checkKeyboard();

	// Call the base class because that takes care of updating the rendering engine itself.
	BaseApplication::update( timeSincePrevious );
}

void TestApp::renderGUI()
{
	if( mInfoHUD ) {
		mInfoHUD->render();
	}
}

void TestApp::checkKeyboard()
{
	// Get the current keyboard state.
	const float KMovementSpeed = 0.35f;
	const float KRotationSpeed = 0.7f;
	const uint8_t* keyboardState = SDL_GetKeyboardState( NULL );

	// Exit if ESC is pressed.
	if( keyboardState[ SDL_SCANCODE_ESCAPE ] )
	{
		requestApplicationExit();
	}

	// Move the camera if WASD keys are pressed.
	std::shared_ptr<Camera> camera = getRenderCore().getCamera();
	if( keyboardState[ SDL_SCANCODE_W ] )
	{
		camera->moveForward( KMovementSpeed );
	}
	else if( keyboardState[ SDL_SCANCODE_S ] )
	{
		camera->moveForward( -KMovementSpeed );
	}
	if( keyboardState[ SDL_SCANCODE_A ] )
	{
		camera->moveSideways( KMovementSpeed );
	}
	else if( keyboardState[ SDL_SCANCODE_D ] )
	{
		camera->moveSideways( -KMovementSpeed );
	}
	if( keyboardState[ SDL_SCANCODE_Q ] )
	{
		camera->move( Vec3( 0.0f, KMovementSpeed, 0.0f ), true );
	}
	else if( keyboardState[ SDL_SCANCODE_E ] )
	{
		camera->move( Vec3( 0.0f, -KMovementSpeed, 0.0f ), true );
	}

	// Rotate camera.
	if( keyboardState[ SDL_SCANCODE_LEFT ] )
	{
		camera->rotateLookAt( Quaternion::newAxisRotation( KRotationSpeed, Vec3::yaxis() ) );
	}
	else if( keyboardState[ SDL_SCANCODE_RIGHT ] )
	{
		camera->rotateLookAt( Quaternion::newAxisRotation( -KRotationSpeed, Vec3::yaxis() ) );
	}
	if( keyboardState[ SDL_SCANCODE_UP ] )
	{
		camera->rotateLookAt( Quaternion::newAxisRotation( -KRotationSpeed, camera->getRightVec() ) );
	}
	else if( keyboardState[ SDL_SCANCODE_DOWN ] )
	{
		camera->rotateLookAt( Quaternion::newAxisRotation( KRotationSpeed, camera->getRightVec() ) );
	}

	// Move with gamecontroller.
	Vec2 leftStick = getGameController().getLeftStick();
	Vec2 rightStick = getGameController().getRightStick();
	camera->moveForward( -leftStick.mY * KMovementSpeed );
	camera->moveSideways( -leftStick.mX * KMovementSpeed );
	camera->rotateLookAt( Quaternion::newAxisRotation( -rightStick.mX * KRotationSpeed, Vec3::yaxis() ) );
	camera->rotateLookAt( Quaternion::newAxisRotation( rightStick.mY * KRotationSpeed, camera->getRightVec() ) );

	// Spacebar key.
	if( keyboardState[ SDL_SCANCODE_SPACE ] )
	{
		// Space bar is pressed.
		if( ! mSpaceBarDown )
		{
			// Process the space bar press.
			processSpaceBarPressed();
		}

		// Set the indicator flag.
		mSpaceBarDown = true;
	}
	else
	{
		// Space bar is not pressed. Reset the flag.
		mSpaceBarDown = false;
	}
}

void TestApp::loadModels()
{
	MaterialManager &matman = getRenderCore().getMaterialManager();
	MeshManager &mesman = getRenderCore().getMeshManager();
	ModelManager &modman = getRenderCore().getModelManager();

	// Instantiate the importer.
	WaveFrontImporter importer( getRenderCore(), "assets://models/textures" );

	// Create the ground slab.
	VertexLayout layoutSlab = VertexLayout::getDefaultLayout_DeferredShading();
	auto texturerange = MeshCreation::UVTextureRange::repeatTexture( 32.0f, 32.0f );
	std::shared_ptr<Mesh> mesh = MeshCreation::createQuad( getRenderCore(), "slab", layoutSlab, FaceSides::SingleSide, 800, 800, texturerange );
	mesh->setTexture2D( 0, 1, Vec2( 0.0f, 0.0f ) );
	mesh->setTexture2D( 1, 1, Vec2( 32.0f, 0.0f ) );
	mesh->setTexture2D( 2, 1, Vec2( 32.0f, 32.0f ) );
	mesh->setTexture2D( 3, 1, Vec2( 0.0f, 32.0f ) );
	Quaternion rotQuat = Quaternion::newAxisRotation( -90.0f, Vec3( 1.0f, 0.0f, 0.0f ) );
	mesh->rotateVertices( rotQuat.toRotationMatrix3() );
	mesh->moveVertices( Vec3( 0.0f, 0.0f, 0.0f ) );
	std::shared_ptr<Model> slabModel = getRenderCore().getModelManager().createModel( "slab" );
	slabModel->addSection( mesh, getRenderCore().getMaterialManager().findMaterial( "ground-slab-material" ) );

	// Create tree meshes and materials.
	std::shared_ptr<Material> treeBodyMaterial = getRenderCore().getMaterialManager().createMaterial( "tree-body-material" );
	treeBodyMaterial->setSolidColor( Rgba( 0.7f, 0.4f, 0.1f ) );
	std::shared_ptr<Material> treeLeavesMaterial = getRenderCore().getMaterialManager().createMaterial( "tree-leaves-material" );
	treeLeavesMaterial->setSolidColor( Rgba( 0.0f, 1.0f, 0.0f ) );
	VertexLayout treeMeshLayout = VertexLayout::getDefaultLayout_DeferredShading();
	float treeHeight = 20.0f;
	std::shared_ptr<Mesh> treeBodyMesh = MeshCreation::createCylinder( getRenderCore(), "tree-body-1", treeMeshLayout, 0.8f, 1.0f, treeHeight, 30 );
	treeBodyMesh->setTextureCoordinatesToDisabled( VertexLayout::KNormalmapTextureIndex );
	treeBodyMesh->moveVertices( Vec3( 0.0f, treeHeight / 2.0f, 0.0f ) );
	std::shared_ptr<Mesh> treeLeavesMesh = MeshCreation::createSolidColorCone( getRenderCore(), "tree-leaves-1", treeMeshLayout, 3.0f, treeHeight, 32, true );
	treeLeavesMesh->setTextureCoordinatesToDisabled( VertexLayout::KNormalmapTextureIndex );
	treeLeavesMesh->moveVertices( Vec3( 0.0f, treeHeight, 0.0f ) );
	std::shared_ptr<Model> treeModel = modman.createModel( "tree-model-1" );
	treeModel->addSection( treeBodyMesh, treeBodyMaterial );
	treeModel->addSection( treeLeavesMesh, treeLeavesMaterial );

	// Load human.
	//WaveFrontImporter importer( getRenderCore() );
	importer.setScalingFactor( 5.0f );
	auto modelSharedPointer = importer.importModel(
			"assets://models/human/human.wfobj",
			"human",
			true );

	// Import house.
	importer.setScalingFactor( 8.0f );
	auto modelHouse = importer.importModel(
			"assets://models/building.obj",
			"building",
			true );

	// Import lamp.
	importer.setScalingFactor( 4.0f );
	auto modelLamp = importer.importModel(
			"assets://models/lamppost.obj",
			"light-post-1",
			true );

	// Import rock.
	auto modelRock = importer.importModel(
			"assets://models/rock-02.obj",
			"rock-02",
			true );

	// Import tree.
	auto modelTree = importer.importModel(
			"assets://models/tree-01.obj",
			"tree-01",
			true );
	auto modelTree3 = importer.importModel(
			"assets://models/tree-03.obj",
			"tree-03",
			true );
	auto modelTree4 = importer.importModel(
			"assets://models/tree-04.obj",
			"tree-04",
			true );
	importer.setScalingFactor( 4.0f );
	auto fence1 = importer.importModel(
			"assets://models/fence-01.obj",
			"fence-01",
			true );
	importer.setScalingFactor( 7.0f );
	auto flowerpot = importer.importModel(
			"assets://models/flowerpot.obj",
			"flowerpot-01",
			true );

	// Create dice with material.
	{
		auto diceMaterial1 = getRenderCore().getMaterialManager().createMaterial( "dice-white-material" );
		diceMaterial1->setTexture( 0, "dice-512x256.jpg" );
		diceMaterial1->setTexture( 1, "bricks_norm.JPG" );
		VertexLayout layout = VertexLayout::getDefaultLayout_DeferredShading();
		std::shared_ptr<Mesh> diceMesh = MeshCreation::createCuboidNotTextured(
				getRenderCore(), "dice-mesh", layout, Size3( 10.0f, 10.0f, 10.0f ), false );
		MeshCreation::UVTextureRange uvt = MeshCreation::UVTextureRange::mapPixelsToUV( 128.0f, 255.0f, 127.0f, 0.0f, 512.0f, 256.0f );
		MeshCreation::wrapCuboidTexture( diceMesh,
				0,
				MeshCreation::UVTextureRange::mapPixelsToUV( 127.0f, 0.0f, 127.0f, 0.0f, 512.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 383.0f, 256.0f, 255.0f, 128.0f, 512.f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 127.0f, 0.0f, 255.0f, 128.0f, 512.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 383.0f, 256.0f, 127.0f, 0.0f, 512.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 255.0f, 128.0f, 127.0f, 0.0f, 512.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 255.0f, 128.0f, 255.0f, 128.0f, 512.0f, 256.0f ) );
		auto diceNormalMapCoords = MeshCreation::UVTextureRange::repeatTexture( 3.0f, 3.0f );
		MeshCreation::wrapCuboidTexture( diceMesh,
				1,
				diceNormalMapCoords,
				diceNormalMapCoords,
				diceNormalMapCoords,
				diceNormalMapCoords,
				diceNormalMapCoords,
				diceNormalMapCoords );

		// Model 1.
		auto diceModel1 = getRenderCore().getModelManager().createModel( "dice-model-1" );
		diceModel1->addSection( diceMesh, diceMaterial1 );

		// Terrain
		TerrainImporterParameters terrainParams;
		terrainParams.setAltitudeRange( -2.5f, 8.0f );
		terrainParams.setSize( 500.0f, 500.0f );
		terrainParams.setTextureWrapCount( 32.0f, 32.0f );
		std::string heightMapFile = "assets://heightmaps/testheight_grey_256x256.png";
		TerrainImporter terrainImporter( getRenderCore() );
		auto terrainMesh = terrainImporter.createFromHeightMap( heightMapFile, terrainParams, "main-terrain" );
		auto terrainModel = getRenderCore().getModelManager().createModel( "main-terrain" );
		auto terrainMaterial = getRenderCore().getMaterialManager().findMaterial( "ground-slab-material" );
		terrainModel->addSection( terrainMesh, terrainMaterial );
	}

	// Create pavement block.
	{
		auto mat = getRenderCore().getMaterialManager().createMaterial( "pavement-block" );
		mat->setTexture( 0, "pavement.JPG" );
		mat->setTexture( 1, "pavement_norm.JPG" );
		auto layout = VertexLayout::getDefaultLayout_DeferredShading();
		auto pavementMesh = MeshCreation::createCuboidNotTextured(
				getRenderCore(), "pavement-mesh", layout, Size3( 120.0f, 0.1f, 120.0f ), false );
		MeshCreation::wrapCuboidTexture( 
				pavementMesh,
				0,
				MeshCreation::UVTextureRange::mapPixelsToUV( 0.0f, 512.0f, 0.0f, 88.0f, 256.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 0.0f, 512.0f, 0.0f, 88.0f, 256.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 0.0f, 512.0f, 0.0f, 512.0f, 256.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 512.0f, 0.0f, 512.0f, 0.0f, 256.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 0.0f, 512.0f, 0.0f, 88.0f, 256.0f, 256.0f ),
				MeshCreation::UVTextureRange::mapPixelsToUV( 0.0f, 512.0f, 0.0f, 88.0f, 256.0f, 256.0f ) );
		auto normalMapCoords = MeshCreation::UVTextureRange::repeatTexture( 3.0f, 3.0f );
		MeshCreation::wrapCuboidTexture(
				pavementMesh,
				1,
				normalMapCoords,
				normalMapCoords,
				normalMapCoords,
				normalMapCoords,
				normalMapCoords,
				normalMapCoords );
		auto pavementModel = getRenderCore().getModelManager().createModel( "pavement-block" );
		pavementModel->addSection( pavementMesh, mat );
	}
}

void TestApp::createMaterials()
{
	// The "wet concrete slab" material.
	std::shared_ptr<Material> groundMaterial = getRenderCore().getMaterialManager().createMaterial( "ground-slab-material" );
	groundMaterial->setTexture( 0, "grass-2.JPG" );
	groundMaterial->setTexture( 1, "grass-2_norm.JPG" );
	groundMaterial->setSpecularColor( SpecularColor( Rgba::White, 1.0f ) );
}

void TestApp::createTextures()
{
	// Load the general textures by using the xml-based texture list.
	getRenderCore().getTextureManager().loadTexturesFromListFile( "assets://textures/texturelist.xml" );

	// Skybox texture. The engine tries to use texture that is named 'custom-skybox'. Here we load the
	// texture called 'custom-skybox'.
	std::shared_ptr<Texture> texture;
	texture = getRenderCore().getTextureManager().createTexture( "custom-skybox", "assets://textures/skybox3.png" );
	texture->setWrapMode( Texture::Wrap::ClampToEdge, Texture::Wrap::ClampToEdge );
}

void TestApp::processSpaceBarPressed()
{
	// The coordinate of camera's focus point.
	Vec3 lookat = getRenderCore().getCamera()->getLookAt();

	// Spawn particles there.
	std::string particleSystemId = getRenderCore().generateId( RenderCore::IDGenerator::Particles );
	std::shared_ptr<Scene> scene = getRenderCore().getScene();
	ParticleSystem* ps = scene->createParticleSystem(
			particleSystemId, KParticleSystemTypeSparks, scene->getRootNode() );
	if( ps != nullptr )
	{
		// Spark system.
		ParticleSparks* sparks = dynamic_cast< ParticleSparks* >( ps );
		sparks->setParticleSystemPosition( lookat );
		sparks->initializeSparks( Vec3( -60.0f, 0.0f, 0.0f ) );
	}
}

std::shared_ptr<Renderable> TestApp::newRenderable(
		const std::string& renderableName,
		const std::string& modelId,
		const Vec3& pos,
		const Quaternion& rot )
{
	auto rend = getRenderCore().getScene()->createRenderable( renderableName );
	rend->setModel( modelId );
	rend->getParentNode()->setPosition( pos );
	rend->getParentNode()->setRotation( rot );
	return rend;
}

std::shared_ptr<LightSource> TestApp::newPointLight(
		const std::string& lightsourceId,
		const Vec3& lightPos,
		const Rgba& lightColor,
		float lightIntensity )
{
	auto ls = getRenderCore().getScene()->createLightSource( lightsourceId, nullptr );
	ls->makePointLightSource(
			lightPos,
			lightColor,
			lightIntensity );
	return ls;
}
