
#pragma once

#include <memory>
#include "../rengineGL/app/baseapplication.h"

// Forward declarations.
class InfoHUD;
class LightSource;
class Quaternion;
class Renderable;

/// <summary>
/// A test application for rengineGL development purposes.
/// </summary>
class TestApp : public BaseApplication
{
	public:
		/// <summary>
		/// Default constructor.
		/// </summary>
		TestApp();

		/// <summary>
		/// Destructor.
		/// </summary>
		virtual ~TestApp();

		/// <summary>
		/// Disabled copy constructor.
		/// </summary>
		TestApp( const TestApp &obj ) = delete;

		/// <summary>
		/// Disabled assignment operator.
		/// </summary>
		TestApp &operator=( const TestApp &obj ) = delete;

	public:  // from BaseApplication

		/// <summary>From BaseApplication::initApplication()</summary>
		virtual void initApplication() override;

		/// <summary>From BaseApplication::uninitApplication()</summary>
		virtual void uninitApplication() override;

		/// <summary>From BaseApplication::prepareScene()</summary>
		virtual void prepareScene() override;

		/// <summary>From BaseApplication::update()</summary>
		virtual void update( double timeSincePrevious ) override;

		/// <summary>From BaseApplication::renderGUI</summary>
		virtual void renderGUI() override;

	private:

		/// <summary>
		/// Checks the pressed keys and moves camera as needed.
		/// </summary>
		void checkKeyboard();

		/// <summary>
		/// Loads the 3D models for this application from files.
		/// <summary>
		void loadModels();

		/// <summary>
		/// Creates the materials for the application.
		/// </summary>
		void createMaterials();

		/// <summary>
		/// Creates and loads the textures.
		/// </summary>
		void createTextures();

		/// <summary>
		/// Handles the action when space bar is pressed.
		/// </summary>
		void processSpaceBarPressed();

		std::shared_ptr<Renderable> newRenderable( const std::string& renderableName,
				const std::string& modelId,
				const Vec3& pos,
				const Quaternion& rot );
		std::shared_ptr<LightSource> newPointLight(const std::string& lightsourceId,
				const Vec3& lightPos,
				const Rgba& lightColor,
				float lightIntensity );

	private:

		/// <summary>Directional sunlight rotation angle.</summary>
		double mRotation;

		/// <summary>Flag that tells if space bar was pressed on previous update round</summary>
		bool mSpaceBarDown;

		std::unique_ptr< InfoHUD > mInfoHUD;
};
