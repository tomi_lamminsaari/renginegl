#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes.
	layout(location=0) in vec3 aPos;
	layout(location=1) in vec2 aTexCoord;

	// Vertex shader output variables.
	out vec2 vTextureCoord;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;

	// The vertex shader application.
	void main()
	{
		// Output the texture coordinate.
		vTextureCoord = aTexCoord;

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER
	
	// Fragment shader's output variable.
	out vec4 frag_color;

	// Input parameters from vertex shader.
	in vec2 vTextureCoord;

	// Texture uniforms.
	uniform sampler2D uTexture0;

	void main()
	{

		// Get the base diffuse color from the texture.
		vec4 texelColor = texture( uTexture0, vTextureCoord );

		// Set the output value.
		frag_color = texelColor;
	}

#endif
