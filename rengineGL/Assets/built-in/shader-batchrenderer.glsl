#version 400

#define LIGHTSOURCE_COUNT %LIGHTSOURCE_COUNT%
#define FLOATS_PER_LIGHTSOURCE %LIGHTSOURCE_FLOAT_COUNT%
#define LIGHT_ATTENUATION %LIGHT_ATTENUATION%

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout(location=0) in vec3 aPos;
	layout(location=1) in vec3 aNormal;
	layout(location=2) in vec2 aTexCoord;
	layout(location=3) in vec3 aColor;
	layout(location=4) in float aTextureIndex;

	// Vertex shader output variables.
	out vec3 vVertex;
	out vec3 vNormal;
	out vec2 vTextureCoord;
	out vec3 vColor;
	out float vTextureIndex;
	out vec4 vShadowMapCoords;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;
	uniform mat4 uLightSourceMVP;

	// The vertex shader application.
	void main()
	{
		// Output the texture coordinates, color and texture index.
		vTextureCoord = aTexCoord;
		vColor = aColor;
		vTextureIndex = aTextureIndex;

		// Calculate vertex location in world coordinates. Pass it to the fragment shader.
		vVertex = vec3( uM * vec4( aPos, 1.0 ) );
		
		// Project the vertex coordinate to light space. The uLightSourceMVP has also
		// the object's model matrix applied so vertex gets also translated to world
		// coordintanes as well.
		vShadowMapCoords = uLightSourceMVP * vec4( aPos, 1.0 );

		// Calculate vertex normal in world coordinates. Pass it to the fragment shader.
		vNormal = vec3( uM * vec4( aNormal, 0.0 ) );

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	out vec4 frag_color;

	// Input parameters from vertex shader.
	in vec3 vVertex;
	in vec3 vNormal;
	in vec2 vTextureCoord;
	in vec3 vColor;
	in float vTextureIndex;
	in vec4 vShadowMapCoords;

	// Texture uniforms.
	uniform sampler2D uTexture0;
	uniform sampler2D uTexture1;
	uniform sampler2D uTexture2;
	uniform sampler2D uTexture3;
	uniform sampler2D uTexture4;
	uniform sampler2D uTexture5;
	uniform sampler2D uTexture6;
	uniform sampler2D uTexture7;
	uniform sampler2D uTexture8;
	uniform sampler2D uTexture9;
	uniform sampler2D uTexture10;
	uniform sampler2D uTexture11;
	uniform sampler2D uTexture12;
	uniform sampler2D uTexture13;
	uniform sampler2D uTexture14;
	uniform sampler2D uTexture15;

	// Generic uniforms.
	uniform vec3 uEyePos;
	
	// Material related uniforms.
	uniform vec4 uSpecularColor;
	uniform float uSpecularExponent;
	uniform int uSpecularEnabled;

	// Ambient light related uniforms.
	uniform vec4 uAmbientColor;
	uniform float uAmbientIntensity;

	// Diffuse light uniforms
	uniform float uDiffuseLightArray[ FLOATS_PER_LIGHTSOURCE * LIGHTSOURCE_COUNT ];  // Array for multiple light sources.
	
	// Shadowmap texture slot.
	uniform sampler2D uShadowMap;

	// Include the shadowmap functions.
	#include "assets://built-in/inc-shadowmap-func.glsl"
	// End of shadowmap functions

	// Include the lightsourcearray functions.
	#include "assets://built-in/inc-lights.glsl"
	// End of lightsourcearray functions.

	// The fragment shader application.
	void main()
	{
		// Calculate vector from vertex location to the diffuse light sources. And get the
		// vertex normal vector.
		vec3 vertexNormal = normalize( vNormal );
		vec3 cameraPos = uEyePos;  // After transformations camera is in the origo.
		vec3 surfaceToCamera = normalize( cameraPos - vVertex );

		// Determine the amount of light the fragment receives.
		//float shadowmapLightAmount = shadowmap_calculateLightAmount( uShadowMap, vShadowMapCoords );
		float shadowmapLightAmount = 1.0;
		
		// Get the base diffuse color from the texture or from given color value.
		vec4 texelColor;
		int textureIndex = int( vTextureIndex );
		if( textureIndex >= 0 )
		{
			if( textureIndex < 8 )
			{
				if( textureIndex < 4 )
				{
					if( textureIndex == 0 )
						texelColor = texture( uTexture0, vTextureCoord );
					else if( textureIndex == 1 )
						texelColor = texture( uTexture1, vTextureCoord );
					else if( textureIndex == 2 )
						texelColor = texture( uTexture2, vTextureCoord );
					else
						texelColor = texture( uTexture3, vTextureCoord );
				}
				else
				{
					if( textureIndex == 4 )
						texelColor = texture( uTexture4, vTextureCoord );
					else if( textureIndex == 5 )
						texelColor = texture( uTexture5, vTextureCoord );
					else if( textureIndex == 6 )
						texelColor = texture( uTexture6, vTextureCoord );
					else
						texelColor = texture( uTexture7, vTextureCoord );
				}
			}
			else
			{
				if( textureIndex < 12 )
				{
					if( textureIndex == 8 )
						texelColor = texture( uTexture8, vTextureCoord );
					else if( textureIndex == 9 )
						texelColor = texture( uTexture9, vTextureCoord );
					else if( textureIndex == 10 )
						texelColor = texture( uTexture10, vTextureCoord );
					else
						texelColor = texture( uTexture11, vTextureCoord );
				}
				else
				{
					if( textureIndex == 12 )
						texelColor = texture( uTexture12, vTextureCoord );
					else if( textureIndex == 13 )
						texelColor = texture( uTexture13, vTextureCoord );
					else if( textureIndex == 14 )
						texelColor = texture( uTexture14, vTextureCoord );
					else
						texelColor = texture( uTexture15, vTextureCoord );
				}
			}
		}
		else
		{
			// No texture. Use color from color attribute.
			texelColor = vec4( vColor, 1.0 );
		}

		// Calculate the diffusion color amount of primary shadow casting light source.
		vec4 diffuseSunlight = calculateDiffuseColor(
									vec3(	uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_LIGHT_X ],
											uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_LIGHT_Y ],
											uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_LIGHT_Z ] ),
									vVertex,
									vertexNormal,
									uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_INTENSITY ],
									vec4(	uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_COLOR_R ],
											uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_COLOR_G ],
											uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_COLOR_B ],
											1.0 ),
									texelColor );

		// This shader supports multiple light sources. Loop all lightsources through
		// and calculate the cumulative diffuse and specular light amounts. Start from the
		// second lightsource in lightsources array.
		vec4 diffuseLightCumulative = vec4( 0.0, 0.0, 0.0, 0.0 );
		int baseIndex = FLOATS_PER_LIGHTSOURCE;
		for( int i = 1; i < LIGHTSOURCE_COUNT; ++i )
		{
			if( uDiffuseLightArray[ baseIndex + LIGHTARRAY_INTENSITY ] > 0.0 )
			{
				// Get the normal vector from current fragment towards the light source.
				vec3 toLightsource = normalize( vec3( uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_LIGHT_X ],
													  uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_LIGHT_Y ],
													  uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_LIGHT_Z ] ) - vVertex );

				// Calculate the diffuse light from the lightsource.
				diffuseLightCumulative += calculateDiffuseColorWithNormal(
						toLightsource,
						vertexNormal,
						uDiffuseLightArray[ baseIndex + LIGHTARRAY_INTENSITY ],
						vec4( uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_COLOR_R ],
							  uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_COLOR_G ],
							  uDiffuseLightArray[ baseIndex + LIGHTARRAY_DIFFUSE_COLOR_B ],
							  1.0 ),
						texelColor );
			}

			// Move to next lightsource.
			baseIndex += FLOATS_PER_LIGHTSOURCE;
		}

		// Apply the shadowmap value to diffuse and specular colors.
		vec4 diffuseColor = diffuseLightCumulative * shadowmapLightAmount;

		// Calculate the ambient color amount.
		vec4 ambientColor = ( uAmbientIntensity * uAmbientColor ) * texelColor;
		ambientColor += shadowmapLightAmount * diffuseSunlight;

		// Combine the ambient and diffuse light amounts.
		frag_color = clamp( ambientColor + diffuseColor, 0.0, 1.0 );
		frag_color.a = 1.0;
	}
	
#endif
