
/** The shadow bias amount to remove shadow acne. */
const float SHADOWMAP_BIAS = 0.0005;

/** The shadow dynamics range. The shadow multiplier will vary between [1.0 - this value] and 1.0*/
const float SHADOWMAP_AMOUNT_OF_LIGHT_IN_SHADOW = 0.7;

/**
* Calculates the amount of light the shadow map coordinate receives. The given shadow map
* must contain the depth texture.
* @param	shadowMap = The shadowmap sampler.
* @param	initialShadowMapCoord = Shadow map coordinate to access.
* @return	Returns 1.0 if fragment is entirely in light. Returns AMOUNT_OF_LIGHT_IN_SHADOW constant if fragment
*			entirely in shadow.
*/
float shadowmap_calculateLightAmount( sampler2D shadowMap, vec4 initialShadowMapCoord )
{
	// Get the relative size of a single texel.
	vec2 texelSize = 1.0 / textureSize( shadowMap, 0 );

	// Calculate the shadowmap coordinate from where we will read the shadowmap texel. The z-value
	// of this coordinate is the depth value of fragment that is being rendered at the moment.
	vec3 shadowMapCoord = ( ( initialShadowMapCoord.xyz / initialShadowMapCoord.w ) * 0.5 ) + vec3( 0.5 );

/***
	// Sample the shadowmap texture. Soften the shadow edges by multi-sampling the shadow map.
	float shadowLevel = 0.0;  // Start from 0.0 level, which means that fragment is not in shadow.
	for( int x = -1; x <= 1; ++x )
	{
		for( int y = -1; y <= 1; ++y )
		{
			float depthValue = texture( shadowMap, shadowMapCoord.xy + vec2( x, y ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue < shadowMapCoord.z ? 1.0 : 0.0;
		}
	}
	shadowLevel /= 9.0;
***/
	
	// Check if fragment is in shadow.
	float depthValue = texture( shadowMap, shadowMapCoord.xy ).r + SHADOWMAP_BIAS;
	float shadowLevel = depthValue < shadowMapCoord.z ? 1.0 : 0.0;
	shadowLevel *= ( 1.0 - SHADOWMAP_AMOUNT_OF_LIGHT_IN_SHADOW );
	return 1.0 - shadowLevel;
}
