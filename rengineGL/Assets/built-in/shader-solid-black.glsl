#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes.
	layout(location=0) in vec3 aPos;
	
	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;

	// The vertex shader application.
	void main()
	{
		// Define the 4-dimensional vertex position.
		vec4 vp = vec4( aPos, 1.0 );

		// Transform the vertex coordinate to perspect projected coordinate.
		gl_Position = uMVP * vp;
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	out vec4 frag_color;

	// The fragment shader application.
	void main()
	{
		frag_color = vec4( 0.0, 0.0, 0.0, 1.0 );
	}

#endif
