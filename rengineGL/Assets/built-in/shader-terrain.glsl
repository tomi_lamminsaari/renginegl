#version 400

#define LIGHTSOURCE_COUNT %LIGHTSOURCE_COUNT%
#define FLOATS_PER_LIGHTSOURCE %LIGHTSOURCE_FLOAT_COUNT%

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout(location=0) in vec3 aPos;
	layout(location=1) in vec3 aNormal;
	layout(location=2) in vec2 aTexCoord;

	// Vertex shader output variables.
	out vec3 vVertex;
	out vec3 vNormal;
	out vec2 vTextureCoord;
	out vec4 vShadowMapCoords;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;
	uniform mat4 uLightSourceMVP;

	// The vertex shader application.
	void main()
	{
		// Output the vertex coordinate.
		vTextureCoord = aTexCoord;

		// Calculate vertex location in world coordinates. Pass it to the fragment shader.
		vVertex = vec3( uM * vec4( aPos, 1.0 ) );
		
		// Project the vertex coordinate to light space. The uLightSourceMVP has also
		// the object's model matrix applied so vertex gets also translated to world
		// coordintanes as well.
		vShadowMapCoords = uLightSourceMVP * vec4( aPos, 1.0 );

		// Calculate vertex normal in world coordinates. Pass it to the fragment shader.
		vNormal = vec3( uM * vec4( aNormal, 0.0 ) );

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	out vec4 frag_color;

	// Input parameters from vertex shader.
	in vec3 vVertex;
	in vec3 vNormal;
	in vec2 vTextureCoord;
	in vec4 vShadowMapCoords;

	// Texture uniforms.
	uniform sampler2D uTerrainTex0;
	uniform sampler2D uTerrainTex1;
	uniform sampler2D uTerrainTex2;
	uniform sampler2D uTerrainTex3;
	uniform sampler2D uBlendMap;  // Blendmap for mixing different terrain types.
	uniform sampler2D uShadowMap;  // Shadowmap texture for dynamics lighting.

	// Uniforms that define how many times the textures will be tiled.
	uniform float uTexture0TileCount;
	uniform float uTexture1TileCount;
	uniform float uTexture2TileCount;
	uniform float uTexture3TileCount;

	// Ambient light related uniforms.
	uniform vec4 uAmbientColor;
	uniform float uAmbientIntensity;

	// Diffuse light uniforms
	uniform float uDiffuseLightArray[ FLOATS_PER_LIGHTSOURCE * LIGHTSOURCE_COUNT ];  // Array for multiple light sources.
	
	// Include the shadowmap functions.
	#include "assets://built-in/inc-shadowmap-func.glsl"
	// End of shadowmap functions


	// The fragment shader application.
	void main()
	{
		vec3 toLight;
		vec3 vertexNormal;

		// Determine the amount of light the fragment receives.
		float shadowmapLightAmount = shadowmap_calculateLightAmount( uShadowMap, vShadowMapCoords );

		// Get the blend map color that defines the amounts of different terrain types
		// get blend together to form the final terrain color.
		vec4 blendMapColor = texture( uBlendMap, vTextureCoord );

		// Get the terrain colors from different terrain type textures. We multiply the
		// texture coordinate with tiling factor. This means that when vTextureCoord gets
		// interpolated from [0.0, 0.0] to [1.0, 1.0], the terrain type texture gets
		// interpolated from [0.0, 0.0] to [ uTextureXTileCount, uTextureXTileCount].
		vec4 terrain0Color = texture( uTerrainTex0, vTextureCoord * uTexture0TileCount );
		vec4 terrain1Color = texture( uTerrainTex1, vTextureCoord * uTexture1TileCount );
		vec4 terrain2Color = texture( uTerrainTex2, vTextureCoord * uTexture2TileCount );
		vec4 terrain3Color = texture( uTerrainTex3, vTextureCoord * uTexture3TileCount );

		// Mix the terrain colors together with the relation defined by the blend map.
		float terrain0Amount = 1.0 - blendMapColor.x - blendMapColor.y - blendMapColor.z;
		vec4 texelColor;
		texelColor = ( terrain0Color * terrain0Amount ) +
					( terrain1Color * blendMapColor.x ) +
					( terrain2Color * blendMapColor.y ) +
					( terrain3Color * blendMapColor.z );


		// Calculate vector from vertex location to the diffuse light source. And get the
		// vertex normal vector.
//		toLight = uDiffuseLightPos - vVertex;
//		toLight = normalize( toLight );
		vertexNormal = normalize( vNormal );
		vec4 diffuseColor = vec4( 0.0, 0.0, 0.0, 0.0 );  // = diffuseColor;
		int baseIndex = 0;
		for( int i = 0; i < LIGHTSOURCE_COUNT; ++i )
		{
			// Calculate lightsource specific diffuse color.
			toLight = vec3( uDiffuseLightArray[ baseIndex + 0 ],
							uDiffuseLightArray[ baseIndex + 1 ],
							uDiffuseLightArray[ baseIndex + 2 ] ) - vVertex;
			toLight = normalize( toLight );
			float diffuseCosAngle = dot( vertexNormal, toLight );
			diffuseCosAngle = clamp( diffuseCosAngle, 0.0, 1.0 );
			vec4 tmpDiffuseColor = vec4( uDiffuseLightArray[ baseIndex + 3 ],
							 uDiffuseLightArray[ baseIndex + 4 ],
							 uDiffuseLightArray[ baseIndex + 5 ],
							 1.0 );
			diffuseColor += ( uDiffuseLightArray[ baseIndex + 6 ] * tmpDiffuseColor ) * ( texelColor * diffuseCosAngle );

			// Terrain does not have specular light.

			// Move to next lightsource.
			baseIndex += FLOATS_PER_LIGHTSOURCE;
		}

		// Calculate the amvient color amount.
		vec4 ambientColor;
		ambientColor = ( uAmbientIntensity * uAmbientColor ) * texelColor;

		// Combine the ambient and diffuse light amounts.
		frag_color = clamp( ambientColor + diffuseColor * shadowmapLightAmount, 0.0, 1.0 );
		frag_color.a = 1.0;
	}
	
#endif
