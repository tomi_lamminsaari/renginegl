#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes.
	layout(location=0) in vec3 aPos;
	layout(location=1) in vec4 aColor;
	
	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;

	// Output variables.
	out vec4 vColor;

	// The vertex shader application.
	void main()
	{
		// Pass color data to fragment shader.
		vColor = aColor;

		// Transform the vertex coordinate to perspect projected coordinate.
		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Input variables.
	in vec4 vColor;

	// Fragment shader's output variable.
	out vec4 frag_color;

	// The fragment shader application.
	void main()
	{
		frag_color = vColor;
	}

#endif
