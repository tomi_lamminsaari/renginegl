#version 400

// This shader is will render textured mesh without any transformation.

#ifdef VERTEX_SHADER

	layout(location=0) in vec3 aPos;  // The vertex coordinate.
	layout(location=1) in vec2 aTexCoord;  // The texture coordinate of the vertex.
	
	out vec2 vTexCoord;

	uniform vec3 uOffsetPosition;

	void main()
	{
		// Output the texture coordinate.
		vTexCoord = aTexCoord;
		
		// Output the vertex location.
		vec3 vertexPos = aPos + uOffsetPosition;
		gl_Position = vec4( vertexPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	out vec4 frag_color;  // Fragment shader's output color.
	
	in vec2 vTexCoord;
	
	uniform sampler2D uTexture0;
	
	void main()
	{
		frag_color = texture( uTexture0, vTexCoord );
	}

#endif
