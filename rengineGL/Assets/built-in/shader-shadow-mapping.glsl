#version 400

// This shader is intended to be used when rendering the shadow maps. The fragment shader output
// will contain only the depth value of the fragment.

#ifdef VERTEX_SHADER

	layout(location=0) in vec3 aPos;  // The vertex coordinate.
	
	uniform mat4 uMVP;  // Model-View-Projection matrix.

	void main()
	{
		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	out float fragmentDepth;
	
	void main()
	{
		// The output will be the fragment's depth value.
		//fragmentDepth = gl_FragCoord.z;
	}

#endif
