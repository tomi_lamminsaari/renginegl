/**
* @file inc-common-helpers.glsl
* @brief A collection of generic helper functions.
*/

/**
* A macro that constructs a vec3 object from a float array. It takes the vec3 components from 3 consequential
* indices from the FLOATARRAY starting from BASEINDEX index.
*/
#define VEC3FROMFLOATARRAY( FLOATARRAY, BASEINDEX ) vec3( FLOATARRAY##[ ##BASEINDEX + 0 ], FLOATARRAY##[ ##BASEINDEX + 1 ], FLOATARRAY##[ ##BASEINDEX + 2 ] )
