
/*!
* Makes it a bit easier to create a vec3 variable from an float array. This function
* expects the vec3 components to subsequential order in given float array.
* @param floatarray This will be the name of your float array.
* @param baseIndex Index of X-coordinate in given float array.
* @return A vec3 float float array.
*/
vec3 vec3FromFloatArray( float floatArray[], int baseIndex )
{
	return vec3( floatArray[ baseIndex + 0 ], floatArray[ baseIndex + 1 ], floatArray[ baseIndex + 2 ] );
}
