/**
* This file contains utilities that help for handling diffuse and directional light sources.
*/

// Indices of directional light source's position. The position is normally very far away
// from the scene so that the direction of the light is pretty much equal to all vertices.
#define LIGHTARRAY_DIRECTIONAL_LIGHT_X 0
#define LIGHTARRAY_DIRECTIONAL_LIGHT_Y 1
#define LIGHTARRAY_DIRECTIONAL_LIGHT_Z 2

// Indices of directional light source's color components.
#define LIGHTARRAY_DIRECTIONAL_COLOR_R 3
#define LIGHTARRAY_DIRECTIONAL_COLOR_G 4
#define LIGHTARRAY_DIRECTIONAL_COLOR_B 5

// Index of directional light source's intensity.
#define LIGHTARRAY_DIRECTIONAL_INTENSITY 6

// Indices of diffuse light source's position.
#define LIGHTARRAY_DIFFUSE_LIGHT_X 0
#define LIGHTARRAY_DIFFUSE_LIGHT_Y 1
#define LIGHTARRAY_DIFFUSE_LIGHT_Z 2

// Indices of diffuse light source's color components.
#define LIGHTARRAY_DIFFUSE_COLOR_R 3
#define LIGHTARRAY_DIFFUSE_COLOR_G 4
#define LIGHTARRAY_DIFFUSE_COLOR_B 5

// Index of diffuse and specular light source's intensity.
#define LIGHTARRAY_INTENSITY 6

/**
* Calculates the diffuse color of the fragment.
* @param lightPos The position of the light source in world space.
* @param vertexPos The position of the fragment in world space.
* @param vertexNormal The normal vector of the fragment.
* @param lightIntensity The intensity of the light source.
* @param lightColor Color of the light source. Only RGB components.
* @param objectColor The color of the object in this fragment. Only RGB components.
* @return Returns the diffuse color of the fragment. 4th component will be 1.0.
*/
vec4 calculateDiffuseColor(
	vec3 lightPos,
	vec3 fragmentPos,
	vec3 vertexNormal,
	float lightIntensity,
	vec4 lightColor,
	vec4 objectColor
)
{
	// Calculate the unit vector that points from fragment position towards the light source.
	// Then calculate the light intensity on the surface based on fragment's normal vector. The
	// more closer to perpedicular the more intense the light amount.
	vec3 toLightsource = normalize( lightPos - fragmentPos );
	float diffuseCosAngle = dot( vertexNormal, toLightsource );
	vec4 diffuseColor = ( lightIntensity * lightColor ) * ( diffuseCosAngle * objectColor );
	return diffuseColor;
}

/**
* Calculates the amount and color of the diffuse light the face receives from the the light source.
* @param toLightsource Normal vector from fragment location towards the light source.
* @param vertexNormal The currently rasterized face's normal vector.
* @param lightIntensity Amount of light comes from the light source.
* @param lightColor The color of the light source.
* @param objectColor The base color of the fragment.
* @return Color of the fragment after including the diffuse light.
*/
vec4 calculateDiffuseColorWithNormal(
	vec3 toLightsource,
	vec3 vertexNormal,
	float lightIntensity,
	vec4 lightColor,
	vec4 objectColor
)
{
	// Calculate the diffuse light intensity by using the normal vector from fragment towards the
	// light source and face normal vector. The more close to perpendicular the more intense light.
	float diffuseCosAngle = dot( vertexNormal, toLightsource );
	vec4 diffuseColor = ( lightIntensity * lightColor ) * ( diffuseCosAngle * objectColor );
	return diffuseColor;
}

/**
* Calculates the specular light for the fragment.
*
* Note! This function expects that application has following uniform variables available:
*	uSpecularExponent	Shininess coefficient of current material.
*	uSpecularColor		Specular reflection color.
*
* @param toLightsource Normal vector from fragment location towards the light source.
* @param vertexNormal Normal vector of the face in current fragment.
* @param toCamera Normal vector from fragment position towards the camera.
* @param lightIntensity The intensity of the light source.
* @return The specular light of the fragment.
*/
vec4 calculateSpecularColor(
	vec3 toLightsource,
	vec3 vertexNormal,
	vec3 toCamera,
	float lightIntensity
)
{
	float diffuseCosAngle = dot( vertexNormal, toLightsource );
	float specularCoefficient = 0.0;
	if( diffuseCosAngle > 0.0 )
	{
		vec3 reflectionVec = reflect( -toLightsource, vertexNormal );
		float specCosAngle = max( 0.0, dot( toCamera, reflectionVec ) );
		specularCoefficient = pow( specCosAngle, uSpecularExponent );
	}
	else
	{
		// Light source is on wrong side of the face.
		specularCoefficient = 0.0;
	}
	return lightIntensity * specularCoefficient * uSpecularColor;
}
