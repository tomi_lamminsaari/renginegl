#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout ( location = 0 ) in vec3 aPosition;
	layout ( location = 1 ) in vec2 aTexCoord;

	// Vertex shader output variables.
	out vec2 vTexCoord;

	// The vertex shader application.
	void main()
	{
		// Pass the texture coordinate to fragment shader.
		vTexCoord = aTexCoord;

		// Fullscreen quad. No need to do any coordinate transformations.
		gl_Position = vec4( aPosition, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	out vec4 frag_color;

	// Input parameters from vertex shader.
	in vec2 vTexCoord;

	// Lightsource structure.
	struct LightSource {
		vec3 pos;
		vec3 color;
		float intensity;
	};
	const int NUMBER_OF_LIGHTS = 16;
	const float SHADOWMAP_BIAS = 0.0005;
	const float SHADOWMAP_AMOUNT_OF_DIMMING = 0.3;
	const float SHADOWMAP_TRANSITION_START_DISTANCE = 200.0;
	const float SHADOWMAP_TRANSITION_DISTANCE = 20.0;

	// Texture uniforms.
	uniform sampler2D uTexture0;
	uniform sampler2D uTexture1;
	uniform sampler2D uTexture2;
	uniform sampler2D uTexture3;

	// All lightsources of the scene. The directional "sunlight" is expected to be at index 0.
	uniform LightSource uLightSources[ NUMBER_OF_LIGHTS ];
	uniform vec4 uAmbientColor;
	uniform vec3 uEyePos;
	uniform mat4 uLightSourceMVP;

	// The fragment shader application.
	void main()
	{
		// Get the fragment position, normal vector and diffuse color from input textures-
		vec3 fragPos = texture( uTexture2, vTexCoord ).rgb;
		vec3 fragNormal = texture( uTexture1, vTexCoord ).rgb;
		vec4 diffuseColVec4 = texture( uTexture0, vTexCoord );
		vec3 diffuseColor = diffuseColVec4.rgb;
		if( fragNormal.r + fragNormal.g + fragNormal.b == 0.0 )
		{
			// If fragment's normal vector is 0, it means that no geometry has been
			// rendered at this position. We can safely discard the fragment and see
			// whatever has been drawn in the framebuffer with earlier render passes.
			// Such as skybox.
			discard;
		}
		else
		{
			float specularExponent = 1000.0 * diffuseColVec4.a;

			// Calculate the total color by using the lightsources. Point light sources
			// are in uLightSources array starting from index 1.
			vec3 cumulativeColor = diffuseColor * uAmbientColor.xyz;
			vec3 eyeDirection = normalize( uEyePos - fragPos );
			for( int i = 1; i < NUMBER_OF_LIGHTS; ++i )
			{
				// Calculate light distance from the fragment and some other useful vectors.
				vec3 lightDir = normalize( uLightSources[ i ].pos - fragPos );
				float lightDistance = length( uLightSources[ i ].pos - fragPos );
				float attenuation = uLightSources[ i ].intensity / ( 1.0 + lightDistance + lightDistance * lightDistance );

				// Diffuse light amount.
				vec3 lightDiffuse = max( dot( fragNormal, lightDir ), 0.0 ) * diffuseColor * uLightSources[ i ].color * attenuation;

				// Specular light amount.
				vec3 halfNormal = normalize( lightDir + eyeDirection );
				float spec = pow( max( dot( fragNormal, halfNormal ), 0.0 ), specularExponent );
				vec3 specularLight = uLightSources[ i ].color * spec * attenuation;

				// Adds diffuse and specular lights.
				cumulativeColor += lightDiffuse;
				cumulativeColor += specularLight;
			}

			// The lightsource at index 0 is always the directional "sunlight" lightsouce.
			float directionalDiffuseAmount = max( -dot( fragNormal, uLightSources[ 0 ].pos ), 0.0 ) * uLightSources[ 0 ].intensity;
			cumulativeColor += uLightSources[ 0 ].color * directionalDiffuseAmount;

			// Do shadowmapping.
			// -----------------
			vec4 shadowMapCoord = uLightSourceMVP * vec4(fragPos, 1.0);
			vec2 texelSize = 1.0 / textureSize( uTexture3, 0 );  // relative size of single texel.
			float shadowLevel = 0.0;
			vec3 shadowmapCoord3d = ( ( shadowMapCoord.xyz / shadowMapCoord.w ) * 0.5 ) + vec3( 0.5 );
			float fragDistance = length( fragPos.xz - uEyePos.xz );
			fragDistance -= SHADOWMAP_TRANSITION_START_DISTANCE;
			fragDistance = clamp( fragDistance / SHADOWMAP_TRANSITION_DISTANCE, 0.0, 1.0 );
			float shadowMapIntensity = 1.0 - fragDistance;
			shadowMapIntensity = 1.0 - SHADOWMAP_AMOUNT_OF_DIMMING * shadowMapIntensity;

			// Soften the shadows.
			float depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( 0, 0 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( -1.9, 0.9 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( -0.1, 0.8 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( 0.6, 0.6 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( -0.9, -0.3 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( 0.8, 0.4 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( -0.9, -0.4 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( -0.1, -0.6 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
			depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( 0.9, -1.2 ) * texelSize ).r + SHADOWMAP_BIAS;
			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;

//			for( int x = -1; x <= 1; ++x ) {
//				for( int y = -1; y <= 1; ++y ) {
//					float depthValue = texture( uTexture3, shadowmapCoord3d.xy + vec2( x, y ) * texelSize ).r + SHADOWMAP_BIAS;
//					shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;
//				}
//			}
			shadowLevel /= 8.0;
//			float depthValue = texture( uTexture3, shadowmapCoord3d.xy ).r + SHADOWMAP_BIAS;
//			shadowLevel += depthValue > shadowmapCoord3d.z ? 1.0 : shadowMapIntensity;

			// Get the fragment color from diffuse texture.
			frag_color = vec4( cumulativeColor * shadowLevel, 1.0 );
		}
	}
	
#endif
