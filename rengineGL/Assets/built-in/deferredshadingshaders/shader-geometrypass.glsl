#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout( location = 0 ) in vec3 aPos;
	layout( location = 1 ) in vec3 aNormal;
	layout( location = 2 ) in vec2 aTexCoord;
	layout( location = 3 ) in vec3 aColor;
	layout( location = 4 ) in float aTexIndex;
	layout( location = 5 ) in float aSpecular;
	layout( location = 6 ) in vec2 aNormalTexCoord;
	layout( location = 7 ) in vec3 aTangentVec;

	// Vertex shader output variables.
	out vec3 vPos;
	out vec3 vNormal;
	out vec2 vTexCoord;
	out vec3 vColor;
	out float vTexIndex;
	out float vSpecular;
	out vec2 vNormalTexCoord;
	out vec3 vTangentVec;
	out vec3 vBitangentVec;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;
	uniform mat4 uLightSourceMVP;

	// The vertex shader application.
	void main()
	{
		// Output the texture coordinates, color and texture index.
		vTexCoord = aTexCoord;
		vColor = aColor;
		vTexIndex = aTexIndex;
		vSpecular = aSpecular;
		vNormalTexCoord = aNormalTexCoord;

		// Calculate vertex location in world coordinates. Pass it to the fragment shader.
		vPos = vec3( uM * vec4( aPos, 1.0 ) );

		// Calculate the vertex normal direction in world coordinates.
		vNormal = vec3( uM * vec4( aNormal, 0.0 ) );
		vTangentVec = vec3( uM * vec4( aTangentVec, 0.0 ) );
		vBitangentVec = normalize( cross( vNormal, vTangentVec ) );

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	layout (location = 0) out vec4 outDiffuseSpecular;
	layout (location = 1) out vec3 outNormal;
	layout (location = 2) out vec3 outPosition;

	// Input parameters from vertex shader.
	in vec3 vPos;
	in vec3 vNormal;
	in vec2 vTexCoord;
	in vec3 vColor;
	in float vTexIndex;
	in float vSpecular;
	in vec2 vNormalTexCoord;
	in vec3 vTangentVec;
	in vec3 vBitangentVec;

	// Texture uniforms.
	uniform sampler2D uTexture0;
	uniform sampler2D uTexture1;
	
	// Material related uniforms.
	uniform vec4 uSpecularColor;
	uniform float uSpecularExponent;

	// Ambient light related uniforms.
	uniform vec4 uAmbientColor;
	uniform float uAmbientIntensity;
	
	// The fragment shader application.
	void main()
	{
		// Check if we should get the diffuse color from texture or from vertex.
		int textureIndex = int( vTexIndex );

		// Set the output values.
		outPosition = vPos;

		// Calculate normals.
		if( vNormalTexCoord.x < 0.0 ) {
			outNormal = normalize( vNormal );
		} else {
			vec3 fn = normalize( texture( uTexture1, vNormalTexCoord ).xyz * 2.0 - 1.0  );
			mat3 tbnMat = mat3( normalize( vTangentVec ), normalize( vBitangentVec ), normalize( vNormal ) );
			outNormal = normalize( tbnMat * fn );
		}

		// The output color comes either from the texture or from vertex attributes.
		if( textureIndex >= 0 ) {
			// Texture index indicates that we should take the fragment color from texture.
			outDiffuseSpecular = vec4( texture( uTexture0, vTexCoord ).xyz, 0.0 );

		} else {
			// Take fragment color from texture attributes.
			outDiffuseSpecular = vec4( vColor, 0.0 );

		}
		outDiffuseSpecular.a = 1.0 / ( 0.001 + vSpecular );
	}
	
#endif
