#version 400

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout( location = 0 ) in vec3 aPos;
	layout( location = 1 ) in vec3 aNormal;
	layout( location = 2 ) in vec2 aTexCoord;
	layout( location = 3 ) in vec3 aColor;
	layout( location = 4 ) in float aTexIndex;
	layout( location = 5 ) in float aSpecular;

	// Vertex shader output variables.
	out vec3 vPos;
	out vec3 vNormal;
	out vec2 vTexCoord;
	out vec3 vColor;
	out float vTexIndex;
	out float vSpecular;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;
	uniform mat4 uLightSourceMVP;

	// The vertex shader application.
	void main()
	{
		// Output the texture coordinates, color and texture index.
		vTexCoord = aTexCoord;
		vColor = aColor;
		vTexIndex = aTexIndex;
		vSpecular = aSpecular;

		// Calculate vertex location in world coordinates. Pass it to the fragment shader.
		vPos = vec3( uM * vec4( aPos, 1.0 ) );

		// Calculate the vertex normal direction in world coordinates.
		vNormal = vec3( uM * vec4( aNormal, 0.0 ) );

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	layout (location = 0) out vec4 outDiffuseSpecular;
	layout (location = 1) out vec3 outNormal;
	layout (location = 2) out vec3 outPosition;

	// Input parameters from vertex shader.
	in vec3 vPos;
	in vec3 vNormal;
	in vec2 vTexCoord;
	in vec3 vColor;
	in float vTexIndex;
	in float vSpecular;

	// Texture uniforms.
	uniform sampler2D uTexture0;
	uniform sampler2D uTexture1;
	uniform sampler2D uTexture2;
	uniform sampler2D uTexture3;
	uniform sampler2D uTexture4;
	uniform sampler2D uTexture5;
	uniform sampler2D uTexture6;
	uniform sampler2D uTexture7;
	
	// Material related uniforms.
	uniform vec4 uSpecularColor;
	uniform float uSpecularExponent;

	// Ambient light related uniforms.
	uniform vec4 uAmbientColor;
	uniform float uAmbientIntensity;
	
	// The fragment shader application.
	void main()
	{
		// Check if we should get the diffuse color from texture or from vertex.
		int textureIndex = int( vTexIndex );

		// Set the output values.
		outPosition = vPos;
		outNormal = normalize( vNormal );

		// Take fragment color either from textures or from vertex attributes.
		if( textureIndex >= 0 ) {
			// Texture index indicates that we should take the fragment color from texture.
			switch( textureIndex ) {
				case 0:
					outDiffuseSpecular = vec4( texture( uTexture0, vTexCoord ).xyz, 0.0 );
					break;
				case 1:
					outDiffuseSpecular = vec4( texture( uTexture1, vTexCoord ).xyz, 0.0 );
					break;
				case 2:
					outDiffuseSpecular = vec4( texture( uTexture2, vTexCoord ).xyz, 0.0 );
					break;
				case 3:
					outDiffuseSpecular = vec4( texture( uTexture3, vTexCoord ).xyz, 0.0 );
					break;
				case 4:
					outDiffuseSpecular = vec4( texture( uTexture4, vTexCoord ).xyz, 0.0 );
					break;
				case 5:
					outDiffuseSpecular = vec4( texture( uTexture5, vTexCoord ).xyz, 0.0 );
					break;
				case 6:
					outDiffuseSpecular = vec4( texture( uTexture6, vTexCoord ).xyz, 0.0 );
					break;
				case 7:
					outDiffuseSpecular = vec4( texture( uTexture7, vTexCoord ).xyz, 0.0 );
					break;
				default:
					outDiffuseSpecular = vec4( 0.0, 0.0, 0.0, 0.0 );
					break;
			}

		} else {
			// Take fragment color from texture attributes.
			outDiffuseSpecular = vec4( vColor, 0.0 );
		}
		outDiffuseSpecular.a = 1.0 / ( 0.001 + vSpecular );
	}
	
#endif
