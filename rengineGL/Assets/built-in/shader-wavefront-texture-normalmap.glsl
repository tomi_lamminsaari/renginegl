#version 400

#define LIGHTSOURCE_COUNT %LIGHTSOURCE_COUNT%
#define FLOATS_PER_LIGHTSOURCE %LIGHTSOURCE_FLOAT_COUNT%
#define LIGHT_ATTENUATION %LIGHT_ATTENUATION%

#ifdef VERTEX_SHADER

	// Vertex attributes
	layout(location=0) in vec3 aPos;
	layout(location=1) in vec3 aNormal;
	layout(location=2) in vec2 aTexCoord;
	layout(location=3) in vec2 aNormalsTexCoord;
	layout(location=4) in vec3 aTangent;
	layout(location=5) in vec3 aBitangent;

	// Vertex shader output variables.
	out vec3 vVertex;
	out vec3 vNormal;
	out vec3 vTangent;
	out vec3 vBitangent;
	out vec2 vTextureCoord;
	out vec2 vNormalsTextureCoord;
	out vec4 vShadowMapCoords;

	// Constant uniforms
	uniform mat4 uMVP;
	uniform mat4 uMV;
	uniform mat4 uM;
	uniform mat4 uLightSourceMVP;

	// The vertex shader application.
	void main()
	{
		// Output the texture coordinates.
		vTextureCoord = aTexCoord;
		vNormalsTextureCoord = aNormalsTexCoord;

		// Calculate vertex location in world coordinates. Pass it to the fragment shader.
		vVertex = vec3( uM * vec4( aPos, 1.0 ) );
		
		// Project the vertex coordinate to light space. The uLightSourceMVP has also
		// the object's model matrix applied so vertex gets also translated to world
		// coordintanes as well.
		vShadowMapCoords = uLightSourceMVP * vec4( aPos, 1.0 );

		// Calculate vertex normal in world coordinates. Pass it to the fragment shader.
		vNormal = normalize( vec3( uM * vec4( aNormal, 0.0 ) ) );
		vTangent = normalize( vec3( uM * vec4( aTangent, 0.0 ) ) );
		vBitangent = normalize( vec3( uM * vec4( aBitangent, 0.0 ) ) );

		// Calculate the vertex location in screen coordinates. The actual output of the vertex shader.
  		gl_Position = uMVP * vec4( aPos, 1.0 );
	}

#endif

#ifdef FRAGMENT_SHADER

	// Fragment shader's output variable.
	out vec4 frag_color;

	// Input parameters from vertex shader.
	in vec3 vVertex;
	in vec3 vNormal;
	in vec3 vTangent;
	in vec3 vBitangent;
	in vec2 vTextureCoord;
	in vec2 vNormalsTextureCoord;
	in vec4 vShadowMapCoords;

	// Texture uniforms.
	uniform sampler2D uTexture0;  // Diffuse texture
	uniform sampler2D uTexture1;  // Normals texture

	// Generic uniforms.
	uniform vec3 uEyePos;

	// Material related uniforms.
	uniform vec4 uSpecularColor;
	uniform float uSpecularExponent;
	uniform int uSpecularEnabled;

	// Ambient light related uniforms.
	uniform vec4 uAmbientColor;
	uniform float uAmbientIntensity;

	// Diffuse light uniforms.
	// The first lightsource in this array will be the primary shadow casting light source.
	uniform float uDiffuseLightArray[ FLOATS_PER_LIGHTSOURCE * LIGHTSOURCE_COUNT ];  // Array for multiple light sources.
	
	// Shadowmap texture slot.
	uniform sampler2D uShadowMap;
	
	// Include the shadowmap functions.
	#include "assets://built-in/inc-shadowmap-func.glsl"
	// End of shadowmap functions

	// Include the lightsourcearray functions.
	#include "assets://built-in/inc-lights.glsl"
	// End of lightsourcearray functions.

	// Include the helper macros.
	#include "assets://built-in/inc-common-helpers.glsl"
	// End of helper macros.

	// The fragment shader application.
	void main()
	{
		// Calculate vector from vertex location to the diffuse light sources. And get the
		// vertex normal vector.
		vec3 vertexNormal = normalize( vNormal );
		vec3 cameraPos = uEyePos;  // After transformations camera is in the origo.
		vec3 surfaceToCamera = normalize( cameraPos - vVertex );

		// Determine the amount of light the fragment receives.
		float shadowmapLightAmount = shadowmap_calculateLightAmount( uShadowMap, vShadowMapCoords );
		
		// Get the base diffuse color from the texture.
		vec4 texelColor = texture( uTexture0, vTextureCoord );
		texelColor = vec4( 0.4, 0.0, 0.0, 1.0 );

		// Calculate the fragment normal with normal mapping.
		vec3 fn = normalize( texture( uTexture1, vNormalsTextureCoord ).xyz * 2.0 - 1.0  );
		mat3 tbnMat = mat3( normalize( vTangent ), normalize( vBitangent ), normalize( vertexNormal ) );
		vec3 fragmentNormal = normalize( tbnMat * fn );

		// Calculate the diffusion color amount of primary shadow casting light source.
		vec4 diffuseSunlight = calculateDiffuseColor(
									VEC3FROMFLOATARRAY( uDiffuseLightArray, LIGHTARRAY_DIRECTIONAL_LIGHT_X ),
									vVertex,
									fragmentNormal,
									uDiffuseLightArray[ LIGHTARRAY_DIRECTIONAL_INTENSITY ],
									vec4( VEC3FROMFLOATARRAY( uDiffuseLightArray, LIGHTARRAY_DIRECTIONAL_COLOR_R ), 1.0 ),
									texelColor );

		// This shader supports multiple light sources. Loop all lightsources through
		// and calculate the cumulative diffuse and specular light amounts. Start from the
		// second lightsource in lightsources array.
		vec4 specularLightCumulative = vec4( 0.0, 0.0, 0.0, 0.0 );
		vec4 diffuseLightCumulative = vec4( 0.0, 0.0, 0.0, 0.0 );
		int baseIndex = FLOATS_PER_LIGHTSOURCE;
		for( int i = 1; i < LIGHTSOURCE_COUNT; ++i )
		{
			if( uDiffuseLightArray[ baseIndex + LIGHTARRAY_INTENSITY ] > 0.0 )
			{
				// Get the normal vector from current fragment towards the light source.
				vec3 toLightsource = normalize( VEC3FROMFLOATARRAY( uDiffuseLightArray, ( baseIndex + LIGHTARRAY_DIFFUSE_LIGHT_X ) ) - vVertex );

				// Calculate the diffuse light from the lightsource.
				diffuseLightCumulative += calculateDiffuseColorWithNormal(
						toLightsource,
						fragmentNormal,
						uDiffuseLightArray[ baseIndex + LIGHTARRAY_INTENSITY ],
						vec4( VEC3FROMFLOATARRAY( uDiffuseLightArray, ( baseIndex + LIGHTARRAY_DIFFUSE_COLOR_R ) ), 1.0 ),
						texelColor );

				// Calculate lightsource specific specular color.
				specularLightCumulative += calculateSpecularColor(
						toLightsource, fragmentNormal, surfaceToCamera,
						uDiffuseLightArray[ baseIndex + LIGHTARRAY_INTENSITY ] );
				specularLightCumulative *= float( uSpecularEnabled );
			}

			// Move to next lightsource.
			baseIndex += FLOATS_PER_LIGHTSOURCE;
		}

		// Apply the shadowmap value to diffuse and specular colors.
		vec4 diffuseColor = diffuseLightCumulative * shadowmapLightAmount;
		vec4 specularColor = specularLightCumulative * shadowmapLightAmount;

		// Calculate the ambient color amount.
		vec4 ambientColor = ( uAmbientIntensity * uAmbientColor ) * texelColor;
		ambientColor += shadowmapLightAmount * diffuseSunlight;

		// Combine the ambient and diffuse light amounts.
		frag_color = clamp( ambientColor + diffuseColor + specularColor, 0.0, 1.0 );
		frag_color.a = 1.0;
	}
	
#endif
